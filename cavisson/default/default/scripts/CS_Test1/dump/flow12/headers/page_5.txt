--Request 
GET https://api-cdn.prod-aws.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/settings?settingsVersion=20&botVersionId=12034483-9bfe-47a8-bbfa-ada6398deb1f
Host: api-cdn.prod-aws.artibot.ai
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: */*
Origin: https://www.cavisson.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
Transfer-Encoding: chunked
Connection: keep-alive
Date: Mon, 05 Sep 2022 11:31:44 GMT
Server: Kestrel
Cache-Control: public,max-age=2147483647
Access-Control-Allow-Origin: *
X-Cache: Hit from cloudfront
Via: 1.1 60bf8c31583fc8615410cf45d263fddc.cloudfront.net (CloudFront)
X-Amz-Cf-Pop: LAX3-C3
X-Amz-Cf-Id: ocpU30tm7ycBsp-XQacdWYEmEK8VDo9BzOvi4X2Hofl60uIK6vbILQ==
Age: 344324
----
--Request 
GET https://www.cavisson.com/wp-content/uploads/2016/05/cropped-Cavisson.png
Host: www.cavisson.com
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.cavisson.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
Cookie: _wpjshd_session_=e079107a338cc2fea1baae95e8d17ea94670401%2F1662723626%2F1662725426; PHPSESSID=14a84564aec7cdc5047c1d3d08ac53d5; _ga=GA1.2.393330446.1662721828; _gid=GA1.2.399320800.1662721828; _gat=1
----
--Response 
HTTP/1.1 200
strict-transport-security: max-age=31536000; preload; includeSubDomains
content-security-policy: upgrade-insecure-requests;
content-security-policy: frame-ancestors 'self'
last-modified: Tue, 20 Sep 2016 07:30:08 GMT
accept-ranges: bytes
content-length: 22084
cache-control: max-age=2628000, public
expires: Sat, 09 Sep 2023 11:10:28 GMT
x-xss-protection: 1; mode=block
x-frame-options: SAMEORIGIN
x-content-type-options: nosniff
referrer-policy: same-origin
feature-policy: geolocation 'self'; vibrate 'none'
permissions-policy: geolocation 'self'; microphone 'none'
vary: User-Agent
content-type: image/png
date: Fri, 09 Sep 2022 11:10:28 GMT
server: Apache
----
--Request 
GET https://app.artibot.ai/chat-window?i=948c4b2e-cf5d-4d58-9e77-2b636e5bbd98&cbv=12034483-9bfe-47a8-bbfa-ada6398deb1f&v=20&cp=https%3A%2F%2Fwww.cavisson.com%2F&th=light&em=true&mo=true
Host: app.artibot.ai
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: navigate
Sec-Fetch-Dest: iframe
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----

--Request 
GET https://app.artibot.ai/chat-window?i=948c4b2e-cf5d-4d58-9e77-2b636e5bbd98&cbv=12034483-9bfe-47a8-bbfa-ada6398deb1f&v=20&cp=https%3A%2F%2Fwww.cavisson.com%2F&th=light&em=true&mo=true&sh=false&tb=true
Host: app.artibot.ai
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: navigate
Sec-Fetch-Dest: iframe
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: text/html; charset=utf-8
last-modified: Tue, 12 Oct 2021 19:43:29 GMT
x-amz-version-id: null
server: AmazonS3
content-encoding: gzip
date: Fri, 09 Sep 2022 11:10:28 GMT
cache-control: public, max-age=900
etag: W/\"14d7c7c69f131bf34750c3a7e93fb020\"
vary: Accept-Encoding
x-cache: Hit from cloudfront
via: 1.1 a395ab921d8c9cd3e200604240c4e840.cloudfront.net (CloudFront)
x-amz-cf-pop: LAX3-C3
x-amz-cf-id: _qQDUTtbIhpecPvPo04QatYKr3XtwEBq012iXXcIo0CShtyqqHvF9w==
age: 116
----
--Request 
GET https://www.google.com/recaptcha/api2/bframe?hl=en&v=g8G8cw32bNQPGUVoDvt680GA&k=6Ldt8qEUAAAAAH-QhEOUuqZ3-IqCap0nlTw6xrli
Host: www.google.com
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: navigate
Sec-Fetch-Dest: iframe
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
cross-origin-resource-policy: cross-origin
cross-origin-embedder-policy: require-corp
report-to: {\"group\":\"recaptcha\",\"max_age\":2592000,\"endpoints\":[{\"url\":\"https://csp.withgoogle.com/csp/report-to/recaptcha\"}]}
content-type: text/html; charset=utf-8
cache-control: no-cache, no-store, max-age=0, must-revalidate
pragma: no-cache
expires: Mon, 01 Jan 1990 00:00:00 GMT
date: Fri, 09 Sep 2022 11:10:28 GMT
content-security-policy: script-src 'nonce-2bO36w_tCmNkSHdcvvBEzQ' 'unsafe-inline' 'strict-dynamic' https: http: 'unsafe-eval';object-src 'none';base-uri 'self';report-uri https://csp.withgoogle.com/csp/recaptcha/1
content-encoding: gzip
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
content-length: 1197
server: GSE
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://app.artibot.ai/chat_window.4514.css
Host: app.artibot.ai
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: text/css,*/*;q=0.1
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: style
Referer: https://app.artibot.ai/chat-window?i=948c4b2e-cf5d-4d58-9e77-2b636e5bbd98&cbv=12034483-9bfe-47a8-bbfa-ada6398deb1f&v=20&cp=https%3A%2F%2Fwww.cavisson.com%2F&th=light&em=true&mo=true&sh=false&tb=true
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: text/css; charset=utf-8
date: Mon, 05 Sep 2022 16:54:32 GMT
cache-control: public, max-age=31536000
last-modified: Tue, 12 Oct 2021 19:43:25 GMT
x-amz-version-id: null
etag: W/\"a802632c00ec3f2db3bf7622090aec7e\"
server: AmazonS3
content-encoding: gzip
vary: Accept-Encoding
x-cache: Hit from cloudfront
via: 1.1 a395ab921d8c9cd3e200604240c4e840.cloudfront.net (CloudFront)
x-amz-cf-pop: LAX3-C3
x-amz-cf-id: Dz_rYEzk9DaM8HW_Bg6Gs9Z1xDRiQSGXstQaeoCRaTjeEd2AnXCW7A==
age: 324957
----
--Request 
GET https://js.stripe.com/v3/
Host: js.stripe.com
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://app.artibot.ai/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
last-modified: Thu, 08 Sep 2022 19:36:59 GMT
etag: \"36a51da531d166bd44b90f97c6bc62c9\"
cache-control: max-age=60
content-type: text/javascript; charset=utf-8
strict-transport-security: max-age=31556926; includeSubDomains; preload
x-content-type-options: nosniff
access-control-allow-origin: *
server: Fastly
content-encoding: br
accept-ranges: bytes
date: Fri, 09 Sep 2022 11:10:28 GMT
via: 1.1 varnish
age: 5
x-request-id: 82eb3b05-334e-4266-8d7c-a558c5afd8be
x-served-by: cache-lax10669-LGB
x-cache: HIT
x-cache-hits: 1
vary: Accept-Encoding
timing-allow-origin: *
content-length: 83210
----
--Request 
GET https://app.artibot.ai/chat_window.4514.js
Host: app.artibot.ai
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: */*
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://app.artibot.ai/chat-window?i=948c4b2e-cf5d-4d58-9e77-2b636e5bbd98&cbv=12034483-9bfe-47a8-bbfa-ada6398deb1f&v=20&cp=https%3A%2F%2Fwww.cavisson.com%2F&th=light&em=true&mo=true&sh=false&tb=true
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: application/javascript; charset=utf-8
date: Mon, 05 Sep 2022 16:54:32 GMT
cache-control: public, max-age=31536000
last-modified: Tue, 12 Oct 2021 19:43:23 GMT
x-amz-version-id: null
etag: W/\"acb5538a812851913e40c77db031e30e\"
server: AmazonS3
content-encoding: gzip
vary: Accept-Encoding
x-cache: Hit from cloudfront
via: 1.1 a395ab921d8c9cd3e200604240c4e840.cloudfront.net (CloudFront)
x-amz-cf-pop: LAX3-C3
x-amz-cf-id: wIAZ2yRr1unOCCHiEJkD6iIA8lT_hBkesH69BJmE_218c8QtAEWRbw==
age: 324957
----
--Request 
GET https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,600,700
Host: fonts.googleapis.com
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: text/css,*/*;q=0.1
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: style
Referer: https://app.artibot.ai/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: text/css; charset=utf-8
access-control-allow-origin: *
timing-allow-origin: *
link: <https://fonts.gstatic.com>; rel=preconnect; crossorigin
strict-transport-security: max-age=31536000
expires: Fri, 09 Sep 2022 11:10:29 GMT
date: Fri, 09 Sep 2022 11:10:29 GMT
cache-control: private, max-age=86400, stale-while-revalidate=604800
last-modified: Fri, 09 Sep 2022 11:10:29 GMT
cross-origin-resource-policy: cross-origin
cross-origin-opener-policy: same-origin-allow-popups
content-encoding: gzip
server: ESF
x-xss-protection: 0
x-frame-options: SAMEORIGIN
x-content-type-options: nosniff
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://fonts.googleapis.com/css?family=Source+Serif+Pro:300,400,400i,600,700
Host: fonts.googleapis.com
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: text/css,*/*;q=0.1
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: style
Referer: https://app.artibot.ai/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: text/css; charset=utf-8
access-control-allow-origin: *
timing-allow-origin: *
link: <https://fonts.gstatic.com>; rel=preconnect; crossorigin
strict-transport-security: max-age=31536000
expires: Fri, 09 Sep 2022 11:10:29 GMT
date: Fri, 09 Sep 2022 11:10:29 GMT
cache-control: private, max-age=86400, stale-while-revalidate=604800
last-modified: Fri, 09 Sep 2022 11:10:29 GMT
cross-origin-resource-policy: cross-origin
cross-origin-opener-policy: same-origin-allow-popups
content-encoding: gzip
server: ESF
x-xss-protection: 0
x-frame-options: SAMEORIGIN
x-content-type-options: nosniff
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://fonts.googleapis.com/css?family=Space+Mono:300,400,400i,600,700
Host: fonts.googleapis.com
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: text/css,*/*;q=0.1
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: style
Referer: https://app.artibot.ai/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: text/css; charset=utf-8
access-control-allow-origin: *
timing-allow-origin: *
link: <https://fonts.gstatic.com>; rel=preconnect; crossorigin
strict-transport-security: max-age=31536000
expires: Fri, 09 Sep 2022 11:10:29 GMT
date: Fri, 09 Sep 2022 11:10:29 GMT
cache-control: private, max-age=86400, stale-while-revalidate=604800
last-modified: Fri, 09 Sep 2022 11:10:29 GMT
cross-origin-opener-policy: same-origin-allow-popups
cross-origin-resource-policy: cross-origin
content-encoding: gzip
server: ESF
x-xss-protection: 0
x-frame-options: SAMEORIGIN
x-content-type-options: nosniff
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://js.stripe.com/v3/m-outer-e4758ef2f8aa5add4514c7ebabe5d935.html#url=https%3A%2F%2Fapp.artibot.ai%2Fchat-window%3Fi%3D948c4b2e-cf5d-4d58-9e77-2b636e5bbd98%26cbv%3D12034483-9bfe-47a8-bbfa-ada6398deb1f%26v%3D20%26cp%3Dhttps%253A%252F%252Fwww.cavisson.com%252F%26th%3Dlight%26em%3Dtrue%26mo%3Dtrue%26sh%3Dfalse%26tb%3Dtrue&title=ArtiBot.ai%20%E2%80%94%20AI%20Powered%20Bot%20for%20Lead%20Capture&referrer=&muid=NA&sid=NA&version=6&preview=false
Host: js.stripe.com
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: navigate
Sec-Fetch-Dest: iframe
Referer: https://app.artibot.ai/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
last-modified: Thu, 08 Sep 2022 19:06:11 GMT
etag: \"e4758ef2f8aa5add4514c7ebabe5d935\"
cache-control: max-age=31536000
content-type: text/html; charset=utf-8
content-security-policy: base-uri 'none'; connect-src 'self' https://r.stripe.com; default-src 'self'; font-src 'none'; form-action 'none'; frame-src https://m.stripe.network; img-src https://q.stripe.com; media-src 'none'; object-src 'none'; script-src 'self'; style-src 'self'; report-uri https://q.stripe.com/csp-report
content-security-policy-report-only: base-uri 'none'; connect-src 'self' https://r.stripe.com; default-src 'self'; font-src 'none'; form-action 'none'; frame-src https://m.stripe.network; img-src https://q.stripe.com; media-src 'none'; object-src 'none'; script-src 'self'; style-src 'self' 'sha256-0hAheEzaMe6uXIKV4EehS9pu1am1lj/KnnzrOYqckXk='; report-uri https://q.stripe.com/csp-report
strict-transport-security: max-age=31556926; includeSubDomains; preload
x-content-type-options: nosniff
access-control-allow-origin: *
server: Fastly
content-encoding: br
accept-ranges: bytes
date: Fri, 09 Sep 2022 11:10:29 GMT
via: 1.1 varnish
age: 57744
x-request-id: 641452f7-9cbc-4747-aaa8-48e61fe27051
x-served-by: cache-lax10669-LGB
x-cache: HIT
x-cache-hits: 11536
vary: Accept-Encoding
timing-allow-origin: *
content-length: 114
----
--Request 
GET https://js.stripe.com/v3/fingerprinted/js/m-outer-f8ad55abd24592109e479217fa994c39.js
Host: js.stripe.com
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: */*
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://js.stripe.com/v3/m-outer-e4758ef2f8aa5add4514c7ebabe5d935.html
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
last-modified: Thu, 08 Sep 2022 19:06:10 GMT
etag: \"d96c709017743c0759cf3853d1806ba5\"
cache-control: max-age=60
content-type: text/javascript; charset=utf-8
strict-transport-security: max-age=31556926; includeSubDomains; preload
x-content-type-options: nosniff
access-control-allow-origin: *
server: Fastly
content-encoding: br
accept-ranges: bytes
date: Fri, 09 Sep 2022 11:10:29 GMT
via: 1.1 varnish
age: 1
x-request-id: 9c761233-9a51-4095-98e5-1ec6da4dfe4d
x-served-by: cache-lax10669-LGB
x-cache: HIT
x-cache-hits: 1
vary: Accept-Encoding
timing-allow-origin: *
content-length: 256
----
--Request 
GET https://m.stripe.network/inner.html#url=https%3A%2F%2Fapp.artibot.ai%2Fchat-window%3Fi%3D948c4b2e-cf5d-4d58-9e77-2b636e5bbd98%26cbv%3D12034483-9bfe-47a8-bbfa-ada6398deb1f%26v%3D20%26cp%3Dhttps%253A%252F%252Fwww.cavisson.com%252F%26th%3Dlight%26em%3Dtrue%26mo%3Dtrue%26sh%3Dfalse%26tb%3Dtrue&title=ArtiBot.ai%20%E2%80%94%20AI%20Powered%20Bot%20for%20Lead%20Capture&referrer=&muid=NA&sid=NA&version=6&preview=false
Host: m.stripe.network
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: navigate
Sec-Fetch-Dest: iframe
Referer: https://js.stripe.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
cache-control: max-age=300, public
content-type: text/html; charset=utf-8
content-security-policy: base-uri 'none'; connect-src https://m.stripe.network https://m.stripe.com; default-src 'none'; font-src https://m.stripe.network https://fonts.gstatic.com; form-action 'none'; frame-src https://m.stripe.network https://js.stripe.com; img-src https://m.stripe.network https://m.stripe.com https://b.stripecdn.com; script-src https://m.stripe.network 'sha256-e/Jqu4k9Gk1ZCWO6StAsfhF3i7qgIwfuitaD1g9DyvE='; style-src https://m.stripe.network; report-uri https://q.stripe.com/csp-report
strict-transport-security: max-age=31556926; includeSubDomains; preload
x-content-type-options: nosniff
server: Fastly
content-encoding: gzip
accept-ranges: bytes
date: Fri, 09 Sep 2022 11:10:29 GMT
via: 1.1 varnish
age: 225
x-request-id: 318d63f9-f097-4530-93b1-c579fddcb8e5
x-served-by: cache-lax10669-LGB
x-cache: HIT
x-cache-hits: 14
x-timer: S1662721829.382555,VS0,VE0
vary: Accept-Encoding, Origin
content-length: 527
----
--Request 
GET https://m.stripe.network/out-4.5.42.js
Host: m.stripe.network
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: */*
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://m.stripe.network/inner.html
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
cache-control: max-age=300, public
content-type: text/javascript; charset=utf-8
strict-transport-security: max-age=31556926; includeSubDomains; preload
x-content-type-options: nosniff
server: Fastly
content-encoding: gzip
accept-ranges: bytes
date: Fri, 09 Sep 2022 11:10:29 GMT
via: 1.1 varnish
age: 156
x-request-id: fee39f2d-aa60-4e4b-95f8-a28ea791a2ae
x-served-by: cache-lax10669-LGB
x-cache: HIT
x-cache-hits: 13
x-timer: S1662721829.441064,VS0,VE0
vary: Accept-Encoding, Origin
content-length: 16031
----

