/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: SuryaK
    Date of recording: 08/26/2022 12:28:24
    Flow details:
    Build details: 4.8.0 (build# 129)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

int exit_script()
{
    return 0;
}
