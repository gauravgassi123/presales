/*-----------------------------------------------------------------------------
    Name: Flow_1
    Recorded By: SuryaK
    Date of recording: 08/26/2022 12:28:24
    Flow details:
    Build details: 4.8.0 (build# 129)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void Flow_1()
{
    ns_start_transaction("lntwiki");
    ns_web_url ("lntwiki",
        "URL=https://wiki.lntdccs.com/lntwiki",
        "HEADER=Host:wiki.lntdccs.com",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
        "HEADER=Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Encoding:gzip, deflate, br",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "PreSnapshot=webpage_1661497058006.png",
        "Snapshot=webpage_1661497079228.png",
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki", "HEADER=Host:wiki.lntdccs.com", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", "HEADER=Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Encoding:gzip, deflate, br", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("lntwiki", NS_AUTO_STATUS);
    ns_page_think_time(49.443);

}
