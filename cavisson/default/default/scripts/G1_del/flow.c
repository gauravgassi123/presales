/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: Anjali.chauhan
    Date of recording: 09/02/2022 10:02:57
    Flow details:
    Build details: 4.8.0 (build# 129)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("index");
    ns_web_url ("index",
        "URL=https://www.cavisson.com/",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_8640b38e14ff3447d5c23ee6f4458dfc.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_single_b48f29a223d1760d6f3e1d0dfb850c96.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_single_411536fd225f7c2dc6daf53b293f3d11.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_single_762dedf90e6e1af28b7c11aaee1679d4.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.google.com/recaptcha/api.js?onload=wpformsRecaptchaLoad&render=explicit", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/logo/logo_original.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/banner/USTandCavisson2021.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/banner/insight-success-banner.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/banner/banner2aopti.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/banner/banner_new_opti.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/banner/banner3_opti.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/banner/business_continuity_opti.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/devops1.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/apes_slider.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/sre_slider.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/cloud-monitoring1b.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/product-icon/netstorm.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/product-icon/netcloud.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/product-icon/netocean.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/nethavoc.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/product-icon/diagnostic.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/product-icon/netvision.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/product-icon/netforest.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/sap.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/snmp.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/kubernetes.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/mongo-db.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/my-sql.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/ibm-db2.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/couchbase_a.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/dockers.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/rdt.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/rbu.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/jms.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/logo/Michaels.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/logo/macys.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/logo/pathkind.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/logo/redbox.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/logo/nha.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/line-bg.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/award0.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/award1.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/award2.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/award3.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/award4.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/award5.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/award6.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/award7.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/svg/ask.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/svg/feedback.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/svg/demo.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/js/autoptimize_3146c83f6f21fc70327eff5222e9d8bd.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://69.61.127.66:9026/nv/cavisson/nv_bootstrap.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google-analytics.com/analytics.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_13a5250f3cb21fe97a4196a34dddde79.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_6d3d148c5a30bb7dc1bafe5dd300ce89.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_f1b664e7f1e8a8e6bea090d2e3f909a1.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/products_bg.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/uploads/2020/10/y-cavisson.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/plugins/magee-shortcodes/assets/bootstrap/fonts/glyphicons-halflings-regular.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.cavisson.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/plugins/magee-shortcodes/assets/font-awesome/fonts/fontawesome-webfont.woff2?v=4.7.0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.cavisson.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/plugins/wp-social-widget/assets/fonts/socialicon.ttf?4xqn5s", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.cavisson.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/releases/mBwkfBPLFWI0ygbsp8eJNMkw/recaptcha__en.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.cavisson.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/loader.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://snap.licdn.com/li.lms-analytics/insight.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/plugins/mobile-menu/includes/css/font/mobmenu.woff2?31192480", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.cavisson.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE
    );

    ns_end_transaction("index", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect");
    ns_web_url ("collect",
        "URL=https://www.google-analytics.com/j/collect?v=1&_v=j96&a=360238483&t=pageview&_s=1&dl=https%3A%2F%2Fwww.cavisson.com%2F&ul=en-us&de=UTF-8&dt=Performance%20Testing%2C%20Monitoring%20%26%20Diagnostics%20Software%20%7C%20Cavisson%20-&sd=24-bit&sr=8192x4096&vp=775x473&je=0&_u=IEBAAEABAAAAAC~&jid=1371237911&gjid=1737090374&cid=426436839.1662112922&tid=UA-77809548-1&_gid=1167819051.1662112922&_r=1&_slc=1&z=1788218615",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://www.cavisson.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("collect", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("version");
    ns_web_url ("version",
        "URL=https://api.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/version",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://www.cavisson.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://px.ads.linkedin.com/collect?v=2&fmt=js&pid=775971&time=1662112922205&url=https%3A%2F%2Fwww.cavisson.com%2F", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/recaptcha/api2/anchor?ar=1&k=6Ldt8qEUAAAAAH-QhEOUuqZ3-IqCap0nlTw6xrli&co=aHR0cHM6Ly93d3cuY2F2aXNzb24uY29tOjQ0Mw..&hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw&size=normal&cb=x25bxr2le7gg", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("version", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_2");
    ns_web_url ("collect_2",
        "URL=https://stats.g.doubleclick.net/j/collect?t=dc&aip=1&_r=3&v=1&_v=j96&tid=UA-77809548-1&cid=426436839.1662112922&jid=1371237911&gjid=1737090374&_gid=1167819051.1662112922&_u=IEBAAEAAAAAAAC~&z=566215471",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://www.cavisson.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://px.ads.linkedin.com/collect?v=2&fmt=js&pid=775971&time=1662112922205&url=https%3A%2F%2Fwww.cavisson.com%2F&cookiesTest=true", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/ads/ga-audiences?t=sr&aip=1&_r=4&slf_rd=1&v=1&_v=j96&tid=UA-77809548-1&cid=426436839.1662112922&jid=1371237911&_u=IEBAAEAAAAAAAC~&z=1901368430", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/releases/mBwkfBPLFWI0ygbsp8eJNMkw/styles__ltr.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/releases/mBwkfBPLFWI0ygbsp8eJNMkw/recaptcha__en.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://prod.artibotcdn.com/manifest/_ArtiBotLauncherCB_Manifest?_=_", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/api2/logo_48.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxKKTU1Kg.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.google.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.youtube.com/iframe_api", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://prod.artibotcdn.com/launcher.4514.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.youtube.com/iframe_api", END_INLINE,
            "URL=https://www.youtube.com/s/player/5a3b6271/www-widgetapi.vflset/www-widgetapi.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxKKTU1Kg.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.google.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/recaptcha/api2/webworker.js?hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:same-origin", "HEADER=Sec-Fetch-Dest:worker", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collect_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("settings");
    ns_web_url ("settings",
        "URL=https://api-cdn.prod-aws.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/settings?settingsVersion=20&botVersionId=12034483-9bfe-47a8-bbfa-ada6398deb1f",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://www.cavisson.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://app.artibot.ai/chat-window?i=948c4b2e-cf5d-4d58-9e77-2b636e5bbd98&cbv=12034483-9bfe-47a8-bbfa-ada6398deb1f&v=20&cp=https%3A%2F%2Fwww.cavisson.com%2F&th=light&em=true&mo=true", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/chat-window?i=948c4b2e-cf5d-4d58-9e77-2b636e5bbd98&cbv=12034483-9bfe-47a8-bbfa-ada6398deb1f&v=20&cp=https%3A%2F%2Fwww.cavisson.com%2F&th=light&em=true&mo=true&sh=false&tb=true", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/recaptcha/api2/bframe?hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw&k=6Ldt8qEUAAAAAH-QhEOUuqZ3-IqCap0nlTw6xrli", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/chat_window.4514.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://js.stripe.com/v3/", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/chat_window.4514.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,600,700", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.googleapis.com/css?family=Source+Serif+Pro:300,400,400i,600,700", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.googleapis.com/css?family=Space+Mono:300,400,400i,600,700", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://js.stripe.com/v3/m-outer-2a0f7db50009238158f4274fa211fa55.html#url=https%3A%2F%2Fapp.artibot.ai%2Fchat-window%3Fi%3D948c4b2e-cf5d-4d58-9e77-2b636e5bbd98%26cbv%3D12034483-9bfe-47a8-bbfa-ada6398deb1f%26v%3D20%26cp%3Dhttps%253A%252F%252Fwww.cavisson.com%252F%26th%3Dlight%26em%3Dtrue%26mo%3Dtrue%26sh%3Dfalse%26tb%3Dtrue&title=ArtiBot.ai%20%E2%80%94%20AI%20Powered%20Bot%20for%20Lead%20Capture&referrer=&muid=NA&sid=NA&version=6&preview=false", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://js.stripe.com/v3/fingerprinted/js/m-outer-900a76d673da7dda0f4c2eb5c9c54cdd.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.stripe.network/inner.html#url=https%3A%2F%2Fapp.artibot.ai%2Fchat-window%3Fi%3D948c4b2e-cf5d-4d58-9e77-2b636e5bbd98%26cbv%3D12034483-9bfe-47a8-bbfa-ada6398deb1f%26v%3D20%26cp%3Dhttps%253A%252F%252Fwww.cavisson.com%252F%26th%3Dlight%26em%3Dtrue%26mo%3Dtrue%26sh%3Dfalse%26tb%3Dtrue&title=ArtiBot.ai%20%E2%80%94%20AI%20Powered%20Bot%20for%20Lead%20Capture&referrer=&muid=NA&sid=NA&version=6&preview=false", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.stripe.network/out-4.5.42.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("settings", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("settings_2");
    ns_web_url ("settings_2",
        "URL=https://api-cdn.prod-aws.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/settings?settingsVersion=20&botVersionId=12034483-9bfe-47a8-bbfa-ada6398deb1f",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://app.artibot.ai",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.cavisson.com/wp-content/uploads/2016/05/cropped-Cavisson.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat", END_INLINE
    );

    ns_end_transaction("settings_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("X");
    ns_web_url ("X",
        "URL=https://m.stripe.com/6",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.stripe.network",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "JTdCJTIydjIlMjIlM0ExJTJDJTIyaWQlMjIlM0ElMjI5ZTNjYmVjMTJiYzg0ODdhY2Y4ZDNlNTk4ODI4YzFiNSUyMiUyQyUyMnQlMjIlM0ExODMuNDclMkMlMjJ0YWclMjIlM0ElMjI0LjUuNDIlMjIlMkMlMjJzcmMlMjIlM0ElMjJqcyUyMiUyQyUyMmElMjIlM0ElN0IlMjJhJTIyJTNBJTdCJTIydiUyMiUzQSUyMnRydWUlMjIlMkMlMjJ0JTIyJTNBMS4wMzUlN0QlMkMlMjJiJTIyJTNBJTdCJTIydiUyMiUzQSUyMmZhbHNlJTIyJTJDJTIydCUyMiUzQTAuMDI1JTdEJTJDJTIyYyUyMiUzQSU3QiUyMnYlMjIlM0ElMjJlbi1VUyUyQ2VuJTIyJTJDJTIydCUyMiUzQTAuMDUlN0QlMkMlMjJkJTIyJTNBJTdCJTIydiUyMiUzQSUyMkxpbnV4JTIweDg2XzY0JTIyJTJDJTIydCUyMiUzQTAuMDI1JTdEJTJDJTIyZSUyMiUzQSU3QiUyMnYlMjIlM0ElMjIlMjIlMkMlMjJ0JTIyJTNBMC4yNDUlN0QlMkMlMjJmJTIyJTNBJTdCJTIydiUyMiUzQSUyMjgxOTJ3XzQwOTZoXzI0ZF8xciUyMiUyQyUyMnQlMjIlM0EwLjI4JTdEJTJDJTIyZyUyMiUzQSU3QiUyMnYlMjIlM0ElMjItNSUyMiUyQyUyMnQlMjIlM0EwLjAzNSU3RCUyQyUyMmglMjIlM0ElN0IlMjJ2JTIyJTNBJTIyZmFsc2UlMjIlMkMlMjJ0JTIyJTNBMC4wMzUlN0QlMkMlMjJpJTIyJTNBJTdCJTIydiUyMiUzQSUyMnNlc3Npb25TdG9yYWdlLWVuYWJsZWQlMkMlMjBsb2NhbFN0b3JhZ2UtZW5hYmxlZCUyMiUyQyUyMnQlMjIlM0E2LjcxNSU3RCUyQyUyMmolMjIlM0ElN0IlMjJ2JTIyJTNBJTIyMDAwMDAwMDExMDAwMDAwMDAwMDAwMDAwMTAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDExMDAwMCUyMiUyQyUyMnQlMjIlM0ExNzQuNjclMkMlMjJhdCUyMiUzQTExOS42MSU3RCUyQyUyMmslMjIlM0ElN0IlMjJ2JTIyJTNBJTIyJTIyJTJDJTIydCUyMiUzQTAuMDMlN0QlMkMlMjJsJTIyJTNBJTdCJTIydiUyMiUzQSUyMk1vemlsbGElMkY1LjAlMjAoTGludXglM0IlMjBBbmRyb2lkJTIwNi4wLjElM0IlMjBNb3RvJTIwRyUyMCg0KSUyMEJ1aWxkJTJGTVBKMjQuMTM5LTY0KSUyMEFwcGxlV2ViS2l0JTJGNTM3LjM2JTIwKEtIVE1MJTJDJTIwbGlrZSUyMEdlY2tvKSUyMENocm9tZSUyRjU4LjAuMzAyOS44MSUyME1vYmlsZSUyMFNhZmFyaSUyRjUzNy4zNiUyMFBUU1QlMkYxJTIyJTJDJTIydCUyMiUzQTAuMDM1JTdEJTJDJTIybSUyMiUzQSU3QiUyMnYlMjIlM0ElMjIlMjIlMkMlMjJ0JTIyJTNBMC44JTdEJTJDJTIybiUyMiUzQSU3QiUyMnYlMjIlM0ElMjJ0cnVlJTIyJTJDJTIydCUyMiUzQTEwMS45NiUyQyUyMmF0JTIyJTNBMS4xNTUlN0QlMkMlMjJvJTIyJTNBJTdCJTIydiUyMiUzQSUyMjFkYTA0Y2RmOGYyZDUxZjdkZWRjYTc1MmQ1NzgyZGM1JTIyJTJDJTIydCUyMiUzQTQ1LjE3JTdEJTdEJTJDJTIyYiUyMiUzQSU3QiUyMmElMjIlM0ElMjIlMjIlMkMlMjJiJTIyJTNBJTIyaHR0cHMlM0ElMkYlMkZqdDV6WVZKWXBxVDJlcmFtTGNCMGhYU0Z5UUFId3lKbFQ1WTlRXzhfMWR3LkNsX3JPcVB4Uld6MWhvSlMwekx3U09remRmYWtxaHREVG9ZdVl1bWFhN3Mub0ZhYmJLV3dDekp3anQwTl9NRFVnMnNSa1ZsQXdJYmhSZTcyOU1tN2xXUSUyRlZaSHVid0FraU9ETjhZOVhvTUlzTE90aTF0UnloaFR1V2M3Qms4U0YxdVUlM0ZDN1FyV09iSDl0YnJial8ybDNJdnRhVUNfaWhlYnR6aVpPTEtocHgzelhnJTNERFZRN3lraTUzVUtHenNtWE53dEJwVkxwRUtkMUNBTUdkY09EUHJRTm5sSSUyNjBiTEVhMUhCUjlsd1NBTFA3cVlDTy1uaXNBbHBwb3RMdHhWTmV2aUlPY2slM0RJamc3eFNMLUJIT0VPWnBSNkFIeWMzVzZOTk5OVVMxMXlMM0I1bUxyNEdBJTI2dW5COEdLalVHRVlnUm02a3B1Q2ptSXN5WHBfYVByN3RnVjZBRG1MdUZyZyUzRGhBU2RlYzJ4N080cFpOOHV3WEs3bjVsNjFKZnNybllfeDBDSW9WR1lSWHMlMjZvcU9idmtxTk9VN3dHMG81blZPdHR0aHlLYmU0MzBRVHBLRGhrTktSWEc4JTNEQzF1R1NIMEh3bHF6eFdQQ3lGYlo5MXNzTk5hcXU3Ukg5Z3J6UFd1LWVVQSUyNk1aRVRCbktWeXVMa2E2RXJYWDZPZkthd3JpX1V3elR1bVVSUFJJcVNRVGMlM0RaaEc0bVZOdVh1cXdqT0F4N1FRRUphYlNfdnhYTUVfTzMwS3lJdmZfMHhBJTI2REFGNnBCJTNEMlN4UHJhJTI2YzZhQjdwJTNEMlN4UHJhJTI2dnRzbWEwJTNEOGszal9HJTI2VTQ3N3VqJTNEMlN4UHJhJTIyJTJDJTIyYyUyMiUzQSUyMmpOUVNWWVdSZjIwQ04ybWk0UDlZdGN4QTlRQ2xZSkFVTVZlUk1CWXVmVXMlMjIlMkMlMjJkJTIyJTNBJTIyTkElMjIlMkMlMjJlJTIyJTNBJTIyTkElMjIlMkMlMjJmJTIyJTNBZmFsc2UlMkMlMjJnJTIyJTNBdHJ1ZSUyQyUyMmglMjIlM0F0cnVlJTJDJTIyaSUyMiUzQSU1QiUyMmxvY2F0aW9uJTIyJTVEJTJDJTIyaiUyMiUzQSU1QiU1RCUyQyUyMm4lMjIlM0ExMTQuMzM0OTk5OTkxMTE1MTglMkMlMjJ1JTIyJTNBJTIyYXBwLmFydGlib3QuYWklMjIlN0QlMkMlMjJoJTIyJTNBJTIyOTYxZGNjYWE5NmMxYjdkMWFmZGElMjIlN0Q=",
        BODY_END
    );

    ns_end_transaction("X", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("index_6");
    ns_web_url ("index_6",
        "URL=https://api.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/chats/init/",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://app.artibot.ai",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"botVersionId":"12034483-9bfe-47a8-bbfa-ada6398deb1f","contactId":"deabc787-8299-4886-87d6-b75f495c9a4d","state":null,"timezone":"America/Chicago"}",
        BODY_END
    );

    ns_end_transaction("index_6", NS_AUTO_STATUS);
    ns_page_think_time(15.526);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("index_7");
    ns_web_url ("index_7",
        "URL=https://www.cavisson.com/request-product-demo/",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat",
        INLINE_URLS,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_62982bae0a0b42033dadbd5d63c29c1a.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/js?id=AW-846414309", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/js/autoptimize_215922ccdcbfc1b44f4530c3610bdeef.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat", END_INLINE,
            "URL=https://69.61.127.66:9026/nv/cavisson/nv_bootstrap.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("index_7", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_3");
    ns_web_url ("collect_3",
        "URL=https://www.google-analytics.com/j/collect?v=1&_v=j96&a=911019106&t=pageview&_s=1&dl=https%3A%2F%2Fwww.cavisson.com%2Frequest-product-demo%2F&ul=en-us&de=UTF-8&dt=Performance%20Testing%20and%20Diagnostics%20Product%20Demo%20Request%20%7C%20Cavisson&sd=24-bit&sr=1313x489&vp=1298x361&je=0&_u=AACAAEABAAAAAC~&jid=&gjid=&cid=426436839.1662112922&tid=UA-77809548-1&_gid=1167819051.1662112922&_slc=1&z=1140180272",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://www.cavisson.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.googleadservices.com/pagead/conversion_async.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/wcm/loader.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collect_3", NS_AUTO_STATUS);
    ns_page_think_time(0.03);

    ns_start_transaction("index_2");
    ns_web_url ("index_2",
        "URL=https://www.google.com/recaptcha/api2/anchor?ar=1&k=6Ldt8qEUAAAAAH-QhEOUuqZ3-IqCap0nlTw6xrli&co=aHR0cHM6Ly93d3cuY2F2aXNzb24uY29tOjQ0Mw..&hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw&size=normal&cb=q0obmw80ffs8",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-Dest:iframe",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("index_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("version_2");
    ns_web_url ("version_2",
        "URL=https://api.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/version",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://www.cavisson.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.gstatic.com/recaptcha/releases/mBwkfBPLFWI0ygbsp8eJNMkw/styles__ltr.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/releases/mBwkfBPLFWI0ygbsp8eJNMkw/recaptcha__en.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://px.ads.linkedin.com/collect?v=2&fmt=js&pid=775971&time=1662112941251&url=https%3A%2F%2Fwww.cavisson.com%2Frequest-product-demo%2F", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/api2/logo_48.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxKKTU1Kg.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.google.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/call-tracking/call-tracking_7.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/846414309/?random=1662112941325&cv=9&fst=1662112941325&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&u_h=489&u_w=1313&u_ah=489&u_aw=1313&u_cd=24&u_his=3&u_tz=-300&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa8v0&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.cavisson.com%2Frequest-product-demo%2F&ref=https%3A%2F%2Fwww.cavisson.com%2F&tiba=Performance%20Testing%20and%20Diagnostics%20Product%20Demo%20Request%20%7C%20Cavisson&auid=2130207980.1662112941&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://px.ads.linkedin.com/collect?v=2&fmt=js&pid=775971&time=1662112941251&url=https%3A%2F%2Fwww.cavisson.com%2Frequest-product-demo%2F&cookiesTest=true", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("version_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("settings_3");
    ns_web_url ("settings_3",
        "URL=https://api-cdn.prod-aws.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/settings?settingsVersion=20&botVersionId=12034483-9bfe-47a8-bbfa-ada6398deb1f",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://www.cavisson.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.googleadservices.com/pagead/conversion/846414309/wcm?cc=ZZ&dn=16503197412&cl=irwhCLex05cBEOWDzZMD&ref=https%3A%2F%2Fwww.cavisson.com%2F&ct_eid=2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://www.cavisson.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/recaptcha/api2/webworker.js?hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:same-origin", "HEADER=Sec-Fetch-Dest:worker", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("settings_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("wcm");
    ns_web_url ("wcm",
        "URL=https://www.google.com/pagead/attribution/wcm?cc=ZZ&dn=16503197412&cl=irwhCLex05cBEOWDzZMD",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:null",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.google.com/pagead/1p-user-list/846414309/?random=1662112941325&cv=9&fst=1662112800000&num=1&bg=ffffff&guid=ON&u_h=489&u_w=1313&u_ah=489&u_aw=1313&u_cd=24&u_his=3&u_tz=-300&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa8v0&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.cavisson.com%2Frequest-product-demo%2F&ref=https%3A%2F%2Fwww.cavisson.com%2F&tiba=Performance%20Testing%20and%20Diagnostics%20Product%20Demo%20Request%20%7C%20Cavisson&async=1&fmt=3&is_vtc=1&random=1425991895&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/chat-window?i=948c4b2e-cf5d-4d58-9e77-2b636e5bbd98&cbv=12034483-9bfe-47a8-bbfa-ada6398deb1f&v=20&cp=https%3A%2F%2Fwww.cavisson.com%2Frequest-product-demo%2F&th=light&em=true&mo=true", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/chat-window?i=948c4b2e-cf5d-4d58-9e77-2b636e5bbd98&cbv=12034483-9bfe-47a8-bbfa-ada6398deb1f&v=20&cp=https%3A%2F%2Fwww.cavisson.com%2Frequest-product-demo%2F&th=light&em=true&mo=true&sh=false&tb=true", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/chat_window.4514.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://js.stripe.com/v3/", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/chat_window.4514.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/846414309/wcm?cc=ZZ&dn=18007016125&cl=9penCM3u55cBEOWDzZMD&ref=https%3A%2F%2Fwww.cavisson.com%2F&ct_eid=2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://www.cavisson.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,600,700", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.googleapis.com/css?family=Source+Serif+Pro:300,400,400i,600,700", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.googleapis.com/css?family=Space+Mono:300,400,400i,600,700", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/recaptcha/api2/bframe?hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw&k=6Ldt8qEUAAAAAH-QhEOUuqZ3-IqCap0nlTw6xrli", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("wcm", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("wcm_2");
    ns_web_url ("wcm_2",
        "URL=https://www.google.com/pagead/attribution/wcm?cc=ZZ&dn=18007016125&cl=9penCM3u55cBEOWDzZMD",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:null",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://js.stripe.com/v3/m-outer-2a0f7db50009238158f4274fa211fa55.html#url=https%3A%2F%2Fapp.artibot.ai%2Fchat-window%3Fi%3D948c4b2e-cf5d-4d58-9e77-2b636e5bbd98%26cbv%3D12034483-9bfe-47a8-bbfa-ada6398deb1f%26v%3D20%26cp%3Dhttps%253A%252F%252Fwww.cavisson.com%252Frequest-product-demo%252F%26th%3Dlight%26em%3Dtrue%26mo%3Dtrue%26sh%3Dfalse%26tb%3Dtrue&title=ArtiBot.ai%20%E2%80%94%20AI%20Powered%20Bot%20for%20Lead%20Capture&referrer=&muid=NA&sid=NA&version=6&preview=false", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://js.stripe.com/v3/fingerprinted/js/m-outer-900a76d673da7dda0f4c2eb5c9c54cdd.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.stripe.network/inner.html#url=https%3A%2F%2Fapp.artibot.ai%2Fchat-window%3Fi%3D948c4b2e-cf5d-4d58-9e77-2b636e5bbd98%26cbv%3D12034483-9bfe-47a8-bbfa-ada6398deb1f%26v%3D20%26cp%3Dhttps%253A%252F%252Fwww.cavisson.com%252Frequest-product-demo%252F%26th%3Dlight%26em%3Dtrue%26mo%3Dtrue%26sh%3Dfalse%26tb%3Dtrue&title=ArtiBot.ai%20%E2%80%94%20AI%20Powered%20Bot%20for%20Lead%20Capture&referrer=&muid=NA&sid=NA&version=6&preview=false", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.stripe.network/out-4.5.42.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("wcm_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("X_2");
    ns_web_url ("X_2",
        "URL=https://m.stripe.com/6",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.stripe.network",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "JTdCJTIydjIlMjIlM0ExJTJDJTIyaWQlMjIlM0ElMjIyZWQ4NTNjOTQ2M2UxYzFkOTdmYzM2NDczZmEzZGM2ZSUyMiUyQyUyMnQlMjIlM0EzOS4wNzUlMkMlMjJ0YWclMjIlM0ElMjI0LjUuNDIlMjIlMkMlMjJzcmMlMjIlM0ElMjJqcyUyMiUyQyUyMmElMjIlM0ElN0IlMjJhJTIyJTNBJTdCJTIydiUyMiUzQSUyMnRydWUlMjIlMkMlMjJ0JTIyJTNBMC41MyU3RCUyQyUyMmIlMjIlM0ElN0IlMjJ2JTIyJTNBJTIyZmFsc2UlMjIlMkMlMjJ0JTIyJTNBMC4wMTUlN0QlMkMlMjJjJTIyJTNBJTdCJTIydiUyMiUzQSUyMmVuLVVTJTJDZW4lMjIlMkMlMjJ0JTIyJTNBMC4wNCU3RCUyQyUyMmQlMjIlM0ElN0IlMjJ2JTIyJTNBJTIyTGludXglMjB4ODZfNjQlMjIlMkMlMjJ0JTIyJTNBMC4wMiU3RCUyQyUyMmUlMjIlM0ElN0IlMjJ2JTIyJTNBJTIyJTIyJTJDJTIydCUyMiUzQTAuMDYlN0QlMkMlMjJmJTIyJTNBJTdCJTIydiUyMiUzQSUyMjEzMTN3XzQ4OWhfMjRkXzFyJTIyJTJDJTIydCUyMiUzQTAuMDI1JTdEJTJDJTIyZyUyMiUzQSU3QiUyMnYlMjIlM0ElMjItNSUyMiUyQyUyMnQlMjIlM0EwLjAyNSU3RCUyQyUyMmglMjIlM0ElN0IlMjJ2JTIyJTNBJTIyZmFsc2UlMjIlMkMlMjJ0JTIyJTNBMC4wMiU3RCUyQyUyMmklMjIlM0ElN0IlMjJ2JTIyJTNBJTIyc2Vzc2lvblN0b3JhZ2UtZW5hYmxlZCUyQyUyMGxvY2FsU3RvcmFnZS1lbmFibGVkJTIyJTJDJTIydCUyMiUzQTIuNjA1JTdEJTJDJTIyaiUyMiUzQSU3QiUyMnYlMjIlM0ElMjIwMDAwMDAwMTEwMDAwMDAwMDAwMDAwMDAxMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMTEwMDAwJTIyJTJDJTIydCUyMiUzQTM1LjI4NSUyQyUyMmF0JTIyJTNBMC4xNiU3RCUyQyUyMmslMjIlM0ElN0IlMjJ2JTIyJTNBJTIyJTIyJTJDJTIydCUyMiUzQTAuMDElN0QlMkMlMjJsJTIyJTNBJTdCJTIydiUyMiUzQSUyMk1vemlsbGElMkY1LjAlMjAoTGludXglM0IlMjBBbmRyb2lkJTIwNi4wLjElM0IlMjBNb3RvJTIwRyUyMCg0KSUyMEJ1aWxkJTJGTVBKMjQuMTM5LTY0KSUyMEFwcGxlV2ViS2l0JTJGNTM3LjM2JTIwKEtIVE1MJTJDJTIwbGlrZSUyMEdlY2tvKSUyMENocm9tZSUyRjU4LjAuMzAyOS44MSUyME1vYmlsZSUyMFNhZmFyaSUyRjUzNy4zNiUyMFBUU1QlMkYxJTIyJTJDJTIydCUyMiUzQTAuMDElN0QlMkMlMjJtJTIyJTNBJTdCJTIydiUyMiUzQSUyMiUyMiUyQyUyMnQlMjIlM0EwLjA2NSU3RCUyQyUyMm4lMjIlM0ElN0IlMjJ2JTIyJTNBJTIydHJ1ZSUyMiUyQyUyMnQlMjIlM0EzNC44OSUyQyUyMmF0JTIyJTNBMC4wMTUlN0QlMkMlMjJvJTIyJTNBJTdCJTIydiUyMiUzQSUyMjFkYTA0Y2RmOGYyZDUxZjdkZWRjYTc1MmQ1NzgyZGM1JTIyJTJDJTIydCUyMiUzQTM0LjMyNSU3RCU3RCUyQyUyMmIlMjIlM0ElN0IlMjJhJTIyJTNBJTIyJTIyJTJDJTIyYiUyMiUzQSUyMmh0dHBzJTNBJTJGJTJGanQ1ellWSllwcVQyZXJhbUxjQjBoWFNGeVFBSHd5SmxUNVk5UV84XzFkdy5DbF9yT3FQeFJXejFob0pTMHpMd1NPa3pkZmFrcWh0RFRvWXVZdW1hYTdzLm9GYWJiS1d3Q3pKd2p0ME5fTURVZzJzUmtWbEF3SWJoUmU3MjlNbTdsV1ElMkZWWkh1YndBa2lPRE44WTlYb01Jc0xPdGkxdFJ5aGhUdVdjN0JrOFNGMXVVJTNGQzdRcldPYkg5dGJyYmpfMmwzSXZ0YVVDX2loZWJ0emlaT0xLaHB4M3pYZyUzRERWUTd5a2k1M1VLR3pzbVhOd3RCcFZMcEVLZDFDQU1HZGNPRFByUU5ubEklMjYwYkxFYTFIQlI5bHdTQUxQN3FZQ08tbmlzQWxwcG90THR4Vk5ldmlJT2NrJTNESWpnN3hTTC1CSE9FT1pwUjZBSHljM1c2Tk5OTlVTMTF5TDNCNW1McjRHQSUyNnVuQjhHS2pVR0VZZ1JtNmtwdUNqbUlzeVhwX2FQcjd0Z1Y2QURtTHVGcmclM0RoQVNkZWMyeDdPNHBaTjh1d1hLN241bDYxSmZzcm5ZX3gwQ0lvVkdZUlhzJTI2b3FPYnZrcU5PVTd3RzBvNW5WT3R0dGh5S2JlNDMwUVRwS0Roa05LUlhHOCUzRElNRkcyTWFNbHlFUjhIZ3FpSDB5UW5EaHhyU0pPcmpNcVR2bndxTGt1X0klMjZNWkVUQm5LVnl1TGthNkVyWFg2T2ZLYXdyaV9Vd3pUdW1VUlBSSXFTUVRjJTNEWmhHNG1WTnVYdXF3ak9BeDdRUUVKYWJTX3Z4WE1FX08zMEt5SXZmXzB4QSUyNkRBRjZwQiUzRDJTeFByYSUyNmM2YUI3cCUzRDJTeFByYSUyNnZ0c21hMCUzRDhrM2pfRyUyNlU0Nzd1aiUzRDJTeFByYSUyMiUyQyUyMmMlMjIlM0ElMjJqTlFTVllXUmYyMENOMm1pNFA5WXRjeEE5UUNsWUpBVU1WZVJNQll1ZlVzJTIyJTJDJTIyZCUyMiUzQSUyMk5BJTIyJTJDJTIyZSUyMiUzQSUyMk5BJTIyJTJDJTIyZiUyMiUzQWZhbHNlJTJDJTIyZyUyMiUzQXRydWUlMkMlMjJoJTIyJTNBdHJ1ZSUyQyUyMmklMjIlM0ElNUIlMjJsb2NhdGlvbiUyMiU1RCUyQyUyMmolMjIlM0ElNUIlNUQlMkMlMjJuJTIyJTNBODIuOTc0OTk5OTY4NDI4MTYlMkMlMjJ1JTIyJTNBJTIyYXBwLmFydGlib3QuYWklMjIlN0QlMkMlMjJoJTIyJTNBJTIyZTQ5OWM3YzhkOTZmYTIzODAzNjMlMjIlN0Q=",
        BODY_END
    );

    ns_end_transaction("X_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("index_8");
    ns_web_url ("index_8",
        "URL=https://www.cavisson.com/request-product-demo/?\cavisson.ico",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat;_gcl_au"
    );

    ns_end_transaction("index_8", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("settings_4");
    ns_web_url ("settings_4",
        "URL=https://api-cdn.prod-aws.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/settings?settingsVersion=20&botVersionId=12034483-9bfe-47a8-bbfa-ada6398deb1f",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://app.artibot.ai",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("settings_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("index_9");
    ns_web_url ("index_9",
        "URL=https://api.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/chats/init/",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://app.artibot.ai",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"botVersionId":"12034483-9bfe-47a8-bbfa-ada6398deb1f","contactId":"deabc787-8299-4886-87d6-b75f495c9a4d","state":"Fi0xBJlSp6S0iaA90VbT2xrC/0H8hvGWrcZGr5BqDCY+dltTe0ZJQP8/U8XvaP246VITHWvWy89akZoYHIjGlQwYlKzbZJd9E7Xc4P78hAwLR0sBeCv/u9F0j0/Ps7OEsLMj9xylCSs=","timezone":"America/Chicago"}",
        BODY_END
    );

    ns_end_transaction("index_9", NS_AUTO_STATUS);
    ns_page_think_time(1.619);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("index_3");
    ns_web_url ("index_3",
        "URL=https://www.cavisson.com/cavisson-systems-performance-intelligence-platform/",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat;_gcl_au",
        INLINE_URLS,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_c5e58a610ae173b4589281eda526da3f.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat;_gcl_au", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_single_b48f29a223d1760d6f3e1d0dfb850c96.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat;_gcl_au", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_single_411536fd225f7c2dc6daf53b293f3d11.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat;_gcl_au", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_single_762dedf90e6e1af28b7c11aaee1679d4.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat;_gcl_au", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/js?id=AW-846414309", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cavisson.com/wp-content/themes/onetone/images/pyramid.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_ga;_gid;_gat;_gcl_au", END_INLINE,
            "URL=https://69.61.127.66:9026/nv/cavisson/nv_bootstrap.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google-analytics.com/analytics.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_13a5250f3cb21fe97a4196a34dddde79.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat;_gcl_au", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_6d3d148c5a30bb7dc1bafe5dd300ce89.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat;_gcl_au", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_f1b664e7f1e8a8e6bea090d2e3f909a1.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat;_gcl_au", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion_async.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/wcm/loader.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/releases/mBwkfBPLFWI0ygbsp8eJNMkw/recaptcha__en.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.cavisson.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/loader.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://snap.licdn.com/li.lms-analytics/insight.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("index_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_4");
    ns_web_url ("collect_4",
        "URL=https://www.google-analytics.com/j/collect?v=1&_v=j96&a=328966615&t=pageview&_s=1&dl=https%3A%2F%2Fwww.cavisson.com%2Fcavisson-systems-performance-intelligence-platform%2F&ul=en-us&de=UTF-8&dt=Performance%20Intelligence%20Platform%20for%20Mission%20Critical%20Applications&sd=24-bit&sr=1313x489&vp=1298x361&je=0&_u=AACAAEABAAAAAC~&jid=&gjid=&cid=426436839.1662112922&tid=UA-77809548-1&_gid=1167819051.1662112922&_slc=1&z=2037631730",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://www.cavisson.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.gstatic.com/call-tracking/call-tracking_7.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/846414309/?random=1662112945424&cv=9&fst=1662112945424&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&u_h=489&u_w=1313&u_ah=489&u_aw=1313&u_cd=24&u_his=4&u_tz=-300&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa8v0&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.cavisson.com%2Fcavisson-systems-performance-intelligence-platform%2F&ref=https%3A%2F%2Fwww.cavisson.com%2Frequest-product-demo%2F&tiba=Performance%20Intelligence%20Platform%20for%20Mission%20Critical%20Applications&auid=2130207980.1662112941&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=test_cookie", END_INLINE
    );

    ns_end_transaction("collect_4", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("version_3");
    ns_web_url ("version_3",
        "URL=https://api.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/version",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://www.cavisson.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://px.ads.linkedin.com/collect?v=2&fmt=js&pid=775971&time=1662112945434&url=https%3A%2F%2Fwww.cavisson.com%2Fcavisson-systems-performance-intelligence-platform%2F", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/recaptcha/api2/anchor?ar=1&k=6Ldt8qEUAAAAAH-QhEOUuqZ3-IqCap0nlTw6xrli&co=aHR0cHM6Ly93d3cuY2F2aXNzb24uY29tOjQ0Mw..&hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw&size=normal&cb=ozy2b4vcwgxf", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("version_3", NS_AUTO_STATUS);
    ns_page_think_time(0.031);

    //Page Auto split for 
    ns_start_transaction("collect_5");
    ns_web_url ("collect_5",
        "URL=https://px.ads.linkedin.com/collect?v=2&fmt=js&pid=775971&time=1662112945434&url=https%3A%2F%2Fwww.cavisson.com%2Fcavisson-systems-performance-intelligence-platform%2F&cookiesTest=true",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://prod.artibotcdn.com/manifest/_ArtiBotLauncherCB_Manifest?_=_", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/846414309/wcm?cc=ZZ&dn=16503197412&cl=irwhCLex05cBEOWDzZMD&ref=https%3A%2F%2Fwww.cavisson.com%2F&ct_eid=2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://www.cavisson.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://prod.artibotcdn.com/launcher.4514.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/releases/mBwkfBPLFWI0ygbsp8eJNMkw/styles__ltr.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/releases/mBwkfBPLFWI0ygbsp8eJNMkw/recaptcha__en.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collect_5", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("settings_5");
    ns_web_url ("settings_5",
        "URL=https://api-cdn.prod-aws.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/settings?settingsVersion=20&botVersionId=12034483-9bfe-47a8-bbfa-ada6398deb1f",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://www.cavisson.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("settings_5", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("wcm_3");
    ns_web_url ("wcm_3",
        "URL=https://www.google.com/pagead/attribution/wcm?cc=ZZ&dn=16503197412&cl=irwhCLex05cBEOWDzZMD",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:null",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.gstatic.com/recaptcha/api2/logo_48.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxKKTU1Kg.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.google.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/846414309/?random=1662112945424&cv=9&fst=1662112800000&num=1&bg=ffffff&guid=ON&u_h=489&u_w=1313&u_ah=489&u_aw=1313&u_cd=24&u_his=4&u_tz=-300&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa8v0&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.cavisson.com%2Fcavisson-systems-performance-intelligence-platform%2F&ref=https%3A%2F%2Fwww.cavisson.com%2Frequest-product-demo%2F&tiba=Performance%20Intelligence%20Platform%20for%20Mission%20Critical%20Applications&async=1&fmt=3&is_vtc=1&random=3334544944&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/chat-window?i=948c4b2e-cf5d-4d58-9e77-2b636e5bbd98&cbv=12034483-9bfe-47a8-bbfa-ada6398deb1f&v=20&cp=https%3A%2F%2Fwww.cavisson.com%2Fcavisson-systems-performance-intelligence-platform%2F&th=light&em=true&mo=true", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/846414309/wcm?cc=ZZ&dn=18007016125&cl=9penCM3u55cBEOWDzZMD&ref=https%3A%2F%2Fwww.cavisson.com%2F&ct_eid=2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://www.cavisson.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/chat-window?i=948c4b2e-cf5d-4d58-9e77-2b636e5bbd98&cbv=12034483-9bfe-47a8-bbfa-ada6398deb1f&v=20&cp=https%3A%2F%2Fwww.cavisson.com%2Fcavisson-systems-performance-intelligence-platform%2F&th=light&em=true&mo=true&sh=false&tb=true", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("wcm_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("wcm_4");
    ns_web_url ("wcm_4",
        "URL=https://www.google.com/pagead/attribution/wcm?cc=ZZ&dn=18007016125&cl=9penCM3u55cBEOWDzZMD",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:null",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.google.com/recaptcha/api2/webworker.js?hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:same-origin", "HEADER=Sec-Fetch-Dest:worker", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/chat_window.4514.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://js.stripe.com/v3/", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/chat_window.4514.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,600,700", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.googleapis.com/css?family=Source+Serif+Pro:300,400,400i,600,700", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.googleapis.com/css?family=Space+Mono:300,400,400i,600,700", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/recaptcha/api2/bframe?hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw&k=6Ldt8qEUAAAAAH-QhEOUuqZ3-IqCap0nlTw6xrli", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://js.stripe.com/v3/m-outer-2a0f7db50009238158f4274fa211fa55.html#url=https%3A%2F%2Fapp.artibot.ai%2Fchat-window%3Fi%3D948c4b2e-cf5d-4d58-9e77-2b636e5bbd98%26cbv%3D12034483-9bfe-47a8-bbfa-ada6398deb1f%26v%3D20%26cp%3Dhttps%253A%252F%252Fwww.cavisson.com%252Fcavisson-systems-performance-intelligence-platform%252F%26th%3Dlight%26em%3Dtrue%26mo%3Dtrue%26sh%3Dfalse%26tb%3Dtrue&title=ArtiBot.ai%20%E2%80%94%20AI%20Powered%20Bot%20for%20Lead%20Capture&referrer=&muid=NA&sid=NA&version=6&preview=false", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://js.stripe.com/v3/fingerprinted/js/m-outer-900a76d673da7dda0f4c2eb5c9c54cdd.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.stripe.network/inner.html#url=https%3A%2F%2Fapp.artibot.ai%2Fchat-window%3Fi%3D948c4b2e-cf5d-4d58-9e77-2b636e5bbd98%26cbv%3D12034483-9bfe-47a8-bbfa-ada6398deb1f%26v%3D20%26cp%3Dhttps%253A%252F%252Fwww.cavisson.com%252Fcavisson-systems-performance-intelligence-platform%252F%26th%3Dlight%26em%3Dtrue%26mo%3Dtrue%26sh%3Dfalse%26tb%3Dtrue&title=ArtiBot.ai%20%E2%80%94%20AI%20Powered%20Bot%20for%20Lead%20Capture&referrer=&muid=NA&sid=NA&version=6&preview=false", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("wcm_4", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("settings_6");
    ns_web_url ("settings_6",
        "URL=https://api-cdn.prod-aws.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/settings?settingsVersion=20&botVersionId=12034483-9bfe-47a8-bbfa-ada6398deb1f",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://app.artibot.ai",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://m.stripe.network/out-4.5.42.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("settings_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("X_3");
    ns_web_url ("X_3",
        "URL=https://m.stripe.com/6",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.stripe.network",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "JTdCJTIydjIlMjIlM0ExJTJDJTIyaWQlMjIlM0ElMjIyZWQ4NTNjOTQ2M2UxYzFkOTdmYzM2NDczZmEzZGM2ZSUyMiUyQyUyMnQlMjIlM0EzNy41MiUyQyUyMnRhZyUyMiUzQSUyMjQuNS40MiUyMiUyQyUyMnNyYyUyMiUzQSUyMmpzJTIyJTJDJTIyYSUyMiUzQW51bGwlMkMlMjJiJTIyJTNBJTdCJTIyYSUyMiUzQSUyMiUyMiUyQyUyMmIlMjIlM0ElMjJodHRwcyUzQSUyRiUyRmp0NXpZVkpZcHFUMmVyYW1MY0IwaFhTRnlRQUh3eUpsVDVZOVFfOF8xZHcuQ2xfck9xUHhSV3oxaG9KUzB6THdTT2t6ZGZha3FodERUb1l1WXVtYWE3cy5vRmFiYktXd0N6SndqdDBOX01EVWcyc1JrVmxBd0liaFJlNzI5TW03bFdRJTJGVlpIdWJ3QWtpT0ROOFk5WG9NSXNMT3RpMXRSeWhoVHVXYzdCazhTRjF1VSUzRkM3UXJXT2JIOXRicmJqXzJsM0l2dGFVQ19paGVidHppWk9MS2hweDN6WGclM0REVlE3eWtpNTNVS0d6c21YTnd0QnBWTHBFS2QxQ0FNR2RjT0RQclFObmxJJTI2MGJMRWExSEJSOWx3U0FMUDdxWUNPLW5pc0FscHBvdEx0eFZOZXZpSU9jayUzRElqZzd4U0wtQkhPRU9acFI2QUh5YzNXNk5OTk5VUzExeUwzQjVtTHI0R0ElMjZ1bkI4R0tqVUdFWWdSbTZrcHVDam1Jc3lYcF9hUHI3dGdWNkFEbUx1RnJnJTNEaEFTZGVjMng3TzRwWk44dXdYSzduNWw2MUpmc3JuWV94MENJb1ZHWVJYcyUyNm9xT2J2a3FOT1U3d0cwbzVuVk90dHRoeUtiZTQzMFFUcEtEaGtOS1JYRzglM0RzN0kxS3pIVGZLMXpZU095VFdDVHR5ZzlPZnQ4WXpwMWV4UUpUakttek5vJTI2TVpFVEJuS1Z5dUxrYTZFclhYNk9mS2F3cmlfVXd6VHVtVVJQUklxU1FUYyUzRFpoRzRtVk51WHVxd2pPQXg3UVFFSmFiU192eFhNRV9PMzBLeUl2Zl8weEElMjZEQUY2cEIlM0QyU3hQcmElMjZjNmFCN3AlM0QyU3hQcmElMjZ2dHNtYTAlM0Q4azNqX0clMjZVNDc3dWolM0QyU3hQcmElMjIlMkMlMjJjJTIyJTNBJTIyak5RU1ZZV1JmMjBDTjJtaTRQOVl0Y3hBOVFDbFlKQVVNVmVSTUJZdWZVcyUyMiUyQyUyMmQlMjIlM0ElMjJOQSUyMiUyQyUyMmUlMjIlM0ElMjJOQSUyMiUyQyUyMmYlMjIlM0FmYWxzZSUyQyUyMmclMjIlM0F0cnVlJTJDJTIyaCUyMiUzQXRydWUlMkMlMjJpJTIyJTNBJTVCJTIybG9jYXRpb24lMjIlNUQlMkMlMjJqJTIyJTNBJTVCJTVEJTJDJTIybiUyMiUzQTg3LjgyMDAwMDAxNTE5OTE4JTJDJTIydSUyMiUzQSUyMmFwcC5hcnRpYm90LmFpJTIyJTdEJTJDJTIyaCUyMiUzQSUyMmUxOGEzOTQ2NzRhMGYyZmI4NmY3JTIyJTdE",
        BODY_END
    );

    ns_end_transaction("X_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("index_10");
    ns_web_url ("index_10",
        "URL=https://www.cavisson.com/cavisson-systems-performance-intelligence-platform/?\cavisson.ico",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat;_gcl_au"
    );

    ns_end_transaction("index_10", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("index_11");
    ns_web_url ("index_11",
        "URL=https://api.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/chats/init/",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://app.artibot.ai",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"botVersionId":"12034483-9bfe-47a8-bbfa-ada6398deb1f","contactId":"deabc787-8299-4886-87d6-b75f495c9a4d","state":"Fi0xBJlSp6S0iaA90VbT2xrC/0H8hvGWrcZGr5BqDCY+dltTe0ZJQP8/U8XvaP246VITHWvWy89akZoYHIjGlQwYlKzbZJd9E7Xc4P78hAwLR0sBeCv/u9F0j0/Ps7OEsLMj9xylCSs=","timezone":"America/Chicago"}",
        BODY_END
    );

    ns_end_transaction("index_11", NS_AUTO_STATUS);
    ns_page_think_time(8.67);

    ns_start_transaction("index_4");
    ns_web_url ("index_4",
        "URL=http://www.macys.com/",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=http://www.macys.com/favicon.ico", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("index_4", NS_AUTO_STATUS);
    ns_page_think_time(5.482);

    ns_start_transaction("index_5");
    ns_web_url ("index_5",
        "URL=https://www.macys.com/",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.macys.com/favicon.ico", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("index_5", NS_AUTO_STATUS);
    ns_page_think_time(5.997);

}
