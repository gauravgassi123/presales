/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: cavisson
    Date of recording: 03/24/2022 03:31:22
    Flow details:
    Build details: 4.7.0 (build# 147)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("index");
    ns_web_url ("index",
        "URL=https://66.220.31.137/nsecom/",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            //"URL=http://certificates.godaddy.com/repository/gdig2.crt", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://66.220.31.137/nsecom/images/logo.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("index", NS_AUTO_STATUS);
    ns_page_think_time(2.243);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("home");
    ns_web_url ("home",
        "URL=https://66.220.31.137/nsecom/home",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=https://66.220.31.137/nsecom/images/nsecomlogo.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("home", NS_AUTO_STATUS);
    ns_page_think_time(6.429);

    //Page Auto split for Button 'search' Clicked by User
    ns_start_transaction("searchProduct");
    ns_web_url ("searchProduct",
        "URL=https://66.220.31.137/nsecom/searchProduct?keyword={Product}",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=https://66.220.31.137/nsecom/images/shirt1.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://66.220.31.137/nsecom/images/shirt2.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://66.220.31.137/nsecom/images/shirt3.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://66.220.31.137/nsecom/images/shirt4.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://66.220.31.137/nsecom/images/shirt5.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://66.220.31.137/nsecom/images/shirt6.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://66.220.31.137/nsecom/images/shirt7.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("searchProduct", NS_AUTO_STATUS);
    ns_page_think_time(1.967);

    //Page Auto split for Image Link '' Clicked by User
    ns_start_transaction("productPage");
    ns_web_url ("productPage",
        "URL=https://66.220.31.137/nsecom/productPage?productid=Shirt001&upc=ShirtUPC11111&prodImage=shirt1.png&prodPrice=19.99",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
    );

    ns_end_transaction("productPage", NS_AUTO_STATUS);
    ns_page_think_time(4.07);

    //Page Auto split for Button 'Add to cart' Clicked by User
    ns_start_transaction("addToBag");
    ns_web_url ("addToBag",
        "URL=https://66.220.31.137/nsecom/addToBag?size=2&productid=NA&price=19.99&upc=ShirtUPC11111&prodImage=shirt1.png&quantity=1",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
    );

    ns_end_transaction("addToBag", NS_AUTO_STATUS);
    ns_page_think_time(2.052);

    //Page Auto split for Button 'Continue Checkout' Clicked by User
    ns_start_transaction("shippingAddress");
    ns_web_url ("shippingAddress",
        "URL=https://66.220.31.137/nsecom/shippingAddress?productid=NA&upc=ShirtUPC11111&size=2&color=blue&quantity=1&price=19.99&status=true&cartId=Cart20220324033006407",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=CARTID",
        INLINE_URLS,
            "URL=https://66.220.31.137/nsecom/images/logo.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=CARTID", END_INLINE
    );

    ns_end_transaction("shippingAddress", NS_AUTO_STATUS);
    ns_page_think_time(23.157);

    //Page Auto split for Button 'Place Order' Clicked by User
    ns_start_transaction("checkOut");
    ns_web_url ("checkOut",
        "URL=https://66.220.31.137/nsecom/checkOut?name=cavisson&address=noida&city=noida&PostalCode=12345&country=US&productid=NA&upc=ShirtUPC11111&size=2&color=blue&quantity=1&price=19.99&status=true&cartId=Cart20220324033006407",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=CARTID",
        INLINE_URLS,
            "URL=https://66.220.31.137/nsecom/images/nsecomlogo.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=CARTID", END_INLINE
    );

    ns_end_transaction("checkOut", NS_AUTO_STATUS);
    ns_page_think_time(2.659);

}
