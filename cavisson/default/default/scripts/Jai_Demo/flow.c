/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: 
    Date of recording: 08/31/2022 07:03:50
    Flow details:
    Build details: 
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("Main_Page");
    ns_web_url ("Main_Page",
        "URL=https://wiki.lntdccs.com/lntwiki",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "REDIRECT=YES",
        "LOCATION=https://wiki.lntdccs.com/lntwiki/",
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://wiki.lntdccs.com/lntwiki/index.php/Main_Page", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/index.php/Main_Page", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=skins.timeless&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=site.styles&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/e/e5/L%26T.png/300px-L%26T.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/user-grey.svg?9781b", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/arrow-down-grey.svg?46d67", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/search-ltr.svg?402b1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/cat.svg?31aa0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/page-grey.svg?a26bc", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/brackets-grey.svg?c043f", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/talk-grey.svg?b4d04", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/clock-grey.svg?48197", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/user-grey.svg?9781b", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/arrow-down-grey.svg?46d67", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/search-ltr.svg?402b1", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/cat.svg?31aa0", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/eyeball-grey.svg?f17bd", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.skinning/images/external-ltr.svg?59558", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/page-grey.svg?a26bc", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&skin=timeless", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery%2Csite%7Cjquery.client%2Ccookie%7Cmediawiki.String%2CTitle%2Capi%2Cbase%2Ccldr%2Ccookie%2CjqueryMsg%2Clanguage%2Ctoc%2Cutil%7Cmediawiki.libs.pluralruleparser%7Cmediawiki.page.ready%7Cskins.timeless.js&skin=timeless&version=1ptgd", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/favicon.ico", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("Main_Page", NS_AUTO_STATUS);
    ns_page_think_time(23.231);

    //Page Auto split for Link 'image' Clicked by User
    ns_start_transaction("index_php");
    ns_web_url ("index_php",
        "URL=https://wiki.lntdccs.com/lntwiki/index.php?title=Special:UserLogin&returnto=Main+Page",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("index_php", NS_AUTO_STATUS);
    ns_page_think_time(0.024);

    //Page Auto split for Link 'image' Clicked by User
    ns_start_transaction("load_php");
    ns_web_url ("load_php",
        "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=mediawiki.htmlform.styles%7Cmediawiki.special.userlogin.common.styles%7Cmediawiki.special.userlogin.login.styles%7Cmediawiki.ui%7Cmediawiki.ui.button%2Ccheckbox%2Cinput%2Cradio%7Cskins.timeless&only=styles&skin=timeless",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:style",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=lntwiki_session",
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&safemode=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.special.userlogin.login.styles/images/glyph-people-large.png?707d6", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&safemode=1&skin=timeless", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.special.userlogin.login.styles/images/glyph-people-large.png?707d6", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery%7Cjquery.lengthLimit%7Cmediawiki.htmlform&skin=timeless&version=1yb9e", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session", END_INLINE
    );

    ns_end_transaction("load_php", NS_AUTO_STATUS);
    ns_page_think_time(14.896);

    //Page Auto split for Button 'Log in' Clicked by User
    ns_start_transaction("Main_Page_2");
    ns_web_url ("Main_Page_2",
        "URL=https://wiki.lntdccs.com/lntwiki/index.php?title=Special:UserLogin&returnto=Main+Page",
        "METHOD=POST",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Origin:https://wiki.lntdccs.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=lntwiki_session",
        "REDIRECT=YES",
        "LOCATION=https://wiki.lntdccs.com/lntwiki/index.php/Main_Page",
        BODY_BEGIN,
            "wpName=Dummy&wpPassword=Test%40123&wploginattempt=Log+in&wpEditToken=%2B%5C&title=Special%3AUserLogin&authAction=login&force=&wpLoginToken=25231ba5b98b1e19c410a84e7f7cfd4a630f0689%2B%5C",
        BODY_END,
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/index.php/Main_Page", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=skins.timeless&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=site.styles&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/page-grey.svg?a26bc", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/talk-grey.svg?b4d04", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/star.svg?2328e", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery%2Csite%7Cmediawiki.page.watch.ajax&skin=timeless&version=n1xai", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/e/e5/L%26T.png/300px-L%26T.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/eyeball-grey.svg?f17bd", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/brackets-grey.svg?c043f", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/clock-grey.svg?48197", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.skinning/images/external-ltr.svg?59558", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery%2Csite%7Cmediawiki.page.watch.ajax&skin=timeless&version=n1xai", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/index.php/Main_Page#Delivery", END_INLINE
    );

    ns_end_transaction("Main_Page_2", NS_AUTO_STATUS);
    ns_page_think_time(129.525);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("Delivery");
    ns_web_url ("Delivery",
        "URL=https://wiki.lntdccs.com/lntwiki/index.php/Delivery",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName",
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=skins.timeless&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=site.styles&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery&skin=timeless&version=bh3lo", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/index.php/Delivery#SOC", END_INLINE
    );

    ns_end_transaction("Delivery", NS_AUTO_STATUS);
    ns_page_think_time(8.204);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("Category_SOC");
    ns_web_url ("Category_SOC",
        "URL=https://wiki.lntdccs.com/lntwiki/index.php/Category:SOC",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName",
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=mediawiki.action.view.categoryPage.styles%7Cmediawiki.helplink%7Cskins.timeless&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=site.styles&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery&skin=timeless&version=bh3lo", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.helplink/images/helpNotice.svg?46d34", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.helplink/images/helpNotice.svg?46d34", END_INLINE
    );

    ns_end_transaction("Category_SOC", NS_AUTO_STATUS);
    ns_page_think_time(14.048);

    //Page Auto split for Link 'image' Clicked by User
    ns_start_transaction("load_php_2");
    ns_web_url ("load_php_2",
        "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=mediawiki.notification%2CvisibleTimeout&skin=timeless&version=1ph72",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:script",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName"
    );

    ns_end_transaction("load_php_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("api_php");
    ns_web_url ("api_php",
        "URL=https://wiki.lntdccs.com/lntwiki/api.php",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-Requested-With:XMLHttpRequest",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://wiki.lntdccs.com",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName",
        BODY_BEGIN,
            "action=logout&format=json&token=522f317079552b938039e8c0c84941b7630f0730%2B%5C",
        BODY_END
    );

    ns_end_transaction("api_php", NS_AUTO_STATUS);
    ns_page_think_time(0.012);

    ns_start_transaction("index_php_2");
    ns_web_url ("index_php_2",
        "URL=https://wiki.lntdccs.com/lntwiki/index.php?title=Special:UserLogout&returnto=Category%3ASOC",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=lntwikiUserName;UseDC;UseCDNCache",
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=skins.timeless&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=site.styles&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery&skin=timeless&version=bh3lo", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwikiUserName;UseDC;UseCDNCache", END_INLINE
    );

    ns_end_transaction("index_php_2", NS_AUTO_STATUS);
    ns_page_think_time(5.199);

}
