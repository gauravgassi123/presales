/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: Apoorva
    Date of recording: 08/29/2022 01:02:27
    Flow details:
    Build details: 4.8.0 (build# 129)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("index");
    ns_web_url ("index",
        "URL=http://www.google.com/",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "REDIRECT=YES",
        "LOCATION=https://www.google.com/?gws_rd=ssl",
        INLINE_URLS,
            "URL=https://www.google.com/?gws_rd=ssl", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.google.com/xjs/_/js/k=xjs.s.en_GB.0RwjdeQGgkw.O/am=AIACgQDwAgAIAEBmAAEAAAAAAAAAAIYAIPGUEwAADBACAjkJAAAAACRMiAAAYAAgBDAQhAAAAAA-Mm8AAv4MAAw04QIAAAAAAAAACOASBAM3SBQEAAIAAAAAACCspq45AAVB/d=1/ed=1/dg=2/br=1/rs=ACT90oGHS51U1Lr1d4VDpqSB1aT3JzqfiQ/m=cdos,dpf,hsm,jsa,d,csi", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/images/searchbox/desktop_searchbox_sprites318_hr.webp", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.gstatic.com/og/_/js/k=og.qtm.en_US.-MMKP3uG9VU.O/rt=j/m=qabr,q_d,qcwid,qapid,qald/exm=qaaw,qadd,qaid,qein,qhaw,qhbr,qhch,qhga,qhid,qhin,qhpr/d=1/ed=1/rs=AA2YrTuS6iZfrsnE7GApv0RWeBgl21VxSA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/og/_/ss/k=og.qtm.tM-BeBGBME0.L.W.O/m=qcwid/excm=qaaw,qadd,qaid,qein,qhaw,qhbr,qhch,qhga,qhid,qhin,qhpr/d=1/ed=1/ct=zgms/rs=AA2YrTtmhrLYVlm7jdu3TI1ROywA3arJnA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("index", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_2");
    ns_web_url ("search_2",
        "URL=https://www.google.com/complete/search?q&cp=0&client=gws-wiz&xssi=t&hl=en-IN&authuser=0&psi=amsMY4-EC77f1sQP86SskAk.1661758317303&nolsbt=1&dpr=1.25",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID",
        INLINE_URLS,
            "URL=https://www.google.com/xjs/_/js/k=xjs.s.en_GB.0RwjdeQGgkw.O/ck=xjs.s.2-FJbM4yaGs.L.W.O/am=AIACgQDwAgAIAEBmAAEAAAAAAAAAAIYAIPGUEwAADBACAjkJAAAAACRMiAAAYAAgBDAQhAAAAAA-Mm8AAv4MAAw04QIAAAAAAAAACOASBAM3SBQEAAIAAAAAACCspq45AAVB/d=1/exm=cdos,csi,d,dpf,hsm,jsa/ed=1/dg=2/br=1/rs=ACT90oFexCTyJRXuGWZkPm7BqaXf3uP2Ag/ee=Pjplud:PoEs9b;QGR0gd:Mlhmy;uY49fb:COQbmf;EVNhjf:pw70Gc;oUlnpc:RagDlc;dtl0hd:lLQWFe;sTsDMc:kHVSUb;dIoSBb:ZgGg9b;pXdRYb:JKoKVe;wR5FRb:TtcOte;yGxLoc:FmAr0c;g8nkx:U4MzKc;KpRAue:Tia57b;daB6be:lMxGPd;Fmv9Nc:O1Tzwc;hK67qb:QWEO5b;jVtPve:wQ95P;BMxAGc:E5bFse;R4IIIb:QWfeKf;xbe2wc:wbTLEd;wQlYve:aLUfP;G6wU6e:hezEbd;fAO5td:yzxsuf;SJsSc:H1GVub;SMDL4c:fTfGO;oSUNyd:fTfGO;zxnPse:GkRiKb;oGtAuc:sOXFj;zOsCQe:Ko78Df;WCEKNd:I46Hvd;NPKaK:PVlQOd;LBgRLc:XVMNvd;kbAm9d:MkHyGd;UyG7Kb:wQd0G;LsNahb:ucGLNb;rQSrae:C6D5Fc;kCQyJ:ueyPK;EABSZ:MXZt9d;qavrXe:zQzcXe;TxfV6d:YORN0b;UDrY1c:eps46d;GleZL:J1A7Od;nKl0s:xxrckd;JXS8fb:Qj0suc;qaS3gd:yiLg6e;NSEoX:lazG7b;w3bZCb:ZPGaIb;VGRfx:VFqbr;aAJE9c:WHW6Ef;imqimf:jKGL2e;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;F9mqte:UoRcbe;SLtqO:Kh1xYe;tosKvd:ZCqP3;WDGyFe:jcVOxd;VxQ32b:k0XsBb;DULqB:RKfG5c;Np8Qkd:Dpx6qc;aZ61od:arTwJ;cFTWae:gT8qnd;gaub4:TN6bMe;DpcR3d:zL72xf;hjRo6e:F62sG;BjwMce:cXX2Wb;CUcugf:BIaADc;qddgKe:x4FYXe;eBAeSb:Ck63tb;lkq0A:Z0MWEf;eHDfl:ofjVkb;SNUn3:x8cHvb;LEikZe:byfTOb,lsjVmc;io8t5d:sgY6Zb;j7137d:KG2eXe;Oj465e:KG2eXe;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;nAFL3:s39S4;iFQyKf:QIhFr;whEZac:F4AmNb;vfVwPd:OXTqFb;w9w86d:dt4g2b;KQzWid:mB4wNe;pNsl2d:j9Yuyc;Nyt6ic:jn2sGd/m=DhPYme,EkevXb,GU4Gab,NzU6V,aa,abd,async,dvl,fKZehd,mu,pHXghd,sb_wiz,sf,sonic,spch?xjs=s1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/client_204?&atyp=i&biw=1536&bih=714&dpr=1.25&ei=amsMY4-EC77f1sQP86SskAk", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID", END_INLINE
    );

    ns_end_transaction("search_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204");
    ns_web_url ("gen_204",
        "URL=https://www.google.com/gen_204?s=webhp&t=aft&atyp=csi&ei=amsMY4-EC77f1sQP86SskAk&rt=wsrt.1203,aft.615,afti.615,prt.476&wh=714&imn=1&ima=1&imad=0&aftp=714&bl=IuYP",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID",
        INLINE_URLS,
            "URL=https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.IK5OmUURd2E.O/m=gapi_iframes,googleapis_client/rt=j/sv=1/d=1/ed=1/rs=AHpOoo932JinkSJHK92WgVjIV-Jwwyu3Rw/cb=gapi.loaded_0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://ogs.google.com/widget/callout?prid=19028915&pgid=19027681&puid=3e9e72cbc9a83b93&cce=1&dc=1&origin=https%3A%2F%2Fwww.google.com&cn=callout&pid=1&spid=538&hl=en", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/xjs/_/js/k=xjs.s.en_GB.0RwjdeQGgkw.O/ck=xjs.s.2-FJbM4yaGs.L.W.O/am=AIACgQDwAgAIAEBmAAEAAAAAAAAAAIYAIPGUEwAADBACAjkJAAAAACRMiAAAYAAgBDAQhAAAAAA-Mm8AAv4MAAw04QIAAAAAAAAACOASBAM3SBQEAAIAAAAAACCspq45AAVB/d=1/exm=DhPYme,EkevXb,GU4Gab,NzU6V,aa,abd,async,cdos,csi,d,dpf,dvl,fKZehd,hsm,jsa,mu,pHXghd,sb_wiz,sf,sonic,spch/ed=1/dg=2/br=1/rs=ACT90oFexCTyJRXuGWZkPm7BqaXf3uP2Ag/ee=Pjplud:PoEs9b;QGR0gd:Mlhmy;uY49fb:COQbmf;EVNhjf:pw70Gc;oUlnpc:RagDlc;dtl0hd:lLQWFe;sTsDMc:kHVSUb;dIoSBb:ZgGg9b;pXdRYb:JKoKVe;wR5FRb:TtcOte;yGxLoc:FmAr0c;g8nkx:U4MzKc;KpRAue:Tia57b;daB6be:lMxGPd;Fmv9Nc:O1Tzwc;hK67qb:QWEO5b;jVtPve:wQ95P;BMxAGc:E5bFse;R4IIIb:QWfeKf;xbe2wc:wbTLEd;wQlYve:aLUfP;G6wU6e:hezEbd;fAO5td:yzxsuf;SJsSc:H1GVub;SMDL4c:fTfGO;oSUNyd:fTfGO;zxnPse:GkRiKb;oGtAuc:sOXFj;zOsCQe:Ko78Df;WCEKNd:I46Hvd;NPKaK:PVlQOd;LBgRLc:XVMNvd;kbAm9d:MkHyGd;UyG7Kb:wQd0G;LsNahb:ucGLNb;rQSrae:C6D5Fc;kCQyJ:ueyPK;EABSZ:MXZt9d;qavrXe:zQzcXe;TxfV6d:YORN0b;UDrY1c:eps46d;GleZL:J1A7Od;nKl0s:xxrckd;JXS8fb:Qj0suc;qaS3gd:yiLg6e;NSEoX:lazG7b;w3bZCb:ZPGaIb;VGRfx:VFqbr;aAJE9c:WHW6Ef;imqimf:jKGL2e;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;F9mqte:UoRcbe;SLtqO:Kh1xYe;tosKvd:ZCqP3;WDGyFe:jcVOxd;VxQ32b:k0XsBb;DULqB:RKfG5c;Np8Qkd:Dpx6qc;aZ61od:arTwJ;cFTWae:gT8qnd;gaub4:TN6bMe;DpcR3d:zL72xf;hjRo6e:F62sG;BjwMce:cXX2Wb;CUcugf:BIaADc;qddgKe:x4FYXe;eBAeSb:Ck63tb;lkq0A:Z0MWEf;eHDfl:ofjVkb;SNUn3:x8cHvb;LEikZe:byfTOb,lsjVmc;io8t5d:sgY6Zb;j7137d:KG2eXe;Oj465e:KG2eXe;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;nAFL3:s39S4;iFQyKf:QIhFr;whEZac:F4AmNb;vfVwPd:OXTqFb;w9w86d:dt4g2b;KQzWid:mB4wNe;pNsl2d:j9Yuyc;Nyt6ic:jn2sGd/m=CnSW2d,DPreE,HGv0mf,WlNQGd,fXO0xe,kQvlef,nabPbb?xjs=s2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID", END_INLINE
    );

    ns_end_transaction("gen_204", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_2");
    ns_web_url ("gen_204_2",
        "URL=https://www.google.com/gen_204?atyp=i&ei=amsMY4-EC77f1sQP86SskAk&dt19=2&zx=1661758317403",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID",
        INLINE_URLS,
            "URL=https://www.google.com/client_204?cs=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/xjs/_/js/md=1/k=xjs.s.en_GB.0RwjdeQGgkw.O/am=AIACgQDwAgAIAEBmAAEAAAAAAAAAAIYAIPGUEwAADBACAjkJAAAAACRMiAAAYAAgBDAQhAAAAAA-Mm8AAv4MAAw04QIAAAAAAAAACOASBAM3SBQEAAIAAAAAACCspq45AAVB/rs=ACT90oGHS51U1Lr1d4VDpqSB1aT3JzqfiQ", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/xjs/_/js/k=xjs.s.en_GB.0RwjdeQGgkw.O/ck=xjs.s.2-FJbM4yaGs.L.W.O/am=AIACgQDwAgAIAEBmAAEAAAAAAAAAAIYAIPGUEwAADBACAjkJAAAAACRMiAAAYAAgBDAQhAAAAAA-Mm8AAv4MAAw04QIAAAAAAAAACOASBAM3SBQEAAIAAAAAACCspq45AAVB/d=1/exm=CnSW2d,DPreE,DhPYme,EkevXb,GU4Gab,HGv0mf,NzU6V,WlNQGd,aa,abd,async,cdos,csi,d,dpf,dvl,fKZehd,fXO0xe,hsm,jsa,kQvlef,mu,nabPbb,pHXghd,sb_wiz,sf,sonic,spch/ed=1/dg=2/br=1/rs=ACT90oFexCTyJRXuGWZkPm7BqaXf3uP2Ag/ee=Pjplud:PoEs9b;QGR0gd:Mlhmy;uY49fb:COQbmf;EVNhjf:pw70Gc;oUlnpc:RagDlc;dtl0hd:lLQWFe;sTsDMc:kHVSUb;dIoSBb:ZgGg9b;pXdRYb:JKoKVe;wR5FRb:TtcOte;yGxLoc:FmAr0c;g8nkx:U4MzKc;KpRAue:Tia57b;daB6be:lMxGPd;Fmv9Nc:O1Tzwc;hK67qb:QWEO5b;jVtPve:wQ95P;BMxAGc:E5bFse;R4IIIb:QWfeKf;xbe2wc:wbTLEd;wQlYve:aLUfP;G6wU6e:hezEbd;fAO5td:yzxsuf;SJsSc:H1GVub;SMDL4c:fTfGO;oSUNyd:fTfGO;zxnPse:GkRiKb;oGtAuc:sOXFj;zOsCQe:Ko78Df;WCEKNd:I46Hvd;NPKaK:PVlQOd;LBgRLc:XVMNvd;kbAm9d:MkHyGd;UyG7Kb:wQd0G;LsNahb:ucGLNb;rQSrae:C6D5Fc;kCQyJ:ueyPK;EABSZ:MXZt9d;qavrXe:zQzcXe;TxfV6d:YORN0b;UDrY1c:eps46d;GleZL:J1A7Od;nKl0s:xxrckd;JXS8fb:Qj0suc;qaS3gd:yiLg6e;NSEoX:lazG7b;w3bZCb:ZPGaIb;VGRfx:VFqbr;aAJE9c:WHW6Ef;imqimf:jKGL2e;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;F9mqte:UoRcbe;SLtqO:Kh1xYe;tosKvd:ZCqP3;WDGyFe:jcVOxd;VxQ32b:k0XsBb;DULqB:RKfG5c;Np8Qkd:Dpx6qc;aZ61od:arTwJ;cFTWae:gT8qnd;gaub4:TN6bMe;DpcR3d:zL72xf;hjRo6e:F62sG;BjwMce:cXX2Wb;CUcugf:BIaADc;qddgKe:x4FYXe;eBAeSb:Ck63tb;lkq0A:Z0MWEf;eHDfl:ofjVkb;SNUn3:x8cHvb;LEikZe:byfTOb,lsjVmc;io8t5d:sgY6Zb;j7137d:KG2eXe;Oj465e:KG2eXe;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;nAFL3:s39S4;iFQyKf:QIhFr;whEZac:F4AmNb;vfVwPd:OXTqFb;w9w86d:dt4g2b;KQzWid:mB4wNe;pNsl2d:j9Yuyc;Nyt6ic:jn2sGd/m=aLUfP?xjs=s2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.gstatic.com/_/mss/boq-one-google/_/js/k=boq-one-google.OneGoogleWidgetUi.en.4W0FOKDKC3M.es5.O/am=uAEAQA/d=1/excm=_b,_r,_tp,calloutview/ed=1/dg=0/wt=2/rs=AM-SdHtimnV-YbsMf4O1-Tvb1ZZJj4s7Sw/m=_b,_tp,_r", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.google.com/images/hpp/gsa_super_g-64.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://fonts.gstatic.com/s/googlesans/v14/4UabrENHsxJlGDuGo1OIlLU94YtzCwY.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://ogs.google.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxK.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://ogs.google.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("gen_204_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("log");
    ns_web_url ("log",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en-IN"]],1655,[["1661758317418",null,[],null,null,null,null,"[[[\"/client_streamz/location/location_prompt/prompt_decisions_count\",null,[\"decision\",\"browser\"],[[[[\"NO_PROMPT_NOT_GRANTED\"],[\"UNKNOWN\"]],[1]]],null,[]]]]",null,null,null,null,null,null,-19800,null,null,null,null,[],1,null,null,null,null,null,[]]],"1661758317418",[]]",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.google.com/gen_204?use_corp=on&atyp=i&zx=1661758317731&ogsr=1&ei=amsMY8yXDPCo1sQPqpKm6A4&ct=7&cad=i&id=19028915&loc=webhp&prid=538&ogd=co.in&ogprm=up&ap=1&vis=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID;OGPC", END_INLINE,
            "URL=https://www.gstatic.com/_/mss/boq-one-google/_/js/k=boq-one-google.OneGoogleWidgetUi.en.4W0FOKDKC3M.es5.O/ck=boq-one-google.OneGoogleWidgetUi.jqRUnq2L2dw.L.B1.O/am=uAEAQA/d=1/exm=_b,_r,_tp/excm=_b,_r,_tp,calloutview/ed=1/wt=2/rs=AM-SdHvNk7RosR5N1ano8TQRpsQcmPFSwA/ee=cEt90b:ws9Tlc;QGR0gd:Mlhmy;uY49fb:COQbmf;yxTchf:KUM7Z;qddgKe:xQtZb;dIoSBb:SpsfSb;EmZ2Bf:zr1jrb;zxnPse:GkRiKb;EVNhjf:pw70Gc;NSEoX:lazG7b;oGtAuc:sOXFj;eBAeSb:zbML3c;Pjplud:EEDORb;io8t5d:yDVVkb;j7137d:KG2eXe;Oj465e:KG2eXe;ul9GGd:VDovNc;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;NPKaK:SdcwHb;pXdRYb:MdUzUe;nAFL3:s39S4;iFQyKf:QIhFr;SNUn3:ZwDk9d;LBgRLc:SdcwHb;wR5FRb:O1Gjze/m=n73qwf,ws9Tlc,e5qFLc,GkRiKb,IZT63,UUJqVe,O1Gjze,byfTOb,lsjVmc,xUdipf,OTA3Ae,COQbmf,fKUV3e,aurFic,U0aPgd,ZwDk9d,V3dDOb,mI3LFb,O6y8ed,PrPYRd,MpJwZc,LEikZe,NwH0H,OmgaI,lazG7b,Mpq4Ee,XVMNvd,L1AAkb,KUM7Z,Mlhmy,s39S4,lwddkf,gychg,w9hDv,EEDORb,RMhBfe,SdcwHb,aW3pY,pw70Gc,EFQ78c,Ulmmrd,ZfAoz,mdR7q,xQtZb,JNoxi,kWgXee,MI6k7c,kjKdXe,BVgquf,QIhFr,ovKuLd,hKSk3e,yDVVkb,hc6Ubd,SpsfSb,KG2eXe,MdUzUe,VwDzFe,zbML3c,zr1jrb,lsPsHb,A7fCU,hnN99e,Uas9Hd,yYB61,pjICDe", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/_/mss/boq-one-google/_/js/k=boq-one-google.OneGoogleWidgetUi.en.4W0FOKDKC3M.es5.O/ck=boq-one-google.OneGoogleWidgetUi.jqRUnq2L2dw.L.B1.O/am=uAEAQA/d=1/exm=A7fCU,BVgquf,COQbmf,EEDORb,EFQ78c,GkRiKb,IZT63,JNoxi,KG2eXe,KUM7Z,L1AAkb,LEikZe,MI6k7c,MdUzUe,Mlhmy,MpJwZc,Mpq4Ee,NwH0H,O1Gjze,O6y8ed,OTA3Ae,OmgaI,PrPYRd,QIhFr,RMhBfe,SdcwHb,SpsfSb,U0aPgd,UUJqVe,Uas9Hd,Ulmmrd,V3dDOb,VwDzFe,XVMNvd,ZfAoz,ZwDk9d,_b,_r,_tp,aW3pY,aurFic,byfTOb,e5qFLc,fKUV3e,gychg,hKSk3e,hc6Ubd,hnN99e,kWgXee,kjKdXe,lazG7b,lsPsHb,lsjVmc,lwddkf,mI3LFb,mdR7q,n73qwf,ovKuLd,pjICDe,pw70Gc,s39S4,w9hDv,ws9Tlc,xQtZb,xUdipf,yDVVkb,yYB61,zbML3c,zr1jrb/excm=_b,_r,_tp,calloutview/ed=1/wt=2/rs=AM-SdHvNk7RosR5N1ano8TQRpsQcmPFSwA/ee=cEt90b:ws9Tlc;QGR0gd:Mlhmy;uY49fb:COQbmf;yxTchf:KUM7Z;qddgKe:xQtZb;dIoSBb:SpsfSb;EmZ2Bf:zr1jrb;zxnPse:GkRiKb;EVNhjf:pw70Gc;NSEoX:lazG7b;oGtAuc:sOXFj;eBAeSb:zbML3c;Pjplud:EEDORb;io8t5d:yDVVkb;j7137d:KG2eXe;Oj465e:KG2eXe;ul9GGd:VDovNc;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;NPKaK:SdcwHb;pXdRYb:MdUzUe;nAFL3:s39S4;iFQyKf:QIhFr;SNUn3:ZwDk9d;LBgRLc:SdcwHb;wR5FRb:O1Gjze/m=bm51tf", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("log", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_3");
    ns_web_url ("gen_204_3",
        "URL=https://www.google.com/gen_204?atyp=csi&ei=amsMY4-EC77f1sQP86SskAk&s=webhp&t=all&bl=IuYP&wh=714&imn=1&ima=1&imad=0&aftp=714&adh=&ime=1&imex=1&imeh=0&imea=0&imeb=0&imel=0&scp=0&net=dl.9500,ect.4g,rtt.50&mem=ujhs.17,tjhs.23,jhsl.1099,dm.8&sto=&sys=hc.4&rt=aft.615,prt.476,xjses.496,xjsee.570,xjs.570,dcl.572,afti.615,aftqf.617,ol.1300,lcp.637,fcp.524,wsrt.1203,cst.107,dnst.0,rqst.1014,rspt.467,sslt.101,rqstt.656,unt.544,cstt.549,dit.1681&zx=1661758318014",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC",
        INLINE_URLS,
            "URL=https://adservice.google.com/adsid/google/ui", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID;OGPC", END_INLINE,
            "URL=https://www.google.com/gen_204?atyp=i&ct=psnt&cad=&nt=navigate&ei=amsMY4-EC77f1sQP86SskAk&zx=1661758318018", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID;OGPC", END_INLINE,
            "URL=https://www.gstatic.com/_/mss/boq-one-google/_/js/k=boq-one-google.OneGoogleWidgetUi.en.4W0FOKDKC3M.es5.O/ck=boq-one-google.OneGoogleWidgetUi.jqRUnq2L2dw.L.B1.O/am=uAEAQA/d=1/exm=A7fCU,BVgquf,COQbmf,EEDORb,EFQ78c,GkRiKb,IZT63,JNoxi,KG2eXe,KUM7Z,L1AAkb,LEikZe,MI6k7c,MdUzUe,Mlhmy,MpJwZc,Mpq4Ee,NwH0H,O1Gjze,O6y8ed,OTA3Ae,OmgaI,PrPYRd,QIhFr,RMhBfe,SdcwHb,SpsfSb,U0aPgd,UUJqVe,Uas9Hd,Ulmmrd,V3dDOb,VwDzFe,XVMNvd,ZfAoz,ZwDk9d,_b,_r,_tp,aW3pY,aurFic,bm51tf,byfTOb,e5qFLc,fKUV3e,gychg,hKSk3e,hc6Ubd,hnN99e,kWgXee,kjKdXe,lazG7b,lsPsHb,lsjVmc,lwddkf,mI3LFb,mdR7q,n73qwf,ovKuLd,pjICDe,pw70Gc,s39S4,w9hDv,ws9Tlc,xQtZb,xUdipf,yDVVkb,yYB61,zbML3c,zr1jrb/excm=_b,_r,_tp,calloutview/ed=1/wt=2/rs=AM-SdHvNk7RosR5N1ano8TQRpsQcmPFSwA/ee=cEt90b:ws9Tlc;QGR0gd:Mlhmy;uY49fb:COQbmf;yxTchf:KUM7Z;qddgKe:xQtZb;dIoSBb:SpsfSb;EmZ2Bf:zr1jrb;zxnPse:GkRiKb;EVNhjf:pw70Gc;NSEoX:lazG7b;oGtAuc:sOXFj;eBAeSb:zbML3c;Pjplud:EEDORb;io8t5d:yDVVkb;j7137d:KG2eXe;Oj465e:KG2eXe;ul9GGd:VDovNc;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;NPKaK:SdcwHb;pXdRYb:MdUzUe;nAFL3:s39S4;iFQyKf:QIhFr;SNUn3:ZwDk9d;LBgRLc:SdcwHb;wR5FRb:O1Gjze/m=Wt6vjf,hhhU8,FCpbqb,WhJNk", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("gen_204_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("manifest");
    ns_web_url ("manifest",
        "URL=https://www.google.com/manifest?pwa=webhp",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC"
    );

    ns_end_transaction("manifest", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("log_2");
    ns_web_url ("log_2",
        "URL=https://play.google.com/log?format=json&hasfast=true",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Origin:https://ogs.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en",null,"boq_onegooglehttpserver_20220822.03_p0"]],241,[["1661758318044",null,[],null,null,null,null,"[1661758318040,[[\"2397168675742140944\",null,[[227,1]]],[\"16147638372540442232\",null,[[249,1]]],[\"14719340685975485085\",null,[[523,1]]],[\"17077408715954654437\",null,[[629,1]]],[\"3318688667027929436\",null,[[232,1]]],[\"5790177495296899286\",null,[[4,1]]],[\"16829267986558572790\",null,[[97,1]]],[\"16339156775003354937\",null,[[112,1]]],[\"749851692583976763\",null,[[4,1]]],[\"15419336178855610526\",null,[[215,1]]],[\"17276521865292187132\",null,[[0,1]]],[\"8257051839445688306\",null,[[628,1]]],[\"7792735449360349632\",null,[[628,1]]],[\"12542193546769209995\",null,[[553,1]]],[\"16254156456118481799\",null,[[553,1]]],[\"13622174389243279923\",null,[[568,1]]],[\"7094487270460551484\",null,[[6,1]]],[\"12563104964214410683\",null,[[624,1]]],[\"15605813632677093659\",null,[[153,1]]],[\"17914751415692637656\",null,[[3,1]]],[\"9797767207516844257\",null,[[1,1]]],[\"4553553160178503526\",null,[[311,1]]],[\"15768337714740149157\",null,[[311,1]]],[\"7099598553576769501\",null,[[0,1]]],[\"14906952326733574741\",null,[[2,1]]],[\"4891744519482609478\",null,[[144,1]]],[\"14307859671070593733\",null,[[141,1]]],[\"7494582641517049914\",null,[[0,1]]],[\"6667106912793420619\",null,[[1,1]]],[\"8147743178319688099\",null,[[311,1]]],[\"13378126313938116970\",null,[[311,1]]],[\"8519598536373642887\",null,[[0,1]]],[\"10118692516388306266\",null,[[0,1]]],[\"6342145065879578001\",null,[[3,1]]],[\"13596961294000664596\",null,[[400,1]]],[\"2107494750385856652\",null,[[109,1]]],[\"1309831198388189068\",null,[[3,1]]],[\"522022639063469804\",null,[[133,1]]],[\"4950535922500196698\",null,[[1048,1]]],[\"1757184925777806825\",null,[[22,1]]],[\"3079121564595244695\",null,[[16,1]]],[\"10652791942255425261\",null,[[7387,1]]],[\"4132870161583308123\",null,[[111,1]]]],null,null,\"[1,\\\"HgvS1c_PL\\\"]\"]",null,null,null,89785052,null,null,-19800,[null,[],null,"[[],[],[1763433,1772879,45814370,47977019],[]]"],null,null,null,[],1,null,null,null,null,null,[]]],"1661758318044",[]]",
        BODY_END
    );

    ns_end_transaction("log_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("log_3");
    ns_web_url ("log_3",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Origin:https://ogs.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en",null,"boq_onegooglehttpserver_20220822.03_p0"]],729,[["1661758317996",null,[],null,null,null,null,"[[[70881,null,[],null],538,[]]]",null,null,null,null,null,null,-19800,null,null,null,null,[],1,null,null,null,null,null,[]]],"1661758317997",[]]",
        BODY_END
    );

    ns_end_transaction("log_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("log_4");
    ns_web_url ("log_4",
        "URL=https://play.google.com/log?format=json&hasfast=true",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en-IN"]],373,[["1661758317372",null,[],null,null,null,null,"[108,40400,538,1,\"469070501.0\",\"amsMY8yXDPCo1sQPqpKm6A4\",null,null,null,\"en\",\"IND\",0,8,1869,null,false,false,null,\"og-826fbb5d-5ac4-4c9f-a078-5a1f9a998231\",null,null,null,null,null,null,null,0,null,null,null,19028915,null,null,true,null,false,[1],1,null,null,null,null,null,null,null,null,null,true,null,[false,2],null,null,null,null,false,null,[2,5,\"cg\",281]]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[3700949],[]]"],null,null,null,[],1,null,null,null,null,null,[]],["1661758317376",null,[],null,null,null,null,"[107,40400,538,1,\"469070501.0\",\"amsMY8yXDPCo1sQPqpKm6A4\",null,null,null,\"en\",\"IND\",0,8,1873,null,false,false,null,\"og-826fbb5d-5ac4-4c9f-a078-5a1f9a998231\",null,null,null,null,null,null,null,8,null,null,null,19028915,null,null,true,null,false,[2],2,null,null,null,null,null,null,null,null,null,true,null,[false,2],null,null,null,null,false,null,[2,5,\"cg\",281]]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[3700949],[]]"],null,null,null,[],2,null,null,null,null,null,[]],["1661758317726",null,[],null,null,null,null,"[109,40400,538,1,\"469070501.0\",\"amsMY8yXDPCo1sQPqpKm6A4\",null,null,null,\"en\",\"IND\",0,8,2223,null,false,false,null,\"og-826fbb5d-5ac4-4c9f-a078-5a1f9a998231\",null,null,null,null,null,null,null,32936,null,null,null,19028915,null,null,true,null,false,[3],3,null,null,null,null,null,null,null,null,null,true,null,[false,2],null,null,null,null,false,null,[2,5,\"cg\",281]]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[3700949],[]]"],null,null,null,[],3,null,null,null,null,null,[]],["1661758317732",null,[],null,null,null,null,"[36,40400,538,1,\"469070501.0\",\"amsMY8yXDPCo1sQPqpKm6A4\",null,null,null,\"en\",\"IND\",0,8,2229,null,false,false,null,\"og-826fbb5d-5ac4-4c9f-a078-5a1f9a998231\",null,null,null,null,null,null,null,32938,null,null,null,19028915,null,null,true,null,null,null,4,-1,null,null,null,76,256,1135,1505,null,true,null,[false,2,1,false,false],null,null,null,null,false,null,[2,5,\"cg\",281]]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[3700949],[]]"],null,null,null,[],4,null,null,null,null,null,[]]],"1661758318377",[]]",
        BODY_END
    );

    ns_end_transaction("log_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_4");
    ns_web_url ("gen_204_4",
        "URL=https://www.google.com/gen_204?atyp=i&ei=amsMY4-EC77f1sQP86SskAk&ct=slh&v=t1&im=M&m=HV&pv=0.21969355406327273&me=1:1661758317182,V,0,0,1536,714:0,B,714:0,N,1,amsMY4-EC77f1sQP86SskAk:0,R,1,1,0,0,1536,714:223,x:1443,h,1,1,i:1609,e,B&zx=1661758320457",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC"
    );

    ns_end_transaction("gen_204_4", NS_AUTO_STATUS);
    ns_page_think_time(0.162);

    //Page Auto split for Button '' Clicked by User
    ns_start_transaction("gen_204_5");
    ns_web_url ("gen_204_5",
        "URL=https://www.google.com/gen_204?use_corp=on&atyp=i&zx=1661758320705&ogsr=1&ei=amsMY8yXDPCo1sQPqpKm6A4&ct=7&cad=n&id=19028915&loc=webhp&prid=538&ogd=co.in&ogprm=up&ap=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP"
    );

    ns_end_transaction("gen_204_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("log_5");
    ns_web_url ("log_5",
        "URL=https://play.google.com/log?format=json&hasfast=true",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en-IN"]],373,[["1661758320706",null,[],null,null,null,null,"[37,40400,538,1,\"469070501.0\",\"amsMY8yXDPCo1sQPqpKm6A4\",null,null,null,\"en\",\"IND\",0,8,5203,null,false,false,null,\"og-826fbb5d-5ac4-4c9f-a078-5a1f9a998231\",null,null,null,null,null,null,null,185,2,null,null,19028915,null,null,true,null,null,null,5,-1,null,null,null,76,256,1135,1505,null,true,null,[null,2,1,false],null,null,null,null,false,null,[2,5,\"cg\",281]]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[3700949],[]]"],null,null,null,[],5,null,null,null,null,null,[]]],"1661758321717",[]]",
        BODY_END
    );

    ns_end_transaction("log_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_6");
    ns_web_url ("gen_204_6",
        "URL=https://www.google.com/gen_204?atyp=csi&ei=amsMY4-EC77f1sQP86SskAk&s=webhp&st=6347&fid=0&t=fi&zx=1661758321863",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP"
    );

    ns_end_transaction("gen_204_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_7");
    ns_web_url ("gen_204_7",
        "URL=https://www.google.com/gen_204?atyp=csi&ei=amsMY4-EC77f1sQP86SskAk&s=jsa&jsi=s,st.6563,t.0,at.14,et.click,n.vZr2rb,cn.1,ie.0,vi.1&zx=1661758322081",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP"
    );

    ns_end_transaction("gen_204_7", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_8");
    ns_web_url ("gen_204_8",
        "URL=https://www.google.com/gen_204?atyp=i&ei=amsMY4-EC77f1sQP86SskAk&ved=0ahUKEwiP4ZOQxOv5AhW-r5UCHXMSC5IQ39UDCAQ&zx=1661758322082",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP"
    );

    ns_end_transaction("gen_204_8", NS_AUTO_STATUS);
    ns_page_think_time(0.295);

    //Page Auto split for 
    ns_start_transaction("search_3");
    ns_web_url ("search_3",
        "URL=https://www.google.com/complete/search?q=s&cp=1&client=gws-wiz&xssi=t&hl=en-IN&authuser=0&psi=amsMY4-EC77f1sQP86SskAk.1661758317303&dpr=1.25",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP"
    );

    ns_end_transaction("search_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_9");
    ns_web_url ("gen_204_9",
        "URL=https://www.google.com/gen_204?atyp=i&ei=amsMY4-EC77f1sQP86SskAk&ved=0ahUKEwiP4ZOQxOv5AhW-r5UCHXMSC5IQ39UDCAQ&zx=1661758322461",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP",
        INLINE_URLS,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTmhJ51S8R5kE5C2iThgviI2wJ3h4k9Jt3AUmFH5_Y&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQr9wztJOW5L5yvT71EfVF91zvG3Aa4V8DTJs40SbE&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("gen_204_9", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_4");
    ns_web_url ("search_4",
        "URL=https://www.google.com/complete/search?q=sd&cp=2&client=gws-wiz&xssi=t&hl=en-IN&authuser=0&psi=amsMY4-EC77f1sQP86SskAk.1661758317303&dpr=1.25",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP"
    );

    ns_end_transaction("search_4", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_5");
    ns_web_url ("search_5",
        "URL=https://www.google.com/complete/search?q=sdf&cp=3&client=gws-wiz&xssi=t&hl=en-IN&authuser=0&psi=amsMY4-EC77f1sQP86SskAk.1661758317303&dpr=1.25",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP"
    );

    ns_end_transaction("search_5", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_6");
    ns_web_url ("search_6",
        "URL=https://www.google.com/complete/search?q=sdfj&cp=4&client=gws-wiz&xssi=t&hl=en-IN&authuser=0&psi=amsMY4-EC77f1sQP86SskAk.1661758317303&dpr=1.25",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP"
    );

    ns_end_transaction("search_6", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_7");
    ns_web_url ("search_7",
        "URL=https://www.google.com/complete/search?q=sdfjg&cp=5&client=gws-wiz&xssi=t&hl=en-IN&authuser=0&psi=amsMY4-EC77f1sQP86SskAk.1661758317303&dpr=1.25",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP",
        INLINE_URLS,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ9AL5t4YB_B1_VmIVpWDb53x5UnmH75YT0SCtmEiE&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRUf93HQbpKrxzSnIXufMR70VhVK7chdlNDaBtIUSp5s2bVfvCj1UfrZn4&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVVP8Z-63TPOq6wwtY-eodzoNr1GHeYCAVMGUPd8w&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRwNSRoCt0Cizhn4MS8d6jOyfjyvcRIFlaQUGc2lGaXKiQ7fJClIV8vyWQ&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcST7PCyJqKMRNsXzxLJkGPV4pddh1aTpE_DocAnY6E&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.google.com/images/experiments/wavy-underline.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID;OGPC;OGP", END_INLINE
    );

    ns_end_transaction("search_7", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_8");
    ns_web_url ("search_8",
        "URL=https://www.google.com/complete/search?q=d&cp=1&client=gws-wiz&xssi=t&hl=en-IN&authuser=0&psi=amsMY4-EC77f1sQP86SskAk.1661758317303&dpr=1.25",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP",
        INLINE_URLS,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSLXX0hMZvFRwbbeoil3Pc-VRZGoUE-HDgdm0kS0HvHZR17wftwCrt58FY&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkhhChErE4G4WEMYnby-ijgFcwVbO5_Gv7K2qS5uA&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("search_8", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_9");
    ns_web_url ("search_9",
        "URL=https://www.google.com/complete/search?q=de&cp=2&client=gws-wiz&xssi=t&hl=en-IN&authuser=0&psi=amsMY4-EC77f1sQP86SskAk.1661758317303&dpr=1.25",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP",
        INLINE_URLS,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRgKv0skhuF4S8yWzCMG719As67LQxdVn8a7QwokCQ&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSbzdHRWVkypoVgab0dx56xE2oZH0HOeHodHH-RQmM&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT1BMupefPezsDbQxSvvp9trd9gfuDSZQCNBgyyhKgda1JmAbSuzc1mVaU&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRz-ltG5xK-SZPT3PFeFdtfE6KNFUF3t72nUtaPpv3LgSL5DjXvCqnJBpHV&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6xfjGvqCUXh9mj_EK-4u8x2-E_GI6sM1PaYTKrJx5jn6sYZPleZEGQ6w&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRV5o0KpLQCyDXS6SCbXFW6sORYlxmOdrw7tVqBVLA&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("search_9", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_10");
    ns_web_url ("search_10",
        "URL=https://www.google.com/complete/search?q=del&cp=3&client=gws-wiz&xssi=t&hl=en-IN&authuser=0&psi=amsMY4-EC77f1sQP86SskAk.1661758317303&dpr=1.25",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP",
        INLINE_URLS,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTCc0tZJLdw8gMS4SoV422e-WniG3Ptw2sSKoFo_bE&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("search_10", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_11");
    ns_web_url ("search_11",
        "URL=https://www.google.com/complete/search?q=delh&cp=4&client=gws-wiz&xssi=t&hl=en-IN&authuser=0&psi=amsMY4-EC77f1sQP86SskAk.1661758317303&dpr=1.25",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP"
    );

    ns_end_transaction("search_11", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_12");
    ns_web_url ("search_12",
        "URL=https://www.google.com/complete/search?q=delhi&cp=5&client=gws-wiz&xssi=t&hl=en-IN&authuser=0&psi=amsMY4-EC77f1sQP86SskAk.1661758317303&dpr=1.25",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP"
    );

    ns_end_transaction("search_12", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_10");
    ns_web_url ("gen_204_10",
        "URL=https://www.google.com/gen_204?atyp=i&ei=amsMY4-EC77f1sQP86SskAk&ct=slh&v=t1&im=M&pv=0.21969355406327273&me=8:1661758320458,V,0,0,0,0:1394,V,0,0,1536,714:226,G,1,1,912,257:5,V,0,0,1536,714,1:7256,e,U&zx=1661758329340",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP"
    );

    ns_end_transaction("gen_204_10", NS_AUTO_STATUS);
    ns_page_think_time(0.095);

    //Page Auto split for Button 'Google Search' Clicked by User
    ns_start_transaction("search");
    ns_web_url ("search",
        "URL=https://www.google.com/search?q=delhi&source=hp&ei=amsMY4-EC77f1sQP86SskAk&iflsig=AJiK0e8AAAAAYwx5emvYX91aG-4351NBDXTitEAPdEdO&ved=0ahUKEwiP4ZOQxOv5AhW-r5UCHXMSC5IQ4dUDCAc&uact=5&oq=delhi&gs_lcp=Cgdnd3Mtd2l6EAMyCAgAELEDEIMBMgsIABCABBCxAxCDATILCC4QgAQQsQMQ1AIyCAguELEDEIMBMhEILhCABBCxAxCDARDHARDRAzIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQyCwguEIAEELEDEIMBOg4IABCPARDqAhCMAxDlAjoFCC4QgAQ6CAgAEIAEELEDOhEILhCABBCxAxDHARDRAxDUAjoICC4QgAQQsQM6DgguEIAEELEDEIMBENQCOgUIABCxAzoQCC4QsQMQxwEQ0QMQ1AIQCjoHCC4QsQMQCjoHCAAQsQMQCjoNCC4QsQMQgwEQ1AIQCjoNCC4QsQMQgwEQ1AIQDToECAAQDToUCC4QgAQQsQMQgwEQxwEQ0QMQ1AI6CAguEIAEENQCSgUIPBIBNFD2AljzNWDSOGgGcAB4AIABuwGIAe4KkgEEMC4xMJgBAKABAbABCg&sclient=gws-wiz",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP"
    );

    ns_end_transaction("search", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_11");
    ns_web_url ("gen_204_11",
        "URL=https://www.google.com/gen_204?atyp=i&ei=amsMY4-EC77f1sQP86SskAk&ct=slh&v=t1&im=M&pv=0.21969355406327273&me=13:1661758329368,V,0,0,0,0:502,e,H&zx=1661758329870",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP"
    );

    ns_end_transaction("gen_204_11", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("log_6");
    ns_web_url ("log_6",
        "URL=https://play.google.com/log?hasfast=true&authuser=0&format=json",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://ogs.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en",null,"boq_onegooglehttpserver_20220822.03_p0"]],729,[["1661758320703",null,[],null,null,null,null,"[[[71121,null,[70881],3],538,[]]]",null,null,null,null,null,null,-19800,null,null,null,null,[],2,null,null,null,null,null,[]]],"1661758329875",[]]",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_92x30dp.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID;OGPC;OGP", END_INLINE,
            "URL=https://fonts.gstatic.com/s/googlesans/v14/4UaGrENHsxJlGDuGo1OIlL3Owp4.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.google.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.google.com/images/searchbox/desktop_searchbox_sprites318_hr.webp", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID;OGPC;OGP", END_INLINE,
            "URL=https://www.google.com/maps/vt/data=-ctOUISQr7ki1MGomM6hdn2kkwEdIgjOjE6CDc-L-78SO2bJfOqOYCsHgiYm9wL1zRudOxDq5zunrwaNYDdAyg1VjOux_aq_ePAu4bJuIxZs8_5UrP6FT9nO3_E9d8p-rf4jcZyw9wZahObZVmQ39YeCAdWUYj1xRZr4QEaE869RxwlirpkrDmKNLxQulVixK8MDQUbwPciISbSRgFiCnN7X2t8H1ILGFzZ0L_Eul3n2PGLNE4oLng4zASK4", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID;OGPC;OGP", END_INLINE,
            "URL=https://www.gstatic.com/ui/v1/activityindicator/loading_24.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://id.google.com/verify/AK5h1ilWn0mQ8M1L1IlfJkxdyxzIulcHOJAr_3kszJbfGTDgXFmHoG_KD9aeRgiGx--t2gq3_Zz_ls9wDp1mVRJv3bXdrbheU92glpyuWZQ0Ruc", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID;OGPC;OGP", END_INLINE,
            "URL=https://www.google.com/images/nav_logo321_hr.webp", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID;OGPC;OGP", END_INLINE
    );

    ns_end_transaction("log_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_12");
    ns_web_url ("gen_204_12",
        "URL=https://www.google.com/gen_204?s=web&t=aft&atyp=csi&ei=d2sMY5W8FqK81sQP6IGHsAc&rt=wsrt.498,aft.1038,afti.1038,sct.660&wh=714&imn=60&ima=7&imad=7&aftp=714&bl=IuYP",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;NID;OGPC;OGP",
        INLINE_URLS,
            "URL=https://www.google.com/xjs/_/js/k=xjs.s.en_GB.0RwjdeQGgkw.O/am=AoACgQDwAgDAAEBmAAEAAAAAAAwAAIYAIPH_EwAADBACAjkJAAAAAqRMiAAAYAAgBHAQhAAAAAA-Mn8Acv4MAAw04QIAAAAAAAAAKOASBAMXSBQEAAIAAAAAACCspq6_zAVB/d=1/ed=1/dg=2/br=1/rs=ACT90oHpXkqqU0bhXDMXscWAqzsfRSWN8A/m=attn,cdos,dpf,hsm,jsa,d,csi", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;NID;OGPC;OGP", END_INLINE,
            "URL=https://www.google.com/search?q=delhi&source=hp&ei=amsMY4-EC77f1sQP86SskAk&iflsig=AJiK0e8AAAAAYwx5emvYX91aG-4351NBDXTitEAPdEdO&ved=0ahUKEwiP4ZOQxOv5AhW-r5UCHXMSC5IQ4dUDCAc&uact=5&oq=delhi&gs_lcp=Cgdnd3Mtd2l6EAMyCAgAELEDEIMBMgsIABCABBCxAxCDATILCC4QgAQQsQMQ1AIyCAguELEDEIMBMhEILhCABBCxAxCDARDHARDRAzIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQyCwguEIAEELEDEIMBOg4IABCPARDqAhCMAxDlAjoFCC4QgAQ6CAgAEIAEELEDOhEILhCABBCxAxDHARDRAxDUAjoICC4QgAQQsQM6DgguEIAEELEDEIMBENQCOgUIABCxAzoQCC4QsQMQxwEQ0QMQ1AIQCjoHCC4QsQMQCjoHCAAQsQMQCjoNCC4QsQMQgwEQ1AIQCjoNCC4QsQMQgwEQ1AIQDToECAAQDToUCC4QgAQQsQMQgwEQxwEQ0QMQ1AI6CAguEIAEENQCSgUIPBIBNFD2AljzNWDSOGgGcAB4AIABuwGIAe4KkgEEMC4xMJgBAKABAbABCg&sclient=gws-wiz", END_INLINE,
            "URL=https://www.google.com/xjs/_/js/k=xjs.s.en_GB.0RwjdeQGgkw.O/ck=xjs.s.2-FJbM4yaGs.L.W.O/am=AoACgQDwAgDAAEBmAAEAAAAAAAwAAIYAIPH_EwAADBACAjkJAAAAAqRMiAAAYAAgBHAQhAAAAAA-Mn8Acv4MAAw04QIAAAAAAAAAKOASBAMXSBQEAAIAAAAAACCspq6_zAVB/d=1/exm=attn,cdos,csi,d,dpf,hsm,jsa/excm=D1J6He,FuQWyc,MRb7nf,Zudxcb/ed=1/dg=2/br=1/rs=ACT90oEAdXUBs5fQs6o0OUSVudMDabToxw/ee=Pjplud:PoEs9b;QGR0gd:Mlhmy;uY49fb:COQbmf;EVNhjf:pw70Gc;oUlnpc:RagDlc;dtl0hd:lLQWFe;sTsDMc:kHVSUb;dIoSBb:ZgGg9b;pXdRYb:JKoKVe;wR5FRb:TtcOte;yGxLoc:FmAr0c;g8nkx:U4MzKc;KpRAue:Tia57b;daB6be:lMxGPd;Fmv9Nc:O1Tzwc;hK67qb:QWEO5b;jVtPve:wQ95P;BMxAGc:E5bFse;R4IIIb:QWfeKf;xbe2wc:wbTLEd;wQlYve:aLUfP;G6wU6e:hezEbd;fAO5td:yzxsuf;SJsSc:H1GVub;SMDL4c:fTfGO;oSUNyd:fTfGO;zxnPse:GkRiKb;oGtAuc:sOXFj;zOsCQe:Ko78Df;WCEKNd:I46Hvd;NPKaK:PVlQOd;LBgRLc:XVMNvd;kbAm9d:MkHyGd;UyG7Kb:wQd0G;LsNahb:ucGLNb;rQSrae:C6D5Fc;kCQyJ:ueyPK;EABSZ:MXZt9d;qavrXe:zQzcXe;TxfV6d:YORN0b;UDrY1c:eps46d;GleZL:J1A7Od;nKl0s:xxrckd;JXS8fb:Qj0suc;qaS3gd:yiLg6e;NSEoX:lazG7b;w3bZCb:ZPGaIb;VGRfx:VFqbr;aAJE9c:WHW6Ef;imqimf:jKGL2e;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;F9mqte:UoRcbe;SLtqO:Kh1xYe;tosKvd:ZCqP3;WDGyFe:jcVOxd;VxQ32b:k0XsBb;DULqB:RKfG5c;Np8Qkd:Dpx6qc;aZ61od:arTwJ;cFTWae:gT8qnd;gaub4:TN6bMe;DpcR3d:zL72xf;hjRo6e:F62sG;BjwMce:cXX2Wb;CUcugf:BIaADc;qddgKe:x4FYXe;eBAeSb:Ck63tb;lkq0A:Z0MWEf;eHDfl:ofjVkb;SNUn3:x8cHvb;LEikZe:byfTOb,lsjVmc;io8t5d:sgY6Zb;j7137d:KG2eXe;Oj465e:KG2eXe;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;nAFL3:s39S4;iFQyKf:QIhFr;whEZac:F4AmNb;vfVwPd:OXTqFb;w9w86d:dt4g2b;KQzWid:mB4wNe;pNsl2d:j9Yuyc;Nyt6ic:jn2sGd/m=Eox39d,FmAr0c,GCSbhd,HYSCof,K6HGfd,M0hWhd,OZLguc,Wo3n8,cSX9Xe,d5EhJe,hnlzI,jJcUN,lvAdvf,nPaQu,pHXghd,tIj4fb,w4UyN,xMclgd,yBi4o?xjs=s1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;OGPC;OGP;NID", END_INLINE,
            "URL=https://www.gstatic.com/og/_/js/k=og.qtm.en_US.-MMKP3uG9VU.O/rt=j/m=qabr,q_dnp,qcwid,qapid/exm=qaaw,qadd,qaid,qein,qhaw,qhbr,qhch,qhga,qhid,qhin,qhpr/d=1/ed=1/rs=AA2YrTuS6iZfrsnE7GApv0RWeBgl21VxSA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/og/_/ss/k=og.qtm.tM-BeBGBME0.L.W.O/m=qcwid/excm=qaaw,qadd,qaid,qein,qhaw,qhbr,qhch,qhga,qhid,qhin,qhpr/d=1/ed=1/ct=zgms/rs=AA2YrTtmhrLYVlm7jdu3TI1ROywA3arJnA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("gen_204_12", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_13");
    ns_web_url ("gen_204_13",
        "URL=https://www.google.com/gen_204?atyp=i&ei=d2sMY5W8FqK81sQP6IGHsAc&ct=slh&v=t1&m=HV&pv=0.6826322022990636&me=1:1661758330878,V,0,0,1536,714:0,B,3665:0,N,1,d2sMY5W8FqK81sQP6IGHsAc:0,R,1,8,24,36,92,34:0,R,1,CAIQAw,230,85,78,45:0,R,1,CAIQBA,310,85,78,45:0,R,1,CAIQBQ,390,85,89,45:0,R,1,CAIQBg,480,85,103,45:0,R,1,CAIQNA,180,182,652,2771:0,R,1,CBUQAA,180,182,652,172:0,R,1,CA8QAA,180,284,548,43:0,R,1,CAMQAA,180,398,652,801:0,R,1,CAcQAA,180,426,652,351:0,R,1,CAQQAA,180,467,326,297:0,R,1,CAYQAA,507,467,326,143:0,R,1,CAUQAA,507,611,326,153:0,R,1,CAEQAQ,931,176,369,3370:0,R,1,CGYQAA,932,182,456,1372:0,R,1,CHUQAA,933,183,454,250:0,R,1,CHoQAA,933,183,454,160:0,R,1,CHoQAQ,933,183,454,160:0,R,1,CHkQAA,933,355,454,62:0,R,1,CGYQAw,933,447,454,1070:0,R,1,CGsQAA,933,447,454,1070:0,R,1,CHwQAA,933,447,454,272:0,R,1,CHsQAA,933,447,454,272:0,R,1,CG8QAA,933,447,454,151:0,R,1,CG8QAQ,948,447,424,151:0,R,1,CHcQAA,933,611,454,22:0,R,1,CG4QAA,933,639,454,22:0,R,1,CGwQAA,933,668,454,22:0,R,1,CG0QAA,933,697,454,22:806,x:58,T:0,R,1,8,24,36,92,34:0,R,1,CAIQAw,230,85,78,45:0,R,1,CAIQBA,310,85,78,45:0,R,1,CAIQBQ,390,85,89,45:0,R,1,CAIQBg,480,85,103,45:0,R,1,CAIQNA,180,182,652,2771:0,R,1,CBUQAA,180,182,652,172:0,R,1,CA8QAA,180,284,548,43:0,R,1,CAMQAA,180,398,652,801:0,R,1,CAcQAA,180,426,652,351:0,R,1,CAQQAA,180,467,326,297:0,R,1,CAYQAA,507,467,326,143:0,R,1,CAUQAA,507,611,326,153:0,R,1,CAEQAQ,931,176,369,3370:0,R,1,CGYQAA,932,182,456,1372:0,R,1,CHUQAA,933,183,454,250:0,R,1,CHoQAA,933,183,454,160:0,R,1,CHoQAQ,933,183,454,160:0,R,1,CHkQAA,933,355,454,62:0,R,1,CGYQAw,933,447,454,1070:0,R,1,CGsQAA,933,447,454,1070:0,R,1,CHwQAA,933,447,454,272:0,R,1,CHsQAA,933,447,454,272:0,R,1,CG8QAA,933,447,454,151:0,R,1,CG8QAQ,948,447,424,151:0,R,1,CHcQAA,933,611,454,22:0,R,1,CG4QAA,933,639,454,22:0,R,1,CGwQAA,933,668,454,22:0,R,1,CG0QAA,933,697,454,22:25,T:0,R,1,8,24,36,92,34:0,R,1,CAIQAw,230,85,78,45:0,R,1,CAIQBA,310,85,78,45:0,R,1,CAIQBQ,390,85,89,45&zx=1661758331778",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;OGPC;OGP;NID",
        INLINE_URLS,
            "URL=https://www.google.com/client_204?&atyp=i&biw=1536&bih=714&dpr=1.25&ei=d2sMY5W8FqK81sQP6IGHsAc", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;OGPC;OGP;NID", END_INLINE,
            "URL=https://www.gstatic.com/ui/v1/menu/light_thumbnail2.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/ui/v1/menu/dark_thumbnail2.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/ui/v1/menu/device_default_thumbnail2.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.google.com/xjs/_/js/k=xjs.s.en_GB.0RwjdeQGgkw.O/ck=xjs.s.2-FJbM4yaGs.L.W.O/am=AoACgQDwAgDAAEBmAAEAAAAAAAwAAIYAIPH_EwAADBACAjkJAAAAAqRMiAAAYAAgBHAQhAAAAAA-Mn8Acv4MAAw04QIAAAAAAAAAKOASBAMXSBQEAAIAAAAAACCspq6_zAVB/d=1/exm=Eox39d,FmAr0c,GCSbhd,HYSCof,K6HGfd,M0hWhd,OZLguc,Wo3n8,attn,cSX9Xe,cdos,csi,d,d5EhJe,dpf,hnlzI,hsm,jJcUN,jsa,lvAdvf,nPaQu,pHXghd,tIj4fb,w4UyN,xMclgd,yBi4o/excm=D1J6He,FuQWyc,MRb7nf,Zudxcb/ed=1/dg=2/br=1/rs=ACT90oEAdXUBs5fQs6o0OUSVudMDabToxw/ee=Pjplud:PoEs9b;QGR0gd:Mlhmy;uY49fb:COQbmf;EVNhjf:pw70Gc;oUlnpc:RagDlc;dtl0hd:lLQWFe;sTsDMc:kHVSUb;dIoSBb:ZgGg9b;pXdRYb:JKoKVe;wR5FRb:TtcOte;yGxLoc:FmAr0c;g8nkx:U4MzKc;KpRAue:Tia57b;daB6be:lMxGPd;Fmv9Nc:O1Tzwc;hK67qb:QWEO5b;jVtPve:wQ95P;BMxAGc:E5bFse;R4IIIb:QWfeKf;xbe2wc:wbTLEd;wQlYve:aLUfP;G6wU6e:hezEbd;fAO5td:yzxsuf;SJsSc:H1GVub;zxnPse:GkRiKb;oGtAuc:sOXFj;zOsCQe:Ko78Df;WCEKNd:I46Hvd;NPKaK:PVlQOd;LBgRLc:XVMNvd;kbAm9d:MkHyGd;UyG7Kb:wQd0G;LsNahb:ucGLNb;rQSrae:C6D5Fc;kCQyJ:ueyPK;EABSZ:MXZt9d;qavrXe:zQzcXe;TxfV6d:YORN0b;UDrY1c:eps46d;GleZL:J1A7Od;nKl0s:xxrckd;JXS8fb:Qj0suc;qaS3gd:yiLg6e;NSEoX:lazG7b;w3bZCb:ZPGaIb;VGRfx:VFqbr;aAJE9c:WHW6Ef;imqimf:jKGL2e;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;F9mqte:UoRcbe;SLtqO:Kh1xYe;tosKvd:ZCqP3;WDGyFe:jcVOxd;VxQ32b:k0XsBb;DULqB:RKfG5c;Np8Qkd:Dpx6qc;aZ61od:arTwJ;cFTWae:gT8qnd;gaub4:TN6bMe;DpcR3d:zL72xf;hjRo6e:F62sG;BjwMce:cXX2Wb;CUcugf:BIaADc;qddgKe:x4FYXe;eBAeSb:Ck63tb;lkq0A:Z0MWEf;eHDfl:ofjVkb;SNUn3:x8cHvb;LEikZe:byfTOb,lsjVmc;io8t5d:sgY6Zb;j7137d:KG2eXe;Oj465e:KG2eXe;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;nAFL3:s39S4;iFQyKf:QIhFr;whEZac:F4AmNb;vfVwPd:OXTqFb;w9w86d:dt4g2b;KQzWid:mB4wNe;pNsl2d:j9Yuyc;Nyt6ic:jn2sGd;l8Azde:j4Ca9b;oSUNyd:vjQg0b;SMDL4c:vjQg0b/m=ABJeBb,ANyn1,Ah7cLd,DFfvp,E19wJb,EkevXb,GU4Gab,ILbBec,L1AAkb,MTV2Lb,MeIiV,MpJwZc,R3fhkb,UBXHI,UzbKLd,WOJjZ,Wn3aEc,aLUfP,aa,abd,async,bgd,btdpvd,d2p3q,dvl,fKZehd,fVaWL,fhcUyb,fiAufb,foot,jWdabd,kQvlef,kyn,lcvz5e,lli,mu,pgCXqb,sYEX8b,sb_wiz,sf,sonic,spch,tl,u9YDDf?xjs=s2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;OGPC;OGP;NID", END_INLINE
    );

    ns_end_transaction("gen_204_13", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_13");
    ns_web_url ("search_13",
        "URL=https://www.google.com/complete/search?q=delhi&cp=0&client=desktop-gws-wiz-on-focus-serp&xssi=t&hl=en-IN&authuser=0&pq=delhi&psi=d2sMY5W8FqK81sQP6IGHsAc.1661758331701&ofp=GO--rL_c6ofz5wEYn5Gd0-zYqNlxGJqGjqaerrzk8gEYvqT46p7Ois3CARi614mVuszh5TAQATKGAQoNCgtkZWxoaSBzdGF0ZQoOCgxkZWxoaSBwb2xpY2UKEgoQZGVsaGkgaGlnaCBjb3VydAoQCg5kZWxoaSBkaXN0YW5jZQoPCg1kZWxoaSB3ZWF0aGVyChIKEGRlbGhpIGdvdmVybm1lbnQKCgoIZGVsaGkgY20KDAoKZGVsaGkgYXJlYRBHMlgKFwoVV2hhdCBpcyBEZWxoaSBmYW1vdXM_ChkKF0lzIGl0IERlbGhpIGlzIGEgc3RhdGU_Ch8KHUlzIERlbGhpIGEgZ29vZCBjaXR5IHRvIGxpdmU_EOQC&dpr=1.25",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;OGPC;OGP;NID"
    );

    ns_end_transaction("search_13", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_14");
    ns_web_url ("search_14",
        "URL=https://www.google.com/complete/search?q&cp=0&client=gws-wiz&xssi=t&hl=en-IN&authuser=0&pq=delhi&psi=d2sMY5W8FqK81sQP6IGHsAc.1661758331701&ofp=GO--rL_c6ofz5wEYn5Gd0-zYqNlxGJqGjqaerrzk8gEYvqT46p7Ois3CARi614mVuszh5TA&nolsbt=1&dpr=1.25",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;OGPC;OGP;NID",
        INLINE_URLS,
            "URL=https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.IK5OmUURd2E.O/m=gapi_iframes,googleapis_client/rt=j/sv=1/d=1/ed=1/rs=AHpOoo932JinkSJHK92WgVjIV-Jwwyu3Rw/cb=gapi.loaded_0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;OGPC;OGP;NID", END_INLINE,
            "URL=https://www.google.com/xjs/_/js/md=1/k=xjs.s.en_GB.0RwjdeQGgkw.O/am=AoACgQDwAgDAAEBmAAEAAAAAAAwAAIYAIPH_EwAADBACAjkJAAAAAqRMiAAAYAAgBHAQhAAAAAA-Mn8Acv4MAAw04QIAAAAAAAAAKOASBAMXSBQEAAIAAAAAACCspq6_zAVB/rs=ACT90oHpXkqqU0bhXDMXscWAqzsfRSWN8A", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;OGPC;OGP;NID", END_INLINE,
            "URL=https://www.google.com/xjs/_/js/k=xjs.s.en_GB.0RwjdeQGgkw.O/ck=xjs.s.2-FJbM4yaGs.L.W.O/am=AoACgQDwAgDAAEBmAAEAAAAAAAwAAIYAIPH_EwAADBACAjkJAAAAAqRMiAAAYAAgBHAQhAAAAAA-Mn8Acv4MAAw04QIAAAAAAAAAKOASBAMXSBQEAAIAAAAAACCspq6_zAVB/d=1/exm=ABJeBb,ANyn1,Ah7cLd,DFfvp,E19wJb,EkevXb,Eox39d,FmAr0c,GCSbhd,GU4Gab,HYSCof,ILbBec,K6HGfd,L1AAkb,M0hWhd,MTV2Lb,MeIiV,MpJwZc,OZLguc,R3fhkb,UBXHI,UzbKLd,WOJjZ,Wn3aEc,Wo3n8,aLUfP,aa,abd,async,attn,bgd,btdpvd,cSX9Xe,cdos,csi,d,d2p3q,d5EhJe,dpf,dvl,fKZehd,fVaWL,fhcUyb,fiAufb,foot,hnlzI,hsm,jJcUN,jWdabd,jsa,kQvlef,kyn,lcvz5e,lli,lvAdvf,mu,nPaQu,pHXghd,pgCXqb,sYEX8b,sb_wiz,sf,sonic,spch,tIj4fb,tl,u9YDDf,w4UyN,xMclgd,yBi4o/excm=D1J6He,FuQWyc,MRb7nf,Zudxcb/ed=1/dg=2/br=1/rs=ACT90oEAdXUBs5fQs6o0OUSVudMDabToxw/ee=Pjplud:PoEs9b;QGR0gd:Mlhmy;uY49fb:COQbmf;EVNhjf:pw70Gc;oUlnpc:RagDlc;dtl0hd:lLQWFe;sTsDMc:kHVSUb;dIoSBb:ZgGg9b;pXdRYb:JKoKVe;wR5FRb:TtcOte;yGxLoc:FmAr0c;g8nkx:U4MzKc;KpRAue:Tia57b;daB6be:lMxGPd;Fmv9Nc:O1Tzwc;hK67qb:QWEO5b;jVtPve:wQ95P;BMxAGc:E5bFse;R4IIIb:QWfeKf;xbe2wc:wbTLEd;wQlYve:aLUfP;G6wU6e:hezEbd;fAO5td:yzxsuf;SJsSc:H1GVub;zxnPse:GkRiKb;oGtAuc:sOXFj;zOsCQe:Ko78Df;WCEKNd:I46Hvd;NPKaK:PVlQOd;LBgRLc:XVMNvd;kbAm9d:MkHyGd;UyG7Kb:wQd0G;LsNahb:ucGLNb;rQSrae:C6D5Fc;kCQyJ:ueyPK;EABSZ:MXZt9d;qavrXe:zQzcXe;TxfV6d:YORN0b;UDrY1c:eps46d;GleZL:J1A7Od;nKl0s:xxrckd;JXS8fb:Qj0suc;qaS3gd:yiLg6e;NSEoX:lazG7b;w3bZCb:ZPGaIb;VGRfx:VFqbr;aAJE9c:WHW6Ef;imqimf:jKGL2e;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;F9mqte:UoRcbe;SLtqO:Kh1xYe;tosKvd:ZCqP3;WDGyFe:jcVOxd;VxQ32b:k0XsBb;DULqB:RKfG5c;Np8Qkd:Dpx6qc;aZ61od:arTwJ;cFTWae:gT8qnd;gaub4:TN6bMe;DpcR3d:zL72xf;hjRo6e:F62sG;BjwMce:cXX2Wb;CUcugf:BIaADc;qddgKe:x4FYXe;eBAeSb:Ck63tb;lkq0A:Z0MWEf;eHDfl:ofjVkb;SNUn3:x8cHvb;LEikZe:byfTOb,lsjVmc;io8t5d:sgY6Zb;j7137d:KG2eXe;Oj465e:KG2eXe;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;nAFL3:s39S4;iFQyKf:QIhFr;whEZac:F4AmNb;vfVwPd:OXTqFb;w9w86d:dt4g2b;KQzWid:mB4wNe;pNsl2d:j9Yuyc;Nyt6ic:jn2sGd;l8Azde:j4Ca9b;oSUNyd:vjQg0b;SMDL4c:vjQg0b/m=SHXTGd,TSZEqd,eX5ure,teJewe?xjs=s2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;OGPC;OGP;NID", END_INLINE
    );

    ns_end_transaction("search_14", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("bgasy");
    ns_web_url ("bgasy",
        "URL=https://www.google.com/async/bgasy?ei=d2sMY5W8FqK81sQP6IGHsAc&yv=3&cs=0&async=_fmt:jspb",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;OGPC;OGP;NID"
    );

    ns_end_transaction("bgasy", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_14");
    ns_web_url ("gen_204_14",
        "URL=https://www.google.com/gen_204?atyp=i&ei=d2sMY5W8FqK81sQP6IGHsAc&dt19=2&zx=1661758332134",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;OGPC;OGP;NID;DV"
    );

    ns_end_transaction("gen_204_14", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_15");
    ns_web_url ("gen_204_15",
        "URL=https://www.google.com/gen_204?atyp=i&ei=d2sMY5W8FqK81sQP6IGHsAc&ct=kptm:il&iw=1519&ih=714&r=0&sh=864&sw=1536&tmw=0&tmh=0&nvi=1&eg=0&zx=1661758332181",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;OGPC;OGP;NID;DV",
        INLINE_URLS,
            "URL=https://www.google.com/uviewer?q=delhi&origin=https%3A%2F%2Fwww.google.com&ptzd=1", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;OGPC;OGP;NID;DV", END_INLINE
    );

    ns_end_transaction("gen_204_15", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("log_7");
    ns_web_url ("log_7",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=1P_JAR;AEC;OGPC;OGP;NID",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en-IN"]],1655,[["1661758332201",null,[],null,null,null,null,"[[[\"/client_streamz/location/location_prompt/prompt_decisions_count\",null,[\"decision\",\"browser\"],[[[[\"NO_PROMPT_NOT_GRANTED\"],[\"UNKNOWN\"]],[1]]],null,[]]]]",null,null,null,null,null,null,-19800,null,null,null,null,[],1,null,null,null,null,null,[]]],"1661758332201",[]]",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.google.com/xjs/_/js/k=xjs.s.en_GB.0RwjdeQGgkw.O/ck=xjs.s.2-FJbM4yaGs.L.W.O/am=AoACgQDwAgDAAEBmAAEAAAAAAAwAAIYAIPH_EwAADBACAjkJAAAAAqRMiAAAYAAgBHAQhAAAAAA-Mn8Acv4MAAw04QIAAAAAAAAAKOASBAMXSBQEAAIAAAAAACCspq6_zAVB/d=0/excm=D1J6He,FuQWyc,MRb7nf,Zudxcb/ed=1/dg=2/br=1/rs=ACT90oEAdXUBs5fQs6o0OUSVudMDabToxw/m=sy3l,sy8w,uxMpU,sy2b,sy2d,byfTOb,sy2e,lsjVmc,sy48,OTA3Ae,sy47,COQbmf,PoEs9b,U0aPgd,sy3j,sydf,sydi,WlNQGd,sy385,nabPbb,sy4rv,SC7lYd,synj,sy3gi,sy3hh,QE1bwd,gSZvdb,tuZ5Wc,ql2uGc,sydg,syde,CnSW2d,sy3h8,sy3h9,sy48b,VD4Qme,ND0kmf,sy35r,RdVOmb,sy35t,b1qkGc,sy2c,sy2g,sy2h,LEikZe,Mlhmy,uKlGbf,q00IXe,Fh0l0,sy3gq,sy3gr,b8OZff,qcH9Lc,sy49e,XVaCB,khkNpe,sy3go,Exk9Ld,GGTOgd,P10Owf,uY49fb,Pjplud,QGR0gd,sy42,sy4d,sy4e,sy4f,sy4g,kWgXee,ovKuLd,sy5z,sy60,sy61,sy63,sy64,sy62,sy5t,sy5k,sy8y,sy9f,sy8z,sy3e,sy3g,sy3h,sy3i,sy3f,sy90,sy91,sy92,sy93,sy94,sy97,sy96,sy98,sy9a,sy99,sy9e,sy9c,sy9d,sy9b,sy9h,sy9g,sy9i,sy9j,sy9k,sy9l,sy9m,sy9n,sy8v,sy9o,sgY6Zb,syid,syjt,syjv,DPreE,Mbif2,sya3,sy3ly,p2I2Je,sydh,sy3m0,sy3m1,sy3m3,sy3m4,sy3m2,sy3m6,sy3m5,sy3m8,sy3mh,sy3mi,sy3mm,sy3ml,sy3mk,sy3mj,syps,sy3mn,sy3hr,sy3mo,sy3mq,sy3mp,sy3kj,sy3mr,sy3na,sy3fr,sy3hn,sy3hb,sy3ho,sy3n9,sy3ms,sy3mu,sy3mx,sy3mw,sy3mv,sy3my,sy3n5,sy3mz,sy3n0,sy3n1,sy3n2,sy3n3,sy3n4,sy3nd,sy3ne,sy3nf,sy3nz,sy3o2,sy3o4,sy3o0,sy3o1,sy3o3,sy3n6,sy3n7,sy3n8,sy3ng,sy3nh,sy3mt,sy3nj,sy3nc,sy3nk,sy3ni,sy3nb,sy3nl,sy3nm,sy3no,sy3nr,sy3nn,sy3np,sy3nq,sy3ns,sy3nt,sy3nu,sy3nv,sy3nw,sy3ny,sy3nx,sy3o5,sy3o6,sy3o7,sy3o8,exgaYe,io8t5d,KG2eXe,Oj465e,sy381,DpX64d,EufiNb?xjs=s2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;OGPC;OGP;NID;DV", END_INLINE,
            "URL=https://www.google.com/xjs/_/js/k=xjs.s.en_GB.0RwjdeQGgkw.O/ck=xjs.s.2-FJbM4yaGs.L.W.O/am=AoACgQDwAgDAAEBmAAEAAAAAAAwAAIYAIPH_EwAADBACAjkJAAAAAqRMiAAAYAAgBHAQhAAAAAA-Mn8Acv4MAAw04QIAAAAAAAAAKOASBAMXSBQEAAIAAAAAACCspq6_zAVB/d=0/excm=D1J6He,FuQWyc,MRb7nf,Zudxcb/ed=1/dg=2/br=1/rs=ACT90oEAdXUBs5fQs6o0OUSVudMDabToxw/m=syh6,sy36u,sy36v,dt4g2b?xjs=s2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;AEC;OGPC;OGP;NID;DV", END_INLINE,
            "URL=https://www.gstatic.com/_/mss/boq-search/_/js/k=boq-search.VisualFrontendUi.en_US.1kEbd_bn2wM.es5.O/am=gAfvESgOgBD_EYJAAACADpAAQSQCMMUDTAAAKkEtUQAHKEBwSAAIAAIIDAIAAAC4AGAAAICNgAEAAAAAAIV3HhgIAAAAAAAAAACAUMUAAAAAAAAAANBEAAAAAAAE/d=1/excm=_b,_r,_tp,unifiedviewerview/ed=1/dg=0/wt=2/rs=AH7-fg6gmPBqio1kii3gO95OA-G5e0QZuA/m=_b,_tp,_r", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/_/mss/boq-search/_/js/k=boq-search.VisualFrontendUi.en_US.1kEbd_bn2wM.es5.O/ck=boq-search.VisualFrontendUi.vvMlSvQKo4Q.L.B1.O/am=gAfvESgOgBD_EYJAAACADpAAQSQCMMUDTAAAKkEtUQAHKEBwSAAIAAIIDAIAAAC4AGAAAICNgAEAAAAAAIV3HhgIAAAAAAAAAACAUMUAAAAAAAAAANBEAAAAAAAE/d=1/exm=_b,_r,_tp/excm=_b,_r,_tp,unifiedviewerview/ed=1/wt=2/rs=AH7-fg6DOgWXiWX1uVyZ0HCNG9o5CEzZFA/ee=cEt90b:ws9Tlc;QGR0gd:Mlhmy;uY49fb:COQbmf;yxTchf:KUM7Z;qddgKe:xQtZb;dIoSBb:SpsfSb;EmZ2Bf:zr1jrb;EVNhjf:pw70Gc;NSEoX:lazG7b;qaS3gd:yiLg6e;zOsCQe:Ko78Df;WCEKNd:I46Hvd;xMUn6e:e0kzxe;fWLTFc:TVBJbf;flqRgb:ox2Q7c;pXdRYb:oR20R;oGtAuc:sOXFj;rQSrae:C6D5Fc;kCQyJ:ueyPK;EABSZ:MXZt9d;qavrXe:zQzcXe;TxfV6d:YORN0b;dtl0hd:lLQWFe;UDrY1c:eps46d;wQlYve:aLUfP;GleZL:J1A7Od;nKl0s:xxrckd;g8nkx:U4MzKc;JXS8fb:Qj0suc;sTsDMc:kHVSUb;w3bZCb:ZPGaIb;VGRfx:VFqbr;aAJE9c:WHW6Ef;imqimf:jKGL2e;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;F9mqte:UoRcbe;SLtqO:Kh1xYe;tosKvd:ZCqP3;WDGyFe:jcVOxd;VxQ32b:k0XsBb;DULqB:RKfG5c;Np8Qkd:Dpx6qc;bcPXSc:gSZLJb;aZ61od:arTwJ;cFTWae:gT8qnd;gaub4:TN6bMe;DpcR3d:zL72xf;hjRo6e:F62sG;BjwMce:cXX2Wb;Pjplud:EEDORb;io8t5d:yDVVkb;j7137d:KG2eXe;Oj465e:KG2eXe;ul9GGd:VDovNc;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;NPKaK:SdcwHb;nAFL3:s39S4;iFQyKf:QIhFr;zxnPse:SP0dJe;tGdRVe:oRqHk;yWysfe:pjcr8d;F774Sb:sVEevc;eBAeSb:sVEevc;VoYp5d:BXWsfc;Ti4hX:Y1W8Ad;vGrMZ:Y1W8Ad;zaIgPb:ovuoid;kbAm9d:MkHyGd;okUaUd:Kg1rBc;wV5Pjc:nQze3d;eHDfl:ofjVkb;SNUn3:ZwDk9d;LBgRLc:SdcwHb;wR5FRb:O1Gjze;vfVwPd:TWOpEe;w9w86d:aIe7ef;KQzWid:mB4wNe;pNsl2d:j9Yuyc;Nyt6ic:jn2sGd/m=byfTOb,lsjVmc,LEikZe", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/_/mss/boq-search/_/js/k=boq-search.VisualFrontendUi.en_US.1kEbd_bn2wM.es5.O/ck=boq-search.VisualFrontendUi.vvMlSvQKo4Q.L.B1.O/am=gAfvESgOgBD_EYJAAACADpAAQSQCMMUDTAAAKkEtUQAHKEBwSAAIAAIIDAIAAAC4AGAAAICNgAEAAAAAAIV3HhgIAAAAAAAAAACAUMUAAAAAAAAAANBEAAAAAAAE/d=1/exm=LEikZe,_b,_r,_tp,byfTOb,lsjVmc/excm=_b,_r,_tp,unifiedviewerview/ed=1/wt=2/rs=AH7-fg6DOgWXiWX1uVyZ0HCNG9o5CEzZFA/ee=cEt90b:ws9Tlc;QGR0gd:Mlhmy;uY49fb:COQbmf;yxTchf:KUM7Z;qddgKe:xQtZb;dIoSBb:SpsfSb;EmZ2Bf:zr1jrb;EVNhjf:pw70Gc;NSEoX:lazG7b;qaS3gd:yiLg6e;zOsCQe:Ko78Df;WCEKNd:I46Hvd;xMUn6e:e0kzxe;fWLTFc:TVBJbf;flqRgb:ox2Q7c;pXdRYb:oR20R;oGtAuc:sOXFj;rQSrae:C6D5Fc;kCQyJ:ueyPK;EABSZ:MXZt9d;qavrXe:zQzcXe;TxfV6d:YORN0b;dtl0hd:lLQWFe;UDrY1c:eps46d;wQlYve:aLUfP;GleZL:J1A7Od;nKl0s:xxrckd;g8nkx:U4MzKc;JXS8fb:Qj0suc;sTsDMc:kHVSUb;w3bZCb:ZPGaIb;VGRfx:VFqbr;aAJE9c:WHW6Ef;imqimf:jKGL2e;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;F9mqte:UoRcbe;SLtqO:Kh1xYe;tosKvd:ZCqP3;WDGyFe:jcVOxd;VxQ32b:k0XsBb;DULqB:RKfG5c;Np8Qkd:Dpx6qc;bcPXSc:gSZLJb;aZ61od:arTwJ;cFTWae:gT8qnd;gaub4:TN6bMe;DpcR3d:zL72xf;hjRo6e:F62sG;BjwMce:cXX2Wb;Pjplud:EEDORb;io8t5d:yDVVkb;j7137d:KG2eXe;Oj465e:KG2eXe;ul9GGd:VDovNc;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;NPKaK:SdcwHb;nAFL3:s39S4;iFQyKf:QIhFr;zxnPse:SP0dJe;tGdRVe:oRqHk;yWysfe:pjcr8d;F774Sb:sVEevc;eBAeSb:sVEevc;VoYp5d:BXWsfc;Ti4hX:Y1W8Ad;vGrMZ:Y1W8Ad;zaIgPb:ovuoid;kbAm9d:MkHyGd;okUaUd:Kg1rBc;wV5Pjc:nQze3d;eHDfl:ofjVkb;SNUn3:ZwDk9d;LBgRLc:SdcwHb;wR5FRb:O1Gjze;vfVwPd:TWOpEe;w9w86d:aIe7ef;KQzWid:mB4wNe;pNsl2d:j9Yuyc;Nyt6ic:jn2sGd/m=ws9Tlc,IZT63,n73qwf,UUJqVe,O1Gjze,xUdipf,OTA3Ae,COQbmf,fKUV3e,aurFic,U0aPgd,ZwDk9d,V3dDOb,Xn5N7c,mI3LFb,xhIfAc,BXWsfc,I46Hvd,HU2IR,S1avQ,WO9ee,btdpvd,PrPYRd,MpJwZc,NwH0H,OmgaI,lazG7b,Mpq4Ee,XVMNvd,KUM7Z,Mlhmy,L1AAkb,aIe7ef,s39S4,lwddkf,gychg,w9hDv,EEDORb,RMhBfe,Wq6lxf,nQze3d,fkGYQb,SdcwHb,S2r5lb,aW3pY,pw70Gc,EFQ78c,Ulmmrd,ZfAoz,mdR7q,Rr5NOe,oR20R,xQtZb,Wf0Cmd,JNoxi,kWgXee,MI6k7c,kjKdXe,SRsBqc,G2GqHe,BVgquf,QIhFr,ovKuLd,hKSk3e,Ko78Df,yDVVkb,hc6Ubd,Kg1rBc,SpsfSb,KG2eXe,sVEevc,zbML3c,VwDzFe,GihOkd,zr1jrb,hT8HDb,ClUoee,A7fCU,Uas9Hd,pjICDe", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("log_7", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_16");
    ns_web_url ("gen_204_16",
        "URL=https://www.google.com/gen_204?atyp=csi&ei=d2sMY5W8FqK81sQP6IGHsAc&s=web&t=all&bl=IuYP&wh=714&imn=60&ima=7&imad=7&aftp=714&adh=tv.6&cls=0.0013293824045789227&ime=2&imex=2&imeh=3&imea=0&imeb=17&imel=0&scp=0&fld=1242&net=dl.9500,ect.4g,rtt.50&mem=ujhs.29,tjhs.41,jhsl.1099,dm.8&sto=&sys=hc.4&rt=sct.660,aft.1038,afti.1038,aftqf.1454,prt.1437,xjsls.1610,dcl.1613,xjses.1751,xjsee.1808,xjs.1809,ol.3234,lcp.1253,fcp.355,wsrt.498,cst.0,dnst.0,rqst.2077,rspt.1590,rqstt.11,unt.7,cstt.7,dit.2110&zx=1661758333077",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=AEC;OGPC;OGP;NID;DV;1P_JAR",
        INLINE_URLS,
            "URL=https://adservice.google.com/adsid/google/ui", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=AEC;OGPC;OGP;NID;1P_JAR", END_INLINE,
            "URL=https://www.google.com/gen_204?atyp=i&ct=psnt&cad=&nt=navigate&ei=d2sMY5W8FqK81sQP6IGHsAc&zx=1661758333078", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=AEC;OGPC;OGP;NID;DV;1P_JAR", END_INLINE,
            "URL=https://www.gstatic.com/_/mss/boq-search/_/js/k=boq-search.VisualFrontendUi.en_US.1kEbd_bn2wM.es5.O/ck=boq-search.VisualFrontendUi.vvMlSvQKo4Q.L.B1.O/am=gAfvESgOgBD_EYJAAACADpAAQSQCMMUDTAAAKkEtUQAHKEBwSAAIAAIIDAIAAAC4AGAAAICNgAEAAAAAAIV3HhgIAAAAAAAAAACAUMUAAAAAAAAAANBEAAAAAAAE/d=1/exm=A7fCU,BVgquf,BXWsfc,COQbmf,ClUoee,EEDORb,EFQ78c,G2GqHe,GihOkd,HU2IR,I46Hvd,IZT63,JNoxi,KG2eXe,KUM7Z,Kg1rBc,Ko78Df,L1AAkb,LEikZe,MI6k7c,Mlhmy,MpJwZc,Mpq4Ee,NwH0H,O1Gjze,OTA3Ae,OmgaI,PrPYRd,QIhFr,RMhBfe,Rr5NOe,S1avQ,S2r5lb,SRsBqc,SdcwHb,SpsfSb,U0aPgd,UUJqVe,Uas9Hd,Ulmmrd,V3dDOb,VwDzFe,WO9ee,Wf0Cmd,Wq6lxf,XVMNvd,Xn5N7c,ZfAoz,ZwDk9d,_b,_r,_tp,aIe7ef,aW3pY,aurFic,btdpvd,byfTOb,fKUV3e,fkGYQb,gychg,hKSk3e,hT8HDb,hc6Ubd,kWgXee,kjKdXe,lazG7b,lsjVmc,lwddkf,mI3LFb,mdR7q,n73qwf,nQze3d,oR20R,ovKuLd,pjICDe,pw70Gc,s39S4,sVEevc,w9hDv,ws9Tlc,xQtZb,xUdipf,xhIfAc,yDVVkb,zbML3c,zr1jrb/excm=_b,_r,_tp,unifiedviewerview/ed=1/wt=2/rs=AH7-fg6DOgWXiWX1uVyZ0HCNG9o5CEzZFA/ee=cEt90b:ws9Tlc;QGR0gd:Mlhmy;uY49fb:COQbmf;yxTchf:KUM7Z;qddgKe:xQtZb;dIoSBb:SpsfSb;EmZ2Bf:zr1jrb;EVNhjf:pw70Gc;NSEoX:lazG7b;qaS3gd:yiLg6e;zOsCQe:Ko78Df;WCEKNd:I46Hvd;xMUn6e:e0kzxe;fWLTFc:TVBJbf;flqRgb:ox2Q7c;pXdRYb:oR20R;oGtAuc:sOXFj;rQSrae:C6D5Fc;kCQyJ:ueyPK;EABSZ:MXZt9d;qavrXe:zQzcXe;TxfV6d:YORN0b;dtl0hd:lLQWFe;UDrY1c:eps46d;wQlYve:aLUfP;GleZL:J1A7Od;nKl0s:xxrckd;g8nkx:U4MzKc;JXS8fb:Qj0suc;sTsDMc:kHVSUb;w3bZCb:ZPGaIb;VGRfx:VFqbr;aAJE9c:WHW6Ef;imqimf:jKGL2e;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;F9mqte:UoRcbe;SLtqO:Kh1xYe;tosKvd:ZCqP3;WDGyFe:jcVOxd;VxQ32b:k0XsBb;DULqB:RKfG5c;Np8Qkd:Dpx6qc;bcPXSc:gSZLJb;aZ61od:arTwJ;cFTWae:gT8qnd;gaub4:TN6bMe;DpcR3d:zL72xf;hjRo6e:F62sG;BjwMce:cXX2Wb;Pjplud:EEDORb;io8t5d:yDVVkb;j7137d:KG2eXe;Oj465e:KG2eXe;ul9GGd:VDovNc;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;NPKaK:SdcwHb;nAFL3:s39S4;iFQyKf:QIhFr;zxnPse:SP0dJe;tGdRVe:oRqHk;yWysfe:pjcr8d;F774Sb:sVEevc;eBAeSb:sVEevc;VoYp5d:BXWsfc;Ti4hX:Y1W8Ad;vGrMZ:Y1W8Ad;zaIgPb:ovuoid;kbAm9d:MkHyGd;okUaUd:Kg1rBc;wV5Pjc:nQze3d;eHDfl:ofjVkb;SNUn3:ZwDk9d;LBgRLc:SdcwHb;wR5FRb:O1Gjze;vfVwPd:TWOpEe;w9w86d:aIe7ef;KQzWid:mB4wNe;pNsl2d:j9Yuyc;Nyt6ic:jn2sGd/m=oSegn", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/_/mss/boq-search/_/js/k=boq-search.VisualFrontendUi.en_US.1kEbd_bn2wM.es5.O/ck=boq-search.VisualFrontendUi.vvMlSvQKo4Q.L.B1.O/am=gAfvESgOgBD_EYJAAACADpAAQSQCMMUDTAAAKkEtUQAHKEBwSAAIAAIIDAIAAAC4AGAAAICNgAEAAAAAAIV3HhgIAAAAAAAAAACAUMUAAAAAAAAAANBEAAAAAAAE/d=1/exm=A7fCU,BVgquf,BXWsfc,COQbmf,ClUoee,EEDORb,EFQ78c,G2GqHe,GihOkd,HU2IR,I46Hvd,IZT63,JNoxi,KG2eXe,KUM7Z,Kg1rBc,Ko78Df,L1AAkb,LEikZe,MI6k7c,Mlhmy,MpJwZc,Mpq4Ee,NwH0H,O1Gjze,OTA3Ae,OmgaI,PrPYRd,QIhFr,RMhBfe,Rr5NOe,S1avQ,S2r5lb,SRsBqc,SdcwHb,SpsfSb,U0aPgd,UUJqVe,Uas9Hd,Ulmmrd,V3dDOb,VwDzFe,WO9ee,Wf0Cmd,Wq6lxf,XVMNvd,Xn5N7c,ZfAoz,ZwDk9d,_b,_r,_tp,aIe7ef,aW3pY,aurFic,btdpvd,byfTOb,fKUV3e,fkGYQb,gychg,hKSk3e,hT8HDb,hc6Ubd,kWgXee,kjKdXe,lazG7b,lsjVmc,lwddkf,mI3LFb,mdR7q,n73qwf,nQze3d,oR20R,oSegn,ovKuLd,pjICDe,pw70Gc,s39S4,sVEevc,w9hDv,ws9Tlc,xQtZb,xUdipf,xhIfAc,yDVVkb,zbML3c,zr1jrb/excm=_b,_r,_tp,unifiedviewerview/ed=1/wt=2/rs=AH7-fg6DOgWXiWX1uVyZ0HCNG9o5CEzZFA/ee=cEt90b:ws9Tlc;QGR0gd:Mlhmy;uY49fb:COQbmf;yxTchf:KUM7Z;qddgKe:xQtZb;dIoSBb:SpsfSb;EmZ2Bf:zr1jrb;EVNhjf:pw70Gc;NSEoX:lazG7b;qaS3gd:yiLg6e;zOsCQe:Ko78Df;WCEKNd:I46Hvd;xMUn6e:e0kzxe;fWLTFc:TVBJbf;flqRgb:ox2Q7c;pXdRYb:oR20R;oGtAuc:sOXFj;rQSrae:C6D5Fc;kCQyJ:ueyPK;EABSZ:MXZt9d;qavrXe:zQzcXe;TxfV6d:YORN0b;dtl0hd:lLQWFe;UDrY1c:eps46d;wQlYve:aLUfP;GleZL:J1A7Od;nKl0s:xxrckd;g8nkx:U4MzKc;JXS8fb:Qj0suc;sTsDMc:kHVSUb;w3bZCb:ZPGaIb;VGRfx:VFqbr;aAJE9c:WHW6Ef;imqimf:jKGL2e;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;F9mqte:UoRcbe;SLtqO:Kh1xYe;tosKvd:ZCqP3;WDGyFe:jcVOxd;VxQ32b:k0XsBb;DULqB:RKfG5c;Np8Qkd:Dpx6qc;bcPXSc:gSZLJb;aZ61od:arTwJ;cFTWae:gT8qnd;gaub4:TN6bMe;DpcR3d:zL72xf;hjRo6e:F62sG;BjwMce:cXX2Wb;Pjplud:EEDORb;io8t5d:yDVVkb;j7137d:KG2eXe;Oj465e:KG2eXe;ul9GGd:VDovNc;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;NPKaK:SdcwHb;nAFL3:s39S4;iFQyKf:QIhFr;zxnPse:SP0dJe;tGdRVe:oRqHk;yWysfe:pjcr8d;F774Sb:sVEevc;eBAeSb:sVEevc;VoYp5d:BXWsfc;Ti4hX:Y1W8Ad;vGrMZ:Y1W8Ad;zaIgPb:ovuoid;kbAm9d:MkHyGd;okUaUd:Kg1rBc;wV5Pjc:nQze3d;eHDfl:ofjVkb;SNUn3:ZwDk9d;LBgRLc:SdcwHb;wR5FRb:O1Gjze;vfVwPd:TWOpEe;w9w86d:aIe7ef;KQzWid:mB4wNe;pNsl2d:j9Yuyc;Nyt6ic:jn2sGd/m=iaRXBb", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/_/mss/boq-search/_/js/k=boq-search.VisualFrontendUi.en_US.1kEbd_bn2wM.es5.O/ck=boq-search.VisualFrontendUi.vvMlSvQKo4Q.L.B1.O/am=gAfvESgOgBD_EYJAAACADpAAQSQCMMUDTAAAKkEtUQAHKEBwSAAIAAIIDAIAAAC4AGAAAICNgAEAAAAAAIV3HhgIAAAAAAAAAACAUMUAAAAAAAAAANBEAAAAAAAE/d=1/exm=A7fCU,BVgquf,BXWsfc,COQbmf,ClUoee,EEDORb,EFQ78c,G2GqHe,GihOkd,HU2IR,I46Hvd,IZT63,JNoxi,KG2eXe,KUM7Z,Kg1rBc,Ko78Df,L1AAkb,LEikZe,MI6k7c,Mlhmy,MpJwZc,Mpq4Ee,NwH0H,O1Gjze,OTA3Ae,OmgaI,PrPYRd,QIhFr,RMhBfe,Rr5NOe,S1avQ,S2r5lb,SRsBqc,SdcwHb,SpsfSb,U0aPgd,UUJqVe,Uas9Hd,Ulmmrd,V3dDOb,VwDzFe,WO9ee,Wf0Cmd,Wq6lxf,XVMNvd,Xn5N7c,ZfAoz,ZwDk9d,_b,_r,_tp,aIe7ef,aW3pY,aurFic,btdpvd,byfTOb,fKUV3e,fkGYQb,gychg,hKSk3e,hT8HDb,hc6Ubd,iaRXBb,kWgXee,kjKdXe,lazG7b,lsjVmc,lwddkf,mI3LFb,mdR7q,n73qwf,nQze3d,oR20R,oSegn,ovKuLd,pjICDe,pw70Gc,s39S4,sVEevc,w9hDv,ws9Tlc,xQtZb,xUdipf,xhIfAc,yDVVkb,zbML3c,zr1jrb/excm=_b,_r,_tp,unifiedviewerview/ed=1/wt=2/rs=AH7-fg6DOgWXiWX1uVyZ0HCNG9o5CEzZFA/ee=cEt90b:ws9Tlc;QGR0gd:Mlhmy;uY49fb:COQbmf;yxTchf:KUM7Z;qddgKe:xQtZb;dIoSBb:SpsfSb;EmZ2Bf:zr1jrb;EVNhjf:pw70Gc;NSEoX:lazG7b;qaS3gd:yiLg6e;zOsCQe:Ko78Df;WCEKNd:I46Hvd;xMUn6e:e0kzxe;fWLTFc:TVBJbf;flqRgb:ox2Q7c;pXdRYb:oR20R;oGtAuc:sOXFj;rQSrae:C6D5Fc;kCQyJ:ueyPK;EABSZ:MXZt9d;qavrXe:zQzcXe;TxfV6d:YORN0b;dtl0hd:lLQWFe;UDrY1c:eps46d;wQlYve:aLUfP;GleZL:J1A7Od;nKl0s:xxrckd;g8nkx:U4MzKc;JXS8fb:Qj0suc;sTsDMc:kHVSUb;w3bZCb:ZPGaIb;VGRfx:VFqbr;aAJE9c:WHW6Ef;imqimf:jKGL2e;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;F9mqte:UoRcbe;SLtqO:Kh1xYe;tosKvd:ZCqP3;WDGyFe:jcVOxd;VxQ32b:k0XsBb;DULqB:RKfG5c;Np8Qkd:Dpx6qc;bcPXSc:gSZLJb;aZ61od:arTwJ;cFTWae:gT8qnd;gaub4:TN6bMe;DpcR3d:zL72xf;hjRo6e:F62sG;BjwMce:cXX2Wb;Pjplud:EEDORb;io8t5d:yDVVkb;j7137d:KG2eXe;Oj465e:KG2eXe;ul9GGd:VDovNc;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;NPKaK:SdcwHb;nAFL3:s39S4;iFQyKf:QIhFr;zxnPse:SP0dJe;tGdRVe:oRqHk;yWysfe:pjcr8d;F774Sb:sVEevc;eBAeSb:sVEevc;VoYp5d:BXWsfc;Ti4hX:Y1W8Ad;vGrMZ:Y1W8Ad;zaIgPb:ovuoid;kbAm9d:MkHyGd;okUaUd:Kg1rBc;wV5Pjc:nQze3d;eHDfl:ofjVkb;SNUn3:ZwDk9d;LBgRLc:SdcwHb;wR5FRb:O1Gjze;vfVwPd:TWOpEe;w9w86d:aIe7ef;KQzWid:mB4wNe;pNsl2d:j9Yuyc;Nyt6ic:jn2sGd/m=Wt6vjf,hhhU8,FCpbqb,WhJNk", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("gen_204_16", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_17");
    ns_web_url ("gen_204_17",
        "URL=https://www.google.com/gen_204?ei=d2sMY5W8FqK81sQP6IGHsAc&s=async&atyp=csi&astyp=irc&rt=st.3772%2Caaft.3772%2Cacrt.3772%2Cart.3772",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=AEC;OGPC;OGP;NID;DV;1P_JAR",
        INLINE_URLS,
            "URL=https://www.gstatic.com/_/mss/boq-search/_/js/k=boq-search.VisualFrontendUi.en_US.1kEbd_bn2wM.es5.O/ck=boq-search.VisualFrontendUi.vvMlSvQKo4Q.L.B1.O/am=gAfvESgOgBD_EYJAAACADpAAQSQCMMUDTAAAKkEtUQAHKEBwSAAIAAIIDAIAAAC4AGAAAICNgAEAAAAAAIV3HhgIAAAAAAAAAACAUMUAAAAAAAAAANBEAAAAAAAE/d=1/exm=A7fCU,BVgquf,BXWsfc,COQbmf,ClUoee,EEDORb,EFQ78c,FCpbqb,G2GqHe,GihOkd,HU2IR,I46Hvd,IZT63,JNoxi,KG2eXe,KUM7Z,Kg1rBc,Ko78Df,L1AAkb,LEikZe,MI6k7c,Mlhmy,MpJwZc,Mpq4Ee,NwH0H,O1Gjze,OTA3Ae,OmgaI,PrPYRd,QIhFr,RMhBfe,Rr5NOe,S1avQ,S2r5lb,SRsBqc,SdcwHb,SpsfSb,U0aPgd,UUJqVe,Uas9Hd,Ulmmrd,V3dDOb,VwDzFe,WO9ee,Wf0Cmd,WhJNk,Wq6lxf,Wt6vjf,XVMNvd,Xn5N7c,ZfAoz,ZwDk9d,_b,_r,_tp,aIe7ef,aW3pY,aurFic,btdpvd,byfTOb,fKUV3e,fkGYQb,gychg,hKSk3e,hT8HDb,hc6Ubd,hhhU8,iaRXBb,kWgXee,kjKdXe,lazG7b,lsjVmc,lwddkf,mI3LFb,mdR7q,n73qwf,nQze3d,oR20R,oSegn,ovKuLd,pjICDe,pw70Gc,s39S4,sVEevc,w9hDv,ws9Tlc,xQtZb,xUdipf,xhIfAc,yDVVkb,zbML3c,zr1jrb/excm=_b,_r,_tp,unifiedviewerview/ed=1/wt=2/rs=AH7-fg6DOgWXiWX1uVyZ0HCNG9o5CEzZFA/ee=cEt90b:ws9Tlc;QGR0gd:Mlhmy;uY49fb:COQbmf;yxTchf:KUM7Z;qddgKe:xQtZb;dIoSBb:SpsfSb;EmZ2Bf:zr1jrb;EVNhjf:pw70Gc;NSEoX:lazG7b;qaS3gd:yiLg6e;zOsCQe:Ko78Df;WCEKNd:I46Hvd;xMUn6e:e0kzxe;fWLTFc:TVBJbf;flqRgb:ox2Q7c;pXdRYb:oR20R;oGtAuc:sOXFj;rQSrae:C6D5Fc;kCQyJ:ueyPK;EABSZ:MXZt9d;qavrXe:zQzcXe;TxfV6d:YORN0b;dtl0hd:lLQWFe;UDrY1c:eps46d;wQlYve:aLUfP;GleZL:J1A7Od;nKl0s:xxrckd;g8nkx:U4MzKc;JXS8fb:Qj0suc;sTsDMc:kHVSUb;w3bZCb:ZPGaIb;VGRfx:VFqbr;aAJE9c:WHW6Ef;imqimf:jKGL2e;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;F9mqte:UoRcbe;SLtqO:Kh1xYe;tosKvd:ZCqP3;WDGyFe:jcVOxd;VxQ32b:k0XsBb;DULqB:RKfG5c;Np8Qkd:Dpx6qc;bcPXSc:gSZLJb;aZ61od:arTwJ;cFTWae:gT8qnd;gaub4:TN6bMe;DpcR3d:zL72xf;hjRo6e:F62sG;BjwMce:cXX2Wb;Pjplud:EEDORb;io8t5d:yDVVkb;j7137d:KG2eXe;Oj465e:KG2eXe;ul9GGd:VDovNc;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;NPKaK:SdcwHb;nAFL3:s39S4;iFQyKf:QIhFr;zxnPse:SP0dJe;tGdRVe:oRqHk;yWysfe:pjcr8d;F774Sb:sVEevc;eBAeSb:sVEevc;VoYp5d:BXWsfc;Ti4hX:Y1W8Ad;vGrMZ:Y1W8Ad;zaIgPb:ovuoid;kbAm9d:MkHyGd;okUaUd:Kg1rBc;wV5Pjc:nQze3d;eHDfl:ofjVkb;SNUn3:ZwDk9d;LBgRLc:SdcwHb;wR5FRb:O1Gjze;vfVwPd:TWOpEe;w9w86d:aIe7ef;KQzWid:mB4wNe;pNsl2d:j9Yuyc;Nyt6ic:jn2sGd/m=kHVSUb,LK4Pye,ECEkdf,ZfBJ7b", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/_/mss/boq-search/_/js/k=boq-search.VisualFrontendUi.en_US.1kEbd_bn2wM.es5.O/ck=boq-search.VisualFrontendUi.vvMlSvQKo4Q.L.B1.O/am=gAfvESgOgBD_EYJAAACADpAAQSQCMMUDTAAAKkEtUQAHKEBwSAAIAAIIDAIAAAC4AGAAAICNgAEAAAAAAIV3HhgIAAAAAAAAAACAUMUAAAAAAAAAANBEAAAAAAAE/d=1/exm=A7fCU,BVgquf,BXWsfc,COQbmf,ClUoee,ECEkdf,EEDORb,EFQ78c,FCpbqb,G2GqHe,GihOkd,HU2IR,I46Hvd,IZT63,JNoxi,KG2eXe,KUM7Z,Kg1rBc,Ko78Df,L1AAkb,LEikZe,LK4Pye,MI6k7c,Mlhmy,MpJwZc,Mpq4Ee,NwH0H,O1Gjze,OTA3Ae,OmgaI,PrPYRd,QIhFr,RMhBfe,Rr5NOe,S1avQ,S2r5lb,SRsBqc,SdcwHb,SpsfSb,U0aPgd,UUJqVe,Uas9Hd,Ulmmrd,V3dDOb,VwDzFe,WO9ee,Wf0Cmd,WhJNk,Wq6lxf,Wt6vjf,XVMNvd,Xn5N7c,ZfAoz,ZfBJ7b,ZwDk9d,_b,_r,_tp,aIe7ef,aW3pY,aurFic,btdpvd,byfTOb,fKUV3e,fkGYQb,gychg,hKSk3e,hT8HDb,hc6Ubd,hhhU8,iaRXBb,kHVSUb,kWgXee,kjKdXe,lazG7b,lsjVmc,lwddkf,mI3LFb,mdR7q,n73qwf,nQze3d,oR20R,oSegn,ovKuLd,pjICDe,pw70Gc,s39S4,sVEevc,w9hDv,ws9Tlc,xQtZb,xUdipf,xhIfAc,yDVVkb,zbML3c,zr1jrb/excm=_b,_r,_tp,unifiedviewerview/ed=1/wt=2/rs=AH7-fg6DOgWXiWX1uVyZ0HCNG9o5CEzZFA/ee=cEt90b:ws9Tlc;QGR0gd:Mlhmy;uY49fb:COQbmf;yxTchf:KUM7Z;qddgKe:xQtZb;dIoSBb:SpsfSb;EmZ2Bf:zr1jrb;EVNhjf:pw70Gc;NSEoX:lazG7b;qaS3gd:yiLg6e;zOsCQe:Ko78Df;WCEKNd:I46Hvd;xMUn6e:e0kzxe;fWLTFc:TVBJbf;flqRgb:ox2Q7c;pXdRYb:oR20R;oGtAuc:sOXFj;rQSrae:C6D5Fc;kCQyJ:ueyPK;EABSZ:MXZt9d;qavrXe:zQzcXe;TxfV6d:YORN0b;dtl0hd:lLQWFe;UDrY1c:eps46d;wQlYve:aLUfP;GleZL:J1A7Od;nKl0s:xxrckd;g8nkx:U4MzKc;JXS8fb:Qj0suc;sTsDMc:kHVSUb;w3bZCb:ZPGaIb;VGRfx:VFqbr;aAJE9c:WHW6Ef;imqimf:jKGL2e;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;F9mqte:UoRcbe;SLtqO:Kh1xYe;tosKvd:ZCqP3;WDGyFe:jcVOxd;VxQ32b:k0XsBb;DULqB:RKfG5c;Np8Qkd:Dpx6qc;bcPXSc:gSZLJb;aZ61od:arTwJ;cFTWae:gT8qnd;gaub4:TN6bMe;DpcR3d:zL72xf;hjRo6e:F62sG;BjwMce:cXX2Wb;Pjplud:EEDORb;io8t5d:yDVVkb;j7137d:KG2eXe;Oj465e:KG2eXe;ul9GGd:VDovNc;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;NPKaK:SdcwHb;nAFL3:s39S4;iFQyKf:QIhFr;zxnPse:SP0dJe;tGdRVe:oRqHk;yWysfe:pjcr8d;F774Sb:sVEevc;eBAeSb:sVEevc;VoYp5d:BXWsfc;Ti4hX:Y1W8Ad;vGrMZ:Y1W8Ad;zaIgPb:ovuoid;kbAm9d:MkHyGd;okUaUd:Kg1rBc;wV5Pjc:nQze3d;eHDfl:ofjVkb;SNUn3:ZwDk9d;LBgRLc:SdcwHb;wR5FRb:O1Gjze;vfVwPd:TWOpEe;w9w86d:aIe7ef;KQzWid:mB4wNe;pNsl2d:j9Yuyc;Nyt6ic:jn2sGd/m=bm51tf", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"", "HEADER=sec-ch-ua-platform-version:\"10.0\"", "HEADER=sec-ch-ua-platform:\"Windows\"", "HEADER=X-Client-Data:CKGOywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("gen_204_17", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("log_8");
    ns_web_url ("log_8",
        "URL=https://www.google.com/log?format=json&hasfast=true",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=AEC;OGPC;OGP;NID;DV;1P_JAR",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en-US",null,"boq_visualfrontendserver_20220825.04_p0"]],241,[["1661758333139",null,[],null,null,null,null,"[1661758333135,[[\"2397168675742140944\",null,[[641,1]]],[\"16147638372540442232\",null,[[648,1]]],[\"14719340685975485085\",null,[[712,1]]],[\"17077408715954654437\",null,[[886,1]]],[\"3318688667027929436\",null,[[643,1]]],[\"5790177495296899286\",null,[[0,1]]],[\"16829267986558572790\",null,[[0,1]]],[\"16339156775003354937\",null,[[620,1]]],[\"749851692583976763\",null,[[22,1]]],[\"15419336178855610526\",null,[[623,1]]],[\"17276521865292187132\",null,[[1,1]]],[\"8257051839445688306\",null,[[886,1]]],[\"7792735449360349632\",null,[[886,1]]],[\"7094487270460551484\",null,[[4,1]]],[\"12563104964214410683\",null,[[170,1]]],[\"15605813632677093659\",null,[[36,1]]],[\"17914751415692637656\",null,[[4,1]]],[\"9797767207516844257\",null,[[0,1]]],[\"14906952326733574741\",null,[[1,1]]],[\"4891744519482609478\",null,[[0,1]]],[\"14307859671070593733\",null,[[0,1]]],[\"7494582641517049914\",null,[[1,1]]],[\"6667106912793420619\",null,[[0,1]]],[\"10118692516388306266\",null,[[0,1]]],[\"6342145065879578001\",null,[[3,1]]],[\"13596961294000664596\",null,[[170,1]]],[\"2107494750385856652\",null,[[48,1]]],[\"1309831198388189068\",null,[[3,1]]],[\"522022639063469804\",null,[[0,1]]],[\"4950535922500196698\",null,[[1048,1]]],[\"1757184925777806825\",null,[[40,1]]],[\"3079121564595244695\",null,[[29,1]]],[\"10652791942255425261\",null,[[5107,1]]],[\"4132870161583308123\",null,[[0,1]]]],null,null,\"[1,\\\"T1JGue_PL\\\",\\\"emsMY4yiHqf84-EPvuOswAs\\\"]\"]",null,null,2,1258519684,null,null,-19800,[null,[],null,"[[],[],[1763433,1772879,45814370,47977019],[]]"],null,null,null,[],1,null,null,null,null,null,[]]],"1661758333139",[]]",
        BODY_END
    );

    ns_end_transaction("log_8", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("log_9");
    ns_web_url ("log_9",
        "URL=https://www.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=AEC;OGPC;OGP;NID;DV;OTZ;1P_JAR",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en-US",null,"boq_visualfrontendserver_20220825.04_p0"]],1600,[["1661758333103",null,[],null,null,null,null,"[null,null,null,null,null,null,null,\"emsMY4yiHqf84-EPvuOswAs\"]",null,null,5,null,null,null,-19800,null,null,null,null,[],1,null,null,"[[[1661758333101000,0,0],1],[[124952,null,null,[],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{\"273\":[true]}]],[[1661758330495884,104398375,3087741374]],[null,null,null,null,null,null,null,null,null,null,null,null,null,[]]]",null,null,[]]],"1661758343117",[]]",
        BODY_END
    );

    ns_end_transaction("log_9", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_18");
    ns_web_url ("gen_204_18",
        "URL=https://www.google.com/gen_204?atyp=i&ei=d2sMY5W8FqK81sQP6IGHsAc&ct=slh&v=t1&pv=0.6826322022990636&me=69:1661758331767,R,1,CAIQBg,480,85,103,45:0,R,1,CAIQNA,180,182,652,2771:0,R,1,CBUQAA,180,182,652,172:0,R,1,CA8QAA,180,284,548,43:0,R,1,CAMQAA,180,398,652,801:0,R,1,CAcQAA,180,426,652,351:0,R,1,CAQQAA,180,467,326,297:0,R,1,CAYQAA,507,467,326,143:0,R,1,CAUQAA,507,611,326,153:0,R,1,CAEQAQ,931,176,369,3370:0,R,1,CGYQAA,932,182,456,1372:0,R,1,CHUQAA,933,183,454,250:0,R,1,CHoQAA,933,183,454,160:0,R,1,CHoQAQ,933,183,454,160:0,R,1,CHkQAA,933,355,454,62:0,R,1,CGYQAw,933,447,454,1070:0,R,1,CGsQAA,933,447,454,1070:0,R,1,CHwQAA,933,447,454,272:0,R,1,CHsQAA,933,447,454,272:0,R,1,CG8QAA,933,447,454,151:0,R,1,CG8QAQ,948,447,424,151:0,R,1,CHcQAA,933,611,454,22:0,R,1,CG4QAA,933,639,454,22:0,R,1,CGwQAA,933,668,454,22:0,R,1,CG0QAA,933,697,454,22:1782,h,1,CAIQBg,i:16,h,1,CAIQBg,o:1,h,1,CAIQBQ,i:18,h,1,CAIQBQ,o:12704,e,B&zx=1661758346288",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.93\"",
        "HEADER=sec-ch-ua-platform-version:\"10.0\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Windows\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CKGOywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=AEC;OGPC;OGP;NID;DV;OTZ;1P_JAR"
    );

    ns_end_transaction("gen_204_18", NS_AUTO_STATUS);
    ns_page_think_time(3.027);

}
