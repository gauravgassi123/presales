--Request 
GET http://www.google.com/
Host: www.google.com
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Accept-Encoding: gzip, deflate
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 302 Found
Location: https://www.google.com/?gws_rd=ssl
Cache-Control: private
Content-Type: text/html; charset=UTF-8
BFCache-Opt-In: unload
Date: Mon, 29 Aug 2022 07:31:53 GMT
Server: gws
Content-Length: 231
X-XSS-Protection: 0
X-Frame-Options: SAMEORIGIN
Set-Cookie: 1P_JAR=2022-08-29-07; expires=Wed, 28-Sep-2022 07:31:53 GMT; path=/; domain=.google.com; Secure; SameSite=none
Set-Cookie: AEC=AakniGNF76dqoEarKvXVNM4iZEdKJXTRnkaUH6KqkIeVFmCVonN1-MYCBw; expires=Sat, 25-Feb-2023 07:31:53 GMT; path=/; domain=.google.com; Secure; HttpOnly; SameSite=lax
----
--Request 
GET https://www.google.com/?gws_rd=ssl
Host: www.google.com
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: none
Sec-Fetch-Mode: navigate
Sec-Fetch-User: ?1
Sec-Fetch-Dest: document
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
date: Mon, 29 Aug 2022 07:31:54 GMT
expires: -1
cache-control: private, max-age=0
content-type: text/html; charset=UTF-8
strict-transport-security: max-age=31536000
accept-ch: Sec-CH-UA-Platform
accept-ch: Sec-CH-UA-Platform-Version
accept-ch: Sec-CH-UA-Full-Version
accept-ch: Sec-CH-UA-Arch
accept-ch: Sec-CH-UA-Model
accept-ch: Sec-CH-UA-Bitness
accept-ch: Sec-CH-UA-Full-Version-List
accept-ch: Sec-CH-UA-WoW64
bfcache-opt-in: unload
p3p: CP=\"This is not a P3P policy! See g.co/p3phelp for more info.\"
content-encoding: br
server: gws
content-length: 44687
x-xss-protection: 0
x-frame-options: SAMEORIGIN
set-cookie: 1P_JAR=2022-08-29-07; expires=Wed, 28-Sep-2022 07:31:54 GMT; path=/; domain=.google.com; Secure; SameSite=none
set-cookie: AEC=AakniGOHU8jlkBpkm8pxovYIbhg_lABjr31upBZWIBiZEjtihXngvxLshg; expires=Sat, 25-Feb-2023 07:31:54 GMT; path=/; domain=.google.com; Secure; HttpOnly; SameSite=lax
set-cookie: NID=511=QpQpJ_2FohznYHV8m5wICy5dXY_8jibyZwxNpsCFbZcHD-V2LhuEB_dUSxjsX6KFHZajSZRFec3U4TDpVVNSngkaF7TJvJ9JbkzNUVYSGsC9m8WGgAN_BFX08tPw8Yys5iV6LU_aO3FvSThiiF53S9ScjPzHUMcwRdgPkobtxTE; expires=Tue, 28-Feb-2023 07:31:54 GMT; path=/; domain=.google.com; Secure; HttpOnly; SameSite=none
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://www.google.com/xjs/_/js/k=xjs.s.en_GB.0RwjdeQGgkw.O/am=AIACgQDwAgAIAEBmAAEAAAAAAAAAAIYAIPGUEwAADBACAjkJAAAAACRMiAAAYAAgBDAQhAAAAAA-Mm8AAv4MAAw04QIAAAAAAAAACOASBAM3SBQEAAIAAAAAACCspq45AAVB/d=1/ed=1/dg=2/br=1/rs=ACT90oGHS51U1Lr1d4VDpqSB1aT3JzqfiQ/m=cdos,dpf,hsm,jsa,d,csi
Host: www.google.com
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36
sec-ch-ua-arch: \"x86\"
sec-ch-ua-full-version: \"90.0.4430.93\"
sec-ch-ua-platform-version: \"10.0\"
sec-ch-ua-platform: \"Windows\"
Accept: */*
X-Client-Data: CKGOywE=
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.google.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
Cookie: 1P_JAR=2022-08-29-07; AEC=AakniGOHU8jlkBpkm8pxovYIbhg_lABjr31upBZWIBiZEjtihXngvxLshg; NID=511=QpQpJ_2FohznYHV8m5wICy5dXY_8jibyZwxNpsCFbZcHD-V2LhuEB_dUSxjsX6KFHZajSZRFec3U4TDpVVNSngkaF7TJvJ9JbkzNUVYSGsC9m8WGgAN_BFX08tPw8Yys5iV6LU_aO3FvSThiiF53S9ScjPzHUMcwRdgPkobtxTE
----
--Response 
HTTP/1.1 200
accept-ranges: bytes
vary: Accept-Encoding, Origin
content-encoding: br
content-security-policy-report-only: require-trusted-types-for 'script'; report-uri https://csp.withgoogle.com/csp/gws-team
cross-origin-resource-policy: cross-origin
cross-origin-opener-policy: same-origin; report-to=\"gws-team\"
report-to: {\"group\":\"gws-team\",\"max_age\":2592000,\"endpoints\":[{\"url\":\"https://csp.withgoogle.com/csp/report-to/gws-team\"}]}
content-length: 271229
x-content-type-options: nosniff
server: sffe
x-xss-protection: 0
date: Fri, 26 Aug 2022 21:54:23 GMT
expires: Sat, 26 Aug 2023 21:54:23 GMT
cache-control: public, max-age=31536000
last-modified: Fri, 26 Aug 2022 21:31:36 GMT
content-type: text/javascript; charset=UTF-8
age: 207451
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png
Host: www.google.com
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36
sec-ch-ua-arch: \"x86\"
sec-ch-ua-full-version: \"90.0.4430.93\"
sec-ch-ua-platform-version: \"10.0\"
sec-ch-ua-platform: \"Windows\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
X-Client-Data: CKGOywE=
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.google.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
Cookie: 1P_JAR=2022-08-29-07; AEC=AakniGOHU8jlkBpkm8pxovYIbhg_lABjr31upBZWIBiZEjtihXngvxLshg; NID=511=QpQpJ_2FohznYHV8m5wICy5dXY_8jibyZwxNpsCFbZcHD-V2LhuEB_dUSxjsX6KFHZajSZRFec3U4TDpVVNSngkaF7TJvJ9JbkzNUVYSGsC9m8WGgAN_BFX08tPw8Yys5iV6LU_aO3FvSThiiF53S9ScjPzHUMcwRdgPkobtxTE
----
--Response 
HTTP/1.1 200
accept-ranges: bytes
content-type: image/png
cross-origin-resource-policy: cross-origin
cross-origin-opener-policy-report-only: same-origin; report-to=\"static-on-bigtable\"
report-to: {\"group\":\"static-on-bigtable\",\"max_age\":2592000,\"endpoints\":[{\"url\":\"https://csp.withgoogle.com/csp/report-to/static-on-bigtable\"}]}
content-length: 5969
date: Mon, 29 Aug 2022 07:31:55 GMT
expires: Mon, 29 Aug 2022 07:31:55 GMT
cache-control: private, max-age=31536000
last-modified: Tue, 22 Oct 2019 18:30:00 GMT
x-content-type-options: nosniff
server: sffe
x-xss-protection: 0
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://www.google.com/images/searchbox/desktop_searchbox_sprites318_hr.webp
Host: www.google.com
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36
sec-ch-ua-arch: \"x86\"
sec-ch-ua-full-version: \"90.0.4430.93\"
sec-ch-ua-platform-version: \"10.0\"
sec-ch-ua-platform: \"Windows\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
X-Client-Data: CKGOywE=
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.google.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
Cookie: 1P_JAR=2022-08-29-07; AEC=AakniGOHU8jlkBpkm8pxovYIbhg_lABjr31upBZWIBiZEjtihXngvxLshg; NID=511=QpQpJ_2FohznYHV8m5wICy5dXY_8jibyZwxNpsCFbZcHD-V2LhuEB_dUSxjsX6KFHZajSZRFec3U4TDpVVNSngkaF7TJvJ9JbkzNUVYSGsC9m8WGgAN_BFX08tPw8Yys5iV6LU_aO3FvSThiiF53S9ScjPzHUMcwRdgPkobtxTE
----
--Response 
HTTP/1.1 200
accept-ranges: bytes
content-type: image/webp
cross-origin-resource-policy: cross-origin
cross-origin-opener-policy-report-only: same-origin; report-to=\"static-on-bigtable\"
report-to: {\"group\":\"static-on-bigtable\",\"max_age\":2592000,\"endpoints\":[{\"url\":\"https://csp.withgoogle.com/csp/report-to/static-on-bigtable\"}]}
content-length: 660
date: Mon, 29 Aug 2022 07:31:55 GMT
expires: Mon, 29 Aug 2022 07:31:55 GMT
cache-control: private, max-age=31536000
last-modified: Wed, 22 Apr 2020 22:00:00 GMT
x-content-type-options: nosniff
server: sffe
x-xss-protection: 0
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://www.gstatic.com/og/_/js/k=og.qtm.en_US.-MMKP3uG9VU.O/rt=j/m=qabr,q_d,qcwid,qapid,qald/exm=qaaw,qadd,qaid,qein,qhaw,qhbr,qhch,qhga,qhid,qhin,qhpr/d=1/ed=1/rs=AA2YrTuS6iZfrsnE7GApv0RWeBgl21VxSA
Host: www.gstatic.com
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36
Accept: */*
X-Client-Data: CKGOywE=
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.google.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
accept-ranges: bytes
vary: Accept-Encoding, Origin
content-encoding: gzip
content-security-policy-report-only: require-trusted-types-for 'script'; report-uri https://csp.withgoogle.com/csp/one-google-eng
cross-origin-resource-policy: cross-origin
cross-origin-opener-policy: same-origin; report-to=\"one-google-eng\"
report-to: {\"group\":\"one-google-eng\",\"max_age\":2592000,\"endpoints\":[{\"url\":\"https://csp.withgoogle.com/csp/report-to/one-google-eng\"}]}
content-length: 72356
x-content-type-options: nosniff
server: sffe
x-xss-protection: 0
date: Wed, 24 Aug 2022 06:31:42 GMT
expires: Thu, 24 Aug 2023 06:31:42 GMT
cache-control: public, max-age=31536000
last-modified: Sat, 20 Aug 2022 01:33:02 GMT
content-type: text/javascript; charset=UTF-8
age: 435613
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://www.gstatic.com/og/_/ss/k=og.qtm.tM-BeBGBME0.L.W.O/m=qcwid/excm=qaaw,qadd,qaid,qein,qhaw,qhbr,qhch,qhga,qhid,qhin,qhpr/d=1/ed=1/ct=zgms/rs=AA2YrTtmhrLYVlm7jdu3TI1ROywA3arJnA
Host: www.gstatic.com
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36
Accept: text/css,*/*;q=0.1
X-Client-Data: CKGOywE=
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: style
Referer: https://www.google.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
accept-ranges: bytes
vary: Accept-Encoding, Origin
content-encoding: gzip
content-security-policy-report-only: require-trusted-types-for 'script'; report-uri https://csp.withgoogle.com/csp/one-google-eng
cross-origin-resource-policy: cross-origin
cross-origin-opener-policy: same-origin; report-to=\"one-google-eng\"
report-to: {\"group\":\"one-google-eng\",\"max_age\":2592000,\"endpoints\":[{\"url\":\"https://csp.withgoogle.com/csp/report-to/one-google-eng\"}]}
content-length: 274
x-content-type-options: nosniff
server: sffe
x-xss-protection: 0
date: Thu, 25 Aug 2022 17:48:17 GMT
expires: Fri, 25 Aug 2023 17:48:17 GMT
cache-control: public, max-age=31536000
last-modified: Sat, 06 Aug 2022 01:45:08 GMT
content-type: text/css; charset=UTF-8
age: 308618
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----

