/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: Apoorva
    Date of recording: 08/29/2022 01:02:26
    Flow details:
    Build details: 4.8.0 (build# 129)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

int exit_script()
{
    return 0;
}
