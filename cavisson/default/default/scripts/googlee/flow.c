/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: 
    Date of recording: 09/01/2022 04:56:45
    Flow details:
    Build details: 
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("index");
    ns_web_url ("index",
        "URL=http://www.google.com/",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "REDIRECT=YES",
        "LOCATION=https://www.google.com/?gws_rd=ssl",
        INLINE_URLS,
            "URL=https://www.google.com/?gws_rd=ssl", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_160x56dp.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/images/searchbox/searchbox_sprites317_hr.webp", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.gstatic.com/og/_/js/k=og.qtm.en_US.QnZ9a8JiZMA.O/rt=j/m=qabr,q_d,qcwid,qapid/exm=qaaw,qadd,qaid,qein,qhaw,qhbr,qhch,qhga,qhid,qhin,qhpr/d=1/ed=1/rs=AA2YrTvADb2b3YOT1HIT9E5uZMXxL-ZvJg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/og/_/ss/k=og.qtm.7bf7gt5wYew.L.W.O/m=qcwid/excm=qaaw,qadd,qaid,qein,qhaw,qhbr,qhch,qhga,qhid,qhin,qhpr/d=1/ed=1/ct=zgms/rs=AA2YrTsMOouFsNXkowFiK2s9CgFpc91yHA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/images/nav_logo325_hr.webp", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/xjs/_/js/k=xjs.qs.en_US.0LvOL4cYwZw.O/am=AIgCgQDgBQAYAAgQAIgAEAAAIAABAEAwAABIKMoRAgDCAMEAACAnAgAAACDhBAIAAAOAEICBIAQAAADw0ecNAA4KAI5KuAAAAAAAAAAADHAYghMLHgACgAAAAAAAAIxCFwkwAIIAAAQ/d=1/ed=1/dg=2/rs=ACT90oHdwBdo98BmWXWk5G-njLg0N5foEg/m=bct,cdos,hsm,jsa,mpf,qim,d,csi", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE
    );

    ns_end_transaction("index", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204");
    ns_web_url ("gen_204",
        "URL=https://www.google.com/gen_204?s=webhp&t=aft&atyp=csi&ei=czoQY7LWF-XgkPIPlIug-AY&rt=wsrt.424,aft.112,afti.112,prt.102&wh=473&imn=1&ima=1&imad=0&aftp=473&bl=PgnN",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID",
        INLINE_URLS,
            "URL=https://www.google.com/images/nav_logo325_hr.webp", END_INLINE,
            "URL=https://www.gstatic.com/og/_/ss/k=og.qtm.7bf7gt5wYew.L.W.O/m=qcwid/excm=qaaw,qadd,qaid,qein,qhaw,qhbr,qhch,qhga,qhid,qhin,qhpr/d=1/ed=1/ct=zgms/rs=AA2YrTsMOouFsNXkowFiK2s9CgFpc91yHA", END_INLINE,
            "URL=https://www.gstatic.com/og/_/js/k=og.qtm.en_US.QnZ9a8JiZMA.O/rt=j/m=qabr,q_d,qcwid,qapid/exm=qaaw,qadd,qaid,qein,qhaw,qhbr,qhch,qhga,qhid,qhin,qhpr/d=1/ed=1/rs=AA2YrTvADb2b3YOT1HIT9E5uZMXxL-ZvJg", END_INLINE,
            "URL=https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.IK5OmUURd2E.O/m=gapi_iframes,googleapis_client/rt=j/sv=1/d=1/ed=1/rs=AHpOoo932JinkSJHK92WgVjIV-Jwwyu3Rw/cb=gapi.loaded_0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/?gws_rd=ssl", END_INLINE
    );

    ns_end_transaction("gen_204", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_2");
    ns_web_url ("gen_204_2",
        "URL=https://www.google.com/gen_204?atyp=csi&ei=czoQY7LWF-XgkPIPlIug-AY&s=webhp&t=all&bl=PgnN&wh=473&imn=1&ima=1&imad=0&aftp=473&adh=&ime=1&imex=1&imeh=0&imea=0&imeb=0&imel=0&scp=0&mem=ujhs.10,tjhs.12,jhsl.4295,dm.8&sto=&sys=hc.8&rt=aft.112,prt.102,dcl.106,xjsls.112,afti.112,aftqf.114,xjses.302,xjsee.380,xjs.381,ol.384,wsrt.424,cst.25,dnst.0,rqst.80,rspt.7,sslt.24,rqstt.351,unt.322,cstt.326,dit.530&zx=1662007923836",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID",
        INLINE_URLS,
            "URL=https://www.google.com/gen_204?atyp=i&ct=psnt&cad=&nt=navigate&ei=czoQY7LWF-XgkPIPlIug-AY&zx=1662007923838", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE
    );

    ns_end_transaction("gen_204_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search");
    ns_web_url ("search",
        "URL=https://www.google.com/complete/search?q&cp=0&client=mobile-gws-wiz-hp&xssi=t&hl=en&authuser=0&psi=czoQY7LWF-XgkPIPlIug-AY.1662007923876&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID",
        INLINE_URLS,
            "URL=https://www.google.com/xjs/_/js/k=xjs.qs.en_US.0LvOL4cYwZw.O/ck=xjs.qs.ZusRdtOd5GY.L.W.O/am=AIgCgQDgBQAYAAgQAIgAEAAAIAABAEAwAABIKMoRAgDCAMEAACAnAgAAACDhBAIAAAOAEICBIAQAAADw0ecNAA4KAI5KuAAAAAAAAAAADHAYghMLHgACgAAAAAAAAIxCFwkwAIIAAAQ/d=1/exm=bct,cdos,csi,d,hsm,jsa,mpf,qim/ed=1/dg=2/rs=ACT90oEYWNWhyXkCzGaErXOmFnpPnHfnYA/ee=Pjplud:PoEs9b;QGR0gd:Mlhmy;uY49fb:COQbmf;EVNhjf:pw70Gc;oUlnpc:RagDlc;dtl0hd:lLQWFe;sTsDMc:kHVSUb;dIoSBb:ZgGg9b;pXdRYb:JKoKVe;wR5FRb:TtcOte;yGxLoc:FmAr0c;g8nkx:U4MzKc;KpRAue:Tia57b;daB6be:lMxGPd;Fmv9Nc:O1Tzwc;hK67qb:QWEO5b;jVtPve:wQ95P;BMxAGc:E5bFse;R4IIIb:QWfeKf;wQlYve:aLUfP;G6wU6e:hezEbd;OvePtd:McrICe;fAO5td:yzxsuf;SJsSc:H1GVub;SMDL4c:fTfGO;oSUNyd:fTfGO;zxnPse:GkRiKb;oGtAuc:sOXFj;zOsCQe:Ko78Df;WCEKNd:I46Hvd;EmZ2Bf:zr1jrb;NPKaK:PVlQOd;LBgRLc:XVMNvd;kbAm9d:MkHyGd;UyG7Kb:wQd0G;LsNahb:ucGLNb;rQSrae:C6D5Fc;kCQyJ:ueyPK;EABSZ:MXZt9d;qavrXe:zQzcXe;TxfV6d:YORN0b;UDrY1c:eps46d;GleZL:J1A7Od;nKl0s:xxrckd;JXS8fb:Qj0suc;qaS3gd:yiLg6e;NSEoX:lazG7b;w3bZCb:ZPGaIb;VGRfx:VFqbr;aAJE9c:WHW6Ef;imqimf:jKGL2e;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;SLtqO:Kh1xYe;tosKvd:ZCqP3;WDGyFe:jcVOxd;VxQ32b:k0XsBb;DULqB:RKfG5c;Np8Qkd:Dpx6qc;bcPXSc:gSZLJb;aZ61od:arTwJ;cFTWae:gT8qnd;gaub4:TN6bMe;DpcR3d:zL72xf;hjRo6e:F62sG;BjwMce:cXX2Wb;CUcugf:BIaADc;qddgKe:x4FYXe;xbe2wc:uRMPBc;eBAeSb:Ck63tb;lkq0A:Z0MWEf;eHDfl:ofjVkb;SNUn3:x8cHvb;LEikZe:byfTOb,lsjVmc;io8t5d:sgY6Zb;j7137d:KG2eXe;Oj465e:KG2eXe;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;nAFL3:s39S4;iFQyKf:QIhFr;whEZac:F4AmNb;vfVwPd:OXTqFb;w9w86d:dt4g2b;KQzWid:mB4wNe;pNsl2d:j9Yuyc;F9mqte:rlHKFc;Nyt6ic:jn2sGd/m=Bevgab,EkevXb,KbYvUc,OPwjEf,aBr2Mc,aa,abd,async,dvl,fKZehd,foot,mu,pFsdhd,pHXghd,sb_wiz,sf,sonic,spch?xjs=s1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/client_204?&atyp=i&biw=790&bih=473&ei=czoQY7LWF-XgkPIPlIug-AY", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.gstatic.com/navigationdrawer/home_icon.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/navigationdrawer/search_activity_icon.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/navigationdrawer/save_icon.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/navigationdrawer/settings_icon.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/navigationdrawer/privacy_advisor_icon.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/navigationdrawer/explicit_icon.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/navigationdrawer/how_search_works_icon.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/navigationdrawer/help_icon.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/navigationdrawer/feedback_icon.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("search", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("manifest");
    ns_web_url ("manifest",
        "URL=https://www.google.com/manifest?pwa=webhp",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID",
        INLINE_URLS,
            "URL=https://www.google.com/xjs/_/js/k=xjs.qs.en_US.0LvOL4cYwZw.O/ck=xjs.qs.ZusRdtOd5GY.L.W.O/am=AIgCgQDgBQAYAAgQAIgAEAAAIAABAEAwAABIKMoRAgDCAMEAACAnAgAAACDhBAIAAAOAEICBIAQAAADw0ecNAA4KAI5KuAAAAAAAAAAADHAYghMLHgACgAAAAAAAAIxCFwkwAIIAAAQ/d=1/exm=Bevgab,EkevXb,KbYvUc,OPwjEf,aBr2Mc,aa,abd,async,bct,cdos,csi,d,dvl,fKZehd,foot,hsm,jsa,mpf,mu,pFsdhd,pHXghd,qim,sb_wiz,sf,sonic,spch/ed=1/dg=2/rs=ACT90oEYWNWhyXkCzGaErXOmFnpPnHfnYA/ee=Pjplud:PoEs9b;QGR0gd:Mlhmy;uY49fb:COQbmf;EVNhjf:pw70Gc;oUlnpc:RagDlc;dtl0hd:lLQWFe;sTsDMc:kHVSUb;dIoSBb:ZgGg9b;pXdRYb:JKoKVe;wR5FRb:TtcOte;yGxLoc:FmAr0c;g8nkx:U4MzKc;KpRAue:Tia57b;daB6be:lMxGPd;Fmv9Nc:O1Tzwc;hK67qb:QWEO5b;jVtPve:wQ95P;BMxAGc:E5bFse;R4IIIb:QWfeKf;wQlYve:aLUfP;G6wU6e:hezEbd;OvePtd:McrICe;fAO5td:yzxsuf;SJsSc:H1GVub;SMDL4c:fTfGO;oSUNyd:fTfGO;zxnPse:GkRiKb;oGtAuc:sOXFj;zOsCQe:Ko78Df;WCEKNd:I46Hvd;EmZ2Bf:zr1jrb;NPKaK:PVlQOd;LBgRLc:XVMNvd;kbAm9d:MkHyGd;UyG7Kb:wQd0G;LsNahb:ucGLNb;rQSrae:C6D5Fc;kCQyJ:ueyPK;EABSZ:MXZt9d;qavrXe:zQzcXe;TxfV6d:YORN0b;UDrY1c:eps46d;GleZL:J1A7Od;nKl0s:xxrckd;JXS8fb:Qj0suc;qaS3gd:yiLg6e;NSEoX:lazG7b;w3bZCb:ZPGaIb;VGRfx:VFqbr;aAJE9c:WHW6Ef;imqimf:jKGL2e;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;SLtqO:Kh1xYe;tosKvd:ZCqP3;WDGyFe:jcVOxd;VxQ32b:k0XsBb;DULqB:RKfG5c;Np8Qkd:Dpx6qc;bcPXSc:gSZLJb;aZ61od:arTwJ;cFTWae:gT8qnd;gaub4:TN6bMe;DpcR3d:zL72xf;hjRo6e:F62sG;BjwMce:cXX2Wb;CUcugf:BIaADc;qddgKe:x4FYXe;xbe2wc:uRMPBc;eBAeSb:Ck63tb;lkq0A:Z0MWEf;eHDfl:ofjVkb;SNUn3:x8cHvb;LEikZe:byfTOb,lsjVmc;io8t5d:sgY6Zb;j7137d:KG2eXe;Oj465e:KG2eXe;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;nAFL3:s39S4;iFQyKf:QIhFr;whEZac:F4AmNb;vfVwPd:OXTqFb;w9w86d:dt4g2b;KQzWid:mB4wNe;pNsl2d:j9Yuyc;F9mqte:rlHKFc;Nyt6ic:jn2sGd/m=DPreE,KWMuje,L3vX2d,V23Ql,WlNQGd,kQvlef,lllQlf,nabPbb?xjs=s2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://adservice.google.com/adsid/google/ui", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/xjs/_/js/md=1/k=xjs.qs.en_US.0LvOL4cYwZw.O/am=AIgCgQDgBQAYAAgQAIgAEAAAIAABAEAwAABIKMoRAgDCAMEAACAnAgAAACDhBAIAAAOAEICBIAQAAADw0ecNAA4KAI5KuAAAAAAAAAAADHAYghMLHgACgAAAAAAAAIxCFwkwAIIAAAQ/rs=ACT90oHdwBdo98BmWXWk5G-njLg0N5foEg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/xjs/_/js/k=xjs.qs.en_US.0LvOL4cYwZw.O/ck=xjs.qs.ZusRdtOd5GY.L.W.O/am=AIgCgQDgBQAYAAgQAIgAEAAAIAABAEAwAABIKMoRAgDCAMEAACAnAgAAACDhBAIAAAOAEICBIAQAAADw0ecNAA4KAI5KuAAAAAAAAAAADHAYghMLHgACgAAAAAAAAIxCFwkwAIIAAAQ/d=1/exm=Bevgab,DPreE,EkevXb,KWMuje,KbYvUc,L3vX2d,OPwjEf,V23Ql,WlNQGd,aBr2Mc,aa,abd,async,bct,cdos,csi,d,dvl,fKZehd,foot,hsm,jsa,kQvlef,lllQlf,mpf,mu,nabPbb,pFsdhd,pHXghd,qim,sb_wiz,sf,sonic,spch/ed=1/dg=2/rs=ACT90oEYWNWhyXkCzGaErXOmFnpPnHfnYA/ee=Pjplud:PoEs9b;QGR0gd:Mlhmy;uY49fb:COQbmf;EVNhjf:pw70Gc;oUlnpc:RagDlc;dtl0hd:lLQWFe;sTsDMc:kHVSUb;dIoSBb:ZgGg9b;pXdRYb:JKoKVe;wR5FRb:TtcOte;yGxLoc:FmAr0c;g8nkx:U4MzKc;KpRAue:Tia57b;daB6be:lMxGPd;Fmv9Nc:O1Tzwc;hK67qb:QWEO5b;jVtPve:wQ95P;BMxAGc:E5bFse;R4IIIb:QWfeKf;wQlYve:aLUfP;G6wU6e:hezEbd;OvePtd:McrICe;fAO5td:yzxsuf;SJsSc:H1GVub;SMDL4c:fTfGO;oSUNyd:fTfGO;zxnPse:GkRiKb;oGtAuc:sOXFj;zOsCQe:Ko78Df;WCEKNd:I46Hvd;EmZ2Bf:zr1jrb;NPKaK:PVlQOd;LBgRLc:XVMNvd;kbAm9d:MkHyGd;UyG7Kb:wQd0G;LsNahb:ucGLNb;rQSrae:C6D5Fc;kCQyJ:ueyPK;EABSZ:MXZt9d;qavrXe:zQzcXe;TxfV6d:YORN0b;UDrY1c:eps46d;GleZL:J1A7Od;nKl0s:xxrckd;JXS8fb:Qj0suc;qaS3gd:yiLg6e;NSEoX:lazG7b;w3bZCb:ZPGaIb;VGRfx:VFqbr;aAJE9c:WHW6Ef;imqimf:jKGL2e;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;SLtqO:Kh1xYe;tosKvd:ZCqP3;WDGyFe:jcVOxd;VxQ32b:k0XsBb;DULqB:RKfG5c;Np8Qkd:Dpx6qc;bcPXSc:gSZLJb;aZ61od:arTwJ;cFTWae:gT8qnd;gaub4:TN6bMe;DpcR3d:zL72xf;hjRo6e:F62sG;BjwMce:cXX2Wb;CUcugf:BIaADc;qddgKe:x4FYXe;xbe2wc:uRMPBc;eBAeSb:Ck63tb;lkq0A:Z0MWEf;eHDfl:ofjVkb;SNUn3:x8cHvb;LEikZe:byfTOb,lsjVmc;io8t5d:sgY6Zb;j7137d:KG2eXe;Oj465e:KG2eXe;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;nAFL3:s39S4;iFQyKf:QIhFr;whEZac:F4AmNb;vfVwPd:OXTqFb;w9w86d:dt4g2b;KQzWid:mB4wNe;pNsl2d:j9Yuyc;F9mqte:rlHKFc;Nyt6ic:jn2sGd/m=aLUfP?xjs=s2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE
    );

    ns_end_transaction("manifest", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_2");
    ns_web_url ("search_2",
        "URL=https://www.google.com/complete/search?q&pq&cp=0&client=mweb-insp&xssi=t&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("search_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("log");
    ns_web_url ("log",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],1655,[["1662007923989",null,[],null,null,null,null,"[[[\"/client_streamz/location/location_prompt/prompt_decisions_count\",null,[\"decision\",\"browser\"],[[[[\"NO_PROMPT_NOT_GRANTED\"],[\"UNKNOWN\"]],[1]]],null,[]]]]",null,null,null,null,null,null,18000,null,null,null,null,[],1,null,null,null,null,null,[]]],"1662007923989",[]]",
        BODY_END
    );

    ns_end_transaction("log", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_3");
    ns_web_url ("gen_204_3",
        "URL=https://www.google.com/gen_204?atyp=i&ei=czoQY7LWF-XgkPIPlIug-AY&vet=10ahUKEwjy24z_5fL5AhVlMEQIHZQFCG8Q470GCB0..s&zx=1662007925345",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("gen_204_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_4");
    ns_web_url ("gen_204_4",
        "URL=https://www.google.com/gen_204?atyp=i&ei=czoQY7LWF-XgkPIPlIug-AY&vet=10ahUKEwjy24z_5fL5AhVlMEQIHZQFCG8Q470GCB0..s&zx=1662007925346",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("gen_204_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_5");
    ns_web_url ("gen_204_5",
        "URL=https://www.google.com/gen_204?atyp=i&ei=czoQY7LWF-XgkPIPlIug-AY&vet=10ahUKEwjy24z_5fL5AhVlMEQIHZQFCG8Q470GCB0..s&zx=1662007925347",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("gen_204_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_6");
    ns_web_url ("gen_204_6",
        "URL=https://www.google.com/gen_204?atyp=i&ei=czoQY7LWF-XgkPIPlIug-AY&vet=10ahUKEwjy24z_5fL5AhVlMEQIHZQFCG8QzckICB4..s&zx=1662007925348",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("gen_204_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_7");
    ns_web_url ("gen_204_7",
        "URL=https://www.google.com/gen_204?atyp=i&ei=czoQY7LWF-XgkPIPlIug-AY&vet=10ahUKEwjy24z_5fL5AhVlMEQIHZQFCG8Q470GCB0..s&zx=1662007925349",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("gen_204_7", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_8");
    ns_web_url ("gen_204_8",
        "URL=https://www.google.com/gen_204?atyp=i&ei=czoQY7LWF-XgkPIPlIug-AY&vet=10ahUKEwjy24z_5fL5AhVlMEQIHZQFCG8QzckICB4..s&zx=1662007925349",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("gen_204_8", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_9");
    ns_web_url ("gen_204_9",
        "URL=https://www.google.com/gen_204?atyp=i&ei=czoQY7LWF-XgkPIPlIug-AY&vet=10ahUKEwjy24z_5fL5AhVlMEQIHZQFCG8Q470GCB0..s&zx=1662007925350",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("gen_204_9", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_10");
    ns_web_url ("gen_204_10",
        "URL=https://www.google.com/gen_204?atyp=i&ei=czoQY7LWF-XgkPIPlIug-AY&vet=10ahUKEwjy24z_5fL5AhVlMEQIHZQFCG8QzckICB4..s&zx=1662007925351",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("gen_204_10", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_11");
    ns_web_url ("gen_204_11",
        "URL=https://www.google.com/gen_204?atyp=i&ei=czoQY7LWF-XgkPIPlIug-AY&vet=10ahUKEwjy24z_5fL5AhVlMEQIHZQFCG8Q470GCB0..s&zx=1662007925352",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("gen_204_11", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_12");
    ns_web_url ("gen_204_12",
        "URL=https://www.google.com/gen_204?atyp=csi&ei=czoQY7LWF-XgkPIPlIug-AY&s=jsa&jsi=qs,st.214548,t.0,at.11,et.focus,n.tKsYWe,cn.1,ie.0,vi.1&zx=1662008137583",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID",
        INLINE_URLS,
            "URL=https://www.google.com/?gws_rd=ssl#sbfbu=1&pi=", END_INLINE
    );

    ns_end_transaction("gen_204_12", NS_AUTO_STATUS);
    ns_page_think_time(216.978);

    //Page Auto split for Image Link 'Google' Clicked by User
    ns_start_transaction("trending_up_grey600_24dp_png");
    ns_web_url ("trending_up_grey600_24dp_png",
        "URL=https://www.gstatic.com/images/icons/material/system/2x/trending_up_grey600_24dp.png",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("trending_up_grey600_24dp_png", NS_AUTO_STATUS);
    ns_page_think_time(3.964);

    //Page Auto split for 
    ns_start_transaction("search_3");
    ns_web_url ("search_3",
        "URL=https://www.google.com/complete/search?q=c&cp=1&client=mobile-gws-wiz-hp&xssi=t&hl=en&authuser=0&psi=czoQY7LWF-XgkPIPlIug-AY.1662007923876&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID",
        INLINE_URLS,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR469b0dJHONqLfUoEA0-K13VEJ9oPMjaGfgTiPCjA&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSGrwtVjhAo5m9wyaJS7-2zPckEbBkdYQ7aYR1z4rY&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRe3TD--nWtRxdRlYxbXg3y9wZST49DlMwipHkjwnEYmu0EUJaykxM8&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMFkS2X8pAszF3SFzn4GWWq60GJnTH9b1joxYbuHY&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2t1Prl7H9snpKjDGt_pD_03GcmPwWDeVGNeDo3Mg&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQN5K38nSoflIJiy1hIYeOaMr-74Av1Uem22uMPc5Y&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("search_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_4");
    ns_web_url ("search_4",
        "URL=https://www.google.com/complete/search?q=ca&cp=2&client=mobile-gws-wiz-hp&xssi=t&hl=en&authuser=0&psi=czoQY7LWF-XgkPIPlIug-AY.1662007923876&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID",
        INLINE_URLS,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQpD9QznThN7woJnd3EfEpHwXiGdCV4jRje4c_6Dkk&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQpC1gg2m-OQWsUYjLB5LpbV0qDjE4yfgdDN20ScWgcwewt52KaIf3Jwvg&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSAWQLB7dyZw3FWfn54iTVy8mTJXJ6M7Qka0RtqKYM&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTi_OL-IrSHElD6R4h0aw58EDexXRfjLaxIR4qKdjc&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRJBC1I1p_Kf2Eye7wXoDz6xqvdoXYiakykpA_Gk-c&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQKDhT1Lv64rdS6V8yKgdanrVYGyK2Krql-ysRr5SY&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("search_4", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_5");
    ns_web_url ("search_5",
        "URL=https://www.google.com/complete/search?q=cav&cp=3&client=mobile-gws-wiz-hp&xssi=t&hl=en&authuser=0&psi=czoQY7LWF-XgkPIPlIug-AY.1662007923876&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID",
        INLINE_URLS,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR32tUrT8ADX1Z-JzcxCBhwf-OZawIwT9p1CMipncg&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTFh6wKpT-_4t0Ui8-DmMbdC-I9F1fEXHI8UR8BZ88rS3xMSVSt6jmzMRc&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTNU_ZKyWT1IlNpi37Xx0TqK6BX1z_gBP-e_jbeHEyWu0zuYYU68Xhp-VQ&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUEkrSchcWDgUrgvmpVvKeD79kgD4x0x3GNswpnTTDaaA9P-VWEReDtmk&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS6nwpOXKINorw8115CY6Q8D80uW2T4Y0Ziocz2qBA&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRiXfCM9dAlsoYyhW8wkVlTD9aM4hR0HAq0Wv7S_h8&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQA1y0tY4opGvNn_v1eQq7Ej9L0tGnnq4h6pUhqtnUaGPTQyirmn-5kh9I&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("search_5", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_6");
    ns_web_url ("search_6",
        "URL=https://www.google.com/complete/search?q=cavi&cp=4&client=mobile-gws-wiz-hp&xssi=t&hl=en&authuser=0&psi=czoQY7LWF-XgkPIPlIug-AY.1662007923876&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("search_6", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_7");
    ns_web_url ("search_7",
        "URL=https://www.google.com/complete/search?q=cavis&cp=5&client=mobile-gws-wiz-hp&xssi=t&hl=en&authuser=0&psi=czoQY7LWF-XgkPIPlIug-AY.1662007923876&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID",
        INLINE_URLS,
            "URL=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTu55yg1g6Hdfl0wpMzWYAzUlw_pBUHKOoGXHFnL7s5dwIQHHN7KhfdhwE&s=10", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://lh5.googleusercontent.com/p/AF1QipP-9U-QFn6jKEjQ2galDRXJQwETO7zv6s-OgjVK=w42-h42-n-k-no", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://lh5.googleusercontent.com/p/AF1QipMACy7I46MDFGKYvMeAmHUvg_ybw9JNa7Nqjxe9=w42-h42-n-k-no", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("search_7", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_8");
    ns_web_url ("search_8",
        "URL=https://www.google.com/complete/search?q=caviss&cp=6&client=mobile-gws-wiz-hp&xssi=t&hl=en&authuser=0&psi=czoQY7LWF-XgkPIPlIug-AY.1662007923876&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID",
        INLINE_URLS,
            "URL=https://lh5.googleusercontent.com/p/AF1QipMnRSBpUPw27zp4VEpwHuj-k7ZC13cAR1leVfsZ=w42-h42-n-k-no", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("search_8", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_9");
    ns_web_url ("search_9",
        "URL=https://www.google.com/complete/search?q=cavisso&cp=7&client=mobile-gws-wiz-hp&xssi=t&hl=en&authuser=0&psi=czoQY7LWF-XgkPIPlIug-AY.1662007923876&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("search_9", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_10");
    ns_web_url ("search_10",
        "URL=https://www.google.com/complete/search?q=cavisson&cp=8&client=mobile-gws-wiz-hp&xssi=t&hl=en&authuser=0&psi=czoQY7LWF-XgkPIPlIug-AY.1662007923876&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("search_10", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_11");
    ns_web_url ("search_11",
        "URL=https://www.google.com/complete/search?q=cavisson%20&cp=9&client=mobile-gws-wiz-hp&xssi=t&hl=en&authuser=0&psi=czoQY7LWF-XgkPIPlIug-AY.1662007923876&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID",
        INLINE_URLS,
            "URL=https://lh5.googleusercontent.com/p/AF1QipMlRR5VNeQYFBOUE6OiIujNunvH-GdGwM6noy9M=w42-h42-n-k-no", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("search_11", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_12");
    ns_web_url ("search_12",
        "URL=https://www.google.com/complete/search?q=cavisson%20s&cp=10&client=mobile-gws-wiz-hp&xssi=t&hl=en&authuser=0&psi=czoQY7LWF-XgkPIPlIug-AY.1662007923876&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("search_12", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_13");
    ns_web_url ("search_13",
        "URL=https://www.google.com/complete/search?q=cavisson%20sy&cp=11&client=mobile-gws-wiz-hp&xssi=t&hl=en&authuser=0&psi=czoQY7LWF-XgkPIPlIug-AY.1662007923876&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("search_13", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_14");
    ns_web_url ("search_14",
        "URL=https://www.google.com/complete/search?q=cavisson%20sys&cp=12&client=mobile-gws-wiz-hp&xssi=t&hl=en&authuser=0&psi=czoQY7LWF-XgkPIPlIug-AY.1662007923876&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("search_14", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_15");
    ns_web_url ("search_15",
        "URL=https://www.google.com/complete/search?q=cavisson%20syst&cp=13&client=mobile-gws-wiz-hp&xssi=t&hl=en&authuser=0&psi=czoQY7LWF-XgkPIPlIug-AY.1662007923876&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("search_15", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_16");
    ns_web_url ("search_16",
        "URL=https://www.google.com/complete/search?q=cavisson%20syste&cp=14&client=mobile-gws-wiz-hp&xssi=t&hl=en&authuser=0&psi=czoQY7LWF-XgkPIPlIug-AY.1662007923876&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("search_16", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_17");
    ns_web_url ("search_17",
        "URL=https://www.google.com/complete/search?q=cavisson%20system&cp=15&client=mobile-gws-wiz-hp&xssi=t&hl=en&authuser=0&psi=czoQY7LWF-XgkPIPlIug-AY.1662007923876&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("search_17", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search_18");
    ns_web_url ("search_18",
        "URL=https://www.google.com/complete/search?q=cavisson%20systems&cp=16&client=mobile-gws-wiz-hp&xssi=t&hl=en&authuser=0&psi=czoQY7LWF-XgkPIPlIug-AY.1662007923876&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID",
        INLINE_URLS,
            "URL=https://www.google.com/gen_204?atyp=i&ct=backbutton&cad=&tt=popstate&ei=czoQY7LWF-XgkPIPlIug-AY&trs=46940&nt=navigate&zx=1662008145504", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE
    );

    ns_end_transaction("search_18", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("log_2");
    ns_web_url ("log_2",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],1783,[["1662008145513",null,[],null,null,null,null,"[null,\"mweb-insp\",null,null,null,[[null,0,[143,362,396,357,534]],[null,0,[143,362,396,357,534]],[null,46,[143,362,396,357,534]],[null,46,[143,362,396,357,534]],[null,0,[143,362,396,357,534]],[null,46,[143,340,362,396,357,534]],[null,0,[143,362,396,357,534]]],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,32,null,null,null,null,null,null,null,[],null,null,null,null,null,null,null,null,[],null,null,null,null,null,null,null,null,null,null,1335,null,null,null,null,null,null,null,null,[],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,149,149,null,1],[]]",null,null,null,null,null,null,18000,null,null,null,null,[],1,null,null,null,null,null,[]]],"1662008145514",[]]",
        BODY_END
    );

    ns_end_transaction("log_2", NS_AUTO_STATUS);
    ns_page_think_time(0.014);

    ns_start_transaction("search_3Fq_3Dcavisson_2Bsyst");
    ns_web_url ("search_3Fq_3Dcavisson_2Bsyst",
        "URL=https://www.google.com/search?q=cavisson+systems&source=hp&ei=czoQY7LWF-XgkPIPlIug-AY&oq=cavisson+systems&gs_lcp=ChFtb2JpbGUtZ3dzLXdpei1ocBADMgsILhCABBDHARCvATIFCAAQgAQyCAgAEIAEEMkDMgUIABCABDIFCAAQgAQyBQgAEIAEMgsILhCABBDHARCvATIFCAAQgAQ6DggAEI8BEOoCEIwDEOUCOhEILhCABBCxAxCDARDHARDRAzoICC4QsQMQgwE6CwgAEIAEELEDEIMBOggILhCABBCxAzoLCAAQgAQQsQMQyQM6BQguEIAEOgsILhCABBCxAxCDAToNCC4QsQMQxwEQ0QMQCjoICAAQgAQQsQM6BwguELEDEAo6BwguEIAEEAo6BAgAEApQpB9Yozxg4j1oAXAAeACAAV6IAZIIkgECMTaYAQCgAQGwAQ8&sclient=mobile-gws-wiz-hp",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=X-Client-Data:CMyMywE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID",
        INLINE_URLS,
            "URL=https://www.google.com/sorry/index?continue=https://www.google.com/search%3Fq%3Dcavisson%2Bsystems%26source%3Dhp%26ei%3DczoQY7LWF-XgkPIPlIug-AY%26oq%3Dcavisson%2Bsystems%26gs_lcp%3DChFtb2JpbGUtZ3dzLXdpei1ocBADMgsILhCABBDHARCvATIFCAAQgAQyCAgAEIAEEMkDMgUIABCABDIFCAAQgAQyBQgAEIAEMgsILhCABBDHARCvATIFCAAQgAQ6DggAEI8BEOoCEIwDEOUCOhEILhCABBCxAxCDARDHARDRAzoICC4QsQMQgwE6CwgAEIAEELEDEIMBOggILhCABBCxAzoLCAAQgAQQsQMQyQM6BQguEIAEOgsILhCABBCxAxCDAToNCC4QsQMQxwEQ0QMQCjoICAAQgAQQsQM6BwguELEDEAo6BwguEIAEEAo6BAgAEApQpB9Yozxg4j1oAXAAeACAAV6IAZIIkgECMTaYAQCgAQGwAQ8%26sclient%3Dmobile-gws-wiz-hp&q=EgQXXrUGGNH2wJgGIhCp85ZfU7lwebrKzeiemhk9MgFy", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/recaptcha/api.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/recaptcha/api.js", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/releases/mBwkfBPLFWI0ygbsp8eJNMkw/recaptcha__en.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.google.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/recaptcha/api2/anchor?ar=1&k=6LfwuyUTAAAAAOAmoS0fdqijC2PbbdH4kjq62Y1b&co=aHR0cHM6Ly93d3cuZ29vZ2xlLmNvbTo0NDM.&hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw&size=normal&s=47x4-ZTQ7y-CLXiepiME0lufjia3TT9p0YT8GuoSOx8VX3Ulju7SiezOJ5zo5C1NsAK_9ExAxZBf_Tg272awooqOCCSqXswYray5rBPkbCOixIMKoWS9PThN47aJr4t2cF4qTulqx4esr-J-u_-qrgvyo5yp6MKpmegGo_aknQz9qK1H5JKRgEl3Li9eNCR842h_OT5CWdP51dCn-7BhY90crGE34Jn5GkUvbMs&cb=ac1qs7qvm41q", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/releases/mBwkfBPLFWI0ygbsp8eJNMkw/styles__ltr.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/releases/mBwkfBPLFWI0ygbsp8eJNMkw/recaptcha__en.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/api2/logo_48.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxKKTU1Kg.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.google.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/recaptcha/api2/webworker.js?hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:same-origin", "HEADER=Sec-Fetch-Dest:worker", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/recaptcha/api2/bframe?hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw&k=6LfwuyUTAAAAAOAmoS0fdqijC2PbbdH4kjq62Y1b", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=X-Client-Data:CMyMywE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=http://cavisson.com/", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://www.cavisson.com/", END_INLINE,
            "URL=https://www.cavisson.com/", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_8640b38e14ff3447d5c23ee6f4458dfc.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_single_b48f29a223d1760d6f3e1d0dfb850c96.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_single_411536fd225f7c2dc6daf53b293f3d11.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_single_762dedf90e6e1af28b7c11aaee1679d4.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.google.com/recaptcha/api.js?onload=wpformsRecaptchaLoad&render=explicit", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/logo/logo_original.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/banner/USTandCavisson2021.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/banner/insight-success-banner.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/banner/banner2aopti.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/banner/banner_new_opti.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/banner/banner3_opti.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/banner/business_continuity_opti.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/devops1.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/apes_slider.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/sre_slider.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/cloud-monitoring1b.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/product-icon/netstorm.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/product-icon/netcloud.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/product-icon/netocean.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/nethavoc.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/product-icon/diagnostic.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/product-icon/netvision.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/product-icon/netforest.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/sap.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/snmp.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/kubernetes.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/mongo-db.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/my-sql.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/ibm-db2.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/couchbase_a.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/dockers.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/rdt.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/rbu.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/icons/jms.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/logo/Michaels.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/logo/macys.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/logo/pathkind.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/logo/redbox.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/logo/nha.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/line-bg.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/award0.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/award1.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/award2.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/award3.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/award4.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/award5.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/award6.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/award7.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/svg/ask.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/svg/feedback.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/svg/demo.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/js/autoptimize_3146c83f6f21fc70327eff5222e9d8bd.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://69.61.127.66:9026/nv/cavisson/nv_bootstrap.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google-analytics.com/analytics.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_13a5250f3cb21fe97a4196a34dddde79.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_6d3d148c5a30bb7dc1bafe5dd300ce89.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_f1b664e7f1e8a8e6bea090d2e3f909a1.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/themes/onetone/images/home/products_bg.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/uploads/2020/10/y-cavisson.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/plugins/magee-shortcodes/assets/font-awesome/fonts/fontawesome-webfont.woff2?v=4.7.0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.cavisson.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/plugins/magee-shortcodes/assets/bootstrap/fonts/glyphicons-halflings-regular.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.cavisson.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/plugins/wp-social-widget/assets/fonts/socialicon.ttf?4xqn5s", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.cavisson.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/releases/mBwkfBPLFWI0ygbsp8eJNMkw/recaptcha__en.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.cavisson.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/loader.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://snap.licdn.com/li.lms-analytics/insight.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("search_3Fq_3Dcavisson_2Bsyst", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect");
    ns_web_url ("collect",
        "URL=https://www.google-analytics.com/j/collect?v=1&_v=j96&a=1417728121&t=pageview&_s=1&dl=https%3A%2F%2Fwww.cavisson.com%2F&ul=en-us&de=UTF-8&dt=Performance%20Testing%2C%20Monitoring%20%26%20Diagnostics%20Software%20%7C%20Cavisson%20-&sd=24-bit&sr=1536x714&vp=1521x586&je=0&_u=IEBAAEABAAAAAC~&jid=976793481&gjid=774472833&cid=1230464040.1662008161&tid=UA-77809548-1&_gid=549861340.1662008161&_r=1&_slc=1&z=1033122091",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://www.cavisson.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("collect", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("version");
    ns_web_url ("version",
        "URL=https://api.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/version",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://www.cavisson.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("version", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_2");
    ns_web_url ("collect_2",
        "URL=https://stats.g.doubleclick.net/j/collect?t=dc&aip=1&_r=3&v=1&_v=j96&tid=UA-77809548-1&cid=1230464040.1662008161&jid=976793481&gjid=774472833&_gid=549861340.1662008161&_u=IEBAAEAAAAAAAC~&z=1365916042",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://www.cavisson.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("collect_2", NS_AUTO_STATUS);
    ns_page_think_time(0.009);

    ns_start_transaction("index_2");
    ns_web_url ("index_2",
        "URL=https://www.google.com/recaptcha/api2/anchor?ar=1&k=6Ldt8qEUAAAAAH-QhEOUuqZ3-IqCap0nlTw6xrli&co=aHR0cHM6Ly93d3cuY2F2aXNzb24uY29tOjQ0Mw..&hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw&size=normal&cb=vqfqj2vkd8r6",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-Dest:iframe",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://px.ads.linkedin.com/collect?v=2&fmt=js&pid=775971&time=1662008161213&url=https%3A%2F%2Fwww.cavisson.com%2F", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/ads/ga-audiences?t=sr&aip=1&_r=4&slf_rd=1&v=1&_v=j96&tid=UA-77809548-1&cid=1230464040.1662008161&jid=976793481&_u=IEBAAEAAAAAAAC~&z=565591742", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/releases/mBwkfBPLFWI0ygbsp8eJNMkw/styles__ltr.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/releases/mBwkfBPLFWI0ygbsp8eJNMkw/recaptcha__en.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://px.ads.linkedin.com/collect?v=2&fmt=js&pid=775971&time=1662008161213&url=https%3A%2F%2Fwww.cavisson.com%2F&cookiesTest=true", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://prod.artibotcdn.com/manifest/_ArtiBotLauncherCB_Manifest?_=_", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/api2/logo_48.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxKKTU1Kg.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.google.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/recaptcha/api2/webworker.js?hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:same-origin", "HEADER=Sec-Fetch-Dest:worker", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.youtube.com/iframe_api", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.youtube.com/iframe_api", END_INLINE,
            "URL=https://prod.artibotcdn.com/launcher.4514.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/recaptcha/api2/bframe?hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw&k=6Ldt8qEUAAAAAH-QhEOUuqZ3-IqCap0nlTw6xrli", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.youtube.com/s/player/c57c113c/www-widgetapi.vflset/www-widgetapi.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("index_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("settings");
    ns_web_url ("settings",
        "URL=https://api-cdn.prod-aws.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/settings?settingsVersion=20&botVersionId=12034483-9bfe-47a8-bbfa-ada6398deb1f",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://www.cavisson.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://app.artibot.ai/chat-window?i=948c4b2e-cf5d-4d58-9e77-2b636e5bbd98&cbv=12034483-9bfe-47a8-bbfa-ada6398deb1f&v=20&cp=https%3A%2F%2Fwww.cavisson.com%2F&th=light&em=true&mo=true", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/uploads/2016/05/cropped-Cavisson.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat", END_INLINE,
            "URL=https://app.artibot.ai/chat-window?i=948c4b2e-cf5d-4d58-9e77-2b636e5bbd98&cbv=12034483-9bfe-47a8-bbfa-ada6398deb1f&v=20&cp=https%3A%2F%2Fwww.cavisson.com%2F&th=light&em=true&mo=true&sh=false&tb=true", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/uploads/2016/05/cropped-Cavisson-32x32.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat", END_INLINE,
            "URL=https://app.artibot.ai/chat_window.4514.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://js.stripe.com/v3/", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/chat_window.4514.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,600,700", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.googleapis.com/css?family=Source+Serif+Pro:300,400,400i,600,700", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.googleapis.com/css?family=Space+Mono:300,400,400i,600,700", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://js.stripe.com/v3/m-outer-6a0034e15fdc6a820e161ebc10368dcb.html#url=https%3A%2F%2Fapp.artibot.ai%2Fchat-window%3Fi%3D948c4b2e-cf5d-4d58-9e77-2b636e5bbd98%26cbv%3D12034483-9bfe-47a8-bbfa-ada6398deb1f%26v%3D20%26cp%3Dhttps%253A%252F%252Fwww.cavisson.com%252F%26th%3Dlight%26em%3Dtrue%26mo%3Dtrue%26sh%3Dfalse%26tb%3Dtrue&title=ArtiBot.ai%20%E2%80%94%20AI%20Powered%20Bot%20for%20Lead%20Capture&referrer=&muid=NA&sid=NA&version=6&preview=false", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://js.stripe.com/v3/fingerprinted/js/m-outer-d45840d7b854ab8c334de3b67a83d7c5.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.stripe.network/inner.html#url=https%3A%2F%2Fapp.artibot.ai%2Fchat-window%3Fi%3D948c4b2e-cf5d-4d58-9e77-2b636e5bbd98%26cbv%3D12034483-9bfe-47a8-bbfa-ada6398deb1f%26v%3D20%26cp%3Dhttps%253A%252F%252Fwww.cavisson.com%252F%26th%3Dlight%26em%3Dtrue%26mo%3Dtrue%26sh%3Dfalse%26tb%3Dtrue&title=ArtiBot.ai%20%E2%80%94%20AI%20Powered%20Bot%20for%20Lead%20Capture&referrer=&muid=NA&sid=NA&version=6&preview=false", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.stripe.network/out-4.5.42.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("settings", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("X");
    ns_web_url ("X",
        "URL=https://m.stripe.com/6",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.stripe.network",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "JTdCJTIydjIlMjIlM0ExJTJDJTIyaWQlMjIlM0ElMjJlOWI5NmJkNTc1ZDM1MGJhZjM5NjE5NmQzODY4OTg0ZCUyMiUyQyUyMnQlMjIlM0ExNDUuNjQlMkMlMjJ0YWclMjIlM0ElMjI0LjUuNDIlMjIlMkMlMjJzcmMlMjIlM0ElMjJqcyUyMiUyQyUyMmElMjIlM0ElN0IlMjJhJTIyJTNBJTdCJTIydiUyMiUzQSUyMnRydWUlMjIlMkMlMjJ0JTIyJTNBMC43NyU3RCUyQyUyMmIlMjIlM0ElN0IlMjJ2JTIyJTNBJTIyZmFsc2UlMjIlMkMlMjJ0JTIyJTNBMC4wMyU3RCUyQyUyMmMlMjIlM0ElN0IlMjJ2JTIyJTNBJTIyZW4tVVMlMkNlbiUyMiUyQyUyMnQlMjIlM0EwLjA1NSU3RCUyQyUyMmQlMjIlM0ElN0IlMjJ2JTIyJTNBJTIyTGludXglMjB4ODZfNjQlMjIlMkMlMjJ0JTIyJTNBMC4wMiU3RCUyQyUyMmUlMjIlM0ElN0IlMjJ2JTIyJTNBJTIyJTIyJTJDJTIydCUyMiUzQTAuMDclN0QlMkMlMjJmJTIyJTNBJTdCJTIydiUyMiUzQSUyMjE1MzZ3XzcxNGhfMjRkXzFyJTIyJTJDJTIydCUyMiUzQTAuMDI1JTdEJTJDJTIyZyUyMiUzQSU3QiUyMnYlMjIlM0ElMjItNSUyMiUyQyUyMnQlMjIlM0EwLjAyNSU3RCUyQyUyMmglMjIlM0ElN0IlMjJ2JTIyJTNBJTIyZmFsc2UlMjIlMkMlMjJ0JTIyJTNBMC4wMiU3RCUyQyUyMmklMjIlM0ElN0IlMjJ2JTIyJTNBJTIyc2Vzc2lvblN0b3JhZ2UtZW5hYmxlZCUyQyUyMGxvY2FsU3RvcmFnZS1lbmFibGVkJTIyJTJDJTIydCUyMiUzQTIuOTA1JTdEJTJDJTIyaiUyMiUzQSU3QiUyMnYlMjIlM0ElMjIwMDAwMDAwMTEwMDAwMDAwMDAwMDAwMDAxMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMTEwMDAwJTIyJTJDJTIydCUyMiUzQTEzOS40OCUyQyUyMmF0JTIyJTNBNjQuNDc1JTdEJTJDJTIyayUyMiUzQSU3QiUyMnYlMjIlM0ElMjIlMjIlMkMlMjJ0JTIyJTNBMC4wMiU3RCUyQyUyMmwlMjIlM0ElN0IlMjJ2JTIyJTNBJTIyTW96aWxsYSUyRjUuMCUyMChMaW51eCUzQiUyMEFuZHJvaWQlMjA2LjAuMSUzQiUyME1vdG8lMjBHJTIwKDQpJTIwQnVpbGQlMkZNUEoyNC4xMzktNjQpJTIwQXBwbGVXZWJLaXQlMkY1MzcuMzYlMjAoS0hUTUwlMkMlMjBsaWtlJTIwR2Vja28pJTIwQ2hyb21lJTJGNTguMC4zMDI5LjgxJTIwTW9iaWxlJTIwU2FmYXJpJTJGNTM3LjM2JTIwUFRTVCUyRjElMjIlMkMlMjJ0JTIyJTNBMC4wMjUlN0QlMkMlMjJtJTIyJTNBJTdCJTIydiUyMiUzQSUyMiUyMiUyQyUyMnQlMjIlM0EwLjA4NSU3RCUyQyUyMm4lMjIlM0ElN0IlMjJ2JTIyJTNBJTIydHJ1ZSUyMiUyQyUyMnQlMjIlM0E3Ni4yNTUlMkMlMjJhdCUyMiUzQTEuNTE1JTdEJTJDJTIybyUyMiUzQSU3QiUyMnYlMjIlM0ElMjIxZGEwNGNkZjhmMmQ1MWY3ZGVkY2E3NTJkNTc4MmRjNSUyMiUyQyUyMnQlMjIlM0E0MS4xOSU3RCU3RCUyQyUyMmIlMjIlM0ElN0IlMjJhJTIyJTNBJTIyJTIyJTJDJTIyYiUyMiUzQSUyMmh0dHBzJTNBJTJGJTJGanQ1ellWSllwcVQyZXJhbUxjQjBoWFNGeVFBSHd5SmxUNVk5UV84XzFkdy5DbF9yT3FQeFJXejFob0pTMHpMd1NPa3pkZmFrcWh0RFRvWXVZdW1hYTdzLm9GYWJiS1d3Q3pKd2p0ME5fTURVZzJzUmtWbEF3SWJoUmU3MjlNbTdsV1ElMkZWWkh1YndBa2lPRE44WTlYb01Jc0xPdGkxdFJ5aGhUdVdjN0JrOFNGMXVVJTNGQzdRcldPYkg5dGJyYmpfMmwzSXZ0YVVDX2loZWJ0emlaT0xLaHB4M3pYZyUzRERWUTd5a2k1M1VLR3pzbVhOd3RCcFZMcEVLZDFDQU1HZGNPRFByUU5ubEklMjYwYkxFYTFIQlI5bHdTQUxQN3FZQ08tbmlzQWxwcG90THR4Vk5ldmlJT2NrJTNESWpnN3hTTC1CSE9FT1pwUjZBSHljM1c2Tk5OTlVTMTF5TDNCNW1McjRHQSUyNnVuQjhHS2pVR0VZZ1JtNmtwdUNqbUlzeVhwX2FQcjd0Z1Y2QURtTHVGcmclM0RoQVNkZWMyeDdPNHBaTjh1d1hLN241bDYxSmZzcm5ZX3gwQ0lvVkdZUlhzJTI2b3FPYnZrcU5PVTd3RzBvNW5WT3R0dGh5S2JlNDMwUVRwS0Roa05LUlhHOCUzREMxdUdTSDBId2xxenhXUEN5RmJaOTFzc05OYXF1N1JIOWdyelBXdS1lVUElMjZNWkVUQm5LVnl1TGthNkVyWFg2T2ZLYXdyaV9Vd3pUdW1VUlBSSXFTUVRjJTNEWmhHNG1WTnVYdXF3ak9BeDdRUUVKYWJTX3Z4WE1FX08zMEt5SXZmXzB4QSUyNkRBRjZwQiUzRDJTeFByYSUyNmM2YUI3cCUzRDJTeFByYSUyNnZ0c21hMCUzRDhrM2pfRyUyNlU0Nzd1aiUzRDJTeFByYSUyMiUyQyUyMmMlMjIlM0ElMjJqTlFTVllXUmYyMENOMm1pNFA5WXRjeEE5UUNsWUpBVU1WZVJNQll1ZlVzJTIyJTJDJTIyZCUyMiUzQSUyMk5BJTIyJTJDJTIyZSUyMiUzQSUyMk5BJTIyJTJDJTIyZiUyMiUzQWZhbHNlJTJDJTIyZyUyMiUzQXRydWUlMkMlMjJoJTIyJTNBdHJ1ZSUyQyUyMmklMjIlM0ElNUIlMjJsb2NhdGlvbiUyMiU1RCUyQyUyMmolMjIlM0ElNUIlNUQlMkMlMjJuJTIyJTNBMTE4LjY2MDAwMDAzNjQ3MDU5JTJDJTIydSUyMiUzQSUyMmFwcC5hcnRpYm90LmFpJTIyJTdEJTJDJTIyaCUyMiUzQSUyMjA2MWRjYzRiMjMwY2E4ODhhOWNiJTIyJTdE",
        BODY_END
    );

    ns_end_transaction("X", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("settings_2");
    ns_web_url ("settings_2",
        "URL=https://api-cdn.prod-aws.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/settings?settingsVersion=20&botVersionId=12034483-9bfe-47a8-bbfa-ada6398deb1f",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://app.artibot.ai",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("settings_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("index_4");
    ns_web_url ("index_4",
        "URL=https://api.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/chats/init/",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://app.artibot.ai",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"botVersionId":"12034483-9bfe-47a8-bbfa-ada6398deb1f","contactId":"deabc787-8299-4886-87d6-b75f495c9a4d","state":null,"timezone":"America/Chicago"}",
        BODY_END
    );

    ns_end_transaction("index_4", NS_AUTO_STATUS);
    ns_page_think_time(25.051);

    //Page Auto split for 
    ns_start_transaction("index_3");
    ns_web_url ("index_3",
        "URL=https://www.cavisson.com/netstorm-load-testing-solution/",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat",
        INLINE_URLS,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_c5e58a610ae173b4589281eda526da3f.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_single_b48f29a223d1760d6f3e1d0dfb850c96.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_single_411536fd225f7c2dc6daf53b293f3d11.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_single_762dedf90e6e1af28b7c11aaee1679d4.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/js?id=AW-846414309", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/uploads/2018/12/Enterprise-ready_scalable_and_stable_technology.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/js/autoptimize_215922ccdcbfc1b44f4530c3610bdeef.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat", END_INLINE,
            "URL=https://69.61.127.66:9026/nv/cavisson/nv_bootstrap.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google-analytics.com/analytics.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_13a5250f3cb21fe97a4196a34dddde79.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_6d3d148c5a30bb7dc1bafe5dd300ce89.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/cache/autoptimize/css/autoptimize_f1b664e7f1e8a8e6bea090d2e3f909a1.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/uploads/2018/09/Graphical-Kpi-768x371.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/uploads/2018/09/Advanced-technology-to-handle-load-variables-user-realism-application-realism-and-network-realism-768x371.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat", END_INLINE,
            "URL=https://www.cavisson.com/wp-content/uploads/2018/09/Reusability-of-script-tight-integration-with-Cavisson-products-768x410.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion_async.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/wcm/loader.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/releases/mBwkfBPLFWI0ygbsp8eJNMkw/recaptcha__en.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.cavisson.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/loader.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://snap.licdn.com/li.lms-analytics/insight.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("index_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_3");
    ns_web_url ("collect_3",
        "URL=https://www.google-analytics.com/j/collect?v=1&_v=j96&a=685828733&t=pageview&_s=1&dl=https%3A%2F%2Fwww.cavisson.com%2Fnetstorm-load-testing-solution%2F&ul=en-us&de=UTF-8&dt=NetStorm%20-%20Performance%20Testing%2C%20Load%20Testing%20Capacity%20Analysis%20Appliance&sd=24-bit&sr=1536x714&vp=1521x586&je=0&_u=AACAAEABAAAAAC~&jid=&gjid=&cid=1230464040.1662008161&tid=UA-77809548-1&_gid=549861340.1662008161&_slc=1&z=1128683632",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://www.cavisson.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.gstatic.com/call-tracking/call-tracking_7.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collect_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("version_2");
    ns_web_url ("version_2",
        "URL=https://api.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/version",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://www.cavisson.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://px.ads.linkedin.com/collect?v=2&fmt=js&pid=775971&time=1662008190024&url=https%3A%2F%2Fwww.cavisson.com%2Fnetstorm-load-testing-solution%2F", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/846414309/?random=1662008190047&cv=9&fst=1662008190047&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&u_h=714&u_w=1536&u_ah=714&u_aw=1536&u_cd=24&u_his=5&u_tz=-300&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa8t0&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.cavisson.com%2Fnetstorm-load-testing-solution%2F&ref=https%3A%2F%2Fwww.cavisson.com%2F&tiba=NetStorm%20-%20Performance%20Testing%2C%20Load%20Testing%20Capacity%20Analysis%20Appliance&auid=111818256.1662008190&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/recaptcha/api2/anchor?ar=1&k=6Ldt8qEUAAAAAH-QhEOUuqZ3-IqCap0nlTw6xrli&co=aHR0cHM6Ly93d3cuY2F2aXNzb24uY29tOjQ0Mw..&hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw&size=normal&cb=djrx71tubtjo", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://prod.artibotcdn.com/manifest/_ArtiBotLauncherCB_Manifest?_=_", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/846414309/wcm?cc=ZZ&dn=16503197412&cl=irwhCLex05cBEOWDzZMD&ref=https%3A%2F%2Fwww.cavisson.com%2F&ct_eid=2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://www.cavisson.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://px.ads.linkedin.com/collect?v=2&fmt=js&pid=775971&time=1662008190024&url=https%3A%2F%2Fwww.cavisson.com%2Fnetstorm-load-testing-solution%2F&cookiesTest=true", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://prod.artibotcdn.com/launcher.4514.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("version_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("settings_3");
    ns_web_url ("settings_3",
        "URL=https://api-cdn.prod-aws.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/settings?settingsVersion=20&botVersionId=12034483-9bfe-47a8-bbfa-ada6398deb1f",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://www.cavisson.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.gstatic.com/recaptcha/releases/mBwkfBPLFWI0ygbsp8eJNMkw/styles__ltr.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/recaptcha/releases/mBwkfBPLFWI0ygbsp8eJNMkw/recaptcha__en.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("settings_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("wcm");
    ns_web_url ("wcm",
        "URL=https://www.google.com/pagead/attribution/wcm?cc=ZZ&dn=16503197412&cl=irwhCLex05cBEOWDzZMD",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:null",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.gstatic.com/recaptcha/api2/logo_48.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxKKTU1Kg.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.google.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/chat-window?i=948c4b2e-cf5d-4d58-9e77-2b636e5bbd98&cbv=12034483-9bfe-47a8-bbfa-ada6398deb1f&v=20&cp=https%3A%2F%2Fwww.cavisson.com%2Fnetstorm-load-testing-solution%2F&th=light&em=true&mo=true", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/846414309/?random=1662008190047&cv=9&fst=1662004800000&num=1&bg=ffffff&guid=ON&u_h=714&u_w=1536&u_ah=714&u_aw=1536&u_cd=24&u_his=5&u_tz=-300&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa8t0&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.cavisson.com%2Fnetstorm-load-testing-solution%2F&ref=https%3A%2F%2Fwww.cavisson.com%2F&tiba=NetStorm%20-%20Performance%20Testing%2C%20Load%20Testing%20Capacity%20Analysis%20Appliance&async=1&fmt=3&is_vtc=1&random=2836157374&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/chat-window?i=948c4b2e-cf5d-4d58-9e77-2b636e5bbd98&cbv=12034483-9bfe-47a8-bbfa-ada6398deb1f&v=20&cp=https%3A%2F%2Fwww.cavisson.com%2Fnetstorm-load-testing-solution%2F&th=light&em=true&mo=true&sh=false&tb=true", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/846414309/wcm?cc=ZZ&dn=18007016125&cl=9penCM3u55cBEOWDzZMD&ref=https%3A%2F%2Fwww.cavisson.com%2F&ct_eid=2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://www.cavisson.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("wcm", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("wcm_2");
    ns_web_url ("wcm_2",
        "URL=https://www.google.com/pagead/attribution/wcm?cc=ZZ&dn=18007016125&cl=9penCM3u55cBEOWDzZMD",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:null",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.google.com/recaptcha/api2/webworker.js?hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:same-origin", "HEADER=Sec-Fetch-Dest:worker", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/chat_window.4514.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://js.stripe.com/v3/", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://app.artibot.ai/chat_window.4514.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,600,700", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.googleapis.com/css?family=Source+Serif+Pro:300,400,400i,600,700", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.googleapis.com/css?family=Space+Mono:300,400,400i,600,700", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/recaptcha/api2/bframe?hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw&k=6Ldt8qEUAAAAAH-QhEOUuqZ3-IqCap0nlTw6xrli", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://js.stripe.com/v3/m-outer-6a0034e15fdc6a820e161ebc10368dcb.html#url=https%3A%2F%2Fapp.artibot.ai%2Fchat-window%3Fi%3D948c4b2e-cf5d-4d58-9e77-2b636e5bbd98%26cbv%3D12034483-9bfe-47a8-bbfa-ada6398deb1f%26v%3D20%26cp%3Dhttps%253A%252F%252Fwww.cavisson.com%252Fnetstorm-load-testing-solution%252F%26th%3Dlight%26em%3Dtrue%26mo%3Dtrue%26sh%3Dfalse%26tb%3Dtrue&title=ArtiBot.ai%20%E2%80%94%20AI%20Powered%20Bot%20for%20Lead%20Capture&referrer=&muid=NA&sid=NA&version=6&preview=false", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://js.stripe.com/v3/fingerprinted/js/m-outer-d45840d7b854ab8c334de3b67a83d7c5.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.stripe.network/inner.html#url=https%3A%2F%2Fapp.artibot.ai%2Fchat-window%3Fi%3D948c4b2e-cf5d-4d58-9e77-2b636e5bbd98%26cbv%3D12034483-9bfe-47a8-bbfa-ada6398deb1f%26v%3D20%26cp%3Dhttps%253A%252F%252Fwww.cavisson.com%252Fnetstorm-load-testing-solution%252F%26th%3Dlight%26em%3Dtrue%26mo%3Dtrue%26sh%3Dfalse%26tb%3Dtrue&title=ArtiBot.ai%20%E2%80%94%20AI%20Powered%20Bot%20for%20Lead%20Capture&referrer=&muid=NA&sid=NA&version=6&preview=false", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.stripe.network/out-4.5.42.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("wcm_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("X_2");
    ns_web_url ("X_2",
        "URL=https://m.stripe.com/6",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.stripe.network",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "JTdCJTIydjIlMjIlM0ExJTJDJTIyaWQlMjIlM0ElMjJlOWI5NmJkNTc1ZDM1MGJhZjM5NjE5NmQzODY4OTg0ZCUyMiUyQyUyMnQlMjIlM0E0My42NSUyQyUyMnRhZyUyMiUzQSUyMjQuNS40MiUyMiUyQyUyMnNyYyUyMiUzQSUyMmpzJTIyJTJDJTIyYSUyMiUzQW51bGwlMkMlMjJiJTIyJTNBJTdCJTIyYSUyMiUzQSUyMiUyMiUyQyUyMmIlMjIlM0ElMjJodHRwcyUzQSUyRiUyRmp0NXpZVkpZcHFUMmVyYW1MY0IwaFhTRnlRQUh3eUpsVDVZOVFfOF8xZHcuQ2xfck9xUHhSV3oxaG9KUzB6THdTT2t6ZGZha3FodERUb1l1WXVtYWE3cy5vRmFiYktXd0N6SndqdDBOX01EVWcyc1JrVmxBd0liaFJlNzI5TW03bFdRJTJGVlpIdWJ3QWtpT0ROOFk5WG9NSXNMT3RpMXRSeWhoVHVXYzdCazhTRjF1VSUzRkM3UXJXT2JIOXRicmJqXzJsM0l2dGFVQ19paGVidHppWk9MS2hweDN6WGclM0REVlE3eWtpNTNVS0d6c21YTnd0QnBWTHBFS2QxQ0FNR2RjT0RQclFObmxJJTI2MGJMRWExSEJSOWx3U0FMUDdxWUNPLW5pc0FscHBvdEx0eFZOZXZpSU9jayUzRElqZzd4U0wtQkhPRU9acFI2QUh5YzNXNk5OTk5VUzExeUwzQjVtTHI0R0ElMjZ1bkI4R0tqVUdFWWdSbTZrcHVDam1Jc3lYcF9hUHI3dGdWNkFEbUx1RnJnJTNEaEFTZGVjMng3TzRwWk44dXdYSzduNWw2MUpmc3JuWV94MENJb1ZHWVJYcyUyNm9xT2J2a3FOT1U3d0cwbzVuVk90dHRoeUtiZTQzMFFUcEtEaGtOS1JYRzglM0RqeHhwWHR4Q2VqVmRPbWJiV0ZlVG1WcGhEUFZxYjB2emFYZ1JPNDdUZUdNJTI2TVpFVEJuS1Z5dUxrYTZFclhYNk9mS2F3cmlfVXd6VHVtVVJQUklxU1FUYyUzRFpoRzRtVk51WHVxd2pPQXg3UVFFSmFiU192eFhNRV9PMzBLeUl2Zl8weEElMjZEQUY2cEIlM0QyU3hQcmElMjZjNmFCN3AlM0QyU3hQcmElMjZ2dHNtYTAlM0Q4azNqX0clMjZVNDc3dWolM0QyU3hQcmElMjIlMkMlMjJjJTIyJTNBJTIyak5RU1ZZV1JmMjBDTjJtaTRQOVl0Y3hBOVFDbFlKQVVNVmVSTUJZdWZVcyUyMiUyQyUyMmQlMjIlM0ElMjJOQSUyMiUyQyUyMmUlMjIlM0ElMjJOQSUyMiUyQyUyMmYlMjIlM0FmYWxzZSUyQyUyMmclMjIlM0F0cnVlJTJDJTIyaCUyMiUzQXRydWUlMkMlMjJpJTIyJTNBJTVCJTIybG9jYXRpb24lMjIlNUQlMkMlMjJqJTIyJTNBJTVCJTVEJTJDJTIybiUyMiUzQTkwLjU1OTk5OTk4MjM2MDc1JTJDJTIydSUyMiUzQSUyMmFwcC5hcnRpYm90LmFpJTIyJTdEJTJDJTIyaCUyMiUzQSUyMjFkM2Q0NWRhMDIyYjdjMGNhN2FjJTIyJTdE",
        BODY_END
    );

    ns_end_transaction("X_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("index_5");
    ns_web_url ("index_5",
        "URL=https://www.cavisson.com/netstorm-load-testing-solution/?\cavisson.ico",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=_wpjshd_session_;PHPSESSID;_ga;_gid;_gat;_gcl_au"
    );

    ns_end_transaction("index_5", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("settings_4");
    ns_web_url ("settings_4",
        "URL=https://api-cdn.prod-aws.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/settings?settingsVersion=20&botVersionId=12034483-9bfe-47a8-bbfa-ada6398deb1f",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://app.artibot.ai",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("settings_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("index_6");
    ns_web_url ("index_6",
        "URL=https://api.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/chats/init/",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://app.artibot.ai",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"botVersionId":"12034483-9bfe-47a8-bbfa-ada6398deb1f","contactId":"deabc787-8299-4886-87d6-b75f495c9a4d","state":"Fi0xBJlSp6S0iaA90VbT2xrC/0H8hvGWrcZGr5BqDCY+dltTe0ZJQP8/U8XvaP246VITHWvWy89akZoYHIjGlQwYlKzbZJd9E7Xc4P78hAwLR0sBeCv/u9F0j0/Ps7OEsLMj9xylCSs=","timezone":"America/Chicago"}",
        BODY_END
    );

    ns_end_transaction("index_6", NS_AUTO_STATUS);
    ns_page_think_time(6.359);

}
