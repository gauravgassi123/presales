#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"
#ifdef ENABLE_RUNLOGIC_PROGRESS
  #define UPDATE_USER_FLOW_COUNT(count) update_user_flow_count(count);
#else
  #define UPDATE_USER_FLOW_COUNT(count)
#endif


extern int init_script();
extern int exit_script();

typedef void FlowReturn;

// Note: Following extern declaration is used to find the list of used flows. Do not delete/edit it
// Start - List of used flows in the runlogic
extern FlowReturn Search();
extern FlowReturn ShoppingCart();
extern FlowReturn SignIn();
extern FlowReturn PlaceOrder();
// End - List of used flows in the runlogic


void Runlogic()
{
    NSDL2_RUNLOGIC(NULL, NULL, "Executing init_script()");

    init_script();

    NSDL2_RUNLOGIC(NULL, NULL, "Executing percent block - Home");
    {
        UPDATE_USER_FLOW_COUNT(0)
        int Homepercent = ns_get_random_number_int(1, 10000);

        NSDL2_RUNLOGIC(NULL, NULL, "Percentage random number for block - Home = %d", Homepercent);

        if(Homepercent <= 5000)
        {
            NSDL2_RUNLOGIC(NULL, NULL, "Executing flow - Search (pct value = 50.0%)");
            UPDATE_USER_FLOW_COUNT(1)
            Search();
        }
        else if(Homepercent <= 10000)
        {

            NSDL2_RUNLOGIC(NULL, NULL, "Executing sequence block - Flow (pct value = 50.0%)");
            {
                UPDATE_USER_FLOW_COUNT(3)
                NSDL2_RUNLOGIC(NULL, NULL, "Executing flow - ShoppingCart");
                UPDATE_USER_FLOW_COUNT(4)
                ShoppingCart();
                NSDL2_RUNLOGIC(NULL, NULL, "Executing flow - SignIn");
                UPDATE_USER_FLOW_COUNT(6)
                SignIn();
                NSDL2_RUNLOGIC(NULL, NULL, "Executing flow - PlaceOrder");
                UPDATE_USER_FLOW_COUNT(8)
                PlaceOrder();
            }
        }
    }

    NSDL2_RUNLOGIC(NULL, NULL, "Executing ns_exit_session()");
    ns_exit_session();
}
