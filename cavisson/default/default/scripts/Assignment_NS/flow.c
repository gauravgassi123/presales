/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: Arpita
    Date of recording: 09/17/2022 01:28:29
    Flow details:
    Build details: 4.8.0 (build# 129)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("Home");
    ns_web_url ("Home",
        "URL=https://petstore.octoperf.com/actions/Catalog.action",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=https://petstore.octoperf.com/css/jpetstore.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/logo-topbar.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/cart.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/separator.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/sm_fish.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/sm_dogs.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/sm_reptiles.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/sm_cats.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/sm_birds.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/fish_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/dogs_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/cats_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/reptiles_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/birds_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/splash.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE
    );

    ns_end_transaction("Home", NS_AUTO_STATUS);
    ns_page_think_time(3.848);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("Sign_In_Page");
    ns_web_url ("Sign_In_Page",
        "URL=https://petstore.octoperf.com/actions/Account.action;jsessionid={Jsession_Id}?signonForm=",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID"
    );

    ns_end_transaction("Sign_In_Page", NS_AUTO_STATUS);
    ns_page_think_time(10.589);

    //Page Auto split for Button 'Login' Clicked by User
    ns_start_transaction("Sign_IN");
    ns_web_url ("Sign_IN",
        "URL=https://petstore.octoperf.com/actions/Account.action",
        "METHOD=POST",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Origin:https://petstore.octoperf.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID",
        BODY_BEGIN,
            "username={User_Name}&password={Password}&signon=Login&_sourcePage=uY2pJTe9DqRUiTYOfvgn3ltAx2tu-fhNufuw6ircDC5xzdggowVQlb1Uqs39NVYJQm58Fp32Olk252EOVvLoJs-Rvq300cVxcPCHe73OmpI%3D&__fp=28mO2r2AKcZrPtF1dnxsv5YqkPPI2PQAPn1RPVbUy6U3j8fMFPk1ZceWGs6xbRSC",
        BODY_END,
        INLINE_URLS,
            "URL=https://petstore.octoperf.com/actions/Catalog.action", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE
    );

    ns_end_transaction("Sign_IN", NS_AUTO_STATUS);
    ns_page_think_time(2.417);

    //Page Auto split for Image Link '' Clicked by User
    ns_start_transaction("Search");
    ns_web_url ("Search",
        "URL=https://petstore.octoperf.com/actions/Catalog.action?viewCategory=&categoryId={Category_ID}",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID"
    );

    ns_end_transaction("Search", NS_AUTO_STATUS);
    ns_page_think_time(4.471);

  //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("View_Product");
    ns_web_url ("View_Product",
        "URL=https://petstore.octoperf.com/actions/Catalog.action?viewProduct=&productId={Product_Id}",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID"
    );

    ns_end_transaction("View_Product", NS_AUTO_STATUS);
    ns_page_think_time(2.409);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("Cart");
    ns_web_url ("Cart",
        "URL=https://petstore.octoperf.com/actions/Cart.action?addItemToCart=&workingItemId=EST-28",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID"
    );

    ns_end_transaction("Cart", NS_AUTO_STATUS);
    ns_page_think_time(2.281);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("Checkout");
    ns_web_url ("Checkout",
        "URL=https://petstore.octoperf.com/actions/Order.action?newOrderForm=",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID"
    );

    ns_end_transaction("Checkout", NS_AUTO_STATUS);
    ns_page_think_time(1.988);

    //Page Auto split for Button 'Continue' Clicked by User
    ns_start_transaction("Payment");
    ns_web_url ("Payment",
        "URL=https://petstore.octoperf.com/actions/Order.action",
        "METHOD=POST",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Origin:https://petstore.octoperf.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID",
        BODY_BEGIN,
            "order.cardType={Card_Type}&order.creditCard={Card_NO}&order.expiryDate=12%2F03&order.billToFirstName={First_Name}&order.billToLastName={Last_Name}&order.billAddress1=noida&order.billAddress2=bbsr&order.billCity=BBsr&order.billState={State}&order.billZip=szfkjhzkfg&order.billCountry={Country}&newOrder=Continue&_sourcePage=O4cRRFiqj5eIglwgq1ZM4_EvvFZiyk9wmN0f1zJ0gsaVkfKh6O-5YgVB8FH6L7_I-hjRdOt_E5rfoFReFmjo2qpQgcuxEZmktuvuhtX7rjw%3D&__fp=I9cYkaevhtv0WuBIS3hH02YaXqZj4ehANNPWNNPSkaZ6MImqz1vW6ozsudoYzxSdGAuadCksFZqLOmt-6vqIxeKuxCcY1BlG1Ti6Dv_PcWsazkC3BpTKAw%3D%3D",
        BODY_END
    );

    ns_end_transaction("Payment", NS_AUTO_STATUS);
    ns_page_think_time(2.672);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("Order_Details");
    ns_web_url ("Order_Details",
        "URL=https://petstore.octoperf.com/actions/Order.action?newOrder=&confirmed=true",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID"
    );

    ns_end_transaction("Order_Details", NS_AUTO_STATUS);
    ns_page_think_time(4.169);

    //Page Auto split for Link 'A' Clicked by User
	  ns_start_transaction("SignOut");
        ns_web_url ("SignOut",
        "URL=https://petstore.octoperf.com/actions/Catalog.action",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID",
        INLINE_URLS,
            "URL=https://petstore.octoperf.com/images/fish_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/dogs_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/cats_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/reptiles_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/birds_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/splash.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE
    );

    ns_end_transaction("SignOut", NS_AUTO_STATUS);
    ns_page_think_time(5.788);
}

