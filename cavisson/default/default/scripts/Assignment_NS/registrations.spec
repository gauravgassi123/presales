nsl_static_var(User_Name:1,Password:2, File=Credential, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=All);
nsl_static_var(First_Name:1,Last_Name:2,Country:3,State:4,Card_Type:5,Card_NO:6, File=Billing_Details, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=All);
nsl_search_var(Category_ID, PAGE=Search, LB="categoryId=", RB="\"", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(Jsession_Id, PAGE=Sign_In_Page, LB="jsessionid", RB="?", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(Product_Id, PAGE=View_Product, LB="productId=", RB="\"", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
