/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: 
    Date of recording: 08/26/2022 11:13:23
    Flow details:
    Build details: 
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("index");
    ns_web_url ("index",
        "URL=http://www.google.com/",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "REDIRECT=YES",
        "LOCATION=https://www.google.com/?gws_rd=ssl",
        INLINE_URLS,
            "URL=https://www.google.com/?gws_rd=ssl", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/xjs/_/js/k=xjs.s.en_US.WiVIwMiwRDc.O/am=AIA6gQDwAgAIAEBmAAEAAAAAAAAAAAYAkHjKCQAABggBgZwEAAAAABISIgAAGAAIAQ0MQgAAAAAfmTcAAX8GAAaacAEAAAAAAAAABHAJgoEbJAoCAAEAAAAAABBWU1ccgIIg/d=1/ed=1/dg=2/br=1/rs=ACT90oGUt4uK4zQD3debjzSNSDkYIqgxKQ/m=cdos,dpf,hsm,jsa,d,csi", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/images/searchbox/desktop_searchbox_sprites318_hr.webp", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.gstatic.com/og/_/js/k=og.qtm.en_US.-MMKP3uG9VU.O/rt=j/m=qabr,q_d,qcwid,qapid,qald/exm=qaaw,qadd,qaid,qein,qhaw,qhbr,qhch,qhga,qhid,qhin,qhpr/d=1/ed=1/rs=AA2YrTuS6iZfrsnE7GApv0RWeBgl21VxSA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.gstatic.com/og/_/ss/k=og.qtm.tM-BeBGBME0.L.W.O/m=qcwid/excm=qaaw,qadd,qaid,qein,qhaw,qhbr,qhch,qhga,qhid,qhin,qhpr/d=1/ed=1/ct=zgms/rs=AA2YrTtmhrLYVlm7jdu3TI1ROywA3arJnA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("index", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204");
    ns_web_url ("gen_204",
        "URL=https://www.google.com/gen_204?s=webhp&t=aft&atyp=csi&ei=eqoIY5S5NuiYkPIPovaQ4Ac&rt=wsrt.313,aft.264,afti.264,prt.147&wh=473&imn=2&ima=1&imad=0&aftp=473&bl=zjjY",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CNraygE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID"
    );

    ns_end_transaction("gen_204", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("search");
    ns_web_url ("search",
        "URL=https://www.google.com/complete/search?q&cp=0&client=gws-wiz&xssi=t&hl=en&authuser=0&psi=eqoIY5S5NuiYkPIPovaQ4Ac.1661512315259&nolsbt=1&dpr=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=X-Client-Data:CNraygE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID",
        INLINE_URLS,
            "URL=https://www.google.com/xjs/_/js/k=xjs.s.en_US.WiVIwMiwRDc.O/ck=xjs.s.PegLoVMLRbg.L.W.O/am=AIA6gQDwAgAIAEBmAAEAAAAAAAAAAAYAkHjKCQAABggBgZwEAAAAABISIgAAGAAIAQ0MQgAAAAAfmTcAAX8GAAaacAEAAAAAAAAABHAJgoEbJAoCAAEAAAAAABBWU1ccgIIg/d=1/exm=cdos,csi,d,dpf,hsm,jsa/ed=1/dg=2/br=1/rs=ACT90oH7wwq2VwIQaff89FmuN-utD9BP9A/ee=Pjplud:PoEs9b;QGR0gd:Mlhmy;uY49fb:COQbmf;EVNhjf:pw70Gc;g8nkx:U4MzKc;wQlYve:aLUfP;kbAm9d:MkHyGd;F9mqte:UoRcbe;sTsDMc:kHVSUb;oUlnpc:RagDlc;dtl0hd:lLQWFe;yGxLoc:FmAr0c;dIoSBb:ZgGg9b;pXdRYb:JKoKVe;wR5FRb:TtcOte;KpRAue:Tia57b;aZ61od:arTwJ;JXS8fb:Qj0suc;rQSrae:C6D5Fc;qavrXe:zQzcXe;UDrY1c:eps46d;nKl0s:xxrckd;w3bZCb:ZPGaIb;VGRfx:VFqbr;imqimf:jKGL2e;Np8Qkd:Dpx6qc;BjwMce:cXX2Wb;oGtAuc:sOXFj;NPKaK:PVlQOd;zxnPse:GkRiKb;daB6be:lMxGPd;Fmv9Nc:O1Tzwc;hK67qb:QWEO5b;jVtPve:wQ95P;R4IIIb:QWfeKf;BMxAGc:E5bFse;WDGyFe:jcVOxd;xbe2wc:wbTLEd;DpcR3d:zL72xf;tosKvd:ZCqP3;NSEoX:lazG7b;G6wU6e:hezEbd;kCQyJ:ueyPK;GleZL:J1A7Od;fAO5td:yzxsuf;oSUNyd:fTfGO;SJsSc:H1GVub;SMDL4c:fTfGO;zOsCQe:Ko78Df;WCEKNd:I46Hvd;LBgRLc:XVMNvd;LsNahb:ucGLNb;UyG7Kb:wQd0G;TxfV6d:YORN0b;qaS3gd:yiLg6e;aAJE9c:WHW6Ef;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;SLtqO:Kh1xYe;VxQ32b:k0XsBb;DULqB:RKfG5c;cFTWae:gT8qnd;gaub4:TN6bMe;hjRo6e:F62sG;CUcugf:BIaADc;whEZac:F4AmNb;qddgKe:x4FYXe;eBAeSb:Ck63tb;vfVwPd:OXTqFb;w9w86d:dt4g2b;lkq0A:Z0MWEf;KQzWid:mB4wNe;pNsl2d:j9Yuyc;eHDfl:ofjVkb;Nyt6ic:jn2sGd;SNUn3:x8cHvb;LEikZe:byfTOb,lsjVmc;io8t5d:sgY6Zb;j7137d:KG2eXe;Oj465e:KG2eXe;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;nAFL3:s39S4;iFQyKf:QIhFr/m=DhPYme,EkevXb,GU4Gab,NzU6V,aa,abd,async,dvl,fKZehd,ifl,mu,pHXghd,sb_wiz,sf,sonic,spch?xjs=s1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/client_204?&atyp=i&biw=790&bih=473&ei=eqoIY5S5NuiYkPIPovaQ4Ac", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.IK5OmUURd2E.O/m=gapi_iframes,googleapis_client/rt=j/sv=1/d=1/ed=1/rs=AHpOoo932JinkSJHK92WgVjIV-Jwwyu3Rw/cb=gapi.loaded_0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://ogs.google.com/widget/callout?prid=19027682&pgid=19027681&puid=1421396ff301105b&cce=1&dc=1&origin=https%3A%2F%2Fwww.google.com&cn=callout&pid=1&spid=538&hl=en", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/xjs/_/js/k=xjs.s.en_US.WiVIwMiwRDc.O/ck=xjs.s.PegLoVMLRbg.L.W.O/am=AIA6gQDwAgAIAEBmAAEAAAAAAAAAAAYAkHjKCQAABggBgZwEAAAAABISIgAAGAAIAQ0MQgAAAAAfmTcAAX8GAAaacAEAAAAAAAAABHAJgoEbJAoCAAEAAAAAABBWU1ccgIIg/d=1/exm=DhPYme,EkevXb,GU4Gab,NzU6V,aa,abd,async,cdos,csi,d,dpf,dvl,fKZehd,hsm,ifl,jsa,mu,pHXghd,sb_wiz,sf,sonic,spch/ed=1/dg=2/br=1/rs=ACT90oH7wwq2VwIQaff89FmuN-utD9BP9A/ee=Pjplud:PoEs9b;QGR0gd:Mlhmy;uY49fb:COQbmf;EVNhjf:pw70Gc;g8nkx:U4MzKc;wQlYve:aLUfP;kbAm9d:MkHyGd;F9mqte:UoRcbe;sTsDMc:kHVSUb;oUlnpc:RagDlc;dtl0hd:lLQWFe;yGxLoc:FmAr0c;dIoSBb:ZgGg9b;pXdRYb:JKoKVe;wR5FRb:TtcOte;KpRAue:Tia57b;aZ61od:arTwJ;JXS8fb:Qj0suc;rQSrae:C6D5Fc;qavrXe:zQzcXe;UDrY1c:eps46d;nKl0s:xxrckd;w3bZCb:ZPGaIb;VGRfx:VFqbr;imqimf:jKGL2e;Np8Qkd:Dpx6qc;BjwMce:cXX2Wb;oGtAuc:sOXFj;NPKaK:PVlQOd;zxnPse:GkRiKb;daB6be:lMxGPd;Fmv9Nc:O1Tzwc;hK67qb:QWEO5b;jVtPve:wQ95P;R4IIIb:QWfeKf;BMxAGc:E5bFse;WDGyFe:jcVOxd;xbe2wc:wbTLEd;DpcR3d:zL72xf;tosKvd:ZCqP3;NSEoX:lazG7b;G6wU6e:hezEbd;kCQyJ:ueyPK;GleZL:J1A7Od;fAO5td:yzxsuf;oSUNyd:fTfGO;SJsSc:H1GVub;SMDL4c:fTfGO;zOsCQe:Ko78Df;WCEKNd:I46Hvd;LBgRLc:XVMNvd;LsNahb:ucGLNb;UyG7Kb:wQd0G;TxfV6d:YORN0b;qaS3gd:yiLg6e;aAJE9c:WHW6Ef;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;SLtqO:Kh1xYe;VxQ32b:k0XsBb;DULqB:RKfG5c;cFTWae:gT8qnd;gaub4:TN6bMe;hjRo6e:F62sG;CUcugf:BIaADc;whEZac:F4AmNb;qddgKe:x4FYXe;eBAeSb:Ck63tb;vfVwPd:OXTqFb;w9w86d:dt4g2b;lkq0A:Z0MWEf;KQzWid:mB4wNe;pNsl2d:j9Yuyc;eHDfl:ofjVkb;Nyt6ic:jn2sGd;SNUn3:x8cHvb;LEikZe:byfTOb,lsjVmc;io8t5d:sgY6Zb;j7137d:KG2eXe;Oj465e:KG2eXe;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;nAFL3:s39S4;iFQyKf:QIhFr/m=CnSW2d,DPreE,HGv0mf,WlNQGd,fXO0xe,kQvlef,nabPbb?xjs=s2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE
    );

    ns_end_transaction("search", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_2");
    ns_web_url ("gen_204_2",
        "URL=https://www.google.com/gen_204?atyp=i&ei=eqoIY5S5NuiYkPIPovaQ4Ac&dt19=2&zx=1661512315384",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CNraygE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID",
        INLINE_URLS,
            "URL=https://www.google.com/client_204?cs=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/xjs/_/js/md=1/k=xjs.s.en_US.WiVIwMiwRDc.O/am=AIA6gQDwAgAIAEBmAAEAAAAAAAAAAAYAkHjKCQAABggBgZwEAAAAABISIgAAGAAIAQ0MQgAAAAAfmTcAAX8GAAaacAEAAAAAAAAABHAJgoEbJAoCAAEAAAAAABBWU1ccgIIg/rs=ACT90oGUt4uK4zQD3debjzSNSDkYIqgxKQ", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.google.com/xjs/_/js/k=xjs.s.en_US.WiVIwMiwRDc.O/ck=xjs.s.PegLoVMLRbg.L.W.O/am=AIA6gQDwAgAIAEBmAAEAAAAAAAAAAAYAkHjKCQAABggBgZwEAAAAABISIgAAGAAIAQ0MQgAAAAAfmTcAAX8GAAaacAEAAAAAAAAABHAJgoEbJAoCAAEAAAAAABBWU1ccgIIg/d=1/exm=CnSW2d,DPreE,DhPYme,EkevXb,GU4Gab,HGv0mf,NzU6V,WlNQGd,aa,abd,async,cdos,csi,d,dpf,dvl,fKZehd,fXO0xe,hsm,ifl,jsa,kQvlef,mu,nabPbb,pHXghd,sb_wiz,sf,sonic,spch/ed=1/dg=2/br=1/rs=ACT90oH7wwq2VwIQaff89FmuN-utD9BP9A/ee=Pjplud:PoEs9b;QGR0gd:Mlhmy;uY49fb:COQbmf;EVNhjf:pw70Gc;g8nkx:U4MzKc;wQlYve:aLUfP;kbAm9d:MkHyGd;F9mqte:UoRcbe;sTsDMc:kHVSUb;oUlnpc:RagDlc;dtl0hd:lLQWFe;yGxLoc:FmAr0c;dIoSBb:ZgGg9b;pXdRYb:JKoKVe;wR5FRb:TtcOte;KpRAue:Tia57b;aZ61od:arTwJ;JXS8fb:Qj0suc;rQSrae:C6D5Fc;qavrXe:zQzcXe;UDrY1c:eps46d;nKl0s:xxrckd;w3bZCb:ZPGaIb;VGRfx:VFqbr;imqimf:jKGL2e;Np8Qkd:Dpx6qc;BjwMce:cXX2Wb;oGtAuc:sOXFj;NPKaK:PVlQOd;zxnPse:GkRiKb;daB6be:lMxGPd;Fmv9Nc:O1Tzwc;hK67qb:QWEO5b;jVtPve:wQ95P;R4IIIb:QWfeKf;BMxAGc:E5bFse;WDGyFe:jcVOxd;xbe2wc:wbTLEd;DpcR3d:zL72xf;tosKvd:ZCqP3;NSEoX:lazG7b;G6wU6e:hezEbd;kCQyJ:ueyPK;GleZL:J1A7Od;fAO5td:yzxsuf;oSUNyd:fTfGO;SJsSc:H1GVub;SMDL4c:fTfGO;zOsCQe:Ko78Df;WCEKNd:I46Hvd;LBgRLc:XVMNvd;LsNahb:ucGLNb;UyG7Kb:wQd0G;TxfV6d:YORN0b;qaS3gd:yiLg6e;aAJE9c:WHW6Ef;BgS6mb:fidj5d;z97YGf:oug9te;CxXAWb:YyRLvc;Pguwyb:Xm4ZCd;VN6jIc:ddQyuf;SLtqO:Kh1xYe;VxQ32b:k0XsBb;DULqB:RKfG5c;cFTWae:gT8qnd;gaub4:TN6bMe;hjRo6e:F62sG;CUcugf:BIaADc;whEZac:F4AmNb;qddgKe:x4FYXe;eBAeSb:Ck63tb;vfVwPd:OXTqFb;w9w86d:dt4g2b;lkq0A:Z0MWEf;KQzWid:mB4wNe;pNsl2d:j9Yuyc;eHDfl:ofjVkb;Nyt6ic:jn2sGd;SNUn3:x8cHvb;LEikZe:byfTOb,lsjVmc;io8t5d:sgY6Zb;j7137d:KG2eXe;Oj465e:KG2eXe;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;nAFL3:s39S4;iFQyKf:QIhFr/m=aLUfP?xjs=s2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://www.gstatic.com/_/mss/boq-one-google/_/js/k=boq-one-google.OneGoogleWidgetUi.en.4W0FOKDKC3M.es5.O/am=uAEAQA/d=1/excm=_b,_r,_tp,calloutview/ed=1/dg=0/wt=2/rs=AM-SdHtimnV-YbsMf4O1-Tvb1ZZJj4s7Sw/m=_b,_tp,_r", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/images/hpp/gsa_super_g-64.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID", END_INLINE,
            "URL=https://fonts.gstatic.com/s/googlesans/v14/4UabrENHsxJlGDuGo1OIlLU94YtzCwY.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://ogs.google.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxK.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://ogs.google.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("gen_204_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("log");
    ns_web_url ("log",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CNraygE=",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/log_url_0_1_1661512403207.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.gstatic.com/_/mss/boq-one-google/_/js/k=boq-one-google.OneGoogleWidgetUi.en.4W0FOKDKC3M.es5.O/ck=boq-one-google.OneGoogleWidgetUi.jqRUnq2L2dw.L.B1.O/am=uAEAQA/d=1/exm=_b,_r,_tp/excm=_b,_r,_tp,calloutview/ed=1/wt=2/rs=AM-SdHvNk7RosR5N1ano8TQRpsQcmPFSwA/ee=cEt90b:ws9Tlc;QGR0gd:Mlhmy;uY49fb:COQbmf;yxTchf:KUM7Z;qddgKe:xQtZb;dIoSBb:SpsfSb;EmZ2Bf:zr1jrb;zxnPse:GkRiKb;EVNhjf:pw70Gc;NSEoX:lazG7b;oGtAuc:sOXFj;eBAeSb:zbML3c;Pjplud:EEDORb;io8t5d:yDVVkb;j7137d:KG2eXe;Oj465e:KG2eXe;ul9GGd:VDovNc;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;NPKaK:SdcwHb;pXdRYb:MdUzUe;nAFL3:s39S4;iFQyKf:QIhFr;SNUn3:ZwDk9d;LBgRLc:SdcwHb;wR5FRb:O1Gjze/m=n73qwf,ws9Tlc,e5qFLc,GkRiKb,IZT63,UUJqVe,O1Gjze,byfTOb,lsjVmc,xUdipf,OTA3Ae,COQbmf,fKUV3e,aurFic,U0aPgd,ZwDk9d,V3dDOb,mI3LFb,O6y8ed,PrPYRd,MpJwZc,LEikZe,NwH0H,OmgaI,lazG7b,Mpq4Ee,XVMNvd,L1AAkb,KUM7Z,Mlhmy,s39S4,lwddkf,gychg,w9hDv,EEDORb,RMhBfe,SdcwHb,aW3pY,pw70Gc,EFQ78c,Ulmmrd,ZfAoz,mdR7q,xQtZb,JNoxi,kWgXee,MI6k7c,kjKdXe,BVgquf,QIhFr,ovKuLd,hKSk3e,yDVVkb,hc6Ubd,SpsfSb,KG2eXe,MdUzUe,VwDzFe,zbML3c,zr1jrb,lsPsHb,A7fCU,hnN99e,Uas9Hd,yYB61,pjICDe", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/gen_204?use_corp=on&atyp=i&zx=1661512315584&ogsr=1&ei=eqoIY9O0N_KPkPIPxp2K2Ak&ct=7&cad=i&id=19027682&loc=webhp&prid=538&ogd=com&ogprm=up&ap=1&vis=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID;OGPC", END_INLINE,
            "URL=https://www.gstatic.com/_/mss/boq-one-google/_/js/k=boq-one-google.OneGoogleWidgetUi.en.4W0FOKDKC3M.es5.O/ck=boq-one-google.OneGoogleWidgetUi.jqRUnq2L2dw.L.B1.O/am=uAEAQA/d=1/exm=A7fCU,BVgquf,COQbmf,EEDORb,EFQ78c,GkRiKb,IZT63,JNoxi,KG2eXe,KUM7Z,L1AAkb,LEikZe,MI6k7c,MdUzUe,Mlhmy,MpJwZc,Mpq4Ee,NwH0H,O1Gjze,O6y8ed,OTA3Ae,OmgaI,PrPYRd,QIhFr,RMhBfe,SdcwHb,SpsfSb,U0aPgd,UUJqVe,Uas9Hd,Ulmmrd,V3dDOb,VwDzFe,XVMNvd,ZfAoz,ZwDk9d,_b,_r,_tp,aW3pY,aurFic,byfTOb,e5qFLc,fKUV3e,gychg,hKSk3e,hc6Ubd,hnN99e,kWgXee,kjKdXe,lazG7b,lsPsHb,lsjVmc,lwddkf,mI3LFb,mdR7q,n73qwf,ovKuLd,pjICDe,pw70Gc,s39S4,w9hDv,ws9Tlc,xQtZb,xUdipf,yDVVkb,yYB61,zbML3c,zr1jrb/excm=_b,_r,_tp,calloutview/ed=1/wt=2/rs=AM-SdHvNk7RosR5N1ano8TQRpsQcmPFSwA/ee=cEt90b:ws9Tlc;QGR0gd:Mlhmy;uY49fb:COQbmf;yxTchf:KUM7Z;qddgKe:xQtZb;dIoSBb:SpsfSb;EmZ2Bf:zr1jrb;zxnPse:GkRiKb;EVNhjf:pw70Gc;NSEoX:lazG7b;oGtAuc:sOXFj;eBAeSb:zbML3c;Pjplud:EEDORb;io8t5d:yDVVkb;j7137d:KG2eXe;Oj465e:KG2eXe;ul9GGd:VDovNc;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;NPKaK:SdcwHb;pXdRYb:MdUzUe;nAFL3:s39S4;iFQyKf:QIhFr;SNUn3:ZwDk9d;LBgRLc:SdcwHb;wR5FRb:O1Gjze/m=bm51tf", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("log", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_3");
    ns_web_url ("gen_204_3",
        "URL=https://www.google.com/gen_204?atyp=csi&ei=eqoIY5S5NuiYkPIPovaQ4Ac&s=webhp&t=all&bl=zjjY&wh=473&imn=2&ima=1&imad=0&aftp=473&adh=&cls=0.001654638069941223&ime=2&imex=2&imeh=0&imea=0&imeb=0&imel=0&scp=0&net=dl.1550,ect.4g,rtt.100&mem=ujhs.14,tjhs.19,jhsl.4295,dm.8&sto=&sys=hc.8&rt=aft.264,prt.147,xjses.171,xjsee.257,xjs.258,dcl.261,afti.264,aftqf.265,ol.734,lcp.267,fcp.139,wsrt.313,cst.18,dnst.0,rqst.79,rspt.12,sslt.18,rqstt.246,unt.224,cstt.228,dit.463&zx=1661512315692",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CNraygE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID;OGPC",
        INLINE_URLS,
            "URL=https://adservice.google.com/adsid/google/ui", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID;OGPC", END_INLINE,
            "URL=https://www.google.com/gen_204?atyp=i&ct=psnt&cad=&nt=navigate&ei=eqoIY5S5NuiYkPIPovaQ4Ac&zx=1661512315694", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID;OGPC", END_INLINE,
            "URL=https://www.gstatic.com/_/mss/boq-one-google/_/js/k=boq-one-google.OneGoogleWidgetUi.en.4W0FOKDKC3M.es5.O/ck=boq-one-google.OneGoogleWidgetUi.jqRUnq2L2dw.L.B1.O/am=uAEAQA/d=1/exm=A7fCU,BVgquf,COQbmf,EEDORb,EFQ78c,GkRiKb,IZT63,JNoxi,KG2eXe,KUM7Z,L1AAkb,LEikZe,MI6k7c,MdUzUe,Mlhmy,MpJwZc,Mpq4Ee,NwH0H,O1Gjze,O6y8ed,OTA3Ae,OmgaI,PrPYRd,QIhFr,RMhBfe,SdcwHb,SpsfSb,U0aPgd,UUJqVe,Uas9Hd,Ulmmrd,V3dDOb,VwDzFe,XVMNvd,ZfAoz,ZwDk9d,_b,_r,_tp,aW3pY,aurFic,bm51tf,byfTOb,e5qFLc,fKUV3e,gychg,hKSk3e,hc6Ubd,hnN99e,kWgXee,kjKdXe,lazG7b,lsPsHb,lsjVmc,lwddkf,mI3LFb,mdR7q,n73qwf,ovKuLd,pjICDe,pw70Gc,s39S4,w9hDv,ws9Tlc,xQtZb,xUdipf,yDVVkb,yYB61,zbML3c,zr1jrb/excm=_b,_r,_tp,calloutview/ed=1/wt=2/rs=AM-SdHvNk7RosR5N1ano8TQRpsQcmPFSwA/ee=cEt90b:ws9Tlc;QGR0gd:Mlhmy;uY49fb:COQbmf;yxTchf:KUM7Z;qddgKe:xQtZb;dIoSBb:SpsfSb;EmZ2Bf:zr1jrb;zxnPse:GkRiKb;EVNhjf:pw70Gc;NSEoX:lazG7b;oGtAuc:sOXFj;eBAeSb:zbML3c;Pjplud:EEDORb;io8t5d:yDVVkb;j7137d:KG2eXe;Oj465e:KG2eXe;ul9GGd:VDovNc;sP4Vbe:VwDzFe;kMFpHd:OTA3Ae;NPKaK:SdcwHb;pXdRYb:MdUzUe;nAFL3:s39S4;iFQyKf:QIhFr;SNUn3:ZwDk9d;LBgRLc:SdcwHb;wR5FRb:O1Gjze/m=Wt6vjf,hhhU8,FCpbqb,WhJNk", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("gen_204_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("manifest");
    ns_web_url ("manifest",
        "URL=https://www.google.com/manifest?pwa=webhp",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=X-Client-Data:CNraygE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID;OGPC",
        INLINE_URLS,
            "URL=https://www.google.com/favicon.ico", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID;OGPC", END_INLINE
    );

    ns_end_transaction("manifest", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("log_2");
    ns_web_url ("log_2",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Origin:https://ogs.google.com",
        "HEADER=X-Client-Data:CNraygE=",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID;OGPC",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/log_2_url_0_1_1661512403208.body",
        BODY_END
    );

    ns_end_transaction("log_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("log_3");
    ns_web_url ("log_3",
        "URL=https://play.google.com/log?format=json&hasfast=true",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Origin:https://ogs.google.com",
        "HEADER=X-Client-Data:CNraygE=",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID;OGPC",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/log_3_url_0_1_1661512403209.body",
        BODY_END
    );

    ns_end_transaction("log_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("log_4");
    ns_web_url ("log_4",
        "URL=https://play.google.com/log?format=json&hasfast=true",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CNraygE=",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID;OGPC",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/log_4_url_0_1_1661512403209.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.google.com/gen_204?atyp=i&ct=ifl&cad=1:hungry&ei=eqoIY5S5NuiYkPIPovaQ4Ac&ved=0ahUKEwiU3sbar-T5AhVoDEQIHSI7BHwQnRsIDg&ictx=1&zx=1661512394336", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=sec-ch-ua-arch:\"x86\"", "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"", "HEADER=sec-ch-ua-platform:\"Linux\"", "HEADER=X-Client-Data:CNraygE=", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=1P_JAR;AEC;NID;OGPC", END_INLINE
    );

    ns_end_transaction("log_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("gen_204_4");
    ns_web_url ("gen_204_4",
        "URL=https://www.google.com/gen_204?atyp=i&ei=eqoIY5S5NuiYkPIPovaQ4Ac&ct=slh&v=t1&im=M&m=HV&pv=0.8359758799589894&me=1:1661512315100,V,0,0,790,473:0,B,473:0,N,1,eqoIY5S5NuiYkPIPovaQ4Ac:0,R,1,1,0,0,790,473:286,x:67685,V,0,0,1366,529:10765,h,1,1,i:2334,h,1,1,o:6,h,1,1,i:606,h,1,1,o:5868,e,B&zx=1661512402651",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-arch:\"x86\"",
        "HEADER=sec-ch-ua-full-version:\"90.0.4430.61\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Origin:https://www.google.com",
        "HEADER=X-Client-Data:CNraygE=",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=1P_JAR;AEC;NID;OGPC"
    );

    ns_end_transaction("gen_204_4", NS_AUTO_STATUS);
    ns_page_think_time(0.044);

}
