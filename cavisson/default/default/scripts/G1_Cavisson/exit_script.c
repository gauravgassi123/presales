/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: Anjali.chauhan
    Date of recording: 09/02/2022 08:16:29
    Flow details:
    Build details: 4.8.0 (build# 129)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

int exit_script()
{
    return 0;
}
