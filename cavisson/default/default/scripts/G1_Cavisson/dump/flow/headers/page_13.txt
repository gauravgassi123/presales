--Request 
GET https://api-cdn.prod-aws.artibot.ai/api/bots/948c4b2e-cf5d-4d58-9e77-2b636e5bbd98/settings?settingsVersion=20&botVersionId=12034483-9bfe-47a8-bbfa-ada6398deb1f
Host: api-cdn.prod-aws.artibot.ai
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: */*
Origin: https://www.cavisson.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
Date: Sat, 13 Aug 2022 11:46:59 GMT
Server: Kestrel
Cache-Control: public,max-age=2147483647
Access-Control-Allow-Origin: *
X-Cache: Hit from cloudfront
Via: 1.1 76123233d5cffd2a25437cd32f2ca528.cloudfront.net (CloudFront)
X-Amz-Cf-Pop: LAX3-C3
X-Amz-Cf-Id: 5WgLeaWigY0rTr1I2icmaefpvm3Puy59twuHK9TTyzeHxkWVvyQHvQ==
Age: 1715341
----
--Request 
GET https://www.google.com/recaptcha/api2/webworker.js?hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw
Host: www.google.com
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: */*
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: same-origin
Sec-Fetch-Dest: worker
Referer: https://www.google.com/recaptcha/api2/anchor?ar=1&k=6Ldt8qEUAAAAAH-QhEOUuqZ3-IqCap0nlTw6xrli&co=aHR0cHM6Ly93d3cuY2F2aXNzb24uY29tOjQ0Mw..&hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw&size=normal&cb=glkohmqfgl99
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
expires: Fri, 02 Sep 2022 08:16:00 GMT
date: Fri, 02 Sep 2022 08:16:00 GMT
cache-control: private, max-age=300
cross-origin-embedder-policy: require-corp
report-to: {\"group\":\"recaptcha\",\"max_age\":2592000,\"endpoints\":[{\"url\":\"https://csp.withgoogle.com/csp/report-to/recaptcha\"}]}
content-type: text/javascript; charset=UTF-8
content-encoding: gzip
x-content-type-options: nosniff
x-frame-options: SAMEORIGIN
content-security-policy: frame-ancestors 'self'
x-xss-protection: 1; mode=block
content-length: 112
server: GSE
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://www.googleadservices.com/pagead/conversion/846414309/wcm?cc=ZZ&dn=16503197412&cl=irwhCLex05cBEOWDzZMD&ref=https%3A%2F%2Fwww.cavisson.com%2F&ct_eid=2
Host: www.googleadservices.com
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: */*
Origin: https://www.cavisson.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 302
p3p: policyref=\"https://www.googleadservices.com/pagead/p3p.xml\", CP=\"NOI DEV PSA PSD IVA IVD OTP OUR OTR IND OTC\"
timing-allow-origin: *
cross-origin-resource-policy: cross-origin
location: https://www.google.com/pagead/attribution/wcm?cc=ZZ&dn=16503197412&cl=irwhCLex05cBEOWDzZMD
access-control-allow-origin: https://www.cavisson.com
access-control-allow-credentials: true
content-type: text/html; charset=UTF-8
x-content-type-options: nosniff
date: Fri, 02 Sep 2022 08:16:16 GMT
server: cafe
content-length: 0
x-xss-protection: 0
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://app.artibot.ai/chat-window?i=948c4b2e-cf5d-4d58-9e77-2b636e5bbd98&cbv=12034483-9bfe-47a8-bbfa-ada6398deb1f&v=20&cp=https%3A%2F%2Fwww.cavisson.com%2Fcontact-us%2F&th=light&em=true&mo=true
Host: app.artibot.ai
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: navigate
Sec-Fetch-Dest: iframe
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----

--Request 
GET https://www.google.com/recaptcha/api2/webworker.js?hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw
Host: www.google.com
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: */*
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: same-origin
Sec-Fetch-Dest: worker
Referer: https://www.google.com/recaptcha/api2/anchor?ar=1&k=6Ldt8qEUAAAAAH-QhEOUuqZ3-IqCap0nlTw6xrli&co=aHR0cHM6Ly93d3cuY2F2aXNzb24uY29tOjQ0Mw..&hl=en&v=mBwkfBPLFWI0ygbsp8eJNMkw&size=normal&cb=ykplwuqi4gnd
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
expires: Fri, 02 Sep 2022 08:16:00 GMT
date: Fri, 02 Sep 2022 08:16:00 GMT
cache-control: private, max-age=300
cross-origin-embedder-policy: require-corp
report-to: {\"group\":\"recaptcha\",\"max_age\":2592000,\"endpoints\":[{\"url\":\"https://csp.withgoogle.com/csp/report-to/recaptcha\"}]}
content-type: text/javascript; charset=UTF-8
content-encoding: gzip
x-content-type-options: nosniff
x-frame-options: SAMEORIGIN
content-security-policy: frame-ancestors 'self'
x-xss-protection: 1; mode=block
content-length: 112
server: GSE
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----

--Response 
HTTP/1.1 302
location: /collect?v=2&fmt=js&pid=775971&time=1662106576504&url=https%3A%2F%2Fwww.cavisson.com%2Fcontact-us%2F&cookiesTest=true
set-cookie: li_sugr=1126a5ab-2ac3-43b2-932c-bc3c75d9eb51; Max-Age=7776000; Expires=Thu, 01 Dec 2022 08:16:16 GMT; Path=/; Domain=.linkedin.com; Secure
set-cookie: lang=v=2&lang=en-us; Path=/; Domain=ads.linkedin.com; Secure
set-cookie: bcookie=\"v=2&e9d97e77-1253-48e3-888d-ddce71dc3aef\"; domain=.linkedin.com; Path=/; Secure; Expires=Sat, 02-Sep-2023 08:16:16 GMT
set-cookie: lidc=\"b=VGST09:s=V:r=V:a=V:p=V:g=2371:u=1:x=1:i=1662106576:t=1662192976:v=2:sig=AQHqXQ_x1JtncKLWINz7MQoYn_r25Jov\"; Expires=Sat, 03 Sep 2022 08:16:16 GMT; domain=.linkedin.com; Path=/; Secure
linkedin-action: 1
x-li-fabric: prod-lva1
x-li-pop: afd-prod-lva1-x
x-li-proto: http/2
x-li-uuid: AAXnrVgWXQ03tawa8RGIvA==
x-cache: CONFIG_NOCACHE
x-msedge-ref: Ref A: CA90DB93873747F6A18DA3BB6B1BC863 Ref B: BY3EDGE0119 Ref C: 2022-09-02T08:16:16Z
date: Fri, 02 Sep 2022 08:16:16 GMT
content-length: 0
----
--Request 
GET https://app.artibot.ai/chat-window?i=948c4b2e-cf5d-4d58-9e77-2b636e5bbd98&cbv=12034483-9bfe-47a8-bbfa-ada6398deb1f&v=20&cp=https%3A%2F%2Fwww.cavisson.com%2Fcontact-us%2F&th=light&em=true&mo=true&sh=false&tb=true
Host: app.artibot.ai
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: navigate
Sec-Fetch-Dest: iframe
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: text/html; charset=utf-8
last-modified: Tue, 12 Oct 2021 19:43:29 GMT
x-amz-version-id: null
server: AmazonS3
content-encoding: gzip
date: Fri, 02 Sep 2022 08:16:00 GMT
cache-control: public, max-age=900
etag: W/\"14d7c7c69f131bf34750c3a7e93fb020\"
vary: Accept-Encoding
x-cache: Hit from cloudfront
via: 1.1 5d364edd2927236ece76b1ef58ec87da.cloudfront.net (CloudFront)
x-amz-cf-pop: LAX3-C4
x-amz-cf-id: JH8W-n1JBC4t7ecwHuihdXWciqh8mvxKYZgsFtFaeoI9Ds51B3my-w==
age: 594
----
--Request 
GET https://www.google.com/pagead/1p-user-list/846414309/?random=1662106576616&cv=9&fst=1662105600000&num=1&bg=ffffff&guid=ON&u_h=489&u_w=1313&u_ah=489&u_aw=1313&u_cd=24&u_his=3&u_tz=-300&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa8v0&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.cavisson.com%2Fcontact-us%2F&ref=https%3A%2F%2Fwww.cavisson.com%2F&tiba=Contact%20Us%20-%20Performance%20Testing%2C%20Monitoring%20%26%20Diagnostics%20Software%20%7C%20Cavisson&async=1&fmt=3&is_vtc=1&random=2861364751&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y
Host: www.google.com
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
p3p: policyref=\"https://www.googleadservices.com/pagead/p3p.xml\", CP=\"NOI DEV PSA PSD IVA IVD OTP OUR OTR IND OTC\"
timing-allow-origin: *
cross-origin-resource-policy: cross-origin
date: Fri, 02 Sep 2022 08:16:16 GMT
pragma: no-cache
expires: Fri, 01 Jan 1990 00:00:00 GMT
cache-control: no-cache, no-store, must-revalidate
content-type: image/gif
content-security-policy: script-src 'none'; object-src 'none'
x-content-type-options: nosniff
server: cafe
content-length: 42
x-xss-protection: 0
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://px.ads.linkedin.com/collect?v=2&fmt=js&pid=775971&time=1662106576504&url=https%3A%2F%2Fwww.cavisson.com%2Fcontact-us%2F&cookiesTest=true
Host: px.ads.linkedin.com
sec-ch-ua: \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: application/javascript
set-cookie: lang=v=2&lang=en-us; Path=/; Domain=ads.linkedin.com; Secure
set-cookie: bcookie=\"v=2&7a836735-c1c1-46e2-8bb0-f94b06745c74\"; domain=.linkedin.com; Path=/; Secure; Expires=Sat, 02-Sep-2023 08:16:16 GMT
set-cookie: lidc=\"b=OGST09:s=O:r=O:a=O:p=O:g=2336:u=1:x=1:i=1662106576:t=1662192976:v=2:sig=AQFniuu0UJSnCnii4k95zm6LLlAISN3u\"; Expires=Sat, 03 Sep 2022 08:16:16 GMT; domain=.linkedin.com; Path=/; Secure
linkedin-action: 1
x-li-fabric: prod-lor1
x-li-pop: afd-prod-lor1-x
x-li-proto: http/2
x-li-uuid: AAXnrVgYyAfqHB07Bi1Cww==
x-cache: CONFIG_NOCACHE
x-msedge-ref: Ref A: B47333FDC84A4BE9A649FB9F36AA99EE Ref B: BY3EDGE0119 Ref C: 2022-09-02T08:16:16Z
date: Fri, 02 Sep 2022 08:16:16 GMT
content-length: 0
----

