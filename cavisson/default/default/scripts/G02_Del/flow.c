/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: Anjali.chauhan
    Date of recording: 09/02/2022 10:04:43
    Flow details:
    Build details: 4.8.0 (build# 129)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("index");
    ns_web_url ("index",
        "URL=https://www.kohls.com/",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://m.kohls.com/", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;_abck;bm_sz", END_INLINE,
            "URL=https://m.kohls.com/dist/css/header/css-bundle-rev-62b152927d667f360603be9580ca909c.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;_abck;bm_sz", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/launch-b2dd4b082ed7.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.kohls.com/lib/PDPRedesign_adobeVisitorTargetUpdatedV2.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;_abck;bm_sz", END_INLINE,
            "URL=https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;_abck;bm_sz", END_INLINE,
            "URL=https://dpm.demdex.net/id?d_visid_ver=4.3.0&d_fieldgroup=MC&d_rtbd=json&d_ver=2&d_verify=1&d_orgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&d_nsid=0&ts=1662113019187", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Content-Type:application/x-www-form-urlencoded", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/id/rd?d_visid_ver=4.3.0&d_fieldgroup=MC&d_rtbd=json&d_ver=2&d_verify=1&d_orgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&d_nsid=0&ts=1662113019187", END_INLINE,
            "URL=https://s2.go-mpulse.net/boomerang/4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.kohls.com/_sec/cp_challenge/sec-3-8.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_abck;check;mbox", END_INLINE,
            "URL=https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_abck;check;mbox", END_INLINE,
            "URL=https://m.kohls.com/dist/common-rev-beaa9cc6a25aed7311b5.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_abck;check;mbox", END_INLINE,
            "URL=https://m.kohls.com/dist/header/header-client-main-bundle-rev-ddc7b6be8680eb830afb.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_abck;check;mbox", END_INLINE,
            "URL=https://m.kohls.com/public/83f85c2c48ff22d123a05e668a2ed087f723b7604ced", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_abck;check;mbox", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20220113-drawer-kcash?fmt=png-alpha", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/hp-20220901-gpo-10off-d-a", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/220815_hp_bts-offer_1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/220815_hp_bts-offer_2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/220815_hp_bts-offer_3", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/220815_hp_bts-offer_4", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.kohls.com/_sec/cp_challenge/sec-cpt-3-8.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_abck;check;mbox", END_INLINE,
            "URL=https://m.kohls.com/dist/images/newheader-rev-7f7fbb.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_abck;check;mbox", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/hp-20220829-jeansplit-01-d", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/hp-20220829-jeansplit-02-d", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/hp-220829-jeans-sale-kids-1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20211225-sephora-slide__03", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/content/kohls/oo_tab_icon_retina", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/AppStore-qr_code2020?scl=1&fmt=png8", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://js-cdn.dynatrace.com/jstag/1740040f04f/bf27853irn/6d81dff759102c9_complete.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://m.kohls.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://bat.bing.com/bat.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("index", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("rd");
    ns_web_url ("rd",
        "URL=https://dpm.demdex.net/id/rd?d_visid_ver=4.3.0&d_fieldgroup=MC&d_rtbd=json&d_ver=2&d_verify=1&d_orgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&d_nsid=0&ts=1662113019187",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://media.kohlsimg.com/is/image/kohls/220815_hp_bts-style-hero-d2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://bat.bing.com/p/action/4024145.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=MUID;MR", END_INLINE,
            "URL=https://6249496.collect.igodigital.com/collect.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://ww8.kohls.com/id?d_visid_ver=4.3.0&d_fieldgroup=MC&mcorgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&ts=1662113020069", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Content-Type:application/x-www-form-urlencoded", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;AKA_A2;bm_sz;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;check;mbox;_abck;dtCookie;rxVisitor;dtLatC;dtSa;rxvt;dtPC", END_INLINE
    );

    ns_end_transaction("rd", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("URUY");
    ns_web_url ("URUY",
        "URL=https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=x-dtpc:-34$113019999_200h3vWTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0e0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;check;mbox;_abck;akacd_RWASP-default-phased-release;dtCookie;rxVisitor;dtLatC;dtSa;rxvt;dtPC",
        BODY_BEGIN,
            "{"sensor_data":"7a74G7m23Vrp0o5c9368031.75-1,2,-94,-100,Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1,uaend,12147,20030107,en-US,Gecko,0,0,0,0,408958,3020176,8192,4096,8192,4096,790,473,790,,cpen:0,i1:0,dm:0,cwen:0,non:1,opc:0,fc:0,sc:0,wrc:1,isc:0,vib:1,bat:1,x11:0,x12:1,11023,0.939666264469,831056510087.5,0,loc:-1,2,-94,-131,-1,2,-94,-101,do_en,dm_en,t_en-1,2,-94,-105,0,0,0,0,1708,-1,0;-1,2,-94,-102,0,0,0,0,1708,-1,0;-1,2,-94,-108,-1,2,-94,-110,-1,2,-94,-117,-1,2,-94,-111,-1,2,-94,-109,-1,2,-94,-114,-1,2,-94,-103,-1,2,-94,-112,https://m.kohls.com/-1,2,-94,-115,1,32,32,0,0,0,0,9,0,1662113020175,-999999,17780,0,0,2963,0,0,13,0,0,B6A6969A0C7F79D7A3819FC0D8E40EDA~-1~YAAQEzPdF6KsUPqCAQAAHfan/QjDF7SgT0kwzBjw3/tUC0fUdmpaxxWnXTbYlhBcpVwm5EEa4KRIyfSvzR8Jhl+/2O1ec670ARDtDWlEZfvNOpNcCjyeXRu7T3RxbcvRCfj/3z/dW5nMnYwJvDDVGM34uCXL5Vo08MjeyflylKPHzveEamK8tu8EJZdiwAZ/zQcuIuFEQUC+TrbwgzJnC9xr/CoahLGEudJhPc0oHkbAiGlE3Kuw7EAKwH4GeCHHdtAIICzp8AljF8nJn/ReSrj1QbG5MToKctYDNdjueCJGnUy6cxdaVChxBpuDlBWfjj+/qqMoakJI58MugfQpmmBeR53y3twXadYhubXa179JHxmsdTH/1ZTt8Cm1fsdvtTWCVAY9b6G9ERkkHYMazcjsljqwHXFqVj0jDgONTgp5LJSKbo1eIa934F4LT2jVfiFS7wBYkCJDL2K8nXKjvYgBAEP0/fvKxWFjUz8m+AaymIcx+GWNKBrjH0FKQrzZlcy5Lwp1KHdyC14A2uez5rqwSQ==~-1~-1~1662116522,49981,-1,-1,30261689,PiZtE,82674,40,0,-1-1,2,-94,-106,0,0-1,2,-94,-119,-1-1,2,-94,-122,0,0,0,0,1,0,0-1,2,-94,-123,-1,2,-94,-124,-1,2,-94,-126,-1,2,-94,-127,8-1,2,-94,-70,-1-1,2,-94,-80,94-1,2,-94,-116,9060480-1,2,-94,-118,100394-1,2,-94,-129,-1,2,-94,-121,;8;-1;0"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210913-kck-top", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.kohls.com/dist/images/kohls_mcom_header-rev-0b8515.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;check;mbox;_abck;akacd_RWASP-default-phased-release;dtCookie;rxVisitor;dtLatC;dtSa;rxvt;dtPC;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;s_ecid", END_INLINE
    );

    ns_end_transaction("URUY", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("category");
    ns_web_url ("category",
        "URL=https://m.kohls.com/api/browse/v2/browse/catalog/category",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=x-correlation-id:MCOM-46f4352e4e7e888ac93c8b7b-b713fc26a3aca27d-1662113020399",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=x-channel:MCOM",
        "HEADER=x-dtpc:-34$113019999_200h4vWTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0e0",
        "HEADER=x-pageid:MCOM-homepage",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;check;mbox;akacd_RWASP-default-phased-release;dtCookie;rxVisitor;dtLatC;dtSa;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;s_ecid;_abck;k_deviceId;Correlation-Id;rxvt;dtPC"
    );

    ns_end_transaction("category", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("banners");
    ns_web_url ("banners",
        "URL=https://m.kohls.com/api/v1/browse/monetization/banners?channel=mobile&pageType=homePage",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=x-correlation-id:MCOM-46f4352e4e7e888ac93c8b7b-b713fc26a3aca27d-1662113020422",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=x-channel:MCOM",
        "HEADER=Content-Type:application/json",
        "HEADER=x-dtpc:-34$113019999_200h5vWTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0e0",
        "HEADER=x-pageid:MCOM-homepage",
        "HEADER=channel:mcom",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;check;mbox;akacd_RWASP-default-phased-release;dtCookie;rxVisitor;dtLatC;dtSa;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;s_ecid;k_deviceId;Correlation-Id;_abck;rxvt;dtPC",
        BODY_BEGIN,
            "null",
        BODY_END,
        INLINE_URLS,
            "URL=https://dpm.demdex.net/id?d_visid_ver=4.3.0&d_fieldgroup=AAM&d_rtbd=json&d_ver=2&d_orgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&d_nsid=0&d_mid=78611155637513935743550952407821114794&ts=1662113020573", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Content-Type:application/x-www-form-urlencoded", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.kohls.com/onlineopinionV5/oo_mcom.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;check;mbox;akacd_RWASP-default-phased-release;rxVisitor;dtLatC;dtSa;s_ecid;k_deviceId;Correlation-Id;rxvt;dtPC;dtCookie;_abck;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg", END_INLINE,
            "URL=https://cdn.dynamicyield.com/api/8776374/api_dynamic.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://edge.curalate.com/sites/kohls-dismfc/site/latest/site.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("banners", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("a9a6fb14_365a_4648_b17b_2e47");
    ns_web_url ("a9a6fb14_365a_4648_b17b_2e47",
        "URL=https://mon.domdog.io/events/report/a9a6fb14-365a-4648-b17b-2e47930f8b49",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "null",
        BODY_END,
        INLINE_URLS,
            "URL=https://bat.bing.com/action/0?ti=4024145&tm=al001&Ver=2&mid=bfacb451-242e-4eee-8f1a-206225edcb33&sid=8538b9f02aa611ed8309ebe712901205&vid=85392ce02aa611ed9f4de589d105e7b7&vids=1&msclkid=N&pi=0&lg=en-US&sw=8192&sh=4096&sc=24&tl=Kohl%27s%20%7C%20Shop%20Clothing,%20Shoes,%20Home,%20Kitchen,%20Bedding,%20Toys%20%26%20More&p=https%3A%2F%2Fm.kohls.com%2F&r=&lt=2208&pt=1662113018421,,,,,332,340,352,352,423,385,423,533,719,557,2205,2206,2208,,,&pn=0,0&evt=pageLoad&sv=1&rn=342995", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=MUID;MR", END_INLINE
    );

    ns_end_transaction("a9a6fb14_365a_4648_b17b_2e47", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("id");
    ns_web_url ("id",
        "URL=https://dpm.demdex.net/id?d_visid_ver=4.3.0&d_fieldgroup=AAM&d_rtbd=json&d_ver=2&d_orgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&d_nsid=0&d_mid=78611155637513935743550952407821114794&ts=1662113020573",
        INLINE_URLS,
            "URL=https://edge.curalate.com/sites/kohls-dismfc/site/latest/site.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.kohls.com/onlineopinionV5/oo_mcom.js", END_INLINE
    );

    ns_end_transaction("id", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("a9a6fb14_365a_4648_b17b_2e47_2");
    ns_web_url ("a9a6fb14_365a_4648_b17b_2e47_2",
        "URL=https://mon.domdog.io/events/report/a9a6fb14-365a-4648-b17b-2e47930f8b49",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            ""{\"url\":\"https://m.kohls.com/\",\"referrer\":\"\",\"DOMContentLoaded\":\"2206\",\"PageLoaded\":\"3140\",\"evalScripts\":[0],\"inlineScripts\":[0],\"inlineScriptElems\":[0],\"inlineScriptAttrs\":[0],\"inlineStyles\":[0],\"inlineStyleElems\":[0],\"inlineStyleAttrs\":[0],\"cspViolations\":[],\"webRTC\":[0],\"keyboardCode\":[0],\"keyboardCharCode\":[0],\"keyboardKey\":[0],\"keyboardKeyCode\":[0],\"selection\":[0],\"inputValue\":[58,\"Error\\n    at s (<anonymous>:2:1412)\\n    at a (<anonymous>:2:83)\\n    at HTMLInputElement.get [as value] (<anonymous>:2:2613)\\n    at Object.getforminfo (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:55713)\\n    at Object.startTracking (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:66010)\\n    at https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:85710\",\"Error\\n    at s (<anonymous>:2:1412)\\n    at a (<anonymous>:2:83)\\n    at HTMLInputElement.get [as value] (<anonymous>:2:2613)\\n    at Object.getforminfo (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:55713)\\n    at Object.bpd (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:59516)\\n    at Object.startTracking (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:66066)\\n    at https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:85710\",\"Error\\n    at s (<anonymous>:2:1412)\\n    at a (<anonymous>:2:83)\\n    at HTMLInputElement.get [as value] (<anonymous>:2:2613)\\n    at https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:29:49789\\n    at https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:29:49904\\n    at https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:19:78\\n    at Object.<anonymous> (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:19:191)\\n    at Object.<anonymous> (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:29:71181)\\n    at t (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:1:101)\\n    at Object.<anonymous> (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:43:113734)\\n    at t (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:1:101)\\n    at https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:1:3255\\n    at https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:1:3266\",\"Error\\n    at s (<anonymous>:2:1412)\\n    at a (<anonymous>:2:83)\\n    at HTMLInputElement.get [as value] (<anonymous>:2:2613)\\n    at https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:29:49898\\n    at https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:29:49904\\n    at https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:19:78\\n    at Object.<anonymous> (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:19:191)\\n    at Object.<anonymous> (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:29:71181)\\n    at t (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:1:101)\\n    at Object.<anonymous> (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:43:113734)\\n    at t (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:1:101)\\n    at https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:1:3255\\n    at https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:1:3266\",\"Error\\n    at s (<anonymous>:2:1412)\\n    at a (<anonymous>:2:83)\\n    at HTMLInputElement.get [as value] (<anonymous>:2:2613)\\n    at Object.postMountWrapper (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:43:63297)\\n    at h.s (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:43:51281)\\n    at e.notifyAll (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:43:6518)\\n    at r.close (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:43:76851)\\n    at r.closeAll (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:29:80259)\\n    at r.perform (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:29:79754)\\n    at s (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:43:13196)\\n    at r.perform (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:29:79671)\\n    at Object.batchedUpdates (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:43:70016)\\n    at Object.i [as batchedUpdates] (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:6:3159)\\n    at Object._renderNewRootComponent (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:43:14442)\\n    at Object._renderSubtreeIntoContainer (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:43:15427)\\n    at Object.render (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:43:15554)\\n    at window.renderHeaderView (https://m.kohls.com/dist/header/header-client-main-bundle-rev-ddc7b6be8680eb830afb.js:1:95710)\\n    at Object.568 (https://m.kohls.com/dist/header/header-client-main-bundle-rev-ddc7b6be8680eb830afb.js:1:95962)\\n    at Object.qa (https://js-cdn.dynatrace.com/jstag/1740040f04f/bf27853irn/6d81dff759102c9_complete.js:376:488)\\n    at t (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:1:101)\\n    at Function.window.webpackJsonp (https://m.kohls.com/dist/vendor-rev-9ba6d4c2ea9fdb9b9cd2.js:1:418)\\n    at ma (https://js-cdn.dynatrace.com/jstag/1740040f04f/bf27853irn/6d81dff759102c9_complete.js:378:145)\\n    at https://m.kohls.com/dist/header/header-client-main-bundle-rev-ddc7b6be8680eb830afb.js:1:1\"],\"inputDefaultValue\":[14,\"Error\\n    at s (<anonymous>:2:1412)\\n    at a (<anonymous>:2:83)\\n    at HTMLInputElement.get [as defaultValue] (<anonymous>:2:2613)\\n    at Object.getforminfo (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:55697)\\n    at Object.startTracking (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:66010)\\n    at https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:85710\",\"Error\\n    at s (<anonymous>:2:1412)\\n    at a (<anonymous>:2:83)\\n    at HTMLInputElement.get [as defaultValue] (<anonymous>:2:2613)\\n    at Object.getforminfo (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:55697)\\n    at Object.bpd (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:59516)\\n    at Object.startTracking (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:66066)\\n    at https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:85710\",\"Error\\n    at s (<anonymous>:2:1412)\\n    at a (<anonymous>:2:83)\\n    at HTMLInputElement.get [as defaultValue] (<anonymous>:2:2613)\\n    at Object.getforminfo (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:55697)\\n    at Object.bpd (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:59516)\\n    at calc_fp (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:76012)\",\"Error\\n    at s (<anonymous>:2:1412)\\n    at a (<anonymous>:2:83)\\n    at HTMLInputElement.get [as defaultValue] (<anonymous>:2:2613)\\n    at Object.getforminfo (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:55697)\\n    at Object.bpd (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:59516)\\n    at mn_w (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:75451)\",\"Error\\n    at s (<anonymous>:2:1412)\\n    at a (<anonymous>:2:83)\\n    at HTMLInputElement.get [as defaultValue] (<anonymous>:2:2613)\\n    at Object.getforminfo (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:55697)\\n    at Object.bpd (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:59516)\\n    at Object.cma (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:44754)\\n    at HTMLDocument.hmd (https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY:1:56541)\"],\"inputValueAsDate\":[0],\"inputValueAsNumber\":[0]}"",
        BODY_END,
        INLINE_URLS,
            "URL=https://cm.everesttech.net/cm/dd?d_uuid=72195082367942492304066494206891021442", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=411&dpuuid=YxHU-AAAAHgJAwOA", END_INLINE,
            "URL=https://kohls.demdex.net/dest5.html?d_nsid=0#https%3A%2F%2Fm.kohls.com", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("a9a6fb14_365a_4648_b17b_2e47_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("json");
    ns_web_url ("json",
        "URL=https://kohls.tt.omtrdc.net/m2/kohls/mbox/json?mbox=target-global-mbox&mboxSession=460f1c38cfda4554a42e960007135e4b&mboxPC=&mboxPage=6d58d725e79946c9a0f3fd8a3eb4d183&mboxRid=bf500ad092f44aeab9505b62cf8799bc&mboxVersion=1.7.1&mboxCount=1&mboxTime=1662095019281&mboxHost=m.kohls.com&mboxURL=https%3A%2F%2Fm.kohls.com%2F&mboxReferrer=&browserHeight=473&browserWidth=790&browserTimeOffset=-300&screenHeight=4096&screenWidth=8192&colorDepth=24&devicePixelRatio=1&screenOrientation=landscape&webGLRenderer=Google%20SwiftShader&at_property=b1ebe2fe-cdb6-8289-fe4c-133434dc93f1&customerLoggedStatus=false&tceIsRedesign=True&tceIsPDPRedesign=True&tceIsPMPResponsive2=false&tceIsPDPResponsive3=false&tceIsPDPResponsive4=false&mboxMCSDID=11260FC847338341-4CDA47DE574B2E82&vst.trk=ww9.kohls.com&vst.trks=ww8.kohls.com&mboxMCGVID=78611155637513935743550952407821114794&mboxAAMB=j8Odv6LonN4r3an7LhD3WZrU1bUpAkFkkiY1ncBR96t2PTI&mboxMCGLH=7",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://static.domdog.io/success", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("json", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("URUY_2");
    ns_web_url ("URUY_2",
        "URL=https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=x-dtpc:4$113019999_200h11vWTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0e0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;check;mbox;akacd_RWASP-default-phased-release;rxVisitor;dtLatC;dtSa;s_ecid;k_deviceId;Correlation-Id;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;_abck;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;rxvt;dtPC",
        BODY_BEGIN,
            "{"sensor_data":"7a74G7m23Vrp0o5c9368031.75-1,2,-94,-100,Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1,uaend,12147,20030107,en-US,Gecko,0,0,0,0,408958,3020176,8192,4096,8192,4096,790,473,790,,cpen:0,i1:0,dm:0,cwen:0,non:1,opc:0,fc:0,sc:0,wrc:1,isc:0,vib:1,bat:1,x11:0,x12:1,11023,0.924371799462,831056510087.5,0,loc:-1,2,-94,-131,-1,2,-94,-101,do_en,dm_en,t_en-1,2,-94,-105,0,0,0,0,1708,-1,0;-1,2,-94,-102,0,0,0,0,1708,-1,0;-1,2,-94,-108,-1,2,-94,-110,-1,2,-94,-117,-1,2,-94,-111,-1,2,-94,-109,-1,2,-94,-114,-1,2,-94,-103,-1,2,-94,-112,https://m.kohls.com/-1,2,-94,-115,1,32,32,0,0,0,0,606,0,1662113020175,39,17780,0,0,2963,0,0,607,0,0,B6A6969A0C7F79D7A3819FC0D8E40EDA~-1~YAAQEzPdF/6sUPqCAQAA3Pqn/QjStSUak7oPnoDIU0HiwT/5JVTiVooSHDioSHsSNr1TL+AYOgnjQIBvhU8xPIM9kViaCnCOQtFdv88XlzkZG09HkZwIAdcvbF/haUxAOq0Vi9kqnzH5kvB22B156XyEjTqUZqZPwgnU2MpYjz0RVf0UmbpirLlMhuAS5OCOQ9GJvCJP0JNeMRFuZBWqueWa40DiNjw3OZTKzyBPvqT0DqaS0khT6JqokhoLmOxYt17nuGngdEQrSCgAWKUbdtkpNL8JFTsz34kezUe1nnWwM4Ige7fciGTOkKTiRyxLXg4vKlPfjeT+3mVDMg3al3yDgtolcfB5ASRpvpq2043Cs9yj+67k6tYDunlvgeLUa7TPthlEaP0qG24CXl+BqfPK6i/HM2UnTx2ydql0HFzxeAorLgCm3Nysulyj04vJDZxbddVVpMGgohEpKMgC3/9UiwUXOhAVzcSbWKAZJx8wQjIpxjTcuXeLZlqbHBrBykT6fqmR2fVRLIzlF1dex3jGu/sLIHHH7Msa~-1~||1-RFqQTQmqaq-1-10-1000-2||~-1,52770,939,-206599699,30261689,PiZtE,40179,76,0,-1-1,2,-94,-106,9,1-1,2,-94,-119,-1-1,2,-94,-122,0,0,0,0,1,0,0-1,2,-94,-123,-1,2,-94,-124,-1,2,-94,-126,-1,2,-94,-127,00300044040300043020-1,2,-94,-70,-1127778619;-563023189;dis;;true;true;true;300;true;24;24;true;false;-1-1,2,-94,-80,5383-1,2,-94,-116,9060480-1,2,-94,-118,104480-1,2,-94,-129,,,0,,,,0-1,2,-94,-121,;3;25;0"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://cdn.dynamicyield.com/api/8776374/api_static.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=411&dpuuid=YxHU-AAAAHgJAwOA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=411&dpuuid=YxHU-AAAAHgJAwOA", END_INLINE
    );

    ns_end_transaction("URUY_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("config_json");
    ns_web_url ("config_json",
        "URL=https://c.go-mpulse.net/api/config.json?key=4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T&d=m.kohls.com&t=5540377&v=1.720.0&sl=0&si=39cce6ea-7e0c-4127-9bc9-0302a86d58aa-rhkum2&plugins=AK,ConfigOverride,Continuity,PageParams,IFrameDelay,AutoXHR,SPA,History,Angular,Backbone,Ember,RT,CrossDomain,BW,PaintTiming,NavigationTiming,ResourceTiming,Memory,CACHE_RELOAD,Errors,TPAnalytics,UserTiming,Akamai,Early,EventTiming,LOGN&acao=&ak.ai=223330",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://aa.agkn.com/adscores/g.pixel?sid=9211132908&aam=72195082367942492304066494206891021442", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://st.dynamicyield.com/st?sec=8776374&inHead=true&id=0&jsession=&ref=&scriptVersion=1.129.0&dyid_server=&ctx=%7B%22type%22%3A%22HOMEPAGE%22%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=21&dpuuid=209180804262002690585", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=21&dpuuid=209180804262002690585", END_INLINE,
            "URL=https://track.coherentpath.com/v1/track.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://tjxbfc1n.micpn.com/p/js/1.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cdn.evgnet.com/beacon/kohlsinc2/engage/scripts/evergage.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=411&dpuuid=YxHU-AAAAHgJAwOA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://idsync.rlcdn.com/365868.gif?partner_uid=72195082367942492304066494206891021442", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("config_json", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("browsers");
    ns_web_url ("browsers",
        "URL=https://track.coherentpath.com/v1/websites/browsers",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=Authorization:Bearer public_2db5390c174f9d35981b14ad5500a899",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"browser":{}}",
        BODY_END
    );

    ns_end_transaction("browsers", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("URUY_3");
    ns_web_url ("URUY_3",
        "URL=https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=x-dtpc:4$113019999_200h13vWTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0e0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtLatC;dtSa;s_ecid;k_deviceId;Correlation-Id;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_abck;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dy_soct;rxvt;dtPC",
        BODY_BEGIN,
            "{"sensor_data":"7a74G7m23Vrp0o5c9368031.75-1,2,-94,-100,Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1,uaend,12147,20030107,en-US,Gecko,0,0,0,0,408958,3020176,8192,4096,8192,4096,790,473,790,,cpen:0,i1:0,dm:0,cwen:0,non:1,opc:0,fc:0,sc:0,wrc:1,isc:0,vib:1,bat:1,x11:0,x12:1,11023,0.985110359492,831056510087.5,0,loc:-1,2,-94,-131,-1,2,-94,-101,do_en,dm_en,t_en-1,2,-94,-105,0,0,0,0,1708,-1,0;-1,2,-94,-102,0,0,0,0,1708,-1,0;-1,2,-94,-108,-1,2,-94,-110,-1,2,-94,-117,-1,2,-94,-111,-1,2,-94,-109,-1,2,-94,-114,-1,2,-94,-103,-1,2,-94,-112,https://m.kohls.com/-1,2,-94,-115,1,32,32,0,0,0,0,1160,0,1662113020175,39,17780,0,0,2963,0,0,1164,0,0,B6A6969A0C7F79D7A3819FC0D8E40EDA~-1~YAAQEzPdFxCtUPqCAQAAAvyn/Qi7l5/Q8lfm/45FXGbFVvrXdbFa84QuwISbyj8JsWczKEiwtxUTz6oQ3mCMjrmeOADWrddhxqlN9UQDiiVmKa+VdCXY9asv7Ay9JCdJXZ45uFl6AMKJc6jGi8pPwLuWgwNV237SqdCVyFbOAi3x373M6fzSLnzlPKxM5rpozXMAcu+vVksRdiv8xniTXPaayVXe+suMYZMUh5vjOhM71liP7wDRBHMC/QqXM2MP3wJvX984CYePWX15F1FNUcl5v905hOlewTKQ6m4t0TaM1cekbb1LW4YfjV6/JLYsjml7pXWbRfiOaH24zLGC9C+Q3QYTHQrr6IrWwEWmg7TsYb1fvg712SNogww/rF8ygxfjvOb/1wo6Kh6HAKCAed3ezLLSGe+iPiLD5lZIHWDGYd5Vs6wrTVlgU3qDXIwLMMfAlhog2Ltx6pXyhqMqXE51trVIYyplz9ebVdFWvfSxMjAmr7bqLYZWUde0O+7ClQnoEKAyfZEmy0EM45PEgnKkajbaNuvIJ9xB~-1~||1-RFqQTQmqaq-1-10-1000-2||~-1,52191,939,-206599699,30261689,PiZtE,45609,61,0,-1-1,2,-94,-106,8,2-1,2,-94,-119,-1-1,2,-94,-122,0,0,0,0,1,0,0-1,2,-94,-123,-1,2,-94,-124,0.2f0f807076ac2,0.861ad1e481f0c,0.e251b481dbc67,0.2c32b585edce1,0.6fdb1695291ad,0.2a4cdc14dbdef,0.f622f934604e1,0.174b55d6e8245,0.9afb0bd2a4ca7,0.c257684bacd48;1,0,1,1,6,0,7,1,3,2;0,0,2,0,6,1,4,2,9,5;B6A6969A0C7F79D7A3819FC0D8E40EDA,1662113020175,RFqQTQmqaq,B6A6969A0C7F79D7A3819FC0D8E40EDA1662113020175RFqQTQmqaq,1,1,0.2f0f807076ac2,B6A6969A0C7F79D7A3819FC0D8E40EDA1662113020175RFqQTQmqaq10.2f0f807076ac2,130,136,155,59,99,147,202,88,82,14,195,255,246,169,193,61,67,90,26,35,126,90,57,242,81,173,205,204,127,169,4,194,433,0,1662113021335;-1,2,-94,-126,-1,2,-94,-127,00300044040300043020-1,2,-94,-70,-1127778619;-563023189;dis;;true;true;true;300;true;24;24;true;false;-1-1,2,-94,-80,5383-1,2,-94,-116,9060480-1,2,-94,-118,136170-1,2,-94,-129,,,0,,,,0-1,2,-94,-121,;7;25;0"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=21&dpuuid=209180804262002690585", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://cdn.dynamicyield.com/scripts/1.129.0/dy-coll-nojq-min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=DYID;DYSES", END_INLINE,
            "URL=https://tjxbfc1n.micpn.com/p/cp/-1/track.gif?t=1662113021384&mi_u=anon-1662113021381-1801882823&mi_cid=8212&page_title=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&timezone_offset=300&event_type=pageview&cdate=1662113021381&ck=false&anon=true", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://idsync.rlcdn.com/1000.gif?memo=CKyqFhIxCi0IARCYEhomNzIxOTUwODIzNjc5NDI0OTIzMDQwNjY0OTQyMDY4OTEwMjE0NDIQABoNCP2px5gGEgUI6AcQAEIASgA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=rlas3;pxrc", END_INLINE,
            "URL=https://ib.adnxs.com/getuid?https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D358%26dpuuid%3D%24UID", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=477&dpuuid=924da3c7797cbc4152b7981857f3012ea393198e424bba212ca820f7e0b19920b0da87c991749652", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=477&dpuuid=924da3c7797cbc4152b7981857f3012ea393198e424bba212ca820f7e0b19920b0da87c991749652", END_INLINE,
            "URL=https://async-px.dynamicyield.com/dpx?cnst=1&_=803288&name=User%20Type&props=%7B%22type%22%3A%22new%22%7D&uid=-1233986090078972675&sec=8776374&cl=d.an.c.ss.&ses=5b3e117ce067eeb99b2b5b268b908b68&l=def&p=1&sd=&rf=&trf=0&aud=1408117.1438654.1362538.1362542.1468187&url=https%3A%2F%2Fm.kohls.com%2F&exps=%5B%5B%221067036%22%2C%229863190%22%2C%2225792075%22%2C0%2Cnull%2Cnull%2C%22-5734382000971782118%22%2C%222%22%2C%223%22%5D%2C%5B%221096558%22%2C%2210092862%22%2C%2226047526%22%2C0%2Cnull%2Cnull%2C%22-5734382002361846856%22%2C%221%22%2Cnull%5D%2C%5B%221127582%22%2C%2210281141%22%2C%2226223332%22%2C0%2Cnull%2Cnull%2C%22-5734382000662591173%22%2C%221%22%2Cnull%5D%2C%5B%221179251%22%2C%2211202288%22%2C%2226647761%22%2C0%2Cnull%2Cnull%2C%22-5734382000772232028%22%2C%221%22%2Cnull%5D%2C%5B%221289933%22%2C%2211374557%22%2C%2227236876%22%2C0%2Cnull%2Cnull%2C%22-5734382000036239809%22%2C%221%22%2Cnull%5D%2C%5B%221309946%22%2C%2211499500%22%2C%2227308034%22%2C0%2Cnull%2Cnull%2C%22-5734382000503099902%22%2C%221%22%2Cnull%5D%2C%5B%221245855%22%2C%2211044797%22%2C%2227049604%22%2C0%2Cnull%2Cnull%2C%22-5734382003904465510%22%2C%221%22%2Cnull%5D%5D&expSes=71849&tsrc=Direct&reqts=1662113021487&rri=3343520&geoData=US__", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("URUY_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("uia");
    ns_web_url ("uia",
        "URL=https://async-px.dynamicyield.com/uia?cnst=1&_=1662113021488",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "id=-1233986090078972675&se=8776374&cl=d.an.c.ss.&rf=&trf=0&p=1&sub=m.kohls.com&sd=&url=https%3A%2F%2Fm.kohls.com%2F&title=Kohl's%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&lay=def&ses=5b3e117ce067eeb99b2b5b268b908b68&aud=1408117.1438654.1362538.1362542&gv=&exps=%5B%5B%221067036%22%2C%229863190%22%2C%2225792075%22%2C0%2Cnull%2Cnull%2C%22-5734382000971782118%22%2C%222%22%2C%223%22%5D%2C%5B%221096558%22%2C%2210092862%22%2C%2226047526%22%2C0%2Cnull%2Cnull%2C%22-5734382002361846856%22%2C%221%22%2Cnull%5D%2C%5B%221127582%22%2C%2210281141%22%2C%2226223332%22%2C0%2Cnull%2Cnull%2C%22-5734382000662591173%22%2C%221%22%2Cnull%5D%2C%5B%221179251%22%2C%2211202288%22%2C%2226647761%22%2C0%2Cnull%2Cnull%2C%22-5734382000772232028%22%2C%221%22%2Cnull%5D%2C%5B%221289933%22%2C%2211374557%22%2C%2227236876%22%2C0%2Cnull%2Cnull%2C%22-5734382000036239809%22%2C%221%22%2Cnull%5D%2C%5B%221309946%22%2C%2211499500%22%2C%2227308034%22%2C0%2Cnull%2Cnull%2C%22-5734382000503099902%22%2C%221%22%2Cnull%5D%5D&lts=5%3A3&ctx=%7B%22type%22%3A%22HOMEPAGE%22%7D&lpInfo=false&expSes=71849&br=Motorola&tsrc=Direct&geoData=US__&feedProps%5Bcategories%5D=&feedProps%5Bkeywords%5D=&cookieInfo=0.0.0&reqts=1662113021471&rri=1616431&nocks=false",
        BODY_END,
        INLINE_URLS,
            "URL=https://async-px.dynamicyield.com/var?cnst=1&_=769966&uid=-1233986090078972675&sec=8776374&t=ri&e=1096558&p=1&ve=10092862&va=%5B26047526%5D&ses=5b3e117ce067eeb99b2b5b268b908b68&expSes=71849&aud=1408117.1438654.1362538.1362542.1468187&expVisitId=-5734382002361846856&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1662113021511&rri=8735328", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://async-px.dynamicyield.com/var?cnst=1&_=624644&uid=-1233986090078972675&sec=8776374&t=ri&e=1127582&p=1&ve=10281141&va=%5B26223332%5D&ses=5b3e117ce067eeb99b2b5b268b908b68&expSes=71849&aud=1408117.1438654.1362538.1362542.1468187&expVisitId=-5734382000662591173&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1662113021516&rri=3133617", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://async-px.dynamicyield.com/var?cnst=1&_=367790&uid=-1233986090078972675&sec=8776374&t=ri&e=1179251&p=1&ve=11202288&va=%5B26647761%5D&ses=5b3e117ce067eeb99b2b5b268b908b68&expSes=71849&aud=1408117.1438654.1362538.1362542.1468187&expVisitId=-5734382000772232028&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1662113021523&rri=9338748", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://async-px.dynamicyield.com/var?cnst=1&_=834486&uid=-1233986090078972675&sec=8776374&t=ri&e=1289933&p=1&ve=11374557&va=%5B27236876%5D&ses=5b3e117ce067eeb99b2b5b268b908b68&expSes=71849&aud=1408117.1438654.1362538.1362542.1468187&expVisitId=-5734382000036239809&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1662113021526&rri=3744554", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=477&dpuuid=924da3c7797cbc4152b7981857f3012ea393198e424bba212ca820f7e0b19920b0da87c991749652", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://async-px.dynamicyield.com/var?cnst=1&_=747805&uid=-1233986090078972675&sec=8776374&t=ri&e=1309946&p=1&ve=11499500&va=%5B27308034%5D&ses=5b3e117ce067eeb99b2b5b268b908b68&expSes=71849&aud=1408117.1438654.1362538.1362542.1468187&expVisitId=-5734382000503099902&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1662113021531&rri=5797107", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.kohls.com/dist/css/fonts/hfjfont-bundle-rev-299dfdec408a81d5c00d76e18dd5db1d.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtLatC;dtSa;s_ecid;k_deviceId;Correlation-Id;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_abck;rxvt;dtPC", END_INLINE,
            "URL=https://m.kohls.com/dist/css/header/css-mcom-bundle-defer-rev-1914c8f4a6d5d4899fa46915baa48c89.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtLatC;dtSa;s_ecid;k_deviceId;Correlation-Id;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_abck;rxvt;dtPC", END_INLINE,
            "URL=https://m.kohls.com/dist/recomendation/recomendations-BD-bundle-rev-045c91074147e0ace9be.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtLatC;dtSa;s_ecid;k_deviceId;Correlation-Id;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_abck;rxvt;dtPC", END_INLINE,
            "URL=https://m.kohls.com/dist/personalization/personalization-bundle-rev-ff45f0763a8dbe15b1c6.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtLatC;dtSa;s_ecid;k_deviceId;Correlation-Id;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_abck;rxvt;dtPC", END_INLINE,
            "URL=https://m.kohls.com/dist/homepage/home-page-deferred-bundle-rev-d8ee69f049d96f0c57c7.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtLatC;dtSa;s_ecid;k_deviceId;Correlation-Id;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_abck;rxvt;dtPC", END_INLINE,
            "URL=https://m.kohls.com/dist/pixelTracking/pixelTracking-bundle-rev-e9d590fc2f8aca922ba6.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtLatC;dtSa;s_ecid;k_deviceId;Correlation-Id;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_abck;rxvt;dtPC", END_INLINE,
            "URL=https://m.kohls.com/dist/footer/app-bundle-rev-f65e3390f830c3f87c72.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akaalb_m_kohls_com;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtLatC;dtSa;s_ecid;k_deviceId;Correlation-Id;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_abck;rxvt;dtPC", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC6f229984ef444c0cbd04f0d3a22f28f7-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dp2.33across.com/ps/?pid=897&random=1767302772", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("uia", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("batch");
    ns_web_url ("batch",
        "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1662113021636_652479",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=DYID;DYSES",
        BODY_BEGIN,
            "{"md":{"uid":"-1233986090078972675","sec":"8776374","ses":"5b3e117ce067eeb99b2b5b268b908b68","colors":"d.an.c.ss.","sld":"kohls.com","rurl":"https://m.kohls.com/","ts":"Direct","p":"1","ref":"","rref":"","reqts":1662113021636,"rri":9323262},"data":[{"type":"imp","expId":1096558,"varIds":[26047526],"expSes":"71849","mech":"1","smech":null,"verId":"10092862","aud":"1408117.1438654.1362538.1362542","expVisitId":"-5734382002361846856","eri":null},{"type":"imp","expId":1127582,"varIds":[26223332],"expSes":"71849","mech":"1","smech":null,"verId":"10281141","aud":"1408117.1438654.1362538.1362542","expVisitId":"-5734382000662591173","eri":null},{"type":"imp","expId":1179251,"varIds":[26647761],"expSes":"71849","mech":"1","smech":null,"verId":"11202288","aud":"1408117.1438654.1362538.1362542","expVisitId":"-5734382000772232028","eri":null},{"type":"imp","expId":1289933,"varIds":[27236876],"expSes":"71849","mech":"1","smech":null,"verId":"11374557","aud":"1408117.1438654.1362538.1362542","expVisitId":"-5734382000036239809","eri":null},{"type":"imp","expId":1309946,"varIds":[27308034],"expSes":"71849","mech":"1","smech":null,"verId":"11499500","aud":"1408117.1438654.1362538.1362542","expVisitId":"-5734382000503099902","eri":null},{"type":"imp","expId":1245855,"varIds":[27049604],"expSes":"71849","mech":"1","smech":null,"verId":"11044797","aud":"1408117.1438654.1362538.1362542.1468187","expVisitId":"-5734382003904465510","eri":null}],"sectionId":"8776374"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://media.kohlsimg.com/is/image/kohls/20220901-D-HP-KMN-RayBanBanner", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://async-px.dynamicyield.com/var?cnst=1&_=957504&uid=-1233986090078972675&sec=8776374&t=ri&e=1245855&p=1&ve=11044797&va=%5B27049604%5D&ses=5b3e117ce067eeb99b2b5b268b908b68&expSes=71849&aud=1408117.1438654.1362538.1362542.1468187&expVisitId=-5734382003904465510&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1662113021655&rri=1879711", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("batch", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("manifest_json");
    ns_web_url ("manifest_json",
        "URL=https://m.kohls.com/manifest.json",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://c.tvpixel.com/js/current/dpm_pixel_min.js?aid=kohls-0b87d80d-1f10-41e1-9b1a-e53161757636", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RCefe32536577a4558abe3c8dcd8da7ca6-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://adb2waycm-atl.netmng.com/cm/", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=601&dpuuid=211962501467560&random=1662113021", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=601&dpuuid=211962501467560&random=1662113021", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=adobe_dmp&google_cm&gdpr=0&gdpr_consent=&google_hm=NzIxOTUwODIzNjc5NDI0OTIzMDQwNjY0OTQyMDY4OTEwMjE0NDI=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=601&dpuuid=211962501467560&random=1662113021", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=adobe_dmp&google_cm=&gdpr=0&gdpr_consent=&google_hm=NzIxOTUwODIzNjc5NDI0OTIzMDQwNjY0OTQyMDY4OTEwMjE0NDI=&google_tc=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=test_cookie", END_INLINE,
            "URL=https://navdmp.com/req?adID=72195082367942492304066494206891021442", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=771&dpuuid=CAESEPEpYQM5PIPYWKpbQCfsheQ&google_cver=1?gdpr=0&gdpr_consent=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=771&dpuuid=CAESEPEpYQM5PIPYWKpbQCfsheQ&google_cver=1", END_INLINE
    );

    ns_end_transaction("manifest_json", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("clog");
    ns_web_url ("clog",
        "URL=https://px.dynamicyield.com/clog",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "[{"name":"getProductInterest func error","section":"8776374","message":"DYID:-1233986090078972675,Message: Cannot read property 'weekly' of undefined","stack":"TypeError: Cannot read property 'weekly' of undefined\n    at i (https://cdn.dynamicyield.com/api/8776374/api_static.js:9:5945)\n    at h (https://cdn.dynamicyield.com/api/8776374/api_static.js:9:5830)\n    at purchase (https://cdn.dynamicyield.com/api/8776374/api_static.js:9:6635)\n    at Object.asyncMatchNow (https://cdn.dynamicyield.com/api/8776374/api_static.js:5:31945)\n    at s (https://cdn.dynamicyield.com/api/8776374/api_static.js:9:22269)\n    at https://cdn.dynamicyield.com/api/8776374/api_static.js:9:22483\n    at Array.map (<anonymous>)\n    at Ba (https://cdn.dynamicyield.com/api/8776374/api_static.js:10:19943)\n    at u (https://cdn.dynamicyield.com/api/8776374/api_static.js:9:22456)"}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://cdn.navdmp.com/req?adID=72195082367942492304066494206891021442", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=771&dpuuid=CAESEPEpYQM5PIPYWKpbQCfsheQ&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://idpix.media6degrees.com/orbserv/hbpix?pixId=16873&pcv=70&ptid=66&tpuv=01&tpu=72195082367942492304066494206891021442", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC9e2188c5c7da4b09b1b6062b3fb3ea7c-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://s.yimg.com/wi/ytc.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.kohls.com/dist/libs/z1m_v4.192.js?v=a42f899823411882d6a96648c3627159", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtLatC;dtSa;s_ecid;k_deviceId;Correlation-Id;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;rxvt;dtPC;_abck;_dpm_ses.f2d1;_dpm_id.f2d1", END_INLINE,
            "URL=https://m.kohls.com/dist/global/nudatainitialize-bundle-rev-17d1ecd218000f105eba.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtLatC;dtSa;s_ecid;k_deviceId;Correlation-Id;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;rxvt;dtPC;_abck;_dpm_ses.f2d1;_dpm_id.f2d1", END_INLINE,
            "URL=https://analytics.twitter.com/i/adsct?p_user_id=72195082367942492304066494206891021442&p_id=38594", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://js-sec.indexww.com/ht/p/184399-39002588238727.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagservices.com/tag/js/gpt.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.kohls.com/lib/recommendation/mcom/bd-experience-rendering-sdk.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtLatC;dtSa;s_ecid;k_deviceId;Correlation-Id;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;rxvt;dtPC;_abck;_dpm_ses.f2d1;_dpm_id.f2d1", END_INLINE,
            "URL=https://m.kohls.com/dist/css/recomendation/recomend-mcom-css-bundle-rev-18b9a5e7143933eb2f20ec8ef3b7485a.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtLatC;dtSa;s_ecid;k_deviceId;Correlation-Id;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;rxvt;dtPC;_abck;_dpm_ses.f2d1;_dpm_id.f2d1", END_INLINE,
            "URL=https://cdns.brsrvr.com/v1/br-trk-5117.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://data.adxcel-ec2.com/pixel/?ad_log=referer&action=lead&pixid=abb66c3c-867a-4190-88de-5a99532db190", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC3c54cc4632f0434dae1a686344741ddc-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("clog", NS_AUTO_STATUS);

    //Page Auto split for Ajax Header = XMLHttpRequest
    ns_start_transaction("s_code_js");
    ns_web_url ("s_code_js",
        "URL=https://m.kohls.com/lib/s_code.js",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-Requested-With:XMLHttpRequest",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtLatC;dtSa;s_ecid;k_deviceId;Correlation-Id;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;rxvt;dtPC;_dpm_ses.f2d1;_dpm_id.f2d1;m_prodMerch;akaalb_m_kohls_com;_abck;m_deviceID",
        INLINE_URLS,
            "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fpixel.everesttech.net%2F1x1%3F", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WXhIVS1nQUFBWDdYUkRVVg&url=/1/gr%3furl=https%253A%252F%252Fpixel.everesttech.net%252F1x1%253F", END_INLINE
    );

    ns_end_transaction("s_code_js", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("X0172875_json");
    ns_web_url ("X0172875_json",
        "URL=https://s.yimg.com/wi/config/10172875.json",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://fc.kohls.com/2.2/w/w-756138/sync/js/", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;AKA_A2;bm_sz;check;rxVisitor;dtLatC;dtSa;s_ecid;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;rxvt;dtPC;_dpm_ses.f2d1;_dpm_id.f2d1;_abck", END_INLINE,
            "URL=https://sp.analytics.yahoo.com/sp.pl?a=10000&d=Fri%2C%2002%20Sep%202022%2010%3A03%3A42%20GMT&n=5d&b=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&.yp=10172875&f=https%3A%2F%2Fm.kohls.com%2F&enc=UTF-8&yv=1.13.0&tagmgr=adobe", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sp.analytics.yahoo.com/sp.pl?a=10000&b=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&.yp=10172875&f=https%3A%2F%2Fm.kohls.com%2F&enc=UTF-8&yv=1.13.0&et=custom&ea=&ev=&gv=&product_sku=not%20set&product_id=not%20set&conversion_product_name=not%20set&conversion_sku=not%20set&conversion_product_id=not%20set&conversion_revenue=not%20set&tagmgr=adobe", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("X0172875_json", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("tp2");
    ns_web_url ("tp2",
        "URL=https://p.tvpixel.com/com.snowplowanalytics.snowplow/tp2",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"schema":"iglu:com.snowplowanalytics.snowplow/payload_data/jsonschema/1-0-4","data":[{"e":"pv","url":"https://m.kohls.com/","page":"Kohl's | Shop Clothing, Shoes, Home, Kitchen, Bedding, Toys & More","tv":"js-2.14.0","tna":"co","aid":"kohls-0b87d80d-1f10-41e1-9b1a-e53161757636","p":"web","tz":"America/Chicago","lang":"en-US","cs":"UTF-8","res":"8192x4096","cd":"24","cookie":"1","eid":"e234c89f-40ef-4793-b615-3458f7f0254e","dtm":"1662113022135","cx":"eyJzY2hlbWEiOiJpZ2x1OmNvbS5zbm93cGxvd2FuYWx5dGljcy5zbm93cGxvdy9jb250ZXh0cy9qc29uc2NoZW1hLzEtMC0wIiwiZGF0YSI6W3sic2NoZW1hIjoiaWdsdTpjb20uc25vd3Bsb3dhbmFseXRpY3Muc25vd3Bsb3cvd2ViX3BhZ2UvanNvbnNjaGVtYS8xLTAtMCIsImRhdGEiOnsiaWQiOiJkMDdlN2ExOS03MmMzLTQ0M2QtOTA2OS02YzBiYTNkYmIwNjgifX0seyJzY2hlbWEiOiJpZ2x1Om9yZy53My9QZXJmb3JtYW5jZVRpbWluZy9qc29uc2NoZW1hLzEtMC0wIiwiZGF0YSI6eyJuYXZpZ2F0aW9uU3RhcnQiOjE2NjIxMTMwMTg0MjEsInVubG9hZEV2ZW50U3RhcnQiOjAsInVubG9hZEV2ZW50RW5kIjowLCJyZWRpcmVjdFN0YXJ0IjowLCJyZWRpcmVjdEVuZCI6MCwiZmV0Y2hTdGFydCI6MTY2MjExMzAxODc1MywiZG9tYWluTG9va3VwU3RhcnQiOjE2NjIxMTMwMTg3NjEsImRvbWFpbkxvb2t1cEVuZCI6MTY2MjExMzAxODc3MywiY29ubmVjdFN0YXJ0IjoxNjYyMTEzMDE4NzczLCJjb25uZWN0RW5kIjoxNjYyMTEzMDE4ODQ0LCJzZWN1cmVDb25uZWN0aW9uU3RhcnQiOjE2NjIxMTMwMTg4MDYsInJlcXVlc3RTdGFydCI6MTY2MjExMzAxODg0NCwicmVzcG9uc2VTdGFydCI6MTY2MjExMzAxODk1NCwicmVzcG9uc2VFbmQiOjE2NjIxMTMwMTkxNDAsImRvbUxvYWRpbmciOjE2NjIxMTMwMTg5NzgsImRvbUludGVyYWN0aXZlIjoxNjYyMTEzMDIwNjI2LCJkb21Db250ZW50TG9hZGVkRXZlbnRTdGFydCI6MTY2MjExMzAyMDYyNywiZG9tQ29udGVudExvYWRlZEV2ZW50RW5kIjoxNjYyMTEzMDIwNjI5LCJkb21Db21wbGV0ZSI6MTY2MjExMzAyMTU0NSwibG9hZEV2ZW50U3RhcnQiOjE2NjIxMTMwMjE1NTMsImxvYWRFdmVudEVuZCI6MTY2MjExMzAyMTU5N319XX0","vp":"790x473","ds":"775x9779","vid":"1","sid":"fdcd708f-da42-48fb-bc5f-140de4e1a196","duid":"60ed2d0e-ab34-4988-960e-416c631cdea4","stm":"1662113022139"}]}",
        BODY_END,
        INLINE_URLS,
            "URL=https://securepubads.g.doubleclick.net/gpt/pubads_impl_2022083101.js?cb=31069285", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE
    );

    ns_end_transaction("tp2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("ppub_config");
    ns_web_url ("ppub_config",
        "URL=https://securepubads.g.doubleclick.net/pagead/ppub_config?ippd=m.kohls.com",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fus-u.openx.net%2Fw%2F1.0%2Fsd%3Fid%3D537072980%26val%3D__EFGSURFER__.__EFGCK__", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WXhIVS1nQUFBVm1PR0RkVg&url=/1/gr%3furl=https%253A%252F%252Fus-u.openx.net%252Fw%252F1.0%252Fsd%253Fid%253D537072980%2526val%253D__EFGSURFER__.__EFGCK__", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WXhIVS1nQUFBWDdYUkRVVg&url=/1/gr%3furl=https%253A%252F%252Fpixel.everesttech.net%252F1x1%253F", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=992&dpuuid=1h39lx4qdv86e", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=992&dpuuid=1h39lx4qdv86e", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WXhIVS1nQUFBVm1PR0RkVg&url=/1/gr%3furl=https%253A%252F%252Fus-u.openx.net%252Fw%252F1.0%252Fsd%253Fid%253D537072980%2526val%253D__EFGSURFER__.__EFGCK__", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s94862480279081?AQB=1&ndh=1&pf=1&callback=s_c_il[1].doPostbacks&et=1&t=2%2F8%2F2022%205%3A3%3A42%205%20300&d.&nsid=0&jsonv=1&.d&sdid=11260FC847338341-4CDA47DE574B2E82&mid=78611155637513935743550952407821114794&aamlh=7&ce=UTF-8&ns=kohls&pageName=homepage&g=https%3A%2F%2Fm.kohls.com%2F&c.&k.&pageDomain=m.kohls.com&.k&mcid.&version=4.3.0&icsmcvid=-false&mcidcto=-false&aidcto=-null&.mcid&.c&products=%3Bproductmerch1&aamb=j8Odv6LonN4r3an7LhD3WZrU1bUpAkFkkiY1ncBR96t2PTI&tnt=557254%3A0%3A0%2C543394%3A1%3A0%2C531276%3A0%3A0%2C567511%3A1%3A0%2C567583%3A1%3A0%2C555886%3A1%3A0%2C548781%3A0%3A0%2C526083%3A0%3A0%2C&c4=homepage&c9=homepage%7Chomepage&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=D%3Dv18&v18=fri%7Cweekday%7C05%3A00%20am&c22=2022-09-02&v22=mobile&v39=no%20customer%20id&v40=mcom17&v42=no%20cart&c50=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d%7C05%3A00%20am&c53=homepage&c63=mcom-46f4352e4e7e888ac93c8b7b-b713fc26a3aca27d&c64=VisitorAPI%20Present&v68=homepage&v71=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&v73=no%20loyalty%20id&v86=21_7&v87=hp19&s=8192x4096&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&AQE=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;AKA_A2;bm_sz;check;rxVisitor;dtLatC;dtSa;s_ecid;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;rxvt;dtPC;_dpm_ses.f2d1;_dpm_id.f2d1;_abck;gpv_v9;s_cc", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fpixel.everesttech.net%252F1x1%253F&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBSVU1T2dPRg", END_INLINE,
            "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fib.adnxs.com%2Fpxj%3Faction%3Dsetuid(%27__EFGSURFER__.__EFGCK__%27)%26bidder%3D51%26seg%3D2634060der%3D51%26seg%3D2634060", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WXhIVS1nQUFCSnVZQGxpMg&url=/1/gr%3furl=https%253A%252F%252Fib.adnxs.com%252Fpxj%253Faction%253Dsetuid(%2527__EFGSURFER__.__EFGCK__%2527)%2526bidder%253D51%2526seg%253D2634060der%253D51%2526seg%253D2634060", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBSVU1T2dPRg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fus-u.openx.net%252Fw%252F1.0%252Fsd%253Fid%253D537072980%2526val%253D__EFGSURFER__.__EFGCK__&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBQnVzTHdNMA", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=992&dpuuid=1h39lx4qdv86e", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WXhIVS1nQUFCSnVZQGxpMg&url=/1/gr%3furl=https%253A%252F%252Fib.adnxs.com%252Fpxj%253Faction%253Dsetuid(%2527__EFGSURFER__.__EFGCK__%2527)%2526bidder%253D51%2526seg%253D2634060der%253D51%2526seg%253D2634060", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBQnVzTHdNMA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://api-bd.kohls.com/v1/ecs/correlation/id", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBSFdTWmdNcw", END_INLINE
    );

    ns_end_transaction("ppub_config", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("rid");
    ns_web_url ("rid",
        "URL=https://match.adsrvr.org/track/rid?ttd_pid=casale&fmt=json&p=184399",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://api.rlcdn.com/api/identity?pid=2&rt=envelope", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=rlas3;pxrc", END_INLINE,
            "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fpixel.rubiconproject.com%2Ftap.php%3Fexpires%3D30%26nid%3D2181%26put%3D__EFGSURFER__.__EFGCK__%26v%3D11782", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WXhIVS1nQUFCUlo2OUdRZg&url=/1/gr%3furl=https%253A%252F%252Fpixel.rubiconproject.com%252Ftap.php%253Fexpires%253D30%2526nid%253D2181%2526put%253D__EFGSURFER__.__EFGCK__%2526v%253D11782", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fib.adnxs.com%252Fpxj%253Faction%253Dsetuid(%2527__EFGSURFER__.__EFGCK__%2527)%2526bidder%253D51%2526seg%253D2634060der%253D51%2526seg%253D2634060&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBS0lMdlFPQQ", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBSFdTWmdNcw", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE
    );

    ns_end_transaction("rid", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("sessions");
    ns_web_url ("sessions",
        "URL=https://track.coherentpath.com/v1/websites/sessions",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=Authorization:Bearer public_2db5390c174f9d35981b14ad5500a899",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"session":{"browser_id":"9a395033-b24e-47f7-9398-915c8f54a39c"}}",
        BODY_END,
        INLINE_URLS,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WXhIVS1nQUFCUlo2OUdRZg&url=/1/gr%3furl=https%253A%252F%252Fpixel.rubiconproject.com%252Ftap.php%253Fexpires%253D30%2526nid%253D2181%2526put%253D__EFGSURFER__.__EFGCK__%2526v%253D11782", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBS0lMdlFPQQ", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE
    );

    ns_end_transaction("sessions", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("experiences");
    ns_web_url ("experiences",
        "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=MCom&pgid=Home&plids=RedesignHP1%7C12%2CRedesignHP2%7C12",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-APP-API_KEY:WOBxT3NyxFvOlrFgTMTnJEtzXzqAtGLl",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"cookieId":"78611155637513935743550952407821114794","mcmId":"78611155637513935743550952407821114794"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://sb.scorecardresearch.com/beacon.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RCc702e2fb1b91495f93e33f0ce4677284-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBTE9ab2dOeA", END_INLINE,
            "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fimage2.pubmatic.com%2FAdServer%2FPug%3Fvcode%3Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%3D%26piggybackCookie%3D__EFGSURFER__.__EFGCK__", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WXhIVS1nQUFBWDdYVkRVVg&url=/1/gr%3furl=https%253A%252F%252Fimage2.pubmatic.com%252FAdServer%252FPug%253Fvcode%253Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%253D%2526piggybackCookie%253D__EFGSURFER__.__EFGCK__", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBTE9ab2dOeA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fpixel.rubiconproject.com%252Ftap.php%253Fexpires%253D30%2526nid%253D2181%2526put%253D__EFGSURFER__.__EFGCK__%2526v%253D11782&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBSFRTTlFPWQ", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBS0FQcGdOMg", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBS0FQcGdOMg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WXhIVS1nQUFBWDdYVkRVVg&url=/1/gr%3furl=https%253A%252F%252Fimage2.pubmatic.com%252FAdServer%252FPug%253Fvcode%253Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%253D%2526piggybackCookie%253D__EFGSURFER__.__EFGCK__", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://adservice.google.com/adsid/integrator.js?domain=m.kohls.com", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBSFRTTlFPWQ", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBSVBTUHdPWQ", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBSVBTUHdPWQ", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://match.adsrvr.org/track/cmf/generic?ttd_pid=aam&gdpr=0&gdpr_consent=&ttd_tpi=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fimage2.pubmatic.com%252FAdServer%252FPug%253Fvcode%253Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%253D%2526piggybackCookie%253D__EFGSURFER__.__EFGCK__&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBSUtza0FNMA", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBRnlzZ1FNMA", END_INLINE,
            "URL=https://api-bd.kohls.com/v1/ecs/correlation/id", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBRnlzZ1FNMA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://securepubads.g.doubleclick.net/gampad/ads?pvsid=4335508868998629&correlator=51701663841196&eid=31069285%2C31069331&output=ldjh&gdfp_req=1&vrg=2022083101&ptt=17&impl=fifs&iu_parts=17763952%2Chomepage&enc_prev_ius=%2F0%2F1&prev_iu_szs=320x50%7C320x550&fluid=height&ifi=1&adks=2351561951&sfv=1-0-38&fsapi=false&prev_scp=pos%3DHP_Mobile_Top_Super_Bulletin%26env%3Dprod%26channel%3Dmobile&eri=1&cust_params=pgtype%3Dhome&sc=1&cookie_enabled=1&abxe=1&dt=1662113022826&lmt=1662113022&dlt=1662113018978&idt=3734&adxs=0&adys=5603&biw=775&bih=473&scr_x=0&scr_y=0&btvi=1&ucis=1&oid=2&u_his=2&u_h=4096&u_w=8192&u_ah=4096&u_aw=8192&u_cd=24&u_sd=1&u_tz=-300&dmc=8&bc=31&uach=WyJMaW51eCIsIiIsIng4NiIsIiIsIjkwLjAuNDQzMC42MSIsW10sZmFsc2UsbnVsbCwiIixbXSxmYWxzZV0.&nvt=1&url=https%3A%2F%2Fm.kohls.com%2F&frm=20&vis=1&psz=775x0&msz=775x6&fws=132&ohw=775&ga_vid=1350227648.1662113023&ga_sid=1662113023&ga_hid=2017578148&ga_fc=false", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://securepubads.g.doubleclick.net/gampad/ads?pvsid=4335508868998629&correlator=172325230536845&eid=31069285%2C31069331&output=ldjh&gdfp_req=1&vrg=2022083101&ptt=17&impl=fifs&iu_parts=17763952%2Chomepage&enc_prev_ius=%2F0%2F1&prev_iu_szs=320x50%7C320x550&fluid=height&ifi=2&adks=2351561788&sfv=1-0-38&fsapi=false&prev_scp=pos%3DHP_Mobile_Bottom_Super_Bulletin%26env%3Dprod%26channel%3Dmobile&eri=1&cust_params=pgtype%3Dhome&sc=1&cookie_enabled=1&abxe=1&dt=1662113022849&lmt=1662113022&dlt=1662113018978&idt=3734&adxs=0&adys=6422&biw=775&bih=473&scr_x=0&scr_y=0&btvi=2&ucis=2&oid=2&u_his=2&u_h=4096&u_w=8192&u_ah=4096&u_aw=8192&u_cd=24&u_sd=1&u_tz=-300&dmc=8&bc=31&uach=WyJMaW51eCIsIiIsIng4NiIsIiIsIjkwLjAuNDQzMC42MSIsW10sZmFsc2UsbnVsbCwiIixbXSxmYWxzZV0.&nvt=1&url=https%3A%2F%2Fm.kohls.com%2F&frm=20&vis=1&psz=775x8715&msz=775x6&fws=132&ohw=775&ga_vid=1350227648.1662113023&ga_sid=1662113023&ga_hid=2017578148&ga_fc=false", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBTURvbGdPTg", END_INLINE
    );

    ns_end_transaction("experiences", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("sodar");
    ns_web_url ("sodar",
        "URL=https://pagead2.googlesyndication.com/getconfig/sodar?sv=200&tid=gpt&tv=2022083101&st=env",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBTURvbGdPTg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBSUtza0FNMA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://ae8187239c069eb46d5cc537d06d3fed.safeframe.googlesyndication.com/safeframe/1-0-38/html/container.html", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://ap.lijit.com/rtb/bid?callback=window.headertag.SovrnHtb.adResponseCallback&br=%7B%22id%22%3A%22_LH9feMI6%22%2C%22site%22%3A%7B%22domain%22%3A%22m.kohls.com%22%2C%22page%22%3A%22%2F%22%7D%2C%22imp%22%3A%5B%7B%22id%22%3A%222cHnqGyD%22%2C%22banner%22%3A%7B%22w%22%3A320%2C%22h%22%3A50%7D%2C%22tagid%22%3A%22788165%22%7D%5D%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("sodar", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("prebid");
    ns_web_url ("prebid",
        "URL=https://ib.adnxs.com/ut/v3/prebid",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"sdk":{"version":"2.4.1"},"hb_source":2,"referrer_detection":{"rd_ifs":null,"rd_ref":"https%3A%2F%2Fm.kohls.com%2F","rd_stk":"https%3A%2F%2Fm.kohls.com%2F","rd_top":null},"tags":[{"ad_types":["banner"],"allow_smaller_sizes":false,"disable_psa":true,"id":20889775,"prebid":true,"primary_size":{"width":320,"height":50},"sizes":[{"width":320,"height":50}],"use_pmt_rule":false,"uuid":"VmpHK5vZHZzS90"}]}",
        BODY_END
    );

    ns_end_transaction("prebid", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("translator");
    ns_web_url ("translator",
        "URL=https://hbopenbid.pubmatic.com/translator?source=index-client",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"id":"1662113022912","at":1,"cur":["USD"],"imp":[{"id":"d5a7a79b-04c5-5901-56a5-0545e115160e","tagId":"3262361","secure":1,"ext":{},"banner":{"w":320,"h":50}}],"site":{"page":"https://m.kohls.com/","ref":"","publisher":{"id":"160059","domain":"m.kohls.com"},"domain":"m.kohls.com"},"device":{"ua":"Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1","js":1,"dnt":0,"h":4096,"w":8192,"language":"en-US","geo":{}},"user":{"geo":{}},"ext":{"wrapper":{"wp":"ixjs"}}}",
        BODY_END,
        INLINE_URLS,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBSEdTMGdNcw", END_INLINE
    );

    ns_end_transaction("translator", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("bidRequest");
    ns_web_url ("bidRequest",
        "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2ba9460000f&pos=m_btf_bottom_320x50&secure=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=A3"
    );

    ns_end_transaction("bidRequest", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("fastlane_json");
    ns_web_url ("fastlane_json",
        "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=43&rp_floor=0.01&rf=https%3A%2F%2Fm.kohls.com%2F&p_screen_res=8192x4096&site_id=344110&zone_id=1817912&kw=rp.fastlane&tk_flint=index&rand=0.0939209907185663",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("fastlane_json", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("index_3");
    ns_web_url ("index_3",
        "URL=https://hb.emxdgt.com/?t=1500&ts=1662113022926",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"id":"sr8TMfz9","imp":[{"id":"1","tagid":"79817","secure":1,"bidfloor":0.1,"banner":{"format":[{"w":320,"h":50}],"w":320,"h":50}}],"site":{"domain":"m.kohls.com","page":"https://m.kohls.com/","ref":""},"ext":{"ver":"1.1.0"}}",
        BODY_END
    );

    ns_end_transaction("index_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("auction");
    ns_web_url ("auction",
        "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_m_bottom_listings_rec_header&lib=ix&size=320x50&referrer=https%3A%2F%2Fm.kohls.com%2F&v=2.1.2&tmax=1500",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("auction", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("cygnus");
    ns_web_url ("cygnus",
        "URL=https://htlb.casalemedia.com/cygnus?s=189641",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"id":"32455082","site":{"page":"https://m.kohls.com/"},"imp":[{"banner":{"topframe":1,"format":[{"w":320,"h":50,"ext":{"sid":"1","siteID":"269251"}}]},"id":"1"}],"ext":{"source":"ixwrapper"},"at":1}",
        BODY_END
    );

    ns_end_transaction("cygnus", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("index_4");
    ns_web_url ("index_4",
        "URL=https://hb.emxdgt.com/?t=1500&ts=1662113022935",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"id":"Ib6JWugU","imp":[{"id":"3","tagid":"79819","secure":1,"bidfloor":0.1,"banner":{"format":[{"w":320,"h":50}],"w":320,"h":50}}],"site":{"domain":"m.kohls.com","page":"https://m.kohls.com/","ref":""},"ext":{"ver":"1.1.0"}}",
        BODY_END
    );

    ns_end_transaction("index_4", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("auction_2");
    ns_web_url ("auction_2",
        "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_m_top_listings_rec_header&lib=ix&size=320x50&referrer=https%3A%2F%2Fm.kohls.com%2F&v=2.1.2&tmax=1500",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("auction_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("bidRequest_2");
    ns_web_url ("bidRequest_2",
        "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2ba9460000f&pos=m_btf_morebottom_320x50&secure=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=A3"
    );

    ns_end_transaction("bidRequest_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("fastlane_json_2");
    ns_web_url ("fastlane_json_2",
        "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=43&rp_floor=0.01&rf=https%3A%2F%2Fm.kohls.com%2F&p_screen_res=8192x4096&site_id=344110&zone_id=1817918&kw=rp.fastlane&tk_flint=index&rand=0.38195581095711795",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://mpp.vindicosuite.com/sync/?pid=27&fr=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://ap.lijit.com/rtb/bid?callback=window.headertag.SovrnHtb.adResponseCallback&br=%7B%22id%22%3A%22_zItOwRkt%22%2C%22site%22%3A%7B%22domain%22%3A%22m.kohls.com%22%2C%22page%22%3A%22%2F%22%7D%2C%22imp%22%3A%5B%7B%22id%22%3A%22pZ1IeZ9I%22%2C%22banner%22%3A%7B%22w%22%3A320%2C%22h%22%3A50%7D%2C%22tagid%22%3A%22788167%22%7D%5D%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("fastlane_json_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("auction_3");
    ns_web_url ("auction_3",
        "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_m_middle_listings_rec_header&lib=ix&size=320x50&referrer=https%3A%2F%2Fm.kohls.com%2F&v=2.1.2&tmax=1500",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://ap.lijit.com/rtb/bid?callback=window.headertag.SovrnHtb.adResponseCallback&br=%7B%22id%22%3A%22_CvDYVRaG%22%2C%22site%22%3A%7B%22domain%22%3A%22m.kohls.com%22%2C%22page%22%3A%22%2F%22%7D%2C%22imp%22%3A%5B%7B%22id%22%3A%22oLgPbraZ%22%2C%22banner%22%3A%7B%22w%22%3A320%2C%22h%22%3A50%7D%2C%22tagid%22%3A%22788166%22%7D%5D%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("auction_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("fastlane_json_3");
    ns_web_url ("fastlane_json_3",
        "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=43&rp_floor=0.01&rf=https%3A%2F%2Fm.kohls.com%2F&p_screen_res=8192x4096&site_id=344110&zone_id=1817916&kw=rp.fastlane&tk_flint=index&rand=0.19549356662526063",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("fastlane_json_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("bidRequest_3");
    ns_web_url ("bidRequest_3",
        "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2ba9460000f&pos=m_btf_middle_320x50&secure=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=A3"
    );

    ns_end_transaction("bidRequest_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("index_5");
    ns_web_url ("index_5",
        "URL=https://hb.emxdgt.com/?t=1500&ts=1662113022947",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"id":"nR2h8p5q","imp":[{"id":"2","tagid":"79818","secure":1,"bidfloor":0.1,"banner":{"format":[{"w":320,"h":50}],"w":320,"h":50}}],"site":{"domain":"m.kohls.com","page":"https://m.kohls.com/","ref":""},"ext":{"ver":"1.1.0"}}",
        BODY_END,
        INLINE_URLS,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS1nQUFBSEdTMGdNcw", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://match.adsrvr.org/track/cmb/generic?ttd_pid=aam&gdpr=0&gdpr_consent=&ttd_tpi=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=TDID;TDCPM", END_INLINE,
            "URL=https://servedby.flashtalking.com/map/?key=a74thHgsfK627J6Ftt8sj5ks52bKe&gdpr=0&gdpr_consent=&url=https://dpm.demdex.net/ibs:dpid=3047&dpuuid=[%FT_GUID%]&gdpr=0&gdpr_consent=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=3047&dpuuid=53588F2D83C0A9&gdpr=0&gdpr_consent=", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBRTlCdXdPQQ", END_INLINE
    );

    ns_end_transaction("index_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("prebid_2");
    ns_web_url ("prebid_2",
        "URL=https://ib.adnxs.com/ut/v3/prebid",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"sdk":{"version":"2.4.1"},"hb_source":2,"referrer_detection":{"rd_ifs":null,"rd_ref":"https%3A%2F%2Fm.kohls.com%2F","rd_stk":"https%3A%2F%2Fm.kohls.com%2F","rd_top":null},"tags":[{"ad_types":["banner"],"allow_smaller_sizes":false,"disable_psa":true,"id":20889777,"prebid":true,"primary_size":{"width":320,"height":50},"sizes":[{"width":320,"height":50}],"use_pmt_rule":false,"uuid":"FZLxTKwh768NfK"}]}",
        BODY_END
    );

    ns_end_transaction("prebid_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("prebid_3");
    ns_web_url ("prebid_3",
        "URL=https://ib.adnxs.com/ut/v3/prebid",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"sdk":{"version":"2.4.1"},"hb_source":2,"referrer_detection":{"rd_ifs":null,"rd_ref":"https%3A%2F%2Fm.kohls.com%2F","rd_stk":"https%3A%2F%2Fm.kohls.com%2F","rd_top":null},"tags":[{"ad_types":["banner"],"allow_smaller_sizes":false,"disable_psa":true,"id":20889776,"prebid":true,"primary_size":{"width":320,"height":50},"sizes":[{"width":320,"height":50}],"use_pmt_rule":false,"uuid":"k8mGt8Se95s5cJ"}]}",
        BODY_END,
        INLINE_URLS,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBRTlCdXdPQQ", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=903&dpuuid=56bc35b1-0909-4b6f-840d-20add6a418d9", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=903&dpuuid=56bc35b1-0909-4b6f-840d-20add6a418d9", END_INLINE
    );

    ns_end_transaction("prebid_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("translator_2");
    ns_web_url ("translator_2",
        "URL=https://hbopenbid.pubmatic.com/translator?source=index-client",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"id":"1662113022933","at":1,"cur":["USD"],"imp":[{"id":"ea90a908-e1c0-7496-b3f2-f36137b5ce29","tagId":"3262363","secure":1,"ext":{},"banner":{"w":320,"h":50}}],"site":{"page":"https://m.kohls.com/","ref":"","publisher":{"id":"160059","domain":"m.kohls.com"},"domain":"m.kohls.com"},"device":{"ua":"Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1","js":1,"dnt":0,"h":4096,"w":8192,"language":"en-US","geo":{}},"user":{"geo":{}},"ext":{"wrapper":{"wp":"ixjs"}}}",
        BODY_END
    );

    ns_end_transaction("translator_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("translator_3");
    ns_web_url ("translator_3",
        "URL=https://hbopenbid.pubmatic.com/translator?source=index-client",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"id":"1662113022949","at":1,"cur":["USD"],"imp":[{"id":"2ff528ec-bb6c-f493-9d95-1a35501d8cb2","tagId":"3262362","secure":1,"ext":{},"banner":{"w":320,"h":50}}],"site":{"page":"https://m.kohls.com/","ref":"","publisher":{"id":"160059","domain":"m.kohls.com"},"domain":"m.kohls.com"},"device":{"ua":"Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1","js":1,"dnt":0,"h":4096,"w":8192,"language":"en-US","geo":{}},"user":{"geo":{}},"ext":{"wrapper":{"wp":"ixjs"}}}",
        BODY_END,
        INLINE_URLS,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBRVFNYndPQQ", END_INLINE,
            "URL=https://gum.criteo.com/sync?c=8&r=1&a=1&u=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D28645%26dpuuid%3D%40USERID%40%26gdpr%3D0%26gdpr_consent%3D&gdpr=0&gdpr_consent=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBRVFNYndPQQ", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=903&dpuuid=56bc35b1-0909-4b6f-840d-20add6a418d9", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE
    );

    ns_end_transaction("translator_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("cygnus_2");
    ns_web_url ("cygnus_2",
        "URL=https://htlb.casalemedia.com/cygnus?s=189641",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"id":"23081663","site":{"page":"https://m.kohls.com/"},"imp":[{"banner":{"topframe":1,"format":[{"w":320,"h":50,"ext":{"sid":"3","siteID":"269250"}}]},"id":"1"}],"ext":{"source":"ixwrapper"},"at":1}",
        BODY_END,
        INLINE_URLS,
            "URL=https://dpm.demdex.net/ibs:dpid=3047&dpuuid=53588F2D83C0A9&gdpr=0&gdpr_consent=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=3047&dpuuid=53588F2D83C0A9&gdpr=0&gdpr_consent=", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBQndRZGdOMg", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBQndRZGdOMg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE
    );

    ns_end_transaction("cygnus_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("cygnus_3");
    ns_web_url ("cygnus_3",
        "URL=https://htlb.casalemedia.com/cygnus?s=189641",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"id":"60110294","site":{"page":"https://m.kohls.com/"},"imp":[{"banner":{"topframe":1,"format":[{"w":320,"h":50,"ext":{"sid":"2","siteID":"269249"}}]},"id":"1"}],"ext":{"source":"ixwrapper"},"at":1}",
        BODY_END,
        INLINE_URLS,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBS0xTX1FPWQ", END_INLINE,
            "URL=https://ps.eyeota.net/match?bid=6j5b2cv&uid=72195082367942492304066494206891021442&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D30064%26dpuuid%3D%7BUUID_6j5b2cv%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=/match/bounce/?bid=6j5b2cv&uid=72195082367942492304066494206891021442&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D30064%26dpuuid%3D%7BUUID_6j5b2cv%7D", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBS0xTX1FPWQ", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=3047&dpuuid=53588F2D83C0A9&gdpr=0&gdpr_consent=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBS1k2ZGdPRg", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBS1k2ZGdPRg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://fc.kohls.com/2.2/w/w-756138/init/js/?q=%7B%22e%22%3A870168%2C%22fvq%22%3A%2281p65ns1-93sp-29o6-9q94-30n1n0r37onr1662113023400%22%2C%22oq%22%3A%22790%3A473%3A790%3A473%3A8192%3A4096%22%2C%22wfi%22%3A%22flap-152535%22%2C%22yf%22%3A%7B%7D%2C%22jc%22%3A%22ZPBZYbtva%22%2C%22ov%22%3A%22o2%7C8192k4096%208192k4096%2024%2024%7C360%7Cra-HF%7Coc1-700%7Csnyfr%7C%7CZbmvyyn%2F5.0%20(Yvahk%3B%20Naqebvq%206.0.1%3B%20Zbgb%20T%20(4)%20Ohvyq%2FZCW24.139-64)%20NccyrJroXvg%2F537.36%20(XUGZY%2C%20yvxr%20Trpxb)%20Puebzr%2F58.0.3029.81%20Zbovyr%20Fnsnev%2F537.36%20CGFG%2F1%7Cjt1-78r9qs3735260548%22%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;AKA_A2;bm_sz;check;rxVisitor;dtLatC;dtSa;s_ecid;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;rxvt;dtPC;_dpm_ses.f2d1;_dpm_id.f2d1;_abck;gpv_v9;s_cc", END_INLINE,
            "URL=https://gum.criteo.com/sync?s=1&c=8&r=1&a=1&u=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D28645%26dpuuid%3D%40USERID%40%26gdpr%3D0%26gdpr_consent%3D&gdpr=0&gdpr_consent=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBS0hwVlFPTg", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBS0hwVlFPTg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?guid=ON&;script=0&data=aam=10263239", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://cms.analytics.yahoo.com/cms?partner_id=ADOBE&_hosted_id=72195082367942492304066494206891021442&gdpr=0&gdpr_consent=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=A3", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=28645&dpuuid=&gdpr=0&gdpr_consent=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=28645&dpuuid=&gdpr=0&gdpr_consent=", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBTGloU0FPRg", END_INLINE,
            "URL=https://tpc.googlesyndication.com/sodar/sodar2.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBTGloU0FPRg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBS0pDU2dPQQ", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBS0pDU2dPQQ", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://securepubads.g.doubleclick.net/gampad/ads?pvsid=4335508868998629&correlator=4197523010461983&eid=31069285%2C31069331&output=ldjh&gdfp_req=1&vrg=2022083101&ptt=17&impl=fifs&iu_parts=17763952%2Chomepage&enc_prev_ius=%2F0%2F1&prev_iu_szs=320x50%7C320x50&fluid=height&ifi=3&adks=334369848&sfv=1-0-38&fsapi=false&prev_scp=pos%3Dmiddle%26env%3Dprod%26channel%3Dmobile&eri=1&cust_params=pgtype%3Dhome&sc=1&cookie=ID%3D3f5222e9b94a8872%3AT%3D1662113022%3AS%3DALNI_MYK01WomvsjECicEXFc9qIJqjXnpA&gpic=UID%3D00000872ab15fe48%3AT%3D1662113022%3ART%3D1662113022%3AS%3DALNI_MZecWrmURrSjs3dbQD9hWjY5SQ45Q&abxe=1&dt=1662113023522&lmt=1662113023&dlt=1662113018978&idt=3734&adxs=0&adys=6422&biw=775&bih=473&scr_x=0&scr_y=0&btvi=3&ucis=3&oid=2&u_his=2&u_h=4096&u_w=8192&u_ah=4096&u_aw=8192&u_cd=24&u_sd=1&u_tz=-300&dmc=8&bc=31&uach=WyJMaW51eCIsIiIsIng4NiIsIiIsIjkwLjAuNDQzMC42MSIsW10sZmFsc2UsbnVsbCwiIixbXSxmYWxzZV0.&nvt=1&url=https%3A%2F%2Fm.kohls.com%2F&frm=20&vis=1&psz=775x0&msz=775x6&fws=132&ohw=775&psts=AEC3cPLEC5p1exrRec7vYVQlnhNd%2CAEC3cPLEC5p1exrRec7vYVQlnhNd&ga_vid=1350227648.1662113023&ga_sid=1662113023&ga_hid=2017578148&ga_fc=false", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBR3lhdlFOeA", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=28645&dpuuid=&gdpr=0&gdpr_consent=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBR3lhdlFOeA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1018012790/?guid=ON&data=aam=10263239&is_vtc=1&random=2884255753", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://securepubads.g.doubleclick.net/gampad/ads?pvsid=4335508868998629&correlator=3470053502301160&eid=31069285%2C31069331&output=ldjh&gdfp_req=1&vrg=2022083101&ptt=17&impl=fifs&iu_parts=17763952%2Chomepage&enc_prev_ius=%2F0%2F1&prev_iu_szs=320x50%7C320x50&fluid=height&ifi=4&adks=334370023&sfv=1-0-38&fsapi=false&prev_scp=pos%3Dbottom%26env%3Dprod%26channel%3Dmobile&eri=1&cust_params=pgtype%3Dhome&sc=1&cookie=ID%3D3f5222e9b94a8872%3AT%3D1662113022%3AS%3DALNI_MYK01WomvsjECicEXFc9qIJqjXnpA&gpic=UID%3D00000872ab15fe48%3AT%3D1662113022%3ART%3D1662113022%3AS%3DALNI_MZecWrmURrSjs3dbQD9hWjY5SQ45Q&abxe=1&dt=1662113023561&lmt=1662113023&dlt=1662113018978&idt=3734&adxs=0&adys=6422&biw=775&bih=473&scr_x=0&scr_y=0&btvi=4&ucis=4&oid=2&u_his=2&u_h=4096&u_w=8192&u_ah=4096&u_aw=8192&u_cd=24&u_sd=1&u_tz=-300&dmc=8&bc=31&uach=WyJMaW51eCIsIiIsIng4NiIsIiIsIjkwLjAuNDQzMC42MSIsW10sZmFsc2UsbnVsbCwiIixbXSxmYWxzZV0.&nvt=1&url=https%3A%2F%2Fm.kohls.com%2F&frm=20&vis=1&psz=775x0&msz=775x6&fws=132&ohw=775&psts=AEC3cPLEC5p1exrRec7vYVQlnhNd%2CAEC3cPLEC5p1exrRec7vYVQlnhNd&ga_vid=1350227648.1662113023&ga_sid=1662113023&ga_hid=2017578148&ga_fc=false", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBS01ReXdOMg", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBS01ReXdOMg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=30646?dpuuid=y-fazbScNE2pGtp4lgN2JLc8LjNFR7XK.WQh0-~A", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=30646", END_INLINE,
            "URL=https://fei.pro-market.net/engine?site=141472;size=1x1;mimetype=img;du=67;csync=72195082367942492304066494206891021442", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://ps.eyeota.net/match/bounce/?bid=6j5b2cv&uid=72195082367942492304066494206891021442&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D30064%26dpuuid%3D%7BUUID_6j5b2cv%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=30064&dpuuid={UUID_6j5b2cv}", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBTUhUVWdPWQ", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBTUhUVWdPWQ", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBSk90cndNMA", END_INLINE,
            "URL=https://px.owneriq.net/eucm/p/adpq?redir=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D53196%26dpuuid%3D(OIQ_UUID)", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://px.owneriq.net/ecc?redir=https%3a%2f%2fdpm.demdex.net%2fibs%3adpid%3d53196%26dpuuid%3dQ7153994231789857141&uid=Q7153994231789857141&ref=%2Feucm%2Fp%2Fadpq", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=30646", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBSk90cndNMA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=30064&dpuuid=%7BUUID_6j5b2cv%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=30064&dpuuid=%7BUUID_6j5b2cv%7D", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=575&dpuuid=-7894130319548068841", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=575&dpuuid=-7894130319548068841", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBSVdUNUFNcw", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBSVdUNUFNcw", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBTFRweHdPTg", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBTFRweHdPTg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=30064&dpuuid=%7BUUID_6j5b2cv%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://bttrack.com/dmp/adobe/user?dd_uuid=72195082367942492304066494206891021442", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=//dpm.demdex.net/ibs:dpid=49276&dpuuid=5f07a481-8627-4d15-a25e-c852c2419c16", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=575&dpuuid=-7894130319548068841", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBSEdodGdPRg", END_INLINE,
            "URL=https://px.owneriq.net/ecc?redir=https%3a%2f%2fdpm.demdex.net%2fibs%3adpid%3d53196%26dpuuid%3dQ7153994231789857141&uid=Q7153994231789857141&ref=%2Feucm%2Fp%2Fadpq", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=si;p2", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=53196&dpuuid=Q7153994231789857141", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBSEdodGdPRg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBSGtOU3dPQQ", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBSGtOU3dPQQ", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=49276&dpuuid=5f07a481-8627-4d15-a25e-c852c2419c16", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=49276&dpuuid=5f07a481-8627-4d15-a25e-c852c2419c16", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBTVNiTHdOeA", END_INLINE,
            "URL=https://ads.scorecardresearch.com/p?c1=9&c2=6034944&c3=2&cs_xi=72195082367942492304066494206891021442&rn=1662113020618&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D73426%26dpuuid%3D72195082367942492304066494206891021442", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBTVNiTHdOeA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBTG9SUVFOMg", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBTG9SUVFOMg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://ads.scorecardresearch.com/p2?c1=9&c2=6034944&c3=2&cs_xi=72195082367942492304066494206891021442&rn=1662113020618&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D73426%26dpuuid%3D72195082367942492304066494206891021442", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=UID", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=53196&dpuuid=Q7153994231789857141", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=53196&dpuuid=Q7153994231789857141", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=49276&dpuuid=5f07a481-8627-4d15-a25e-c852c2419c16", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=73426&dpuuid=72195082367942492304066494206891021442", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=73426&dpuuid=72195082367942492304066494206891021442", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBSmM3TlFPRg", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBSmM3TlFPRg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE
    );

    ns_end_transaction("cygnus_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("floop");
    ns_web_url ("floop",
        "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;AKA_A2;bm_sz;check;rxVisitor;dtLatC;dtSa;s_ecid;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;rxvt;dtPC;_dpm_ses.f2d1;_dpm_id.f2d1;_abck;gpv_v9;s_cc;__gads;__gpi;ndcd",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/floop_main_url_1_1662113084870.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://abp.mxptint.net/sn.ashx", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=75557&dpuuid=R1B342_F592975B_67600C&redir=https://abp.mxptint.net/sn.ashx?ak=1", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=53196&dpuuid=Q7153994231789857141", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBRUd1SEFNMA", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVS13QUFBRUd1SEFNMA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/4576090_Jack_Skellington?wid=300&hei=300&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/5010868_Navy_Ombre_Plaid?wid=300&hei=300&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/4970384_Medium_Stone?wid=300&hei=300&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/4569328_Light_Teal?wid=300&hei=300&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/5485729_White?wid=300&hei=300&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/5484998_Yellow_Green?wid=300&hei=300&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=73426&dpuuid=72195082367942492304066494206891021442", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBSktVVkFNcw", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBSktVVkFNcw", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://securepubads.g.doubleclick.net/gampad/ads?pvsid=4335508868998629&correlator=2775111402071550&eid=31069285%2C31069331&output=ldjh&gdfp_req=1&vrg=2022083101&ptt=17&impl=fifs&iu_parts=17763952%2Chomepage&enc_prev_ius=%2F0%2F1&prev_iu_szs=320x50%7C320x50&fluid=height&ifi=5&adks=334369881&sfv=1-0-38&fsapi=false&prev_scp=pos%3Dtop%26env%3Dprod%26channel%3Dmobile&eri=1&cust_params=pgtype%3Dhome&sc=1&cookie=ID%3D3f5222e9b94a8872%3AT%3D1662113022%3AS%3DALNI_MYK01WomvsjECicEXFc9qIJqjXnpA&gpic=UID%3D00000872ab15fe48%3AT%3D1662113022%3ART%3D1662113022%3AS%3DALNI_MZecWrmURrSjs3dbQD9hWjY5SQ45Q&abxe=1&dt=1662113024060&lmt=1662113024&dlt=1662113018978&idt=3734&adxs=0&adys=7453&biw=775&bih=473&scr_x=0&scr_y=0&btvi=5&ucis=5&oid=2&u_his=2&u_h=4096&u_w=8192&u_ah=4096&u_aw=8192&u_cd=24&u_sd=1&u_tz=-300&dmc=8&bc=31&uach=WyJMaW51eCIsIiIsIng4NiIsIiIsIjkwLjAuNDQzMC42MSIsW10sZmFsc2UsbnVsbCwiIixbXSxmYWxzZV0.&nvt=1&url=https%3A%2F%2Fm.kohls.com%2F&frm=20&vis=1&psz=775x492&msz=775x6&fws=132&ohw=775&psts=AEC3cPLEC5p1exrRec7vYVQlnhNd%2CAEC3cPLEC5p1exrRec7vYVQlnhNd&ga_vid=1350227648.1662113023&ga_sid=1662113023&ga_hid=2017578148&ga_fc=false", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://b-code.liadm.com/a-00oc.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC37be6c799a1548d8ba6c480cbbd4cac4-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBSi1xT3dPTg", END_INLINE
    );

    ns_end_transaction("floop", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("headerstats");
    ns_web_url ("headerstats",
        "URL=https://as-sec.casalemedia.com/headerstats?s=189641&u=https%3A%2F%2Fm.kohls.com%2F&v=3",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"p":"display","d":"mobile","c":"39002588238727","s":"aNgycjLG","w":"1896411662113022586KD4Q4ZVBf0dVn","t":1662113024087,"pg":{"t":1662113022586,"e":[]},"gt":"1500","ac":"757","sl":[{"s":"2ff528ec-bb6c-f493-9d95-1a35501d8cb2","t":1662113022943,"xslots":{"TPL":{"2":{"brq":1,"bp":1}},"SVRN":{"2":{"brq":1,"bp":1}},"RUBI":{"2":{"brq":1,"be":1}},"OATHM":{"2":{"brq":1,"bp":1}},"APNX":{"2":{"brq":1,"bp":1}},"BRT":{"2":{"brq":1,"be":1}},"PUBM":{"2":{"brq":1,"be":1}},"INDX":{"2":{"brq":1,"bp":1}}}},{"s":"data","t":1662113024087,"xslots":{"UNKN":{"2":{"rl":1500,"brq":8,"bp":5,"be":3}}}},{"s":"identity","t":1662113022595,"xslots":{"ADSORG":{"before":{"brq":"1","pt":"50"},"after":{"bt":1,"brs":1,"rl":"842"}},"LVRAMP":{"before":{"brq":"1","bt":1,"pt":"50"},"after":{"bp":1,"rl":"810"}}}}]}",
        BODY_END,
        INLINE_URLS,
            "URL=https://aorta.clickagy.com/pixel.gif?ch=124&cm=72195082367942492304066494206891021442&redir=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D79908%26dpuuid%3D%7Bvisitor_id%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBSi1xT3dPTg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE
    );

    ns_end_transaction("headerstats", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("floop_2");
    ns_web_url ("floop_2",
        "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;AKA_A2;bm_sz;check;rxVisitor;dtLatC;dtSa;s_ecid;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;rxvt;dtPC;_dpm_ses.f2d1;_dpm_id.f2d1;_abck;gpv_v9;s_cc;__gads;__gpi;ndcd",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/floop_2_main_url_1_1662113084872.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBSGVpT0FPRg", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBSGVpT0FPRg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE
    );

    ns_end_transaction("floop_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("floop_3");
    ns_web_url ("floop_3",
        "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;AKA_A2;bm_sz;check;rxVisitor;dtLatC;dtSa;s_ecid;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;rxvt;dtPC;_dpm_ses.f2d1;_dpm_id.f2d1;_abck;gpv_v9;s_cc;__gads;__gpi;ndcd",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/floop_3_main_url_1_1662113084872.body",
        BODY_END
    );

    ns_end_transaction("floop_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("floop_4");
    ns_web_url ("floop_4",
        "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;AKA_A2;bm_sz;check;rxVisitor;dtLatC;dtSa;s_ecid;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;rxvt;dtPC;_dpm_ses.f2d1;_dpm_id.f2d1;_abck;gpv_v9;s_cc;__gads;__gpi;ndcd",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/floop_4_main_url_1_1662113084873.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBTUlOd3dPQQ", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBTUlOd3dPQQ", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=75557&dpuuid=R1B342_F592975B_67600C&redir=https://abp.mxptint.net/sn.ashx?ak=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=75557&dpuuid=R1B342_F592975B_67600C&redir=https://abp.mxptint.net/sn.ashx", END_INLINE,
            "URL=https://sync.ipredictive.com/d/sync/cookie/generic?https://dpm.demdex.net/ibs:dpid=2340&dpuuid=${ADELPHIC_CUID}", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=2340&dpuuid=933c62e0-0d07-4c52-9ca7-09145bb45ab8", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBSlVScHdOMg", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBSlVScHdOMg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE
    );

    ns_end_transaction("floop_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("headerstats_2");
    ns_web_url ("headerstats_2",
        "URL=https://as-sec.casalemedia.com/headerstats?s=189641&u=https%3A%2F%2Fm.kohls.com%2F&v=3",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"p":"display","d":"mobile","c":"39002588238727","s":"8sxDaeiH","w":"1896411662113022586KD4Q4ZVBf0dVn","t":1662113024093,"pg":{"t":1662113022586,"e":[]},"gt":"1500","ac":"802","sl":[{"s":"d5a7a79b-04c5-5901-56a5-0545e115160e","t":1662113022909,"xslots":{"SVRN":{"1":{"brq":1,"bp":1}},"APNX":{"1":{"brq":1,"bp":1}},"PUBM":{"1":{"brq":1,"be":1}},"OATHM":{"1":{"brq":1,"bp":1}},"RUBI":{"1":{"brq":1,"be":1}},"BRT":{"1":{"brq":1,"be":1}},"TPL":{"1":{"brq":1,"bp":1}},"INDX":{"1":{"brq":1,"bp":1}}}},{"s":"data","t":1662113024093,"xslots":{"UNKN":{"1":{"rl":1500,"brq":8,"bp":5,"be":3}}}}]}",
        BODY_END,
        INLINE_URLS,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=75557&dpuuid=R1B342_F592975B_67600C&redir=https://abp.mxptint.net/sn.ashx", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBSC1VTlFPWQ", END_INLINE,
            "URL=https://tpc.googlesyndication.com/sodar/sodar2/225/runner.html", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBSC1VTlFPWQ", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://www.google.com/recaptcha/api2/aframe", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBRUk3cndPRg", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC5c9a22d5a56847e08ecebe48b81f6d22-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("headerstats_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("headerstats_3");
    ns_web_url ("headerstats_3",
        "URL=https://as-sec.casalemedia.com/headerstats?s=189641&u=https%3A%2F%2Fm.kohls.com%2F&v=3",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"p":"display","d":"mobile","c":"39002588238727","s":"WjEB4Nef","w":"1896411662113022586KD4Q4ZVBf0dVn","t":1662113024195,"pg":{"t":1662113022586,"e":[]},"gt":"1500","ac":"1300","sl":[{"s":"ea90a908-e1c0-7496-b3f2-f36137b5ce29","t":1662113022933,"xslots":{"PUBM":{"3":{"brq":1,"be":1}},"APNX":{"3":{"brq":1,"bp":1}},"BRT":{"3":{"brq":1,"be":1}},"TPL":{"3":{"brq":1,"bp":1}},"INDX":{"3":{"brq":1,"bp":1}},"OATHM":{"3":{"brq":1,"bp":1}},"RUBI":{"3":{"brq":1,"be":1}},"SVRN":{"3":{"brq":1,"bp":1}}}},{"s":"data","t":1662113024195,"xslots":{"UNKN":{"3":{"rl":1500,"brq":8,"be":3,"bp":5}}}}]}",
        BODY_END,
        INLINE_URLS,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC7705d8cfc8794e0c8ecf7777ec37c7b8-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://b-code.liadm.com/sync-container.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://usermatch.krxd.net/um/v2?partner=adobe&id=72195082367942492304066494206891021442", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBRUk3cndPRg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=79908&dpuuid=YxHVAN1DNdSnJ-885SEQ2hfH", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=79908&dpuuid=YxHVAN1DNdSnJ-885SEQ2hfH", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBQml1c0FNMA", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBQml1c0FNMA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://login.dotomi.com/ucm/UCMController?dtm_com=28&dtm_cid=2683&dtm_cmagic=8420d3&dtm_fid=101&dtm_format=6&cli_promo_id=1&dtm_email_hash=not%20set&dtm_user_id=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sync.crwdcntrl.net/map/c=9828/tp=ADBE/gdpr=0/gdpr_consent=/tpid=72195082367942492304066494206891021442?https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D121998%26dpuuid%3D${profile_id}", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=79908&dpuuid=YxHVAN1DNdSnJ-885SEQ2hfH", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE
    );

    ns_end_transaction("headerstats_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("X776374");
    ns_web_url ("X776374",
        "URL=https://rcom.dynamicyield.com/v3/recommend/8776374?_=1662113024440",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"uid":"-1233986090078972675","ctx":{"type":"HOMEPAGE"},"data":[{"wId":137038,"fId":27148,"maxProducts":8,"rules":[{"id":928207,"type":"include","slots":[],"isContextAware":false}],"filtering":[{"type":"exclude","field":"group_id","value":[]}]}],"skusOnly":false}",
        BODY_END,
        INLINE_URLS,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBSHlVN0FNcw", END_INLINE,
            "URL=https://rp.liadm.com/j?dtstmp=1662113024461&aid=a-00oc&se=e30&duid=0b10d8358f40--01gbytg2cc5vhtb7a1kcbsfwbz&tna=v2.4.2&pu=https%3A%2F%2Fm.kohls.com%2F&ext_s_ecid=MCMID%7C78611155637513935743550952407821114794&wpn=lc-bundle&c=PG1ldGEgbmFtZT0iZGVzY3JpcHRpb24iIGNvbnRlbnQ9IkVuam95IGZyZWUgc2hpcHBpbmcgYW5kIGVhc3kgcmV0dXJucyBldmVyeSBkYXkgYXQgS29obCdzISBGaW5kIGdyZWF0IHNhdmluZ3Mgb24gY2xvdGhpbmcsIHNob2VzLCB0b3lzLCBob21lIGTDqWNvciwgYXBwbGlhbmNlcyBhbmQgZWxlY3Ryb25pY3MgZm9yIHRoZSB3aG9sZSBmYW1pbHkuCiAiPjx0aXRsZT5Lb2hsJ3MgfCBTaG9wIENsb3RoaW5nLCBTaG9lcywgSG9tZSwgS2l0Y2hlbiwgQmVkZGluZywgVG95cyAmYW1wOyBNb3JlCiAKPC90aXRsZT48bGluayByZWw9ImNhbm9uaWNhbCIgaHJlZj0iaHR0cHM6Ly93d3cua29obHMuY29tIj48dGl0bGUgaWQ9InBlcmNlbnQtb2ZmLXRpdGxlIiBhcmlhLWhpZGRlbj0idHJ1ZSIgdGFiaW5kZXg9Ii0xIj5wZXJjZW50IG9mZjwvdGl0bGU-PHRpdGxlIGlkPSJwZXJjZW50LW9mZi10aXRsZSI-cGVyY2VudCBvZmY8L3RpdGxlPjx0aXRsZSBpZD0icGVyY2VudC1vZmYtdGl0bGUiIGFyaWEtaGlkZGVuPSJ0cnVlIiB0YWJpbmRleD0iLTEiPnBlcmNlbnQgb2ZmPC90aXRsZT48dGl0bGUgaWQ9InN2Zy1idHMtbG9ja3VwLXRleHQiPk1vcmUgc3R5bGUuIE1vcmUgc2F2aW5ncy4gQmFjayB0byBzY2hvb2wuPC90aXRsZT48dGl0bGUgaWQ9InN2Zy1qYi10aXRsZSI-anVtcGluZyBiZWFuczwvdGl0bGU-PHRpdGxlIGlkPSJzby1sb2dvLXQiPlNPLiBHb29kcyBmb3IgbGlmZS48L3RpdGxlPjx0aXRsZSBpZD0idGVrZ2Vhci10Ij5UZWsgR2VhcjwvdGl0bGU-PHRpdGxlIGlkPSJjb252ZXJzZS1sb2dvIj5Db252ZXJzZTwvdGl0bGU-PHRpdGxlIGlkPSJ2YW5zLWxvZ28iPlZhbnMgIm9mZiB0aGUgd2FsbCI8L3RpdGxlPjx0aXRsZSBpZD0ibmlrZXNob2VzIj5OaWtlLjwvdGl0bGU-PHRpdGxlIGlkPSJhZGlkYXNzaG9lcyI-YWRpZGFzPC90aXRsZT48dGl0bGUgaWQ9InVhLWxvZ28tYiI-VW5kZXIgQXJtb3VyPC90aXRsZT48dGl0bGUgaWQ9InNsaWRlci1sb2dvLWxldmlzIj5MZXZpJ3M8L3RpdGxlPjx0aXRsZSBpZD0ibGVlVGl0bGUiPkxlZS48L3RpdGxlPjx0aXRsZSBpZD0id3JhbmdsZXJUaXRsZSI-V3JhbmdsZXI8L3RpdGxlPjx0aXRsZSBpZD0ic29ub21hIj5Tb25vbWEgR29vZHMgZm9yIExpZmU8L3RpdGxlPjx0aXRsZSBpZD0ic28tbG9nby10Ij5TTy4gR29vZHMgZm9yIGxpZmUuPC90aXRsZT48dGl0bGUgaWQ9InNvbm9tYSI-U29ub21hIEdvb2RzIGZvciBMaWZlPC90aXRsZT48dGl0bGUgaWQ9InNvbm9tYSI-U29ub21hIEdvb2RzIGZvciBMaWZlPC90aXRsZT48dGl0bGUgaWQ9InN2Zy1mbHgtaHotdGl0bGUiPkZMWDwvdGl0bGU-PHRpdGxlIGlkPSJzdmctYXB0OS10aXRsZSI-QVBULjk8L3RpdGxlPjx0aXRsZSBpZD0ic3ZnLWpiLXRpdGxlIj5qdW1waW5nIGJlYW5zPC90aXRsZT48dGl0bGUgaWQ9InNvLWxvZ28iPlNPLiBHb29kcyBmb3IgbGlmZS48L3RpdGxlPjx0aXRsZSBpZD0iY3JvZnQtYmFycm93Ij5Dcm9mdCBhbmQgQmFycm93PC90aXRsZT48dGl0bGUgaWQ9InRla2dlYXIiPlRlayBHZWFyPC90aXRsZT48dGl0bGUgaWQ9InN2Zy10aGViaWdvbmUtdGl0bGUiPlRIRSBCSUcgT05FPC90aXRsZT48dGl0bGUgaWQ9IkthbmRTSW50cm8iPlNlcGhvcmEgKyBLb2hsJ3MuPC90aXRsZT48dGl0bGUgaWQ9IkthbmRTSW50cm8wMiI-U2VwaG9yYSArIEtvaGwncy48L3RpdGxlPjx0aXRsZSBpZD0iTGFib3JfRGF5X1NhbGUiPkxhYm9yIERheSBTYWxlPC90aXRsZT48dGl0bGUgaWQ9ImtSZXdhcmRzIj5Lb2hsJ3MgUmV3YXJkczwvdGl0bGU-PHRpdGxlIGlkPSJTQkkiPlNlcGhvcmEgQmVhdXR5IEluc2lkZXI8L3RpdGxlPjx0aXRsZSBpZD0ia1Jld2FyZHMiPktvaGwncyBSZXdhcmRzPC90aXRsZT48dGl0bGUgaWQ9IlNCSSI-U2VwaG9yYSBCZWF1dHkgSW5zaWRlci48L3RpdGxlPjx0aXRsZSBpZD0ibGV2aXN0aXRsZSI-TGV2aSdzPC90aXRsZT48dGl0bGUgaWQ9ImNoYW1waW9uLWxvZ28iPkNoYW1waW9uPC90aXRsZT48dGl0bGUgaWQ9ImFtYXpvbi1zbWlsZS1jb3B5Ij5BbWF6b248L3RpdGxlPjx0aXRsZT5GYWNlYm9vazwvdGl0bGU-PHRpdGxlPlR3aXR0ZXI8L3RpdGxlPjx0aXRsZT5QaW50ZXJlc3Q8L3RpdGxlPjx0aXRsZT5JbnN0YWdyYW08L3RpdGxlPjx0aXRsZT5Zb3VUdWJlPC90aXRsZT4", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cdnssl.clicktale.net/www47/ptc/630f0ceb-a670-47fc-a7d0-b2a2133e9266.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://m.kohls.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sp.analytics.yahoo.com/sp.pl?a=10000&b=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&.yp=10156246&f=https%3A%2F%2Fm.kohls.com%2F&enc=UTF-8&yv=1.13.0&tagmgr=adobe", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=A3", END_INLINE,
            "URL=https://sp.analytics.yahoo.com/sp.pl?a=10000&b=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&.yp=10156246&f=https%3A%2F%2Fm.kohls.com%2F&enc=UTF-8&yv=1.13.0&et=custom&ea=&ev=&gv=&product_sku=not%20set&product_id=not%20set&conversion_sku=not%20set&conversion_product_id=not%20set&conversion_revenue=not%20set&tagmgr=adobe", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=A3", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBRjNxdGdPTg", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC8ceaea1fd94a499eb45113af297edb25-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=2340&dpuuid=933c62e0-0d07-4c52-9ca7-09145bb45ab8", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=2340&dpuuid=933c62e0-0d07-4c52-9ca7-09145bb45ab8", END_INLINE,
            "URL=https://pagead2.googlesyndication.com/bg/JRDtgcUl_7OUjJ4QO8bVbwNuRTRqDUxuSBYCwiPHS6U.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBRjNxdGdPTg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://s.yimg.com/wi/ytc.js", END_INLINE,
            "URL=https://sync-tm.everesttech.net/upi/pid/5w3jqr4k?redir=https%3A%2F%2Fcm.g.doubleclick.net%2Fpixel%3Fgoogle_nid%3Dg8f47s39e399f3fe%26google_push%26google_sc%26google_hm%3D%24%7BTM_USER_ID_BASE64ENC_URLENC%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://pagead2.googlesyndication.com/pagead/sodar?id=sodar2&v=225&li=gpt_2022083101&jk=4335508868998629&rc=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBR3Fpd3dPRg", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBR3Fpd3dPRg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://beacon.krxd.net/usermatch.gif?kuid_status=new&partner=adobe&id=72195082367942492304066494206891021442", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=2340&dpuuid=933c62e0-0d07-4c52-9ca7-09145bb45ab8", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBSEZEdlFPQQ", END_INLINE,
            "URL=https://sync-tm.everesttech.net/upi/pid/btu4jd3a?redir=https%3A%2F%2Fpixel.rubiconproject.com%2Ftap.php%3Fv%3D7941%26nid%3D2243%26put%3D%24%7BUSER_ID%7D%26expires%3D90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBRUlPYkFPQQ", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBRUlPYkFPQQ", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://cdnssl.clicktale.net/ptc/630f0ceb-a670-47fc-a7d0-b2a2133e9266.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://m.kohls.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sync.crwdcntrl.net/map/ct=y/c=9828/tp=ADBE/gdpr=0/gdpr_consent=/tpid=72195082367942492304066494206891021442?https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D121998%26dpuuid%3D${profile_id}", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_cc_cc", END_INLINE,
            "URL=https://sync-tm.everesttech.net/ct/upi/pid/5w3jqr4k?redir=https%3A%2F%2Fcm.g.doubleclick.net%2Fpixel%3Fgoogle_nid%3Dg8f47s39e399f3fe%26google_push%26google_sc%26google_hm%3D%24%7BTM_USER_ID_BASE64ENC_URLENC%7D&_test=YxHVAAAEL45eJABN", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBQnVjVHdOeA", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBQnVjVHdOeA", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/3112953_Navy_Burnout?wid=800&hei=800&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/2245601_Black?wid=800&hei=800&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/4296248_Black?wid=800&hei=800&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/4569328_Medium_Gray?wid=800&hei=800&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/3946927_Green_Heather?wid=800&hei=800&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/851246_White?wid=800&hei=800&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/1731442_Madison?wid=800&hei=800&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/893443_White?wid=800&hei=800&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBSzBTWmdOMg", END_INLINE,
            "URL=https://tpc.googlesyndication.com/generate_204?NpHQvw", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sync-tm.everesttech.net/upi/pid/ZMAwryCI?redir=https%3A%2F%2Fdsum-sec.casalemedia.com%2Frum%3Fcm_dsp_id%3D88%26external_user_id%3D%24%7BTM_USER_ID%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sync-tm.everesttech.net/ct/upi/pid/btu4jd3a?redir=https%3A%2F%2Fpixel.rubiconproject.com%2Ftap.php%3Fv%3D7941%26nid%3D2243%26put%3D%24%7BUSER_ID%7D%26expires%3D90&_test=YxHVAAAEL6xfNABN", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("X776374", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("j");
    ns_web_url ("j",
        "URL=https://rp.liadm.com/j?dtstmp=1662113024461&aid=a-00oc&se=e30&duid=0b10d8358f40--01gbytg2cc5vhtb7a1kcbsfwbz&tna=v2.4.2&pu=https%3A%2F%2Fm.kohls.com%2F&ext_s_ecid=MCMID%7C78611155637513935743550952407821114794&wpn=lc-bundle&c=PG1ldGEgbmFtZT0iZGVzY3JpcHRpb24iIGNvbnRlbnQ9IkVuam95IGZyZWUgc2hpcHBpbmcgYW5kIGVhc3kgcmV0dXJucyBldmVyeSBkYXkgYXQgS29obCdzISBGaW5kIGdyZWF0IHNhdmluZ3Mgb24gY2xvdGhpbmcsIHNob2VzLCB0b3lzLCBob21lIGTDqWNvciwgYXBwbGlhbmNlcyBhbmQgZWxlY3Ryb25pY3MgZm9yIHRoZSB3aG9sZSBmYW1pbHkuCiAiPjx0aXRsZT5Lb2hsJ3MgfCBTaG9wIENsb3RoaW5nLCBTaG9lcywgSG9tZSwgS2l0Y2hlbiwgQmVkZGluZywgVG95cyAmYW1wOyBNb3JlCiAKPC90aXRsZT48bGluayByZWw9ImNhbm9uaWNhbCIgaHJlZj0iaHR0cHM6Ly93d3cua29obHMuY29tIj48dGl0bGUgaWQ9InBlcmNlbnQtb2ZmLXRpdGxlIiBhcmlhLWhpZGRlbj0idHJ1ZSIgdGFiaW5kZXg9Ii0xIj5wZXJjZW50IG9mZjwvdGl0bGU-PHRpdGxlIGlkPSJwZXJjZW50LW9mZi10aXRsZSI-cGVyY2VudCBvZmY8L3RpdGxlPjx0aXRsZSBpZD0icGVyY2VudC1vZmYtdGl0bGUiIGFyaWEtaGlkZGVuPSJ0cnVlIiB0YWJpbmRleD0iLTEiPnBlcmNlbnQgb2ZmPC90aXRsZT48dGl0bGUgaWQ9InN2Zy1idHMtbG9ja3VwLXRleHQiPk1vcmUgc3R5bGUuIE1vcmUgc2F2aW5ncy4gQmFjayB0byBzY2hvb2wuPC90aXRsZT48dGl0bGUgaWQ9InN2Zy1qYi10aXRsZSI-anVtcGluZyBiZWFuczwvdGl0bGU-PHRpdGxlIGlkPSJzby1sb2dvLXQiPlNPLiBHb29kcyBmb3IgbGlmZS48L3RpdGxlPjx0aXRsZSBpZD0idGVrZ2Vhci10Ij5UZWsgR2VhcjwvdGl0bGU-PHRpdGxlIGlkPSJjb252ZXJzZS1sb2dvIj5Db252ZXJzZTwvdGl0bGU-PHRpdGxlIGlkPSJ2YW5zLWxvZ28iPlZhbnMgIm9mZiB0aGUgd2FsbCI8L3RpdGxlPjx0aXRsZSBpZD0ibmlrZXNob2VzIj5OaWtlLjwvdGl0bGU-PHRpdGxlIGlkPSJhZGlkYXNzaG9lcyI-YWRpZGFzPC90aXRsZT48dGl0bGUgaWQ9InVhLWxvZ28tYiI-VW5kZXIgQXJtb3VyPC90aXRsZT48dGl0bGUgaWQ9InNsaWRlci1sb2dvLWxldmlzIj5MZXZpJ3M8L3RpdGxlPjx0aXRsZSBpZD0ibGVlVGl0bGUiPkxlZS48L3RpdGxlPjx0aXRsZSBpZD0id3JhbmdsZXJUaXRsZSI-V3JhbmdsZXI8L3RpdGxlPjx0aXRsZSBpZD0ic29ub21hIj5Tb25vbWEgR29vZHMgZm9yIExpZmU8L3RpdGxlPjx0aXRsZSBpZD0ic28tbG9nby10Ij5TTy4gR29vZHMgZm9yIGxpZmUuPC90aXRsZT48dGl0bGUgaWQ9InNvbm9tYSI-U29ub21hIEdvb2RzIGZvciBMaWZlPC90aXRsZT48dGl0bGUgaWQ9InNvbm9tYSI-U29ub21hIEdvb2RzIGZvciBMaWZlPC90aXRsZT48dGl0bGUgaWQ9InN2Zy1mbHgtaHotdGl0bGUiPkZMWDwvdGl0bGU-PHRpdGxlIGlkPSJzdmctYXB0OS10aXRsZSI-QVBULjk8L3RpdGxlPjx0aXRsZSBpZD0ic3ZnLWpiLXRpdGxlIj5qdW1waW5nIGJlYW5zPC90aXRsZT48dGl0bGUgaWQ9InNvLWxvZ28iPlNPLiBHb29kcyBmb3IgbGlmZS48L3RpdGxlPjx0aXRsZSBpZD0iY3JvZnQtYmFycm93Ij5Dcm9mdCBhbmQgQmFycm93PC90aXRsZT48dGl0bGUgaWQ9InRla2dlYXIiPlRlayBHZWFyPC90aXRsZT48dGl0bGUgaWQ9InN2Zy10aGViaWdvbmUtdGl0bGUiPlRIRSBCSUcgT05FPC90aXRsZT48dGl0bGUgaWQ9IkthbmRTSW50cm8iPlNlcGhvcmEgKyBLb2hsJ3MuPC90aXRsZT48dGl0bGUgaWQ9IkthbmRTSW50cm8wMiI-U2VwaG9yYSArIEtvaGwncy48L3RpdGxlPjx0aXRsZSBpZD0iTGFib3JfRGF5X1NhbGUiPkxhYm9yIERheSBTYWxlPC90aXRsZT48dGl0bGUgaWQ9ImtSZXdhcmRzIj5Lb2hsJ3MgUmV3YXJkczwvdGl0bGU-PHRpdGxlIGlkPSJTQkkiPlNlcGhvcmEgQmVhdXR5IEluc2lkZXI8L3RpdGxlPjx0aXRsZSBpZD0ia1Jld2FyZHMiPktvaGwncyBSZXdhcmRzPC90aXRsZT48dGl0bGUgaWQ9IlNCSSI-U2VwaG9yYSBCZWF1dHkgSW5zaWRlci48L3RpdGxlPjx0aXRsZSBpZD0ibGV2aXN0aXRsZSI-TGV2aSdzPC90aXRsZT48dGl0bGUgaWQ9ImNoYW1waW9uLWxvZ28iPkNoYW1waW9uPC90aXRsZT48dGl0bGUgaWQ9ImFtYXpvbi1zbWlsZS1jb3B5Ij5BbWF6b248L3RpdGxlPjx0aXRsZT5GYWNlYm9vazwvdGl0bGU-PHRpdGxlPlR3aXR0ZXI8L3RpdGxlPjx0aXRsZT5QaW50ZXJlc3Q8L3RpdGxlPjx0aXRsZT5JbnN0YWdyYW08L3RpdGxlPjx0aXRsZT5Zb3VUdWJlPC90aXRsZT4&n3pc=true",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://login.dotomi.com/ucm/visit/px?cli_promo_id=1&dtm_com=28&dtm_cid=2683&dtm_fid=101&dtm_email_hash=not+set&dtm_format=6&dtm_cmagic=8420d3&dtm_form_uid=660406677433594160&tcflag=true&tp_user_assignment_type=0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=DotomiUser", END_INLINE
    );

    ns_end_transaction("j", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("batch_2");
    ns_web_url ("batch_2",
        "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1662113024761_47359",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=DYID;DYSES",
        BODY_BEGIN,
            "{"md":{"uid":"-1233986090078972675","sec":"8776374","ses":"5b3e117ce067eeb99b2b5b268b908b68","colors":"d.an.c.ss.","sld":"kohls.com","rurl":"https://m.kohls.com/","ts":"Direct","p":"1","ref":"","rref":"","reqts":1662113024760,"rri":6965856},"data":[{"type":"rcom","timeInMS":534,"isPerBrowser":true,"isPerSection":true},{"type":"imp","expId":1250304,"varIds":[27070038],"expSes":"71849","mech":"1","smech":null,"verId":"11527264","aud":"1408117.1438654.1362538.1362542.1468187","expVisitId":"-5734382003328103099","eri":null,"rcomInfo":[{"context":{"type":"HOMEPAGE"},"wId":137038,"fId":27148,"fallback":true,"slots":[{"pId":"58123483","strId":9,"md":{}},{"pId":"99719860","strId":9,"md":{}},{"pId":"49316482","strId":9,"md":{}},{"pId":"21034257","strId":9,"md":{}},{"pId":"55038835","strId":9,"md":{}},{"pId":"91911364","strId":9,"md":{}},{"pId":"49279169","strId":9,"md":{}},{"pId":"50055877","strId":9,"md":{}}]}]}],"sectionId":"8776374"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?value=0&guid=ON&script=0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=121998&dpuuid=96b59e48b93c90f0ce5e85e4811394ac", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=121998&dpuuid=96b59e48b93c90f0ce5e85e4811394ac", END_INLINE,
            "URL=https://cdnssl.clicktale.net/pcc/630f0ceb-a670-47fc-a7d0-b2a2133e9266.js?DeploymentConfigName=Malka_20220809&Version=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://m.kohls.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cdnssl.clicktale.net/www/bridge-WR110.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://m.kohls.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBSTdVLWdPWQ", END_INLINE,
            "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBSTdVLWdPWQ", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://sync-tm.everesttech.net/upi/pid/UH6TUt9n?redir=https%3A%2F%2Fib.adnxs.com%2Fsetuid%3Fentity%3D158%26code%3D%24%7BTM_USER_ID%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=121998&dpuuid=96b59e48b93c90f0ce5e85e4811394ac", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBSzQ4Y3dPRg", END_INLINE,
            "URL=https://sync-tm.everesttech.net/ct/upi/pid/ZMAwryCI?redir=https%3A%2F%2Fdsum-sec.casalemedia.com%2Frum%3Fcm_dsp_id%3D88%26external_user_id%3D%24%7BTM_USER_ID%7D&_test=YxHVAAAK6PuMFwBC", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sli.kohls.com/baker?dtstmp=1662113024902", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;AKA_A2;bm_sz;check;rxVisitor;dtLatC;dtSa;s_ecid;dtCookie;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;rxvt;dtPC;_dpm_ses.f2d1;_dpm_id.f2d1;_abck;gpv_v9;s_cc;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1071871169/?value=0&guid=ON&script=0&is_vtc=1&random=1587570821", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://i.liadm.com/s/c/a-00oc?s=&cim=&ps=true&ls=true&duid=0b10d8358f40--01gbytg2cc5vhtb7a1kcbsfwbz&ppid=0&euns=0&ci=0&version=sc-v0.2.0&nosync=false&monitorExternalSyncs=false&", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1x1&google_gid=CAESEF5-10mp_fsmZcO_15xnTy0&google_cver=1", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&url=/1x1&google_hm=WXhIVkFBQUFBS3l2WUFNMA", END_INLINE,
            "URL=https://sync-tm.everesttech.net/upi/pid/ny75r2x0?redir=https%3A%2F%2Fus-u.openx.net%2Fw%2F1.0%2Fsd%3Fid%3D537148856%26val%3D%24%7BTM_USER_ID%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://c.clicktale.net/pageview?pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&lv=1662113024&lhd=1662113024&hd=1662113024&pn=1&dw=775&dh=10892&ww=790&wh=473&sw=8192&sh=4096&dr=&url=https%3A%2F%2Fm.kohls.com%2F&uc=0&la=en-US&cvars=%7B%221%22%3A%5B%22Page%20Name%22%2C%22homepage%22%5D%7D&cvarp=%7B%221%22%3A%5B%22Page%20Name%22%2C%22homepage%22%5D%7D&v=11.40.1&r=801145", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sync-tm.everesttech.net/ct/upi/pid/UH6TUt9n?redir=https%3A%2F%2Fib.adnxs.com%2Fsetuid%3Fentity%3D158%26code%3D%24%7BTM_USER_ID%7D&_test=YxHVAAAK6WaPEgBC", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sync-tm.everesttech.net/ct/upi/pid/ny75r2x0?redir=https%3A%2F%2Fus-u.openx.net%2Fw%2F1.0%2Fsd%3Fid%3D537148856%26val%3D%24%7BTM_USER_ID%7D&_test=YxHVAQAK6SyQdwBC", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sync-tm.everesttech.net/upi/pid/b9pj45k4?redir=https%3A%2F%2Fimage2.pubmatic.com%2FAdServer%2FPug%3Fvcode%3Dbz0yJnR5cGU9MSZqcz0xJmNvZGU9MjE5MSZ0bD0yNTkyMDA%3D%26piggybackCookie%3D%24%7BUSER_ID%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://c.clicktale.net/pageEvent?value=MIewdgZglg5gXAAgLIEMA2BrFB9ATABlwIA58BOIAAA%3D&enc=lzstring&isETR=false&isCustomHashId=false&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&r=821767", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://c.clicktale.net/pageEvent?value=IYDgjALAzAbGIAYDsAjGThTAMwMYwBMwBTMAJjBSjJhDIIOwODOJBCAA&enc=lzstring&isETR=false&isCustomHashId=true&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&r=779064", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sync-tm.everesttech.net/ct/upi/pid/b9pj45k4?redir=https%3A%2F%2Fimage2.pubmatic.com%2FAdServer%2FPug%3Fvcode%3Dbz0yJnR5cGU9MSZqcz0xJmNvZGU9MjE5MSZ0bD0yNTkyMDA%3D%26piggybackCookie%3D%24%7BUSER_ID%7D&_test=YxHVAQAK6CySfwBC", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://c.clicktale.net/dvar?v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&dv=N4IgDgTg9mCMDsIBcIDWUAWAbAzgAgDsoAXPLKAcwoFMATPASwIB9yBPAQy2LcJLMo16TEABoQAQQDSEgPoAJAAoAmPAGEoUVA2rIQAGTzM88qAFtqeAPR5F%2BxUbwAlOtRwMKBEAF8gAAA%3D%3D&enc=lzstring&r=719593", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("batch_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("quota");
    ns_web_url ("quota",
        "URL=https://q-aus1.clicktale.net/quota?enc=raw",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"recordingTypes":[6,7],"url":"https://m.kohls.com/","projectId":2399,"uu":"6d513319-e33c-a1a2-f741-3ecaf18284e9","sn":1,"pn":1}",
        BODY_END,
        INLINE_URLS,
            "URL=https://sync-tm.everesttech.net/upi/pid/h0r58thg?redir=https%3A%2F%2Fsync.search.spotxchange.com%2Fpartner%3Fadv_id%3D6409%26uid%3D%24%7BUSER_ID%7D%26img%3D1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://match.adsrvr.org/track/cmf/generic?ttd_pid=liveintent&ttd_tpi=1&gdpr=0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=TDID;TDCPM", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=127444&dpuuid=&redir=https%3A%2F%2Fi.liadm.com%2Fs%2Fe%2Fa-00oc%2F0%2F1f8a3c6be33a44dd8359a367f1f7fb7f%3Fmpid%3D82775%26muid%3D%24%7BDD_UUID%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=127444&dpuuid=&redir=https%3A%2F%2Fi.liadm.com%2Fs%2Fe%2Fa-00oc%2F0%2F1f8a3c6be33a44dd8359a367f1f7fb7f%3Fmpid%3D82775%26muid%3D%24%7BDD_UUID%7D", END_INLINE,
            "URL=https://x.dlx.addthis.com/e/live_intent_sync?na_exid=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://trc.taboola.com/sg/liveintent/1/cm/", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://b1sync.zemanta.com/usersync/liveintent/?cb=%2F%2Fi.liadm.com%2Fs%2F35004%3Fbidder_id%3D98254%26bidder_uuid%3D__ZUID__", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://stags.bluekai.com/site/23178?id=GZ_XFN_XqGeKJgNvKNaB&redir=https%3A%2F%2Fb1sync.zemanta.com%2Fusersync%2Fbluekai%2Fcallback%2F%3Fd%3DF4XWSLTMNFQWI3JOMNXW2L3TF4ZTKMBQGQ7WE2LEMRSXEX3JMQ6TSOBSGU2CMYTJMRSGK4S7OV2WSZB5I5NF6WCGJZPVQ4KHMVFUUZ2OOZFU4YKCEZSXQY3IMFXGOZJ5NRUXMZLJNZ2GK3TU", END_INLINE,
            "URL=https://mid.rkdms.com/bct?pid=bcccb40a-06d2-44fe-bdd2-a91ef4a5bfd0&&puid=&liid=&_ct=im", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://pixel-sync.sitescout.com/dmp/pixelSync?nid=12&rurl=https%3A%2F%2Fi.liadm.com%2Fs%2F35758%3Fbidder_id%3D2380%26bidder_uuid%3D%7BuserId%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sync-tm.everesttech.net/ct/upi/pid/h0r58thg?redir=https%3A%2F%2Fsync.search.spotxchange.com%2Fpartner%3Fadv_id%3D6409%26uid%3D%24%7BUSER_ID%7D%26img%3D1&_test=YxHVAQAK6YCWxgAK", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://i.liadm.com/s/35759?bidder_id=44489&bidder_uuid=56bc35b1-0909-4b6f-840d-20add6a418d9", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=/s/35759?bidder_id=44489&bidder_uuid=56bc35b1-0909-4b6f-840d-20add6a418d9&_li_chk=true&previous_uuid=c41c8fa57bdb41c3a69c8a520b142cab", END_INLINE,
            "URL=https://sync-tm.everesttech.net/upi/pid/r7ifn0SL?redir=https%3A%2F%2Fwww.facebook.com%2Ffr%2Fb.php%3Fp%3D1531105787105294%26e%3D%24%7BTM_USER_ID%7D%26t%3D2592000%26o%3D0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=127444&dpuuid=&redir=https%3A%2F%2Fi.liadm.com%2Fs%2Fe%2Fa-00oc%2F0%2F1f8a3c6be33a44dd8359a367f1f7fb7f%3Fmpid%3D82775%26muid%3D%24%7BDD_UUID%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://i.liadm.com/s/35759?bidder_id=44489&bidder_uuid=56bc35b1-0909-4b6f-840d-20add6a418d9&_li_chk=true&previous_uuid=c41c8fa57bdb41c3a69c8a520b142cab", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://i6.liadm.com/s/35759?bidder_id=44489&bidder_uuid=56bc35b1-0909-4b6f-840d-20add6a418d9", END_INLINE,
            "URL=https://pagead2.googlesyndication.com/pagead/sodar?id=sodar2&v=225&t=2&li=gpt_2022083101&jk=4335508868998629&bg=!MzClMHTNAAZTikH4c4o7ACkAdvg8Wl1AW3Nea0h2umTA00VqTHkHZU1yrjeFO0rxNWUkvlA6lp5uVgIAAADUUgAAAANoAQcKAOISF6ELlvS98k_qJy1ZoW74f9sYGOBezOqhP7iv0XaINsOYvKbt9_7GddjwSfkaEK4I4uGbP4L68y5XqNa9Mdn8msEdFwleaDz7T2eVV9tVjnQSWKsJ6A10Uab7DI2qFZC30vNSPHeIAX8f35CbyoWHyD1jWB4q3A-nMDl1V2De-23ZrD4Jz58zqlzsbcLqAwVc0GAP8XCovUuOGp_yfYHx4CsG_ksQ4Rk7JLKQJtpNCw8Q6oGBFtVKBRH1AU-npm0b2YWbh4J3hMmx8CE9zYd0116sLILO_vMxvLsV1HC3REHNmQKYM0dwc7NDbvuT2llMMLHJtUjyUkJzUgLwnakHWP6t5n9Ec8RP4pAEI1vUTgrSBDmkwLe_xv6thHiqs4bMGwKOV-BSgmedSrpfXAgfbK5EeOdQXSsPa-sfD1l7wUkdxvF3J0EIkWMWwt-LjhCGjmdlXWr7ct4n1jQ3fC29iKxtBWO-yEP3ixGMfwJQMTDZvGcxs3wg0TlCqQW1bJYLrCLBvAAzsINY4m0c2asst7B3mxoIf2YIxnZ63CUW0MpSSRzSVJgVVtgvdbTqQ7JJdqxSQ0zakeaNOjQwtlhk7tAa-ATjvvOxAqcqbUrBdVhglTF_eoeR1rCJtE-Ru-IKgGVjIZ5JYZDTHKhpJDid4q3msfK05AyqprY-FhbVGsuOJxp00W8aOBd7dRqeErxXID9FcflAyFcIvarAdXdG-fTkyniGt9iiox-qFAFk_mBi4qYGoLpwIs_z_FWt0M_AjU3Bgfry9uN68isbf2lHJd7iZmn_Hj3zSqqKxDboQs1XtyvHfUngRj9Yx7_sMYfJOwRSvuUFrAVtOrQkQzmiBkUHuQBcks0E06mVsyaSomY-WiFfKS8M54h12ZPsLK9_3FSITcpXipvFKy_tYNn6NnvVNoVbXNjP_T2tYFpXBSbhcdjdCgzV2I9Oc2hnqXRld94KQnaPWKvzAizN5-E5lZ_FhvjOZk47V4uXO5L7Ogb_TSeps3-xTmX1mNB4lZrxIOwiVTc0tLd6wckgqsHOdEnNCmrSsA4zpvLucKe9AmlNNHDEn20OaXA2nfoybo14hHSQHVrZun238hCHCMO0VBbzZrmqQboQdP-rtSpGzc1ubjVpUgNpbjnXbr05StrLDmQ49YXKRkceSCBGoJeWT5B-I2qZDsN1OtacNw", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sync-tm.everesttech.net/ct/upi/pid/r7ifn0SL?redir=https%3A%2F%2Fwww.facebook.com%2Ffr%2Fb.php%3Fp%3D1531105787105294%26e%3D%24%7BTM_USER_ID%7D%26t%3D2592000%26o%3D0&_test=YxHVAQAEL1JupwBN", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://mid.rkdms.com/bct?pid=8bc436aa-e0fc-4baa-9c9a-06fbeca87826&puid=72195082367942492304066494206891021442&_ct=img", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://i6.liadm.com/s/35759?bidder_id=44489&bidder_uuid=56bc35b1-0909-4b6f-840d-20add6a418d9", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://i.liadm.com/s/19948?bidder_id=178256&bidder_uuid=44ede74def9251a9e20a7f89a36d09f1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=/s/19948?bidder_id=178256&bidder_uuid=44ede74def9251a9e20a7f89a36d09f1&_li_chk=true&previous_uuid=a3b16d4e2af044ada39cb8b1d0fdcdc5", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=129099&dpuuid=a148ca038030ea6d5b80ad3ef564a0c2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=129099&dpuuid=a148ca038030ea6d5b80ad3ef564a0c2", END_INLINE,
            "URL=https://pixel-sync.sitescout.com/dmp/pixelSync?cookieQ=1&nid=12&rurl=https%3A%2F%2Fi.liadm.com%2Fs%2F35758%3Fbidder_id%3D2380%26bidder_uuid%3D%7BuserId%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=ssi", END_INLINE,
            "URL=https://pixel.onaudience.com/?partner=130&mapped=72195082367942492304066494206891021442&redirect=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D161033%26dpuuid%3D%25m", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://stags.bluekai.com/site/23178?id=GZ_XFN_XqGeKJgNvKNaB&redir=https%3A%2F%2Fb1sync.zemanta.com%2Fusersync%2Fbluekai%2Fcallback%2F%3Fd%3DF4XWSLTMNFQWI3JOMNXW2L3TF4ZTKMBQGQ7WE2LEMRSXEX3JMQ6TSOBSGU2CMYTJMRSGK4S7OV2WSZB5I5NF6WCGJZPVQ4KHMVFUUZ2OOZFU4YKCEZSXQY3IMFXGOZJ5NRUXMZLJNZ2GK3TU", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://x.dlx.addthis.com/e/live_intent_sync?na_exid=&rd=Y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=na_id;na_tc;uid;ouid", END_INLINE,
            "URL=https://i.liadm.com/s/19948?bidder_id=178256&bidder_uuid=44ede74def9251a9e20a7f89a36d09f1&_li_chk=true&previous_uuid=a3b16d4e2af044ada39cb8b1d0fdcdc5", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://x.bidswitch.net/sync?dsp_id=42&user_id=", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=129099&dpuuid=a148ca038030ea6d5b80ad3ef564a0c2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://i.liadm.com/s/35758?bidder_id=2380&bidder_uuid=06ab00d0-fb43-42ab-9fc7-b5f0d4c70fd3-6311d501-5553", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=/s/35758?bidder_id=2380&bidder_uuid=06ab00d0-fb43-42ab-9fc7-b5f0d4c70fd3-6311d501-5553&_li_chk=true&previous_uuid=eeeb0383da6a4a0aa3396892fbaf2df1", END_INLINE,
            "URL=https://s.btstatic.com/tag.js#site=4DPyaxM&referrer=https%3A%2F%2Fm.kohls.com%2F", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://x.bidswitch.net/sync?dsp_id=42&user_id=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://x.bidswitch.net/ul_cb/sync?dsp_id=42&user_id=", END_INLINE,
            "URL=https://i.liadm.com/s/35758?bidder_id=2380&bidder_uuid=06ab00d0-fb43-42ab-9fc7-b5f0d4c70fd3-6311d501-5553&_li_chk=true&previous_uuid=eeeb0383da6a4a0aa3396892fbaf2df1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://i6.liadm.com/s/35758?bidder_id=2380&bidder_uuid=06ab00d0-fb43-42ab-9fc7-b5f0d4c70fd3-6311d501-5553", END_INLINE,
            "URL=https://b1sync.zemanta.com/usersync/bluekai/callback/?d=F4XWSLTMNFQWI3JOMNXW2L3TF4ZTKMBQGQ7WE2LEMRSXEX3JMQ6TSOBSGU2CMYTJMRSGK4S7OV2WSZB5I5NF6WCGJZPVQ4KHMVFUUZ2OOZFU4YKCEZSXQY3IMFXGOZJ5NRUXMZLJNZ2GK3TU", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://c.clicktale.net/dvar?v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&dv=N4IgggQg%2BmAqMQAQFpEFlGwEoEYcE4BmFdATwGUAXAewCcBTRMAdwEMGA7egZ28XIDGrADaMATAAYxY5BJzIcEkAC4QEiYgDC1DpVrVhIADThocBCQwAFAFrrFJcpoAyJANICAFuwDmjAOLC1ABGIoiUrAKUAJYCfJJi8hKEyGL4Kmo4iAAi%2BgAOACbUzByIANSIABL0rAX0tMamMPAAGrAkAFJijvSUiABq7IgAZnSIEPSllQCuw8MAtqylqGjsANaIAHL0zIgAqtz13BkAogAeefXRkwKMYI2QzRYrmFiJ%2BFmoWDx5OtzRADdGFY0FZEEsClUanVaJhaKw5rFEFhWPM8ogrJVugkZBIABypboAVgkAFIMuotjpkN9uL8OP8gQ8zPBICRshhsFJ8BpUG40JsqmDYNR0RAllxYTjZASxAAWClZcUcSXMp5s1AcjF2Qh4vGOFwkMACATUaa6KkxYaIBLyHC4wiKpgmvqbahW2KsGI6NXmACKYEQXUwPD6aFN80QAHlaDC4ZE1tEOD5Thcrjc7r7Wu1NRhvqLJvwaAxEM5otw%2BlYsFHsjapBJZETZArVOdLrRrhxbkwQABfIAA&enc=lzstring&r=974582", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://c.clicktale.net/pageEvent?value=CwYwhgHArAJiCMA2KYBGsoE4Ds3oDNF5URtMoBmKgJigmDDE32yAAA%3D%3D&enc=lzstring&isETR=false&isCustomHashId=true&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&r=775451", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC33673eab55e247c8baa7307900d7c6a0-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://x.bidswitch.net/ul_cb/sync?dsp_id=42&user_id=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=tuuid;c;tuuid_lu", END_INLINE,
            "URL=https://i6.liadm.com/s/35758?bidder_id=2380&bidder_uuid=06ab00d0-fb43-42ab-9fc7-b5f0d4c70fd3-6311d501-5553", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&referrer=https%3A%2F%2Fm.kohls.com%2F&H=-1dxqaig", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RCbc6e4b814cd34be58ad54a9bdf99e612-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dpm.demdex.net/ibs:dpid=161033&dpuuid=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=161033&dpuuid=", END_INLINE,
            "URL=https://www.redditstatic.com/ads/pixel.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC06328aa0bc0c48f29cbfa045675a420b-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RCc9206bee094e4fc1ac91cf03b05f4a6e-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dpm.demdex.net/demconf.jpg?et:ibs%7cdata:dpid=161033&dpuuid=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE,
            "URL=https://alb.reddit.com/rp.gif?ts=1662113026125&id=t2_90kv23sl&event=PageVisit&m.itemCount=&m.value=&m.valueDecimal=&m.currency=&m.transactionId=&m.customEventName=&m.products=&uuid=214f42b3-ed7a-42cf-a7c0-1c97815d77e8&aaid=&em=&external_id=62109380ff146a9e9b573f74145632abbfc279ddc29bc9d459be441acaf32645&idfa=&integration=reddit&opt_out=0&sh=1313&sw=489&v=rdt_02c59ad6", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://d.impactradius-event.com/A375953-1cd4-4523-a263-b5b3c8c11fb81.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC7c48da855924464dbf1b33bce959dcf7-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://servedby.flashtalking.com/container/1638;12462;1480;iframe/?spotName=Homepage&U3=78611155637513935743550952407821114794&U7=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&U10=N/A&cachebuster=10208.65205162358", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=flashtalkingad1", END_INLINE
    );

    ns_end_transaction("quota", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("X349");
    ns_web_url ("X349",
        "URL=https://kohls.sjv.io/xc/385561/362119/5349",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "srcref=&landurl=https%3A%2F%2Fm.kohls.com%2F&custid=78611155637513935743550952407821114794&_ir=U58%7C%7C1662113026239",
        BODY_END,
        INLINE_URLS,
            "URL=https://c.clicktale.net/pageEvent?value=EYJgZsDsCmwKwA4DMIAmxUENIBYQiU2lx0yQAYA2HARmhAE4lCHoEgAA&enc=lzstring&isETR=false&isCustomHashId=true&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&r=791391", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RCbb3281f69a1d4511be056bdbcd04b68a-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://servedby.flashtalking.com/segment/modify/xh0;;pixel/?name=Homepage_2019", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=flashtalkingad1", END_INLINE,
            "URL=https://idsync.rlcdn.com/422866.gif?partner_uid=53588F2D83C0A9&", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=rlas3;pxrc", END_INLINE,
            "URL=https://d9.flashtalking.com/d9core", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=flashtalkingad1", END_INLINE
    );

    ns_end_transaction("X349", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("X349_2");
    ns_web_url ("X349_2",
        "URL=https://kohls.sjv.io/bc/385561/362119/5349",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "srcref=&landurl=https%3A%2F%2Fm.kohls.com%2F&custid=78611155637513935743550952407821114794&_ir=U58%7C1662113026239.a3yoq7s1u74%7C1662113026239&xhrf=f",
        BODY_END,
        INLINE_URLS,
            "URL=https://logs-01.loggly.com/inputs/9b965af4-52fb-46fa-be1b-8dc5fb0aad05/tag/jsinsight/1*1.gif?ver=U58&acid=A375953-1cd4-4523-a263-b5b3c8c11fb81&type=UTT&msg=NaN%3A%20undefined%2C%20status%3A%20undefined%2C%20ts%3A%201662113026239%2C%20time%3A%20121%2C%20text%3A%20undefined%2C%20url%3A%20https%3A%2F%2Fkohls.sjv.io%2Fxc%2F385561%2F362119%2F5349&event=doXHR%20problem&agent=Mozilla%2F5.0%20(Linux%3B%20Android%206.0.1%3B%20Moto%20G%20(4)%20Build%2FMPJ24.139-64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F58.0.3029.81%20Mobile%20Safari%2F537.36%20PTST%2F1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("X349_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording");
    ns_web_url ("recording",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=1&rst=1662113025036&let=1662113026381&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_main_url_1_1662113084886.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://static.ads-twitter.com/uwt.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC45c5983a76b643f5a46e94f8beb85a30-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("recording", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("lgc");
    ns_web_url ("lgc",
        "URL=https://d9.flashtalking.com/lgc",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://servedby.flashtalking.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=flashtalkingad1",
        BODY_BEGIN,
            "tbx=%257B%2522D9_1%2522%253A1662113026600%252C%2522D9_6%2522%253Anull%252C%2522D9_7%2522%253Anull%252C%2522D9_8%2522%253Anull%252C%2522D9_9%2522%253A%255B%255D%252C%2522D9_10%2522%253A%255B%255D%252C%2522D9_61%2522%253A%25222ee030229bea879035c93c913dbd524e%2522%252C%2522D9_67%2522%253A%2522d10e54cb41fb43c5945b4d01b9bde1da%2522%252C%2522D9_18%2522%253A%257B%257D%252C%2522D9_16%2522%253A300%252C%2522D9_4%2522%253A%257B%2522width%2522%253A1313%252C%2522height%2522%253A489%257D%252C%2522D9_14%2522%253A%2522Linux%2520x86_64%2522%252C%2522D9_15%2522%253A%2522en-US%2522%252C%2522D9_19%2522%253A%2522Mozilla%2522%252C%2522D9_123%2522%253A0%252C%2522D9_33%2522%253A%2522YTplbiMjI2I6MjQjIyNjOjMwMCMjI2Q6dHJ1ZSMjI2Y6dHJ1ZSMjI2c6dW5kZWZpbmVkIyMjaTp1bmRlZmluZWQjIyNqOkxpbnV4IHg4Nl82NA%253D%253D%2522%252C%2522D9_34%2522%253A1691054835%252C%2522D9_30%2522%253A%255B%255D%252C%2522D9_52%2522%253A%257B%257D%252C%2522D9_57%2522%253Atrue%252C%2522D9_58%2522%253A%257B%2522DeviceID%2522%253Atrue%257D%252C%2522D9_59%2522%253A%257B%2522UserID%2522%253A%252253588F2D83C0A9%2522%252C%2522TAG%2522%253A%25222%2522%252C%2522AdvID%2522%253A%25221121%2522%252C%2522SiteID%2522%253A%25221638%2522%257D%252C%2522D9_63%2522%253A%2522m.kohls.com%2522%252C%2522D9_64%2522%253A1%252C%2522D9_66%2522%253A%2522%2522%257D",
        BODY_END,
        INLINE_URLS,
            "URL=https://connect.facebook.net/en_US/fbevents.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC025c600150bc46e19ab7207b7f046e39-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://t.co/i/adsct?bci=3&eci=2&event_id=ac1efb71-114c-4c17-8920-5e9522338e03&events=%5B%5B%22pageview%22%2C%7B%7D%5D%5D&integration=advertiser&p_id=Twitter&p_user_id=0&pl_id=e06e48cf-4255-472f-9c71-79ee052613db&tw_document_href=https%3A%2F%2Fm.kohls.com%2F&tw_iframe_status=0&tw_order_quantity=0&tw_sale_amount=0&txn_id=nuubl&type=javascript&version=2.3.27", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://analytics.twitter.com/i/adsct?bci=3&eci=2&event_id=ac1efb71-114c-4c17-8920-5e9522338e03&events=%5B%5B%22pageview%22%2C%7B%7D%5D%5D&integration=advertiser&p_id=Twitter&p_user_id=0&pl_id=e06e48cf-4255-472f-9c71-79ee052613db&tw_document_href=https%3A%2F%2Fm.kohls.com%2F&tw_iframe_status=0&tw_order_quantity=0&tw_sale_amount=0&txn_id=nuubl&type=javascript&version=2.3.27", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://i.flashtalking.com/ft/?aid=1638&uid=D9:Bot&seg=xh0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=flashtalkingad1", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RCee4d5a357164477cbf8bc7a03fb2c6ea-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://connect.facebook.net/signals/config/322052792167924?v=2.9.79&r=stable", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC52a3a89d2834475a96ee97f4c4ee7e9a-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://s.pinimg.com/ct/core.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RCda8cf0bfbca44de282ac118e5640cfd0-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC596e84a98b6d4b959af8e5edb77205d3-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC5aff4df580974580892aa2020496fca0-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RCe54205cedb804f319ae0846419718430-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://analytics.tiktok.com/i18n/pixel/events.js?sdkid=C4N70OPPGM656MIJUMHG&lib=ttq", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("lgc", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("tag");
    ns_web_url ("tag",
        "URL=https://tagtracking.vibescm.com/tag",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:script",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC6bdf645b23a94f23a59cf3730c210195-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://s.pinimg.com/ct/lib/main.55e552f9.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("tag", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("index_6");
    ns_web_url ("index_6",
        "URL=https://ct.pinterest.com/user/?tid=2613489297682&pd=%7B%22em%22%3A%22e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855%22%7D&cb=1662113026981",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://ct.pinterest.com/v3/?tid=2613489297682&pd=%7B%22em%22%3A%22e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855%22%7D&event=init&ad=%7B%22loc%22%3A%22https%3A%2F%2Fm.kohls.com%2F%22%2C%22ref%22%3A%22%22%2C%22if%22%3Afalse%2C%22sh%22%3A489%2C%22sw%22%3A1313%2C%22mh%22%3A%2255e552f9%22%2C%22architecture%22%3A%22x86%22%2C%22model%22%3A%22%22%2C%22platform%22%3A%22Linux%22%2C%22platformVersion%22%3A%22%22%2C%22uaFullVersion%22%3A%2290.0.4430.61%22%2C%22ecm_enabled%22%3Afalse%7D&cb=1662113026984", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://ct.pinterest.com/v3/?tid=2616391205865&pd=%7B%22em%22%3A%22e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855%22%7D&event=init&ad=%7B%22loc%22%3A%22https%3A%2F%2Fm.kohls.com%2F%22%2C%22ref%22%3A%22%22%2C%22if%22%3Afalse%2C%22sh%22%3A489%2C%22sw%22%3A1313%2C%22mh%22%3A%2255e552f9%22%2C%22architecture%22%3A%22x86%22%2C%22model%22%3A%22%22%2C%22platform%22%3A%22Linux%22%2C%22platformVersion%22%3A%22%22%2C%22uaFullVersion%22%3A%2290.0.4430.61%22%2C%22ecm_enabled%22%3Afalse%7D&cb=1662113026984", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("index_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("bf");
    ns_web_url ("bf",
        "URL=https://bf27853irn.bf.dynatrace.com/bf?type=js3&sn=v_4_srv_4_sn_IV7VUE896KFTV6I5O956PBOGGHT16VT8_perc_100000_ol_0_mul_1&svrid=4&flavor=cors&vi=WTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0&modifiedSince=1660767549365&rf=https%3A%2F%2Fm.kohls.com%2F&bp=3&app=06d81dff759102c9&crc=546466287&en=ldtlho0d&end=1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$a=1%7C1%7C_load_%7C_load_%7C-%7C1662113018422%7C1662113021676%7Cdn%7C3883%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%2C2%7C15%7C_event_%7C1662113018422%7C_vc_%7CV%7C3254%5Epc%7CVCD%7C5012%7CVCDS%7C1%7CVCS%7C3554%7CVCO%7C6695%7CVCI%7C0%7CVE%7C0%5Ep0%5Ep41850%5Eps%5Esimg%3Anth-child%283%29%7CS%7C1994%2C2%7C16%7C_event_%7C1662113018422%7C_wv_%7ClcpE%7CIMG%7ClcpSel%7Csection.hp2-creative.gpo-0901__hero%3Epicture%3Afirst-child%3Eimg%3Anth-child%282%29%7ClcpS%7C160425%7ClcpT%7C1495%7ClcpU%7Chttps%3A%2F%2Fmedia.kohlsimg.com%2Fis%2Fimage%2Fkohls%2Fhp-20220901-gpo-10off-d-a%7ClcpLT%7C1399%7Cfcp%7C1006%7Cfp%7C1006%7Ccls%7C0.1055%7Clt%7C1800%2C2%7C2%7Cx%7Cxhr%7Cx%7C1662113020078%7C1662113020598%7Cdn%7C1386%7Cxu%7Chttps%3A%2F%2Fww8.kohls.com%2Fid%3Fd_visid_ver%3D4.3.0%26d_fieldgroup%3DMC%26mcorgid%3DF0EF5E09512D2CD20A490D4D%2540AdobeOrg%26ts%3D1662113020069%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%7Cxrt%7Cb1662113020084e0m207T-1z11I1%7Cxcs%7C488%7Cxce%7C520%2C2%7C3%7Cx%7Cxhr%7Cx%7C1662113020195%7C1662113020602%7Cdn%7C1390%7Cxu%7C%2FlBzjNRQPfM%2FSq6c%2F1G5U4A%2FiXEDf6VLOw%2FYnB5eA%2FSm9pFh0%2FURUY%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%7Cxrt%7Cb1662113020200e0f0g0h0i0j0k15l224m228u1212v18w18T-2z11I1%7Cxcs%7C407%7Cxce%7C407%7Crc%7C201%2C2%7C4%7Cx%7Cxhr%7Cx%7C1662113020406%7C1662113020831%7Cdn%7C3704%7Cxu%7C%2Fapi%2Fbrowse%2Fv2%2Fbrowse%2Fcatalog%2Fcategory%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%7Cxrt%7Cb1662113020412e0f0g0h0i0j0k7l88m199u56244v55102w581549T-3z11I1%7Cxcs%7C420%7Cxce%7C425%2C2%7C5%7Cx%7Cxhr%7Cx%7C1662113020426%7C1662113020619%7Cdn%7C1392%7Cxu%7C%2Fapi%2Fv1%2Fbrowse%2Fmonetization%2Fbanners%3Fchannel%3Dmobile%26pageType%3DhomePage%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%7Cxrt%7Cb1662113020435e0f0g0h0i0j0k7l122m127u1270v97w123T-4z11I1%7Cxcs%7C193%7Cxce%7C193%2C3%7C6%7CCannot%20read%20property%20%270%27%20of%20null%7C_error_%7C-%7C1662113020645%7C1662113020645%7Cdn%7C-1%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%2C4%7C7%7CTypeError%7C_type_%7C-%7C1662113020674%7C1662113020674%7Cdn%7C-1%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%2C4%7C8%7C%3Cwrapper%3ETypeError%3A%20Cannot%20read%20property%20%270%27%20of%20null%5Ep%20%20%20%20at%20a%20%28https%3A%2F%2Fm.kohls.com%2Fdist%2Fheader%2Fheader-client-main-bundle-rev-ddc7b6be8680eb830afb.js%3A1%3A83370%29%5Ep%20%20%20%20at%20https%3A%2F%2Fm.kohls.com%2Fdist%2Fheader%2Fheader-client-main-bundle-rev-ddc7b6be8680eb830afb.js%3A1%3A95119%7C_stack_%7C-%7C1662113020678%7C1662113020678%7Cdn%7C-1%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%2C4%7C9%7C646%7C_ts_%7C-%7C1662113020683%7C1662113020683%7Cdn%7C-1%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%2C4%7C10%7C4%7C_source_%7C-%7C1662113020685%7C1662113020685%7Cdn%7C-1%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%2C2%7C11%7Cx%7Cxhr%7Cx%7C1662113020788%7C1662113021157%7Cdn%7C3728%7Cxu%7C%2FlBzjNRQPfM%2FSq6c%2F1G5U4A%2FiXEDf6VLOw%2FYnB5eA%2FSm9pFh0%2FURUY%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%7Cxrt%7Cb1662113020799e0f0g0h0i0j0k4l149m152u1209v18w18T-5z1I1%7Cxcs%7C369%7Cxce%7C369%7Crc%7C971%7Crm%7CXHR%20Canceled%2C2%7C12%7Cx%7Cxhr%7Cx%7C1662113021185%7C1662113021500%7Cdn%7C3746%7Cxu%7Chttps%3A%2F%2Fc.go-mpulse.net%2Fapi%2Fconfig.json%3Fkey%3D4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T%26d%3Dm.kohls.com%26t%3D5540377%26v%3D1.720.0%26sl%3D0%26si%3D39cce6ea-7e0c-4127-9bc9-0302a86d58aa-rhkum2%26plugins%3DAK%5EcConfigOverride%5EcContinuity%5EcPageParams%5EcIFrameDelay%5EcAutoXHR%5EcSPA%5EcHistory%5EcAngular%5EcBackbone%5EcEmber%5EcRT%5EcCrossDomain%5EcBW%5EcPaintTiming%5EcNavigationTiming%5EcResourceTiming%5EcMemory%5EcCACHE_RELOAD%5EcErrors%5EcTPAnalytics%5EcUserTiming%5EcAkamai%5EcEarly%5EcEventTiming%5EcLOGN%26acao%3D%26ak.ai%3D223330%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%7Cxrt%7Cb1662113021189e0f2g14h14i154j78k157l262m264u176T-6z11I1%7Cxcs%7C315%7Cxce%7C315%7Crc%7C204%7Crm%7CNo%20Content%2C2%7C13%7Cx%7Cxhr%7Cx%7C1662113021345%7C1662113021630%7Cdn%7C3883%7Cxu%7C%2FlBzjNRQPfM%2FSq6c%2F1G5U4A%2FiXEDf6VLOw%2FYnB5eA%2FSm9pFh0%2FURUY%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%7Cxrt%7Cb1662113021363e0f0g0h0i0j0k7l177m180u1188v18w18T-7z1I1%7Cxcs%7C285%7Cxce%7C285%7Crc%7C201%2C2%7C14%7C_onload_%7C_load_%7C-%7C1662113021554%7C1662113021597%7Cdn%7C3746%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%2C1%7C17%7C_event_%7C1662113018422%7C_view_%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0$dO=m.kohls.com,$rId=RID_2418$rpId=$domR=1662113021545$tvn=%2F$tvt=1662113018422$tvm=i1%3Bk0%3Bh0$tvtrg=1$w=790$h=473$sw=1313$sh=489$nt=a0b1662113018422e332f340g351h351i422j384k423l532m719o2204p2205q2207r3124s3132t3175u122454v118756w513062M-576870371V0$ni=4g|10$fd=j3.1.1^sr15.4.2$md=mdcc2%2CMozilla%2F5.0%20(Linux%5Es%20Android%206.0.1%5Es%20Moto%20G%20(4)%20Build%2FMPJ24.139-64)%20AppleWebKit%2F537.36%20(KHTML%5Ec%20like%20Gecko)%20Chrome%2F58.0.3029.81%20Mobile%20Safari%2F537.36%20PTST%2F1%3Bmdcc8%2CN%2FA$url=https%3A%2F%2Fm.kohls.com%2F$title=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More$latC=211$app=06d81dff759102c9$vi=WTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0$fId=113019999_200$v=10247220811100421$vID=1662113020005SBVNC9V48OJOAGOMH3HKO8K0MK5NUIMM$nV=1$nVAT=1$time=1662113027016",
        BODY_END,
        INLINE_URLS,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC43754683fc8b46efa71e08b82f39b606-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://connect.facebook.net/signals/config/1159959614410832?v=2.9.79&r=stable", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/js?id=DC-8632166", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC4f870813c50d463486c70bf1dce9bd4c-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://ct.pinterest.com/ct.html", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/4569325_Brick_Red?wid=300&hei=300&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/3979888_New_White?wid=300&hei=300&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/js?id=DC-11577892&l=dataLayer&cx=c", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/js?id=AW-1071871169&l=dataLayer&cx=c", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/js?id=AW-1018012790&l=dataLayer&cx=c", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/js?id=G-ZLYRBY87M8&l=dataLayer&cx=c", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sc-static.net/scevent.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC9f19545dd2c24e9f87f6d7128a2c7544-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://analytics.tiktok.com/i18n/pixel/identify.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://analytics.tiktok.com/i18n/pixel/config.js?sdkid=C4N70OPPGM656MIJUMHG&hostname=m.kohls.com", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.facebook.com/tr/?id=322052792167924&ev=PageView&dl=https%3A%2F%2Fm.kohls.com%2F&rl=&if=false&ts=1662113027987&sw=1313&sh=489&v=2.9.79&r=stable&ec=0&o=30&fbp=fb.1.1662113027985.1842180297&it=1662113026857&coo=false&rqm=GET", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.facebook.com/tr/?id=1159959614410832&ev=PageView&dl=https%3A%2F%2Fm.kohls.com%2F&rl=&if=false&ts=1662113027990&sw=1313&sh=489&v=2.9.79&r=stable&ec=0&o=30&fbp=fb.1.1662113027985.1842180297&it=1662113026857&coo=false&rqm=GET", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google-analytics.com/analytics.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC35b5823f644e4f6caea6de9fb6e9e198-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("bf", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("init");
    ns_web_url ("init",
        "URL=https://tr.snapchat.com/init?pids=99ed79db-3aec-4b85-9a61-ed80a2993b62",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("init", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("is_enabled");
    ns_web_url ("is_enabled",
        "URL=https://tr.snapchat.com/collector/is_enabled?pids=99ed79db-3aec-4b85-9a61-ed80a2993b62&tld=com",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("is_enabled", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_2");
    ns_web_url ("recording_2",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=2&rst=1662113025036&let=1662113026746&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_2_main_url_1_1662113084957.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://tr.snapchat.com/cm/i?pid=99ed79db-3aec-4b85-9a61-ed80a2993b62&u_scsid=2843c17a-5374-4062-ae1a-c004cf6df31d&u_sclid=e998c793-040c-4281-82d9-b9b291967f52", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("recording_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("init_2");
    ns_web_url ("init_2",
        "URL=https://tr.snapchat.com/init?pids=99ed79db-3aec-4b85-9a61-ed80a2993b62,8e9bd9e0-8284-4dbd-ab20-145759728098",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("init_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("is_enabled_2");
    ns_web_url ("is_enabled_2",
        "URL=https://tr.snapchat.com/collector/is_enabled?pids=99ed79db-3aec-4b85-9a61-ed80a2993b62,8e9bd9e0-8284-4dbd-ab20-145759728098&tld=com",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.googleadservices.com/pagead/conversion_async.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sc-static.net/scevent.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=X-AB", END_INLINE,
            "URL=https://tr.snapchat.com/cm/s?bt=1d53c387&pnid=140&cb=1662113028303&u_scsid=8845e0e4-b5c7-4191-9f11-b2779e514411&u_sclid=c82c6477-91e6-46d6-9fe6-49bbc580499c", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=sc_at", END_INLINE
    );

    ns_end_transaction("is_enabled_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect");
    ns_web_url ("collect",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-ZLYRBY87M8&gtm=2oe8v0&_p=2017578148&_gaz=1&cid=1350227648.1662113023&ul=en-us&sr=1313x489&_z=ccd.v9B&_s=1&sid=1662113028&sct=1&seg=0&dl=https%3A%2F%2Fm.kohls.com%2F&dt=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&en=page_view&_fv=1&_ss=1&_ee=1&ep.debug_mode=true",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("collect", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_2");
    ns_web_url ("collect_2",
        "URL=https://stats.g.doubleclick.net/g/collect?v=2&tid=G-ZLYRBY87M8&cid=1350227648.1662113023&gtm=2oe8v0&aip=1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=IDE",
        INLINE_URLS,
            "URL=https://pixel.tapad.com/idsync/ex/push?partner_id=2884&partner_url=https%3A%2F%2Ftr.snapchat.com%2Fcm%2Fp%3Frand%3D1661586283726%26pnid%3D140%26pcid%3D%24%7BTA_DEVICE_ID%7D", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collect_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_3");
    ns_web_url ("recording_3",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=3&rst=1662113025036&let=1662113027357&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_3_main_url_1_1662113084962.body",
        BODY_END
    );

    ns_end_transaction("recording_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_4");
    ns_web_url ("recording_4",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=4&rst=1662113025036&let=1662113027357&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_4_main_url_1_1662113084968.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://tr.snapchat.com/p", "METHOD=POST", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Origin:https://m.kohls.com", "HEADER=Content-Type:application/x-www-form-urlencoded", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=sc_at",
            BODY_BEGIN,
            "trackId=62e3e686-65a5-4486-ad6e-c14e421e588a&pid=99ed79db-3aec-4b85-9a61-ed80a2993b62&ev=PAGE_VIEW&pl=https%3A%2F%2Fm.kohls.com%2F&ts=1662113028186&v=1.6.0&bt=1d53c387&m_sl=9631&m_rd=9765&m_pi=2204.47499997681&m_dcl=2207.234999979846&m_fcps=1005.5899999570101&m_pl=3175.089999975171&m_pv=v2&u_c1=0f36c1ec-e4f6-4791-920c-f1ee231a89b1&u_scsid=2843c17a-5374-4062-ae1a-c004cf6df31d&u_sclid=e998c793-040c-4281-82d9-b9b291967f52",
        BODY_END,
             END_INLINE
    );

    ns_end_transaction("recording_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_5");
    ns_web_url ("recording_5",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=5&rst=1662113025036&let=1662113027357&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_5_main_url_1_1662113084977.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://pixel.tapad.com/idsync/ex/push/check?partner_id=2884&partner_url=https%3A%2F%2Ftr.snapchat.com%2Fcm%2Fp%3Frand%3D1661586283726%26pnid%3D140%26pcid%3D%24%7BTA_DEVICE_ID%7D", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=TapAd_TS;TapAd_DID", END_INLINE,
            "URL=https://11577892.fls.doubleclick.net/activityi;src=11577892;type=pagev0;cat=kmnun0;ord=2858078153920;gtm=2od8v0;auiddc=666653719.1662113028;u1=not%20set;u10=https%3A%2F%2Fm.kohls.com%2F;u11=homepage;u13=not%20set;u2=not%20set;u23=Current%20Customer;u24=78611155637513935743550952407821114794;u25=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fm.kohls.com%2F?", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC82863ebc3d2449b8bac7f33870252455-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("recording_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("pixel");
    ns_web_url ("pixel",
        "URL=https://analytics.tiktok.com/api/v2/pixel",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=_ttp",
        BODY_BEGIN,
            "{"event":"Pageview","message_id":"messageId-1662113028527-2578825338614-C4N70OPPGM656MIJUMHG","event_id":"","is_onsite":false,"timestamp":"2022-09-02T10:03:48.527Z","context":{"ad":{"sdk_env":"external","jsb_status":2},"user":{"anonymous_id":"3d0b5d48-438d-404c-aa82-4d1190d7769c"},"pixel":{"code":"C4N70OPPGM656MIJUMHG"},"page":{"url":"https://m.kohls.com/","referrer":""},"library":{"name":"pixel.js","version":"2.1.33"},"device":{"platform":"android"},"session_id":"sessionId-1662113027961-7884481730050-C4N70OPPGM656MIJUMHG","pageview_id":"pageId-1662113027969-1817368670225-C4N70OPPGM656MIJUMHG","variation_id":"test_2","userAgent":"Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1"},"properties":{"0":"t","1":"t","2":"q"}}",
        BODY_END
    );

    ns_end_transaction("pixel", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("p");
    ns_web_url ("p",
        "URL=https://tr.snapchat.com/p",
        "METHOD=POST",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-Dest:iframe",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=sc_at",
        BODY_BEGIN,
            "trackId=23a0a70c-3ffd-4f03-b920-c75c1909ba9b&pid=8e9bd9e0-8284-4dbd-ab20-145759728098&ev=PAGE_VIEW&pl=https%3A%2F%2Fm.kohls.com%2F&ts=1662113028213&v=1.6.0&bt=1d53c387&m_sl=9631&m_rd=9791&m_pi=2204.47499997681&m_dcl=2207.234999979846&m_fcps=1005.5899999570101&m_pl=3175.089999975171&m_pv=v2&u_c1=0f36c1ec-e4f6-4791-920c-f1ee231a89b1&u_scsid=2843c17a-5374-4062-ae1a-c004cf6df31d&u_sclid=e998c793-040c-4281-82d9-b9b291967f52",
        BODY_END,
        INLINE_URLS,
            "URL=https://tr.snapchat.com/cm/p?rand=1661586283726&pnid=140&pcid=a0b32743-3d5d-4ac4-bc07-a8c9cc98d672", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=sc_at", END_INLINE
    );

    ns_end_transaction("p", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_3");
    ns_web_url ("collect_3",
        "URL=https://stats.g.doubleclick.net/j/collect?t=dc&aip=1&_r=3&v=1&_v=j96&tid=UA-45121696-1&cid=1350227648.1662113023&jid=2000870329&gjid=1468300800&_gid=1529326656.1662113029&_u=YCDAgAABAAAAAE~&z=1124176583",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=IDE",
        INLINE_URLS,
            "URL=https://www.google-analytics.com/collect?v=1&_v=j96&a=2017578148&t=pageview&_s=1&dl=https%3A%2F%2Fm.kohls.com%2F&ul=en-us&de=UTF-8&dt=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&sd=24-bit&sr=1313x489&vp=1298x361&je=0&_u=YCDAgAAB~&jid=2000870329&gjid=1468300800&cid=1350227648.1662113023&tid=UA-45121696-1&_gid=1529326656.1662113029&cd2=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&cd4=&z=1153416099", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google-analytics.com/collect?v=1&_v=j96&a=2017578148&t=event&ni=1&_s=2&dl=https%3A%2F%2Fm.kohls.com%2F&ul=en-us&de=UTF-8&dt=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&sd=24-bit&sr=1313x489&vp=1298x361&je=0&ec=DY%20Smart%20Action&ea=%5Bdy%20test%5D%20aa%20vs%20dy%20m.com&el=Experience%201%20(Variation%201)&_u=YCDAgAABAAAAAE~&jid=&gjid=&cid=1350227648.1662113023&tid=UA-45121696-1&_gid=1529326656.1662113029&cd2=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&cd4=&z=798399323", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google-analytics.com/collect?v=1&_v=j96&a=2017578148&t=event&ni=1&_s=3&dl=https%3A%2F%2Fm.kohls.com%2F&ul=en-us&de=UTF-8&dt=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&sd=24-bit&sr=1313x489&vp=1298x361&je=0&ec=DY%20Smart%20Action&ea=%5BDY%20Test%5D%20-%20Home%20Store%20ID&el=Experience%201%20(Variation%201)&_u=YCDAgAABAAAAAE~&jid=&gjid=&cid=1350227648.1662113023&tid=UA-45121696-1&_gid=1529326656.1662113029&cd2=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&cd4=&z=949024666", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google-analytics.com/collect?v=1&_v=j96&a=2017578148&t=event&ni=1&_s=4&dl=https%3A%2F%2Fm.kohls.com%2F&ul=en-us&de=UTF-8&dt=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&sd=24-bit&sr=1313x489&vp=1298x361&je=0&ec=DY%20Smart%20Action&ea=%5BCS-21414%5D%20Sign-in%2FSign-up%20events%20updated&el=Experience%201%20(Variation%201)&_u=YCDAgAABAAAAAE~&jid=&gjid=&cid=1350227648.1662113023&tid=UA-45121696-1&_gid=1529326656.1662113029&cd2=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&cd4=&z=1781255610", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google-analytics.com/collect?v=1&_v=j96&a=2017578148&t=event&ni=1&_s=5&dl=https%3A%2F%2Fm.kohls.com%2F&ul=en-us&de=UTF-8&dt=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&sd=24-bit&sr=1313x489&vp=1298x361&je=0&ec=DY%20Smart%20Action&ea=%5BDY%20TEST%5D%20Listen%20to%20A2C&el=Experience%201%20(Variation%201)&_u=YCDAgAABAAAAAE~&jid=&gjid=&cid=1350227648.1662113023&tid=UA-45121696-1&_gid=1529326656.1662113029&cd2=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&cd4=&z=1104361146", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google-analytics.com/collect?v=1&_v=j96&a=2017578148&t=event&ni=1&_s=6&dl=https%3A%2F%2Fm.kohls.com%2F&ul=en-us&de=UTF-8&dt=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&sd=24-bit&sr=1313x489&vp=1298x361&je=0&ec=DY%20Smart%20Action&ea=%5BDY%20TEST%5D%20User%20Type&el=New%20Users%20(Variation%201)&_u=YCDAgAABAAAAAE~&jid=&gjid=&cid=1350227648.1662113023&tid=UA-45121696-1&_gid=1529326656.1662113029&cd2=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&cd4=&z=1123751013", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google-analytics.com/collect?v=1&_v=j96&a=2017578148&t=event&ni=1&_s=7&dl=https%3A%2F%2Fm.kohls.com%2F&ul=en-us&de=UTF-8&dt=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&sd=24-bit&sr=1313x489&vp=1298x361&je=0&ec=DY%20Smart%20Action&ea=%5BDY%20TEST%5D%20-%20Identify%20Users&el=Identify%20Users%20(Variation%201)&_u=YCDAgAABAAAAAE~&jid=&gjid=&cid=1350227648.1662113023&tid=UA-45121696-1&_gid=1529326656.1662113029&cd2=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&cd4=&z=1735245608", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google-analytics.com/collect?v=1&_v=j96&a=2017578148&t=event&ni=1&_s=8&dl=https%3A%2F%2Fm.kohls.com%2F&ul=en-us&de=UTF-8&dt=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&sd=24-bit&sr=1313x489&vp=1298x361&je=0&ec=DY%20Smart%20Object&ea=PZ22075_CAT_HP%20DY%20Deep%20Learning%20Recs%20-%20Mobile&el=Deep%20Learning%20M.com%20(Test)&_u=YCDAgAABAAAAAE~&jid=&gjid=&cid=1350227648.1662113023&tid=UA-45121696-1&_gid=1529326656.1662113029&cd2=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&cd4=&z=311370583", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collect_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_6");
    ns_web_url ("recording_6",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=6&rst=1662113025036&let=1662113027357&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_6_main_url_1_1662113084985.body",
        BODY_END
    );

    ns_end_transaction("recording_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_7");
    ns_web_url ("recording_7",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=7&rst=1662113025036&let=1662113027357&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_7_main_url_1_1662113084988.body",
        BODY_END
    );

    ns_end_transaction("recording_7", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_8");
    ns_web_url ("recording_8",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=8&rst=1662113025036&let=1662113027646&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_8_main_url_1_1662113084989.body",
        BODY_END
    );

    ns_end_transaction("recording_8", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_9");
    ns_web_url ("recording_9",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=9&rst=1662113025036&let=1662113027646&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_9_main_url_1_1662113084997.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.google-analytics.com/collect?v=1&_v=j96&a=2017578148&t=timing&_s=9&dl=https%3A%2F%2Fm.kohls.com%2F&ul=en-us&de=UTF-8&dt=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&sd=24-bit&sr=1313x489&vp=1298x361&je=0&plt=3132&pdt=186&dns=12&rrt=332&srt=110&tcp=71&dit=2205&clt=2206&_gst=9612&_gbt=10110&_cst=9428&_cbt=9827&_u=YCDAgAABAAAAAE~&jid=&gjid=&cid=1350227648.1662113023&tid=UA-45121696-1&_gid=1529326656.1662113029&cd2=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&cd4=&z=1531216868", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?random=1662113028640&cv=9&fst=1662113028640&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=376635471&u_h=489&u_w=1313&u_ah=489&u_aw=1313&u_cd=24&u_his=2&u_tz=-300&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa8v0&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fm.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&auid=666653719.1662113028&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://adservice.google.com/ddm/fls/z/src=11577892;type=pagev0;cat=kmnun0;ord=2858078153920;gtm=2od8v0;auiddc=*;u1=not%20set;u10=https%3A%2F%2Fm.kohls.com%2F;u11=homepage;u13=not%20set;u2=not%20set;u23=Current%20Customer;u24=78611155637513935743550952407821114794;u25=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fm.kohls.com%2F", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://bid.g.doubleclick.net/xbbe/pixel?d=KAE", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?random=1662113028687&cv=9&fst=1662113028687&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=376635471&u_h=489&u_w=1313&u_ah=489&u_aw=1313&u_cd=24&u_his=2&u_tz=-300&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa8v0&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fm.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&auid=666653719.1662113028&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://www.google.com/ads/ga-audiences?t=sr&aip=1&_r=4&slf_rd=1&v=1&_v=j96&tid=UA-45121696-1&cid=1350227648.1662113023&jid=2000870329&_u=YCDAgAABAAAAAE~&z=938017862", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("recording_9", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_10");
    ns_web_url ("recording_10",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=10&rst=1662113025036&let=1662113027646&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_10_main_url_1_1662113085005.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://8632166.fls.doubleclick.net/activityi;src=8632166;type=landi0;cat=unive0;ord=463515166064;gtm=2od8v0;auiddc=666653719.1662113028;u1=not%20set;u10=https%3A%2F%2Fm.kohls.com%2F;u11=homepage;u13=not%20set;u2=not%20set;u23=Current%20Customer;u24=78611155637513935743550952407821114794;u25=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fm.kohls.com%2F?", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC3e1b7009ef914bda83baf865955e4515-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("recording_10", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_11");
    ns_web_url ("recording_11",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=11&rst=1662113025036&let=1662113027646&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_11_main_url_1_1662113085012.body",
        BODY_END
    );

    ns_end_transaction("recording_11", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_12");
    ns_web_url ("recording_12",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=12&rst=1662113025036&let=1662113027646&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_12_main_url_1_1662113085014.body",
        BODY_END
    );

    ns_end_transaction("recording_12", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_13");
    ns_web_url ("recording_13",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=13&rst=1662113025036&let=1662113027869&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_13_main_url_1_1662113085016.body",
        BODY_END
    );

    ns_end_transaction("recording_13", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_14");
    ns_web_url ("recording_14",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=14&rst=1662113025036&let=1662113028224&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_14_main_url_1_1662113085017.body",
        BODY_END
    );

    ns_end_transaction("recording_14", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_15");
    ns_web_url ("recording_15",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=15&rst=1662113025036&let=1662113028617&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_15_main_url_1_1662113085019.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.google.com/pagead/1p-user-list/1018012790/?random=1662113028640&cv=9&fst=1662112800000&num=1&bg=ffffff&guid=ON&eid=376635471&u_h=489&u_w=1313&u_ah=489&u_aw=1313&u_cd=24&u_his=2&u_tz=-300&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa8v0&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fm.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&async=1&fmt=3&is_vtc=1&random=3110834150&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://js.cnnx.link/roi/cnxtag-min.js?id=31851", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1071871169/?random=1662113028687&cv=9&fst=1662112800000&num=1&bg=ffffff&guid=ON&eid=376635471&u_h=489&u_w=1313&u_ah=489&u_aw=1313&u_cd=24&u_his=2&u_tz=-300&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa8v0&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fm.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&async=1&fmt=3&is_vtc=1&random=3744672761&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cdn.taboola.com/libtrc/unip/1456663/tfa.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://adservice.google.com/ddm/fls/z/src=8632166;type=landi0;cat=unive0;ord=463515166064;gtm=2od8v0;auiddc=*;u1=not%20set;u10=https%3A%2F%2Fm.kohls.com%2F;u11=homepage;u13=not%20set;u2=not%20set;u23=Current%20Customer;u24=78611155637513935743550952407821114794;u25=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fm.kohls.com%2F", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://trc.taboola.com/1456663/trc/3/json?tim=1662113028913&data=%7B%22id%22%3A963%2C%22ii%22%3A%22%2F%22%2C%22it%22%3A%22video%22%2C%22sd%22%3Anull%2C%22ui%22%3Anull%2C%22vi%22%3A1662113028895%2C%22cv%22%3A%2220220828-2-RELEASE%22%2C%22uiv%22%3A%22default%22%2C%22u%22%3A%22https%3A%2F%2Fwww.kohls.com%2F%22%2C%22e%22%3Anull%2C%22cb%22%3A%22TFASC.trkCallback%22%2C%22qs%22%3A%22%22%2C%22r%22%3A%5B%7B%22li%22%3A%22rbox-tracking%22%2C%22s%22%3A0%2C%22uim%22%3A%22rbox-tracking%3Apub%3Dkohlsnew-sccnx%3Aabp%3D0%22%2C%22uip%22%3A%22rbox-tracking%22%2C%22orig_uip%22%3A%22rbox-tracking%22%7D%5D%2C%22mpv%22%3Atrue%2C%22mpvd%22%3A%7B%22en%22%3A%22page_view%22%2C%22item-url%22%3A%22https%3A%2F%2Fm.kohls.com%2F%22%2C%22tim%22%3A1662113028911%2C%22ref%22%3Anull%2C%22tos%22%3A7%2C%22ssd%22%3A1%2C%22scd%22%3A2%7D%7D&pubit=i", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://trc.taboola.com/1456663/log/3/unip?en=page_view&item-url=https%3A%2F%2Fm.kohls.com%2F&tim=1662113028911&ref=null&cv=20220828-2-RELEASE&tos=112&ssd=1&scd=2&vi=1662113028895&ri=791105237b55af48d85fbf85a7a7a628&sd=v2_7e4ffdfaaca3d751ced3beb96cf5c12f_4e6d2ce6-a080-452e-9455-33841bab4609-tucta0b5a84_1662113028_1662113028_CNawjgYQl_RYGJ-2oO2vMCABKAEw4QE4kaQOQNWmD0juy9kDUIIEWABgAGiXjKXetYHfjKIBcAE&ui=4e6d2ce6-a080-452e-9455-33841bab4609-tucta0b5a84", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=t_gid", END_INLINE
    );

    ns_end_transaction("recording_15", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("bf_2");
    ns_web_url ("bf_2",
        "URL=https://bf27853irn.bf.dynatrace.com/bf?type=js3&sn=v_4_srv_4_sn_IV7VUE896KFTV6I5O956PBOGGHT16VT8_app-3A06d81dff759102c9_1_ol_0_perc_100000_mul_1&svrid=4&flavor=cors&vi=WTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0&modifiedSince=1662094650929&rf=https%3A%2F%2Fm.kohls.com%2F&bp=3&app=06d81dff759102c9&crc=2694031398&en=ldtlho0d&end=1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/bf_2_url_0_1_1662113085020.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.facebook.com/tr/?id=322052792167924&ev=Microdata&dl=https%3A%2F%2Fm.kohls.com%2F&rl=&if=false&ts=1662113029611&cd[DataLayer]=%5B%5D&cd[Meta]=%7B%22title%22%3A%22Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More%5Cn%20%5Cn%22%2C%22meta%3Adescription%22%3A%22Enjoy%20free%20shipping%20and%20easy%20returns%20every%20day%20at%20Kohl%27s!%20Find%20great%20savings%20on%20clothing%2C%20shoes%2C%20toys%2C%20home%20d%C3%A9cor%2C%20appliances%20and%20electronics%20for%20the%20whole%20family.%5Cn%20%22%2C%22meta%3Akeywords%22%3A%22%22%7D&cd[OpenGraph]=%7B%7D&cd[Schema.org]=%5B%5D&cd[JSON-LD]=%5B%5D&sw=1313&sh=489&v=2.9.79&r=stable&ec=1&o=30&fbp=fb.1.1662113027985.1842180297&it=1662113026857&coo=false&es=automatic&tm=3&rqm=GET", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.facebook.com/tr/?id=1159959614410832&ev=Microdata&dl=https%3A%2F%2Fm.kohls.com%2F&rl=&if=false&ts=1662113029615&cd[DataLayer]=%5B%5D&cd[Meta]=%7B%22title%22%3A%22Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More%5Cn%20%5Cn%22%2C%22meta%3Adescription%22%3A%22Enjoy%20free%20shipping%20and%20easy%20returns%20every%20day%20at%20Kohl%27s!%20Find%20great%20savings%20on%20clothing%2C%20shoes%2C%20toys%2C%20home%20d%C3%A9cor%2C%20appliances%20and%20electronics%20for%20the%20whole%20family.%5Cn%20%22%2C%22meta%3Akeywords%22%3A%22%22%7D&cd[OpenGraph]=%7B%7D&cd[Schema.org]=%5B%5D&cd[JSON-LD]=%5B%5D&sw=1313&sh=489&v=2.9.79&r=stable&ec=1&o=30&fbp=fb.1.1662113027985.1842180297&it=1662113026857&coo=false&es=automatic&tm=3&rqm=GET", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://trc-events.taboola.com/1456663/log/3/unip?en=pre_d_eng_tb&tos=1558&scd=2&ssd=1&est=1662113028902&ver=35&isls=true&src=i&invt=1500&rv=1&tim=1662113030463&vi=1662113028895&ri=791105237b55af48d85fbf85a7a7a628&sd=v2_7e4ffdfaaca3d751ced3beb96cf5c12f_4e6d2ce6-a080-452e-9455-33841bab4609-tucta0b5a84_1662113028_1662113028_CNawjgYQl_RYGJ-2oO2vMCABKAEw4QE4kaQOQNWmD0juy9kDUIIEWABgAGiXjKXetYHfjKIBcAE&ui=4e6d2ce6-a080-452e-9455-33841bab4609-tucta0b5a84&ref=null&cv=20220828-2-RELEASE&item-url=https%3A%2F%2Fm.kohls.com%2F", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=t_gid", END_INLINE,
            "URL=https://trc-events.taboola.com/1456663/log/3/unip?en=pre_d_eng_tb&tos=4561&scd=2&ssd=1&est=1662113028902&ver=35&isls=true&src=i&invt=3000&rv=1&tim=1662113033465&vi=1662113028895&ri=791105237b55af48d85fbf85a7a7a628&sd=v2_7e4ffdfaaca3d751ced3beb96cf5c12f_4e6d2ce6-a080-452e-9455-33841bab4609-tucta0b5a84_1662113028_1662113028_CNawjgYQl_RYGJ-2oO2vMCABKAEw4QE4kaQOQNWmD0juy9kDUIIEWABgAGiXjKXetYHfjKIBcAE&ui=4e6d2ce6-a080-452e-9455-33841bab4609-tucta0b5a84&ref=null&cv=20220828-2-RELEASE&item-url=https%3A%2F%2Fm.kohls.com%2F", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=t_gid", END_INLINE,
            "URL=https://trc-events.taboola.com/1456663/log/3/unip?en=pre_d_eng_tb&tos=10564&scd=2&ssd=1&est=1662113028902&ver=35&isls=true&src=i&invt=6000&rv=1&tim=1662113039469&vi=1662113028895&ri=791105237b55af48d85fbf85a7a7a628&sd=v2_7e4ffdfaaca3d751ced3beb96cf5c12f_4e6d2ce6-a080-452e-9455-33841bab4609-tucta0b5a84_1662113028_1662113028_CNawjgYQl_RYGJ-2oO2vMCABKAEw4QE4kaQOQNWmD0juy9kDUIIEWABgAGiXjKXetYHfjKIBcAE&ui=4e6d2ce6-a080-452e-9455-33841bab4609-tucta0b5a84&ref=null&cv=20220828-2-RELEASE&item-url=https%3A%2F%2Fm.kohls.com%2F", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=t_gid", END_INLINE,
            "URL=https://trc-events.taboola.com/1456663/log/3/unip?en=pre_d_eng_tb&tos=22568&scd=2&ssd=1&est=1662113028902&ver=35&isls=true&src=i&invt=12000&rv=1&tim=1662113051473&vi=1662113028895&ri=791105237b55af48d85fbf85a7a7a628&sd=v2_7e4ffdfaaca3d751ced3beb96cf5c12f_4e6d2ce6-a080-452e-9455-33841bab4609-tucta0b5a84_1662113028_1662113028_CNawjgYQl_RYGJ-2oO2vMCABKAEw4QE4kaQOQNWmD0juy9kDUIIEWABgAGiXjKXetYHfjKIBcAE&ui=4e6d2ce6-a080-452e-9455-33841bab4609-tucta0b5a84&ref=null&cv=20220828-2-RELEASE&item-url=https%3A%2F%2Fm.kohls.com%2F", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=t_gid", END_INLINE
    );

    ns_end_transaction("bf_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_16");
    ns_web_url ("recording_16",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=16&rst=1662113025036&let=1662113061569&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_16_main_url_1_1662113085022.body",
        BODY_END
    );

    ns_end_transaction("recording_16", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_17");
    ns_web_url ("recording_17",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=17&rst=1662113025036&let=1662113061912&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_17_main_url_1_1662113085100.body",
        BODY_END
    );

    ns_end_transaction("recording_17", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_18");
    ns_web_url ("recording_18",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=18&rst=1662113025036&let=1662113063715&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_18_main_url_1_1662113085103.body",
        BODY_END
    );

    ns_end_transaction("recording_18", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("URUY_4");
    ns_web_url ("URUY_4",
        "URL=https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=x-dtpc:4$113019999_200h18vWTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0e0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtLatC;dtSa;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_prodMerch;m_deviceID;_abck;gpv_v9;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_ga_ZLYRBY87M8;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;rxvt;dtPC",
        BODY_BEGIN,
            "{"sensor_data":"7a74G7m23Vrp0o5c9368031.75-1,2,-94,-100,Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1,uaend,12147,20030107,en-US,Gecko,0,0,0,0,408958,3020176,1313,489,1313,489,1313,361,1313,,cpen:0,i1:0,dm:0,cwen:0,non:1,opc:0,fc:0,sc:0,wrc:1,isc:0,vib:1,bat:1,x11:0,x12:1,11023,0.277657833138,831056510087.5,0,loc:-1,2,-94,-131,-1,2,-94,-101,do_en,dm_en,t_en-1,2,-94,-105,0,0,0,0,1708,-1,0;-1,2,-94,-102,0,0,0,0,1708,-1,0;-1,2,-94,-108,-1,2,-94,-110,0,1,41060,565,339;1,1,41489,716,230;2,1,41521,835,186;3,1,41562,900,175;4,1,41564,906,174;5,1,41591,909,174;6,1,41622,910,174;7,1,41866,810,91;8,1,41881,781,64;9,1,42556,396,25;10,1,42568,384,44;11,1,42579,373,65;12,1,42598,361,94;13,1,42613,356,111;14,1,42629,349,136;15,1,42662,347,147;16,1,42680,347,150;17,1,42971,336,150;18,1,42976,273,148;19,1,42996,232,146;20,1,43007,216,146;21,1,43024,196,149;22,1,43040,178,152;23,1,43061,166,153;24,1,43090,152,153;25,1,43107,141,153;26,1,43124,132,153;27,1,43141,127,153;28,1,43157,121,153;29,1,43174,119,153;30,1,43191,117,153;31,1,43208,113,153;32,1,43225,110,151;33,1,43241,107,150;34,1,43257,101,147;35,1,43274,95,144;36,1,43292,91,141;37,1,43307,85,138;38,1,43324,81,136;39,1,43341,74,133;40,1,43358,70,132;41,1,43374,68,131;42,1,43390,66,130;43,1,43409,64,129;44,1,43433,62,129;45,1,43441,61,128;46,1,43457,58,128;47,1,43473,57,128;48,1,43490,54,128;49,1,43507,52,128;50,1,43525,50,128;51,1,43543,48,129;52,1,43557,46,129;53,1,43573,44,129;54,1,43591,43,129;55,1,43607,41,129;56,1,43623,39,129;57,1,43641,38,129;58,1,43658,36,129;59,1,43673,35,129;60,1,43691,34,129;61,1,43728,31,129;62,1,43744,30,128;63,1,43777,29,128;64,1,43797,28,127;65,1,43841,27,127;66,1,43862,26,126;67,1,43897,25,125;68,1,43912,24,125;69,1,43949,24,124;70,1,43999,24,123;71,3,44194,24,123,-1;-1,2,-94,-117,-1,2,-94,-111,-1,2,-94,-109,-1,2,-94,-114,-1,2,-94,-103,-1,2,-94,-112,https://m.kohls.com/-1,2,-94,-115,1,3131792,32,0,0,0,3131760,44195,0,1662113020175,39,17780,0,72,2963,1,0,44199,3104283,0,B6A6969A0C7F79D7A3819FC0D8E40EDA~-1~YAAQEzPdF6utUPqCAQAAagGo/QhENzGwVpPyDyYBuII3vHk10FCIhcMGO6v62JQ+8UUWnnty5iAFTM/y+sZ7+8lbkBb96Z0e2ibZdG+oc9hhZdxyN/M12GWvrLak7OcTk8QTBtqTqqRjK4GwDdqCfMZMHSyPfDFs3zFXemBkJrlTDpkIuF9LQHJo1BXx7jcGQbdE+v2EMwp6+GeQ34CJamN+MqPsPq39F+7GLR8iGKXGc+Xr57Nbdd/dtkpWkOjz70kzJY/ebXtLEJwSUGiPcVO3LUzaLad0PRrq0bhUUtozSoR8gXq/hKPbXhJKI3nBPHpy3luKLfFTeO3F7G9V7Qd/2OFPLMClkWJJ0LFB3eQoXzka/+FmtjuH7q4lwbk/yyiau2Eew0nkJ8/VpNRZxRuCGd2qYIqeguQbtpr0Uw6xBgCCmxJ04h4MztDAIJ1t1YQzzxHTYbm1oSHYbBlYpnsWnYgVY3CP/sSZvmOlLK2UEED/MkI/PEWITMn2mNeFkP35CxlXGsfjRsATrjvSqbfN07KbSnczXD6b~-1~||-1||~-1,50099,939,-206599699,30261689,PiZtE,108528,25,0,-1-1,2,-94,-106,1,3-1,2,-94,-119,-1-1,2,-94,-122,0,0,0,0,1,0,0-1,2,-94,-123,-1,2,-94,-124,-1,2,-94,-126,-1,2,-94,-127,00300044040300043020-1,2,-94,-70,-1127778619;-563023189;dis;;true;true;true;300;true;24;24;true;false;-1-1,2,-94,-80,5383-1,2,-94,-116,9060480-1,2,-94,-118,169388-1,2,-94,-129,98f7979f11c93279c5ea09beec21055d155a5331e783f9bfe9e4a1a482531594,1,0,Google Inc.,Google SwiftShader,3c84d65d49fb3b8d03ed2f26bf53630900f726ac7aa573f0a715315b02e21805,27-1,2,-94,-121,;6;25;0"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.facebook.com/tr/?id=322052792167924&ev=SubscribedButtonClick&dl=https%3A%2F%2Fm.kohls.com%2F&rl=&if=false&ts=1662113064601&cd[buttonFeatures]=%7B%22classList%22%3A%22menu-icon%22%2C%22destination%22%3A%22%22%2C%22id%22%3A%22%22%2C%22imageUrl%22%3A%22url(%5C%22https%3A%2F%2Fm.kohls.com%2Fdist%2Fimages%2Fnewheader-rev-7f7fbb.png%5C%22)%22%2C%22innerText%22%3A%22%22%2C%22numChildButtons%22%3A0%2C%22tag%22%3A%22div%22%2C%22type%22%3Anull%7D&cd[buttonText]=&cd[formFeatures]=%5B%5D&cd[pageFeatures]=%7B%22title%22%3A%22Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More%5Cn%20%5Cn%22%7D&cd[parameters]=%5B%5D&sw=1313&sh=489&v=2.9.79&r=stable&ec=2&o=30&fbp=fb.1.1662113027985.1842180297&it=1662113026857&coo=false&es=automatic&tm=3&rqm=GET", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("URUY_4", NS_AUTO_STATUS);
    ns_page_think_time(0.018);

    //Page Auto split for 
    ns_start_transaction("index_7");
    ns_web_url ("index_7",
        "URL=https://www.facebook.com/tr/?id=1159959614410832&ev=SubscribedButtonClick&dl=https%3A%2F%2Fm.kohls.com%2F&rl=&if=false&ts=1662113064606&cd[buttonFeatures]=%7B%22classList%22%3A%22menu-icon%22%2C%22destination%22%3A%22%22%2C%22id%22%3A%22%22%2C%22imageUrl%22%3A%22url(%5C%22https%3A%2F%2Fm.kohls.com%2Fdist%2Fimages%2Fnewheader-rev-7f7fbb.png%5C%22)%22%2C%22innerText%22%3A%22%22%2C%22numChildButtons%22%3A0%2C%22tag%22%3A%22div%22%2C%22type%22%3Anull%7D&cd[buttonText]=&cd[formFeatures]=%5B%5D&cd[pageFeatures]=%7B%22title%22%3A%22Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More%5Cn%20%5Cn%22%7D&cd[parameters]=%5B%5D&sw=1313&sh=489&v=2.9.79&r=stable&ec=2&o=30&fbp=fb.1.1662113027985.1842180297&it=1662113026857&coo=false&es=automatic&tm=3&rqm=GET",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://m.kohls.com/dist/images/pilot-menu-sprite-rev-96fa3a.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtSa;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_prodMerch;m_deviceID;gpv_v9;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_ga_ZLYRBY87M8;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;rxvt;akaalb_m_kohls_com;_abck;dtLatC;dtPC;s_sq", END_INLINE,
            "URL=https://m.kohls.com/dist/images/hb-new-sprite1-rev-1f5489.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtSa;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_prodMerch;m_deviceID;gpv_v9;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_ga_ZLYRBY87M8;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;rxvt;akaalb_m_kohls_com;_abck;dtLatC;dtPC;s_sq", END_INLINE,
            "URL=https://m.kohls.com/dist/images/SephoraLogo-rev-51071c.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtSa;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_prodMerch;m_deviceID;gpv_v9;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_ga_ZLYRBY87M8;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;rxvt;akaalb_m_kohls_com;_abck;dtLatC;dtPC;s_sq", END_INLINE
    );

    ns_end_transaction("index_7", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("category_2");
    ns_web_url ("category_2",
        "URL=https://m.kohls.com/api/browse/v2/browse/catalog/category",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=x-correlation-id:MCOM-46f4352e4e7e888ac93c8b7b-b713fc26a3aca27d-1662113064694",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=x-channel:MCOM",
        "HEADER=x-dtpc:4$113019999_200h19vWTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0e0",
        "HEADER=x-pageid:MCOM-homepage",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtSa;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_prodMerch;m_deviceID;gpv_v9;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_ga_ZLYRBY87M8;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;akaalb_m_kohls_com;_abck;dtLatC;s_sq;rxvt;dtPC"
    );

    ns_end_transaction("category_2", NS_AUTO_STATUS);

    //Page Auto split for Method = PUT
    ns_start_transaction("index_8");
    ns_web_url ("index_8",
        "URL=https://m.kohls.com/api/walletapi/wallet/v1/items/lookup/?includeGPOs=true",
        "METHOD=PUT",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=x-dtpc:4$113019999_200h20vWTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0e0",
        "HEADER=X-Requested-With:XMLHttpRequest",
        "HEADER=x-pageid:MCOM-homepage",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;bm_sz;check;akacd_RWASP-default-phased-release;rxVisitor;dtSa;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_prodMerch;m_deviceID;gpv_v9;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_ga_ZLYRBY87M8;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;akaalb_m_kohls_com;_abck;dtLatC;s_sq;rxvt;dtPC",
        BODY_BEGIN,
            "{"walletItemIds":[]}",
        BODY_END,
        INLINE_URLS,
            "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s91790812289185?AQB=1&ndh=1&pf=1&callback=s_c_il[1].doPostbacks&et=1&t=2%2F8%2F2022%205%3A4%3A24%205%20300&d.&nsid=0&jsonv=1&.d&mid=78611155637513935743550952407821114794&aamlh=7&ce=UTF-8&ns=kohls&pageName=m%3Eshopmenu&g=https%3A%2F%2Fm.kohls.com%2F&c.&k.&pageDomain=m.kohls.com&.k&mcid.&version=4.3.0&icsmcvid=-false&mcidcto=-false&aidcto=-null&.mcid&.c&products=%3Bproductmerch2&aamb=j8Odv6LonN4r3an7LhD3WZrU1bUpAkFkkiY1ncBR96t2PTI&c4=m%7Cshopmenu&c9=shopmenu%7Cshopmenu&v9=homepage&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=D%3Dv18&v18=fri%7Cweekday%7C05%3A00%20am&c22=2022-09-02&v22=mobile&v39=no%20customer%20id&v40=mcom17&v42=no%20cart&c50=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d%7C05%3A00%20am&c53=m%3Eshopmenu&c63=mcom-46f4352e4e7e888ac93c8b7b-b713fc26a3aca27d&c64=VisitorAPI%20Present&v68=m%3Eshopmenu&v71=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&v73=no%20loyalty%20id&v86=21_7&v87=hp19&c.&a.&activitymap.&page=homepage&link=Menu&region=mcom-header&pageIDType=1&.activitymap&.a&.c&pid=homepage&pidt=1&oid=function%28%29%7Bvarb%3DsetInterval%28function%28%29%7Bvarc%3Ddocument.querySelector%28%22li.kohls-charge.hb-mid-blocka%22%29%3B&oidt=2&ot=DIV&s=8192x4096&c=24&j=1.6&v=N&k=Y&bw=790&bh=473&AQE=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;AKA_A2;bm_sz;check;rxVisitor;dtSa;s_ecid;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;s_cc;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;dtCookie;_gcl_au;_fbp;_scid;_ga_ZLYRBY87M8;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;dtLatC;rxvt;dtPC;gpv_v9;_abck;s_sq", END_INLINE
    );

    ns_end_transaction("index_8", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("bf_3");
    ns_web_url ("bf_3",
        "URL=https://bf27853irn.bf.dynatrace.com/bf?type=js3&sn=v_4_srv_4_sn_IV7VUE896KFTV6I5O956PBOGGHT16VT8_app-3A06d81dff759102c9_1_ol_0_perc_100000_mul_1&svrid=4&flavor=cors&vi=WTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0&modifiedSince=1662094650929&rf=https%3A%2F%2Fm.kohls.com%2F&bp=3&app=06d81dff759102c9&crc=2693839218&en=ldtlho0d&end=1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$a=1%7C18%7Cmenu-icon%7CD%7Cx%7C1662113064370%7C1662113064806%7Cdn%7C5463%7Cxu%7C%2FlBzjNRQPfM%2FSq6c%2F1G5U4A%2FiXEDf6VLOw%2FYnB5eA%2FSm9pFh0%2FURUY%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%7Cxrt%7Cb1662113064391e0f0g0h0i0j0k6l172m179u1377v18w18T-8z11I1%7Cxcs%7C202%7Cxce%7C202%7Crc%7C201%2C2%7C21%7C_event_%7C1662113064370%7C_vc_%7CV%7C436%5Epc%7CVCD%7C171%7CVCDS%7C1%7CVCS%7C478%7CVCO%7C479%7CVCI%7C0%7CVE%7C770%5Ep192%5Ep1008%5Eps%5Esspan.avail-offers.hb-new-sprite%7CS%7C436$dO=m.kohls.com,$rId=RID_2418$rpId=$domR=1662113021545$tvn=%2F$tvt=1662113018422$tvm=i1%3Bk0%3Bh0$tvtrg=1$ni=4g|10$md=mdcc2%2CMozilla%2F5.0%20(Linux%5Es%20Android%206.0.1%5Es%20Moto%20G%20(4)%20Build%2FMPJ24.139-64)%20AppleWebKit%2F537.36%20(KHTML%5Ec%20like%20Gecko)%20Chrome%2F58.0.3029.81%20Mobile%20Safari%2F537.36%20PTST%2F1%3Bmdcc8%2CN%2FA$url=https%3A%2F%2Fm.kohls.com%2F$title=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More$latC=2$app=06d81dff759102c9$vi=WTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0$fId=113019999_200$v=10247220811100421$vID=1662113020005SBVNC9V48OJOAGOMH3HKO8K0MK5NUIMM$nV=1$time=1662113065045",
        BODY_END,
        INLINE_URLS,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?guid=ON&;script=0&data=aam=10263239", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1018012790/?guid=ON&data=aam=10263239&is_vtc=1&random=3721635788", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("bf_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("bf_4");
    ns_web_url ("bf_4",
        "URL=https://bf27853irn.bf.dynatrace.com/bf?type=js3&sn=v_4_srv_4_sn_IV7VUE896KFTV6I5O956PBOGGHT16VT8_app-3A06d81dff759102c9_1_ol_0_perc_100000_mul_1&svrid=4&flavor=cors&vi=WTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0&modifiedSince=1662094650929&rf=https%3A%2F%2Fm.kohls.com%2F&bp=3&app=06d81dff759102c9&crc=4038822725&en=ldtlho0d&end=1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$a=1%7C19%7Cmenu-icon%7CC%7Cx%7C1662113064586%7C1662113064942%7Cdn%7C5538%7Cxu%7C%2Fapi%2Fbrowse%2Fv2%2Fbrowse%2Fcatalog%2Fcategory%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%7Cxrt%7Cb1662113064725e0f0g0h0i0j0k4l4m116v55102w581549T-10z11I1%7Cxcs%7C279%7Cxce%7C285%2C2%7C22%7C_event_%7C1662113064586%7C_vc_%7CV%7C220%5Epc%7CVCD%7C149%7CVCDS%7C1%7CVCS%7C410%7CVCO%7C410%7CVCI%7C0%7CVE%7C770%5Ep192%5Ep1008%5Eps%5Esspan.avail-offers.hb-new-sprite%7CS%7C220%2C2%7C20%7Cj3.1.1%7Cxhr%7Cj3.1.1%7C1662113064734%7C1662113064942%7Cdn%7C5538%7Cxu%7C%2Fapi%2Fwalletapi%2Fwallet%2Fv1%2Fitems%2Flookup%2F%3FincludeGPOs%3Dtrue%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%7Cxrt%7Cb1662113064756e0f0g0h0i0j0k4l170m174u1948v326w326T-9z11I1%7Cxcs%7C206%7Cxce%7C208%7Crc%7C403$rId=RID_2418$rpId=$domR=1662113021545$tvn=%2F$tvt=1662113018422$tvm=i1%3Bk0%3Bh0$tvtrg=1$ni=4g|10$md=mdcc2%2CMozilla%2F5.0%20(Linux%5Es%20Android%206.0.1%5Es%20Moto%20G%20(4)%20Build%2FMPJ24.139-64)%20AppleWebKit%2F537.36%20(KHTML%5Ec%20like%20Gecko)%20Chrome%2F58.0.3029.81%20Mobile%20Safari%2F537.36%20PTST%2F1%3Bmdcc8%2CN%2FA$url=https%3A%2F%2Fm.kohls.com%2F$title=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More$latC=2$app=06d81dff759102c9$vi=WTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0$fId=113019999_200$v=10247220811100421$vID=1662113020005SBVNC9V48OJOAGOMH3HKO8K0MK5NUIMM$nV=1$time=1662113065166",
        BODY_END
    );

    ns_end_transaction("bf_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_19");
    ns_web_url ("recording_19",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=19&rst=1662113025036&let=1662113066372&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_19_main_url_1_1662113085108.body",
        BODY_END
    );

    ns_end_transaction("recording_19", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("URUY_5");
    ns_web_url ("URUY_5",
        "URL=https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=x-dtpc:4$113019999_200h23vWTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0e0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;dtSa;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_ga_ZLYRBY87M8;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;m_prodMerch;gpv_v9;s_sq;dtLatC;akaalb_m_kohls_com;_abck;bm_sz;rxvt;dtPC",
        BODY_BEGIN,
            "{"sensor_data":"7a74G7m23Vrp0o5c9368031.75-1,2,-94,-100,Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1,uaend,12147,20030107,en-US,Gecko,0,0,0,0,408958,3020176,1313,489,1313,489,1313,361,1313,,cpen:0,i1:0,dm:0,cwen:0,non:1,opc:0,fc:0,sc:0,wrc:1,isc:0,vib:1,bat:1,x11:0,x12:1,11023,0.317412196158,831056510087.5,0,loc:-1,2,-94,-131,-1,2,-94,-101,do_en,dm_en,t_en-1,2,-94,-105,0,0,0,0,1708,-1,0;-1,2,-94,-102,0,0,0,0,1708,-1,0;-1,2,-94,-108,-1,2,-94,-110,0,1,41060,565,339;1,1,41489,716,230;2,1,41521,835,186;3,1,41562,900,175;4,1,41564,906,174;5,1,41591,909,174;6,1,41622,910,174;7,1,41866,810,91;8,1,41881,781,64;9,1,42556,396,25;10,1,42568,384,44;11,1,42579,373,65;12,1,42598,361,94;13,1,42613,356,111;14,1,42629,349,136;15,1,42662,347,147;16,1,42680,347,150;17,1,42971,336,150;18,1,42976,273,148;19,1,42996,232,146;20,1,43007,216,146;21,1,43024,196,149;22,1,43040,178,152;23,1,43061,166,153;24,1,43090,152,153;25,1,43107,141,153;26,1,43124,132,153;27,1,43141,127,153;28,1,43157,121,153;29,1,43174,119,153;30,1,43191,117,153;31,1,43208,113,153;32,1,43225,110,151;33,1,43241,107,150;34,1,43257,101,147;35,1,43274,95,144;36,1,43292,91,141;37,1,43307,85,138;38,1,43324,81,136;39,1,43341,74,133;40,1,43358,70,132;41,1,43374,68,131;42,1,43390,66,130;43,1,43409,64,129;44,1,43433,62,129;45,1,43441,61,128;46,1,43457,58,128;47,1,43473,57,128;48,1,43490,54,128;49,1,43507,52,128;50,1,43525,50,128;51,1,43543,48,129;52,1,43557,46,129;53,1,43573,44,129;54,1,43591,43,129;55,1,43607,41,129;56,1,43623,39,129;57,1,43641,38,129;58,1,43658,36,129;59,1,43673,35,129;60,1,43691,34,129;61,1,43728,31,129;62,1,43744,30,128;63,1,43777,29,128;64,1,43797,28,127;65,1,43841,27,127;66,1,43862,26,126;67,1,43897,25,125;68,1,43912,24,125;69,1,43949,24,124;70,1,43999,24,123;71,3,44194,24,123,-1;72,4,44408,24,123,-1;73,2,44411,24,123,-1;74,1,45595,88,118;75,1,45704,159,159;76,1,45834,161,197;77,1,45903,136,221;78,1,45965,109,240;79,1,45987,105,243;80,1,46003,103,245;81,1,46012,101,247;82,1,46030,97,249;83,1,46049,95,252;84,1,46067,94,252;85,1,46078,91,255;86,1,46099,90,257;87,1,46115,89,258;88,1,46131,87,260;89,1,46149,87,261;90,1,46164,86,263;91,1,46180,86,265;92,1,46200,85,267;93,1,46213,85,269;94,1,46230,84,270;95,1,46249,83,271;96,1,46266,83,272;97,1,46282,83,274;98,1,46298,82,276;99,1,46331,82,278;100,1,46347,81,279;101,1,46364,81,280;102,1,46395,80,283;106,3,46760,79,290,-1;-1,2,-94,-117,-1,2,-94,-111,-1,2,-94,-109,-1,2,-94,-114,-1,2,-94,-103,3,44227;-1,2,-94,-112,https://m.kohls.com/-1,2,-94,-115,1,4618149,32,0,0,0,4618117,46760,0,1662113020175,39,17780,0,107,2963,3,0,46763,4577102,0,B6A6969A0C7F79D7A3819FC0D8E40EDA~-1~YAAQEzPdFyK2UPqCAQAAzKeo/QhXxc4vRuXgIIY7zjjbGJe9aGoefXfMQ6MvqZvC0VbDyq2win11ZnpbBzyFv8xyYnu8eLIy4uwV+aY/yElAKnrcfEhMt5az3Vki0Zl1n9mfwpewAlXX2ZUDQrbH3FJCvc/LhVscL62y6HuAPhjG+RA09VkwPW6gojrmhqbuwcZAROJCdFpX0a2Xd06cM/NFk6MhsIUteGq0UAEmN0eHdLD26wzgC7xFgW+sQ8VBTyf7v3Lwa0+WEd3DOwvmGptM5hoIlU4cQcJeOaaU3QW5FzpwoZhTGcNL0R+lBT4t+2gNsnNGKmVFzDCM7Ot8FhRvE5ri2KH0inFZYogbufdHDJ28nNh31ZbWzqQdOg3DbSAdy0xXOMEAW5c4zGfUvsZinUR4ucB1Om04Ng8cVBIcbIZ0MX+b/AXmTLpqA/yrpSyYMkMm1AWoewDjwyBfzTC6Hy6lkPYK6pdA/TNxfh4AmM4ncmjZbzEyySNRwtvkRHuaX1xWMEaFHasya9JVopkXNMCMH497MnHz~-1~-1~-1,50352,939,-206599699,30261689,PiZtE,107314,97,0,-1-1,2,-94,-106,1,4-1,2,-94,-119,-1-1,2,-94,-122,0,0,0,0,1,0,0-1,2,-94,-123,-1,2,-94,-124,-1,2,-94,-126,-1,2,-94,-127,00300044040300043020-1,2,-94,-70,-1127778619;-563023189;dis;;true;true;true;300;true;24;24;true;false;-1-1,2,-94,-80,5383-1,2,-94,-116,9060480-1,2,-94,-118,200381-1,2,-94,-129,98f7979f11c93279c5ea09beec21055d155a5331e783f9bfe9e4a1a482531594,1,0,Google Inc.,Google SwiftShader,3c84d65d49fb3b8d03ed2f26bf53630900f726ac7aa573f0a715315b02e21805,27-1,2,-94,-121,;6;25;0"}",
        BODY_END
    );

    ns_end_transaction("URUY_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("bf_5");
    ns_web_url ("bf_5",
        "URL=https://bf27853irn.bf.dynatrace.com/bf?type=js3&sn=v_4_srv_4_sn_IV7VUE896KFTV6I5O956PBOGGHT16VT8_app-3A06d81dff759102c9_1_ol_0_perc_100000_mul_1&svrid=4&flavor=cors&vi=WTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0&modifiedSince=1662094650929&rf=https%3A%2F%2Fm.kohls.com%2F&bp=3&app=06d81dff759102c9&crc=3680672302&en=ldtlho0d&end=1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$tvn=%2F$tvt=1662113018422$tvm=i1%3Bk0%3Bh0$tvtrg=1$ni=4g|10$rt=18-1662113018422%3Bhttps%3A%2F%2Fm.kohls.com%2FlBzjNRQPfM%2FSq6c%2F1G5U4A%2FiXEDf6VLOw%2FYnB5eA%2FSm9pFh0%2FURUY%7Cb45969e0f0g0h0i0j0k6l172m179u1377v18w18K1T-8z11I1$url=https%3A%2F%2Fm.kohls.com%2F$title=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More$latC=2$app=06d81dff759102c9$vi=WTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0$fId=113019999_200$v=10247220811100421$vID=1662113020005SBVNC9V48OJOAGOMH3HKO8K0MK5NUIMM$time=1662113067084",
        BODY_END
    );

    ns_end_transaction("bf_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("bf_6");
    ns_web_url ("bf_6",
        "URL=https://bf27853irn.bf.dynatrace.com/bf?type=js3&sn=v_4_srv_4_sn_IV7VUE896KFTV6I5O956PBOGGHT16VT8_app-3A06d81dff759102c9_1_ol_0_perc_100000_mul_1&svrid=4&flavor=cors&vi=WTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0&modifiedSince=1662094650929&rf=https%3A%2F%2Fm.kohls.com%2F&bp=3&app=06d81dff759102c9&crc=1291245389&en=ldtlho0d&end=1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$a=1%7C23%7CHome%7CD%7Cx%7C1662113066936%7C1662113066936%7Cdn%7C5538%7Cxu%7C%2FlBzjNRQPfM%2FSq6c%2F1G5U4A%2FiXEDf6VLOw%2FYnB5eA%2FSm9pFh0%2FURUY%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%7Cxcs%7C183%7Cxce%7C183%7Crc%7C201%2C2%7C24%7C_event_%7C1662113066936%7C_vc_%7CV%7C-1%5Epl%7CVCD%7C0%7CVCDS%7C0%7CVCS%7C277%7CVCO%7C277%7CVCI%7C0%7CS%7C-1$rId=RID_2418$rpId=$domR=1662113021545$tvn=%2F$tvt=1662113018422$tvm=i1%3Bk0%3Bh0$tvtrg=1$ni=4g|10$rt=23-1662113018422%3Bhttps%3A%2F%2Fm.kohls.com%2FlBzjNRQPfM%2FSq6c%2F1G5U4A%2FiXEDf6VLOw%2FYnB5eA%2FSm9pFh0%2FURUY%7Cb48532e0f0g0h0i0j0k4l159m162u1127v17w17K1z1I1$fd=b51-100$md=mdcc2%2CMozilla%2F5.0%20(Linux%5Es%20Android%206.0.1%5Es%20Moto%20G%20(4)%20Build%2FMPJ24.139-64)%20AppleWebKit%2F537.36%20(KHTML%5Ec%20like%20Gecko)%20Chrome%2F58.0.3029.81%20Mobile%20Safari%2F537.36%20PTST%2F1%3Bmdcc8%2CN%2FA$url=https%3A%2F%2Fm.kohls.com%2F$title=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More$isUnload=1$latC=2$app=06d81dff759102c9$vi=WTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0$fId=113019999_200$v=10247220811100421$vID=1662113020005SBVNC9V48OJOAGOMH3HKO8K0MK5NUIMM$nV=1$time=1662113067231",
        BODY_END
    );

    ns_end_transaction("bf_6", NS_AUTO_STATUS);
    ns_page_think_time(0.171);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("index_2");
    ns_web_url ("index_2",
        "URL=https://m.kohls.com/",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_ga_ZLYRBY87M8;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;m_prodMerch;gpv_v9;dtLatC;akaalb_m_kohls_com;bm_sz;rxvt;_abck;dtPC;s_sq;dtSa"
    );

    ns_end_transaction("index_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("X");
    ns_web_url ("X",
        "URL=https://bat.bing.com/actionp/0?ti=4024145&tm=al001&Ver=2&mid=bfacb451-242e-4eee-8f1a-206225edcb33&sid=8538b9f02aa611ed8309ebe712901205&vid=85392ce02aa611ed9f4de589d105e7b7&vids=1&msclkid=N&evt=pageHide",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=MUID;MR"
    );

    ns_end_transaction("X", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_20");
    ns_web_url ("recording_20",
        "URL=https://k-aus1.clicktale.net/v2/recording?v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&rt=7&ct=0&hlm=true&enc=raw",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("recording_20", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collect_4");
    ns_web_url ("collect_4",
        "URL=https://analytics.google.com/g/collect?v=2&tid=G-ZLYRBY87M8&gtm=2oe8v0&_p=2017578148&cid=1350227648.1662113023&ul=en-us&sr=1313x489&_z=ccd.v9B&_s=2&sid=1662113028&sct=1&seg=0&dl=https%3A%2F%2Fm.kohls.com%2F&dt=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&en=user_engagement&ep.debug_mode=true&_et=2888",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("collect_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("events");
    ns_web_url ("events",
        "URL=https://c.clicktale.net/v2/events?v=11.40.1&str=134&di=1782&dc=2701&fl=2753&sr=4&mdh=10892&pn=1&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&lv=1662113024&lhd=1662113024&hd=1662113024&pid=2399&ct=0&enc=raw",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "[{"type":0,"ts":53,"x":790,"y":473},{"type":19,"name":"FCP","val":1005.5899999570101,"ts":55},{"type":19,"name":"CLS","val":0.10549580274467442,"ts":162},{"type":19,"name":"CLS","val":0.21146379696097045,"ts":162},{"type":19,"name":"LCP","val":1005.589,"ts":162},{"type":19,"name":"LCP","val":1494.789,"ts":163},{"type":19,"name":"TTFB","val":532.4849999742582,"ts":165},{"type":0,"ts":3034,"x":1313,"y":361,"d":775},{"type":2,"ts":36273,"x":565,"y":339,"xRel":28526,"yRel":21066,"tgtHM":"div#open-drawer>div:eq(0)>div:eq(2)"},{"type":6,"ts":36701,"x":716,"y":230,"tgt":"div#open-drawer>div:eq(0)>div:eq(1)>div:eq(0)>div:eq(0)>div:eq(0)>div:eq(4)>div:eq(0)>a:eq(0)>div:eq(0)"},{"type":2,"ts":36703,"x":716,"y":230,"xRel":45493,"yRel":7004,"tgtHM":"div#open-drawer>div:eq(0)>div:eq(1)>div:eq(0)>div:eq(0)>div:eq(0)>div:eq(4)>div:eq(0)>a:eq(0)>div:eq(0)"},{"type":7,"ts":36734,"x":835,"y":186,"tgt":"div#open-drawer>div:eq(0)>div:eq(1)>div:eq(0)>div:eq(0)>div:eq(0)>div:eq(4)>div:eq(0)>a:eq(0)>div:eq(0)"},{"type":6,"ts":37079,"x":810,"y":91,"tgt":"div#kohls-header-banner>a:eq(0)>picture:eq(0)"},{"type":7,"ts":37094,"x":781,"y":64,"tgt":"div#kohls-header-banner>a:eq(0)>picture:eq(0)"},{"type":6,"ts":37095,"x":781,"y":64,"tgt":"div#kohls-header-banner>a:eq(0)>picture:eq(0)>img:eq(0)"},{"type":2,"ts":37103,"x":781,"y":64,"xRel":39432,"yRel":46530,"tgtHM":"div#kohls-header-banner>a:eq(0)>picture:eq(0)>img:eq(0)"},{"type":7,"ts":37496,"x":428,"y":-12,"tgt":"div#kohls-header-banner>a:eq(0)>picture:eq(0)>img:eq(0)"},{"type":6,"ts":37768,"x":396,"y":25,"tgt":"div#kohls-header-banner>a:eq(0)>picture:eq(0)>img:eq(0)"},{"type":2,"ts":37769,"x":396,"y":25,"xRel":19994,"yRel":18176,"tgtHM":""},{"type":7,"ts":37811,"x":361,"y":94,"tgt":"div#kohls-header-banner>a:eq(0)>picture:eq(0)>img:eq(0)"},{"type":2,"ts":38169,"x":347,"y":150,"xRel":17520,"yRel":2109,"tgtHM":"div#open-drawer>div:eq(0)>div:eq(1)"},{"type":2,"ts":38570,"x":74,"y":133,"xRel":29746,"yRel":44274,"tgtHM":"div#mcom-header>div:eq(0)"},{"type":6,"ts":38622,"x":64,"y":129,"tgt":"div#mcom-header>div:eq(0)>div:eq(1)>a:eq(0)"},{"type":7,"ts":38738,"x":50,"y":128,"tgt":"div#mcom-header>div:eq(0)>div:eq(1)>a:eq(0)"},{"type":2,"ts":38970,"x":30,"y":128,"xRel":50412,"yRel":48269,"tgtHM":"div#menu-section>div:eq(0)"},{"type":2,"ts":39370,"x":24,"y":123,"xRel":35288,"yRel":36969,"tgtHM":""},{"type":3,"ts":39431,"x":24,"y":123,"tgt":"div#menu-section>div:eq(0)"},{"type":19,"name":"FID","val":2.940000034868717,"ts":39447},{"type":4,"ts":39621,"x":24,"y":123,"tgt":"div#menu-section>div:eq(0)"},{"type":5,"ts":39624,"x":24,"y":123,"tgt":"div#menu-section>div:eq(0)"},{"type":2,"ts":40808,"x":88,"y":118,"xRel":5057,"yRel":57710,"tgtHM":"div#home-menu-new>div:eq(1)"},{"type":6,"ts":40917,"x":159,"y":159,"tgt":"div#home-menu-new>div:eq(2)>div:eq(0)>a:eq(0)"},{"type":7,"ts":41047,"x":161,"y":197,"tgt":"div#home-menu-new>div:eq(2)>div:eq(0)>a:eq(0)"},{"type":2,"ts":41209,"x":105,"y":243,"xRel":6034,"yRel":50576,"tgtHM":"div#home-menu-new>div:eq(2)>div:eq(1)"},{"type":6,"ts":41608,"x":80,"y":283,"tgt":"div#home-menu-new>ul:eq(0)>li:eq(0)>a:eq(0)"},{"type":2,"ts":41610,"x":80,"y":283,"xRel":3895,"yRel":5699,"tgtHM":"div#home-menu-new>ul:eq(0)>li:eq(0)>a:eq(0)"},{"type":3,"ts":41993,"x":79,"y":290,"tgt":"div#home-menu-new>ul:eq(0)>li:eq(0)>a:eq(0)"},{"type":2,"ts":42011,"x":79,"y":290,"xRel":3836,"yRel":25644,"tgtHM":""},{"type":4,"ts":42202,"x":79,"y":290,"tgt":"div#home-menu-new>ul:eq(0)>li:eq(0)>a:eq(0)"},{"type":5,"ts":42205,"x":79,"y":290,"tgt":"div#home-menu-new>ul:eq(0)>li:eq(0)>a:eq(0)"}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://m.kohls.com/dist/css/header/css-bundle-rev-62b152927d667f360603be9580ca909c.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mbox;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;m_prodMerch;gpv_v9;dtLatC;akaalb_m_kohls_com;bm_sz;rxvt;_abck;dtPC;s_sq;dtSa;_ga_ZLYRBY87M8", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/launch-b2dd4b082ed7.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("events", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("json_2");
    ns_web_url ("json_2",
        "URL=https://mboxedge34.tt.omtrdc.net/m2/kohls/mbox/json?mbox=target-global-mbox&mboxSession=460f1c38cfda4554a42e960007135e4b&mboxPC=460f1c38cfda4554a42e960007135e4b.34_0&mboxPage=0fdacd0a3a7b46cf872362503c5002ca&mboxRid=b668f27248504986909ef1f94dad0c98&mboxVersion=1.7.1&mboxCount=1&mboxTime=1662095067524&mboxHost=m.kohls.com&mboxURL=https%3A%2F%2Fm.kohls.com%2F&mboxReferrer=https%3A%2F%2Fm.kohls.com%2F&browserHeight=361&browserWidth=1313&browserTimeOffset=-300&screenHeight=489&screenWidth=1313&colorDepth=24&devicePixelRatio=1&screenOrientation=landscape&webGLRenderer=Google%20SwiftShader&at_property=b1ebe2fe-cdb6-8289-fe4c-133434dc93f1&customerLoggedStatus=false&tceIsRedesign=True&tceIsPDPRedesign=True&tceIsPMPResponsive2=false&tceIsPDPResponsive3=false&tceIsPDPResponsive4=false&mboxMCSDID=2C8FA7DC757DC478-1B32B8F6CFACCF4A&vst.trk=ww9.kohls.com&vst.trks=ww8.kohls.com&mboxMCGVID=78611155637513935743550952407821114794&mboxAAMB=j8Odv6LonN4r3an7LhD3WZrU1bUpAkFkkiY1ncBR96t2PTI&mboxMCGLH=7",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://s2.go-mpulse.net/boomerang/4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://js-cdn.dynatrace.com/jstag/1740040f04f/bf27853irn/6d81dff759102c9_complete.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://m.kohls.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://bat.bing.com/bat.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=MUID;MR", END_INLINE,
            "URL=https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;m_prodMerch;gpv_v9;dtLatC;bm_sz;rxvt;_abck;dtPC;s_sq;dtSa;_ga_ZLYRBY87M8;mbox", END_INLINE,
            "URL=https://m.kohls.com/_sec/cp_challenge/sec-3-8.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;m_prodMerch;gpv_v9;dtLatC;bm_sz;rxvt;_abck;dtPC;s_sq;dtSa;_ga_ZLYRBY87M8;mbox", END_INLINE,
            "URL=https://m.kohls.com/_sec/cp_challenge/sec-cpt-3-8.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;m_prodMerch;gpv_v9;dtLatC;bm_sz;rxvt;_abck;dtPC;s_sq;dtSa;_ga_ZLYRBY87M8;mbox", END_INLINE,
            "URL=https://bat.bing.com/p/action/4024145.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=MUID;MR", END_INLINE,
            "URL=https://6249496.collect.igodigital.com/collect.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("json_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("URUY_6");
    ns_web_url ("URUY_6",
        "URL=https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=x-dtpc:4$113068061_316h2vWTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0e0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;m_prodMerch;gpv_v9;bm_sz;s_sq;_ga_ZLYRBY87M8;mbox;akaalb_m_kohls_com;_abck;dtLatC;dtSa;rxvt;dtPC",
        BODY_BEGIN,
            "{"sensor_data":"7a74G7m23Vrp0o5c9368031.75-1,2,-94,-100,Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1,uaend,12147,20030107,en-US,Gecko,0,0,0,0,408958,3068186,1313,489,1313,489,1313,361,1313,,cpen:0,i1:0,dm:0,cwen:0,non:1,opc:0,fc:0,sc:0,wrc:1,isc:0,vib:1,bat:1,x11:0,x12:1,11023,0.16870023084,831056534092.5,0,loc:-1,2,-94,-131,-1,2,-94,-101,do_en,dm_en,t_en-1,2,-94,-105,0,0,0,0,1708,-1,0;-1,2,-94,-102,0,0,0,0,1708,-1,0;-1,2,-94,-108,-1,2,-94,-110,-1,2,-94,-117,-1,2,-94,-111,-1,2,-94,-109,-1,2,-94,-114,-1,2,-94,-103,-1,2,-94,-112,https://m.kohls.com/-1,2,-94,-115,1,32,32,0,0,0,0,7,0,1662113068185,-999999,17780,0,0,2963,0,0,11,0,0,B6A6969A0C7F79D7A3819FC0D8E40EDA~-1~YAAQEzPdF9q2UPqCAQAAx7Oo/QjtIe53buu71PJ+wfypFeHhe03WSfXnUcRZ871XssYjlI18/eLhDzW8NP842Jv0jqd29E6HwRDRmjAUyjGFoa5Y2S3dqbtmAJbGcY8m5DR4BhjriJSXK5PcnqqTxrof6aBfZuPtAUkhvN3fFwk/LisngdIFGUPXFy+0KyyG6tBDvofF42/qqTS2FKURDrqde+2loemIS7hWwPNvzgu7webcbLQHM7XSrvE96Vn79vVBnpJmHp+3U0kbYnVtfH/29upSx5tx9vP/0xhdSjFWU9xxRgGMHJyLOcTXDLw33bTE2IZyrpAXhsnm3R/bnyovpFm9tEiboZOBjVf2tHkSs3CuPn54TfNCjfrR3/FRsx58l6Ai/0pLRPfbRs/jGOPAE3Om6d+iFBAzkSzHd29KYYL4lDuKVuEoxNEiT35DtGQfQdsBJl4tTyX/5Rscnbqx0wTXL+VMTeB8aMa6iZ4VXNBA7aOIQkzMCrtkGRFai/+Z6k7EccITOsmzvk9KYzx1+fZCItybkXp7~-1~-1~-1,49863,-1,-1,30261689,PiZtE,79445,63,0,-1-1,2,-94,-106,0,0-1,2,-94,-119,-1-1,2,-94,-122,0,0,0,0,1,0,0-1,2,-94,-123,-1,2,-94,-124,-1,2,-94,-126,-1,2,-94,-127,8-1,2,-94,-70,-1-1,2,-94,-80,94-1,2,-94,-116,745567530-1,2,-94,-118,100219-1,2,-94,-129,-1,2,-94,-121,;11;-1;0"}",
        BODY_END
    );

    ns_end_transaction("URUY_6", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("category_3");
    ns_web_url ("category_3",
        "URL=https://m.kohls.com/api/browse/v2/browse/catalog/category",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=x-correlation-id:MCOM-46f4352e4e7e888ac93c8b7b-b713fc26a3aca27d-1662113068405",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=x-channel:MCOM",
        "HEADER=x-dtpc:4$113068061_316h3vWTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0e0",
        "HEADER=x-pageid:MCOM-homepage",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;m_prodMerch;gpv_v9;bm_sz;s_sq;_ga_ZLYRBY87M8;mbox;akaalb_m_kohls_com;dtLatC;dtSa;_abck;rxvt;dtPC"
    );

    ns_end_transaction("category_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("banners_2");
    ns_web_url ("banners_2",
        "URL=https://m.kohls.com/api/v1/browse/monetization/banners?channel=mobile&pageType=homePage&vid=78611155637513935743550952407821114794",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=x-correlation-id:MCOM-46f4352e4e7e888ac93c8b7b-b713fc26a3aca27d-1662113068430",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=x-channel:MCOM",
        "HEADER=Content-Type:application/json",
        "HEADER=x-dtpc:4$113068061_316h4vWTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0e0",
        "HEADER=x-pageid:MCOM-homepage",
        "HEADER=channel:mcom",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_uetsid;_uetvid;crl8.fpcuid;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;m_prodMerch;gpv_v9;bm_sz;s_sq;_ga_ZLYRBY87M8;mbox;akaalb_m_kohls_com;dtLatC;dtSa;_abck;rxvt;dtPC",
        BODY_BEGIN,
            "null",
        BODY_END,
        INLINE_URLS,
            "URL=https://cdn.dynamicyield.com/api/8776374/api_dynamic.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=DYID;DYSES", END_INLINE,
            "URL=https://bat.bing.com/action/0?ti=4024145&tm=al001&Ver=2&mid=9883e8d9-6df9-4aa1-a266-9c3ac0f285d8&sid=8538b9f02aa611ed8309ebe712901205&vid=85392ce02aa611ed9f4de589d105e7b7&vids=0&msclkid=N&pi=0&lg=en-US&sw=1313&sh=489&sc=24&tl=Kohl%27s%20%7C%20Shop%20Clothing,%20Shoes,%20Home,%20Kitchen,%20Bedding,%20Toys%20%26%20More&p=https%3A%2F%2Fm.kohls.com%2F&r=https%3A%2F%2Fm.kohls.com%2F&lt=1287&pt=1662113067239,74,76,,,5,5,5,5,5,,11,12,27,93,1284,1285,1287,,,&pn=0,0&evt=pageLoad&sv=1&rn=202466", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=MUID;MR", END_INLINE
    );

    ns_end_transaction("banners_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("a9a6fb14_365a_4648_b17b_2e47_3");
    ns_web_url ("a9a6fb14_365a_4648_b17b_2e47_3",
        "URL=https://mon.domdog.io/events/report/a9a6fb14-365a-4648-b17b-2e47930f8b49",
        "METHOD=POST",
        INLINE_URLS,
            "URL=https://static.domdog.io/success", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("a9a6fb14_365a_4648_b17b_2e47_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("banners_3");
    ns_web_url ("banners_3",
        "URL=https://m.kohls.com/api/v1/browse/monetization/banners?channel=mobile&pageType=homePage&vid=78611155637513935743550952407821114794",
        "METHOD=POST",
        INLINE_URLS,
            "URL=https://cdn.dynamicyield.com/api/8776374/api_static.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=DYID;DYSES", END_INLINE,
            "URL=https://pubsaf.global.ssl.fastly.net/prmt/6e94f5b92e87fff07364eafeb536230c", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("banners_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("URUY_7");
    ns_web_url ("URUY_7",
        "URL=https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=x-dtpc:4$113068061_316h5vWTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0e0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_ses_load_seq;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyfs;_dyjsession;dy_fs_page;_dy_lu_ses;_dycst;_dy_geo;_dy_df_geo;_dy_toffset;_dy_soct;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;m_prodMerch;gpv_v9;bm_sz;s_sq;_ga_ZLYRBY87M8;mbox;akaalb_m_kohls_com;dtLatC;dtSa;_uetsid;_uetvid;_abck;rxvt;dtPC",
        BODY_BEGIN,
            "{"sensor_data":"7a74G7m23Vrp0o5c9368031.75-1,2,-94,-100,Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1,uaend,12147,20030107,en-US,Gecko,0,0,0,0,408958,3068186,1313,489,1313,489,1313,361,1313,,cpen:0,i1:0,dm:0,cwen:0,non:1,opc:0,fc:0,sc:0,wrc:1,isc:0,vib:1,bat:1,x11:0,x12:1,11023,0.16871141284,831056534092.5,0,loc:-1,2,-94,-131,-1,2,-94,-101,do_en,dm_en,t_en-1,2,-94,-105,0,0,0,0,1708,-1,0;-1,2,-94,-102,0,0,0,0,1708,-1,0;-1,2,-94,-108,-1,2,-94,-110,-1,2,-94,-117,-1,2,-94,-111,-1,2,-94,-109,-1,2,-94,-114,-1,2,-94,-103,-1,2,-94,-112,https://m.kohls.com/-1,2,-94,-115,1,32,32,0,0,0,0,567,0,1662113068185,10,17780,0,0,2963,0,0,571,0,0,B6A6969A0C7F79D7A3819FC0D8E40EDA~-1~YAAQEzPdFwG3UPqCAQAARbao/QiCxMR2HEAWlGp2xnRmT80rcS6szAjxxIUj2RQLPTbl6ypsLKUxiDTS5to5iEvd8PH7xbe8p9oh+gNRd0okRF9BTyjAcaCZV56yO73dossErbjCImKWspnlnLwlnZXVZcW3LTneBTNZH2DhBhyJ+uAg2OgZSMF7r68/QGDOkoXRfGEBJyvecJxICWP1QSjQyMwLo7NcFK/peKdDPpG/4ffrsWn5bGVSIbhCs0g5nO+c5uBDaxs+4sd14T5xf7v2Nq+XOv+vxJ2ywry1G0HTJ1EzfLmGTPE4CPbwI70CVqkVbcRWnQ38EPxZzK8ZtX4SKqgBoz9nyiXof4OGVnavQMAU/XcIeoKUGJ3F5SgC55lTx8UWvqLgtaDXyl2dvAa/0SLdyKiSpoSZtbooab/vqRFQ5aTwZDRvSaoNtXkvtBOLDLexBTqEMnymdvEpFqqR+HRXKigOPpBGQJQmDHfhlQXE/2Zp9Rw6R+m5AC7f1o86qAuibv8LG5ayJEcxlaZsS7LddEzQ2zHh~-1~-1~-1,50167,426,434296941,30261689,PiZtE,93488,30,0,-1-1,2,-94,-106,9,1-1,2,-94,-119,-1-1,2,-94,-122,0,0,0,0,1,0,0-1,2,-94,-123,-1,2,-94,-124,-1,2,-94,-126,-1,2,-94,-127,00300044040300043020-1,2,-94,-70,-1127778619;-563023189;dis;;true;true;true;300;true;24;24;true;false;-1-1,2,-94,-80,5383-1,2,-94,-116,745567530-1,2,-94,-118,101740-1,2,-94,-129,,,0,,,,0-1,2,-94,-121,;8;36;0"}",
        BODY_END
    );

    ns_end_transaction("URUY_7", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("config_json_2");
    ns_web_url ("config_json_2",
        "URL=https://c.go-mpulse.net/api/config.json?key=4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T&d=m.kohls.com&t=5540377&v=1.720.0&sl=0&si=c108fdad-ef20-423f-aee4-3211a03e39b0-rhkunf&plugins=AK,ConfigOverride,Continuity,PageParams,IFrameDelay,AutoXHR,SPA,History,Angular,Backbone,Ember,RT,CrossDomain,BW,PaintTiming,NavigationTiming,ResourceTiming,Memory,CACHE_RELOAD,Errors,TPAnalytics,UserTiming,Akamai,Early,EventTiming,LOGN&acao=&ak.ai=223330",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://st.dynamicyield.com/st?sec=8776374&inHead=true&id=-1233986090078972675&jsession=843ba9d01f557091cd7796a12350eadd&ref=https%3A%2F%2Fm.kohls.com%2F&scriptVersion=1.129.0&dyid_server=&ctx=%7B%22type%22%3A%22HOMEPAGE%22%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=DYID;DYSES", END_INLINE,
            "URL=https://track.coherentpath.com/v1/track.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://tjxbfc1n.micpn.com/p/js/1.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_mibhv", END_INLINE,
            "URL=https://cdn.evgnet.com/beacon/kohlsinc2/engage/scripts/evergage.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cdn.dynamicyield.com/scripts/1.129.0/dy-coll-nojq-min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=DYID;DYSES", END_INLINE
    );

    ns_end_transaction("config_json_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("uia_2");
    ns_web_url ("uia_2",
        "URL=https://async-px.dynamicyield.com/uia?cnst=1&_=1662113069031",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "id=-1233986090078972675&se=8776374&cl=d.an.c.ms.&rf=internal&trf=0&p=2&sub=m.kohls.com&sd=&url=https%3A%2F%2Fm.kohls.com%2F&title=Kohl's%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&lay=def&ses=5b3e117ce067eeb99b2b5b268b908b68&aud=1408117.1438654.1362538.1362542&gv=&exps=%5B%5B%221067036%22%2C%229863190%22%2C%2225792075%22%2C0%2Cnull%2Cnull%2C%22-5734382000971782118%22%2C%222%22%2C%223%22%5D%2C%5B%221096558%22%2C%2210092862%22%2C%2226047526%22%2C0%2Cnull%2Cnull%2C%22-5734382002361846856%22%2C%221%22%2Cnull%5D%2C%5B%221127582%22%2C%2210281141%22%2C%2226223332%22%2C0%2Cnull%2Cnull%2C%22-5734382000662591173%22%2C%221%22%2Cnull%5D%2C%5B%221179251%22%2C%2211202288%22%2C%2226647761%22%2C0%2Cnull%2Cnull%2C%22-5734382000772232028%22%2C%221%22%2Cnull%5D%2C%5B%221245855%22%2C%2211044797%22%2C%2227049604%22%2C0%2Cnull%2Cnull%2C%22-5734382003904465510%22%2C%221%22%2Cnull%5D%2C%5B%221250304%22%2C%2211527264%22%2C%2227070038%22%2C0%2Cnull%2Cnull%2C%22-5734382003328103099%22%2C%221%22%2Cnull%5D%2C%5B%221289933%22%2C%2211374557%22%2C%2227236876%22%2C0%2Cnull%2Cnull%2C%22-5734382000036239809%22%2C%221%22%2Cnull%5D%2C%5B%221309946%22%2C%2211499500%22%2C%2227308034%22%2C0%2Cnull%2Cnull%2C%22-5734382000503099902%22%2C%221%22%2Cnull%5D%5D&lts=5%3A4&ctx=%7B%22type%22%3A%22HOMEPAGE%22%7D&lpInfo=false&expSes=71849&br=Motorola&tsrc=Direct&geoData=US__&feedProps%5Bcategories%5D=&feedProps%5Bkeywords%5D=&cookieInfo=0.0.5095&reqts=1662113069026&rri=2602260&nocks=false",
        BODY_END,
        INLINE_URLS,
            "URL=https://tjxbfc1n.micpn.com/p/cp/-1/track.gif?t=1662113069050&mi_u=anon-1662113021381-1801882823&mi_cid=8212&page_title=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&referrer=https%3A%2F%2Fm.kohls.com%2F&timezone_offset=300&event_type=pageview&cdate=1662113069047&ck=host&anon=true", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_mibhv", END_INLINE,
            "URL=https://async-px.dynamicyield.com/var?cnst=1&_=583083&uid=-1233986090078972675&sec=8776374&t=ri&e=1179251&p=2&ve=11202288&va=%5B26647761%5D&ses=5b3e117ce067eeb99b2b5b268b908b68&expSes=71849&aud=1408117.1438654.1362538.1362542&expVisitId=-5734382000772232028&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1662113068054&rri=3934675", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://async-px.dynamicyield.com/var?cnst=1&_=558841&uid=-1233986090078972675&sec=8776374&t=ri&e=1245855&p=2&ve=11044797&va=%5B27049604%5D&ses=5b3e117ce067eeb99b2b5b268b908b68&expSes=71849&aud=1408117.1438654.1362538.1362542&expVisitId=-5734382003904465510&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1662113068057&rri=9595922", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://async-px.dynamicyield.com/var?cnst=1&_=402669&uid=-1233986090078972675&sec=8776374&t=ri&e=1289933&p=2&ve=11374557&va=%5B27236876%5D&ses=5b3e117ce067eeb99b2b5b268b908b68&expSes=71849&aud=1408117.1438654.1362538.1362542&expVisitId=-5734382000036239809&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1662113068059&rri=9031893", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://async-px.dynamicyield.com/var?cnst=1&_=236386&uid=-1233986090078972675&sec=8776374&t=ri&e=1309946&p=2&ve=11499500&va=%5B27308034%5D&ses=5b3e117ce067eeb99b2b5b268b908b68&expSes=71849&aud=1408117.1438654.1362538.1362542&expVisitId=-5734382000503099902&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1662113068062&rri=4385479", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.kohls.com/dist/css/fonts/hfjfont-bundle-rev-299dfdec408a81d5c00d76e18dd5db1d.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;m_prodMerch;gpv_v9;bm_sz;s_sq;_ga_ZLYRBY87M8;mbox;akaalb_m_kohls_com;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC", END_INLINE,
            "URL=https://m.kohls.com/dist/css/header/css-mcom-bundle-defer-rev-1914c8f4a6d5d4899fa46915baa48c89.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;m_prodMerch;gpv_v9;bm_sz;s_sq;_ga_ZLYRBY87M8;mbox;akaalb_m_kohls_com;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC", END_INLINE,
            "URL=https://m.kohls.com/dist/recomendation/recomendations-BD-bundle-rev-045c91074147e0ace9be.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;m_prodMerch;gpv_v9;bm_sz;s_sq;_ga_ZLYRBY87M8;mbox;akaalb_m_kohls_com;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC", END_INLINE,
            "URL=https://m.kohls.com/dist/personalization/personalization-bundle-rev-ff45f0763a8dbe15b1c6.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;m_prodMerch;gpv_v9;bm_sz;s_sq;_ga_ZLYRBY87M8;mbox;akaalb_m_kohls_com;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC", END_INLINE,
            "URL=https://m.kohls.com/dist/homepage/home-page-deferred-bundle-rev-d8ee69f049d96f0c57c7.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;m_prodMerch;gpv_v9;bm_sz;s_sq;_ga_ZLYRBY87M8;mbox;akaalb_m_kohls_com;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC", END_INLINE,
            "URL=https://m.kohls.com/dist/pixelTracking/pixelTracking-bundle-rev-e9d590fc2f8aca922ba6.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;m_prodMerch;gpv_v9;bm_sz;s_sq;_ga_ZLYRBY87M8;mbox;akaalb_m_kohls_com;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC", END_INLINE,
            "URL=https://m.kohls.com/dist/footer/app-bundle-rev-f65e3390f830c3f87c72.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;m_prodMerch;gpv_v9;bm_sz;s_sq;_ga_ZLYRBY87M8;mbox;akaalb_m_kohls_com;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC6f229984ef444c0cbd04f0d3a22f28f7-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("uia_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("batch_3");
    ns_web_url ("batch_3",
        "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1662113069160_886874",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=DYID;DYSES",
        BODY_BEGIN,
            "{"md":{"uid":"-1233986090078972675","sec":"8776374","ses":"5b3e117ce067eeb99b2b5b268b908b68","colors":"d.an.c.ms.","sld":"kohls.com","rurl":"https://m.kohls.com/","ts":"Direct","p":"2","ref":"internal","rref":"https://m.kohls.com/","reqts":1662113068160,"rri":4768865},"data":[{"type":"imp","expId":1179251,"varIds":[26647761],"expSes":"71849","mech":"1","smech":null,"verId":"11202288","aud":"1408117.1438654.1362538.1362542","expVisitId":"-5734382000772232028","eri":null},{"type":"imp","expId":1245855,"varIds":[27049604],"expSes":"71849","mech":"1","smech":null,"verId":"11044797","aud":"1408117.1438654.1362538.1362542","expVisitId":"-5734382003904465510","eri":null},{"type":"imp","expId":1289933,"varIds":[27236876],"expSes":"71849","mech":"1","smech":null,"verId":"11374557","aud":"1408117.1438654.1362538.1362542","expVisitId":"-5734382000036239809","eri":null},{"type":"imp","expId":1309946,"varIds":[27308034],"expSes":"71849","mech":"1","smech":null,"verId":"11499500","aud":"1408117.1438654.1362538.1362542","expVisitId":"-5734382000503099902","eri":null}],"sectionId":"8776374"}",
        BODY_END
    );

    ns_end_transaction("batch_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("manifest_json_2");
    ns_web_url ("manifest_json_2",
        "URL=https://m.kohls.com/manifest.json",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://c.tvpixel.com/js/current/dpm_pixel_min.js?aid=kohls-0b87d80d-1f10-41e1-9b1a-e53161757636", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=sp", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RCefe32536577a4558abe3c8dcd8da7ca6-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://m.kohls.com/dist/libs/z1m_v4.192.js?v=a42f899823411882d6a96648c3627159", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;m_prodMerch;gpv_v9;bm_sz;s_sq;_ga_ZLYRBY87M8;mbox;akaalb_m_kohls_com;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC", END_INLINE
    );

    ns_end_transaction("manifest_json_2", NS_AUTO_STATUS);

    //Page Auto split for Ajax Header = XMLHttpRequest
    ns_start_transaction("s_code_js_2");
    ns_web_url ("s_code_js_2",
        "URL=https://m.kohls.com/lib/s_code.js",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-Requested-With:XMLHttpRequest",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;gpv_v9;bm_sz;s_sq;_ga_ZLYRBY87M8;mbox;akaalb_m_kohls_com;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC;m_prodMerch",
        INLINE_URLS,
            "URL=https://m.kohls.com/dist/global/nudatainitialize-bundle-rev-17d1ecd218000f105eba.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;gpv_v9;bm_sz;s_sq;_ga_ZLYRBY87M8;mbox;akaalb_m_kohls_com;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC;m_prodMerch", END_INLINE,
            "URL=https://js-sec.indexww.com/ht/p/184399-39002588238727.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagservices.com/tag/js/gpt.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("s_code_js_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("clog_2");
    ns_web_url ("clog_2",
        "URL=https://px.dynamicyield.com/clog",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "[{"name":"getProductInterest func error","section":"8776374","message":"DYID:-1233986090078972675,Message: Cannot read property 'weekly' of undefined","stack":"TypeError: Cannot read property 'weekly' of undefined\n    at i (https://cdn.dynamicyield.com/api/8776374/api_static.js:9:5945)\n    at h (https://cdn.dynamicyield.com/api/8776374/api_static.js:9:5830)\n    at purchase (https://cdn.dynamicyield.com/api/8776374/api_static.js:9:6635)\n    at Object.asyncMatchNow (https://cdn.dynamicyield.com/api/8776374/api_static.js:5:31945)\n    at s (https://cdn.dynamicyield.com/api/8776374/api_static.js:9:22269)\n    at https://cdn.dynamicyield.com/api/8776374/api_static.js:9:22483\n    at Array.map (<anonymous>)\n    at Ba (https://cdn.dynamicyield.com/api/8776374/api_static.js:10:19943)\n    at u (https://cdn.dynamicyield.com/api/8776374/api_static.js:9:22456)"}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://m.kohls.com/lib/recommendation/mcom/bd-experience-rendering-sdk.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;gpv_v9;bm_sz;s_sq;_ga_ZLYRBY87M8;mbox;akaalb_m_kohls_com;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC;m_prodMerch", END_INLINE,
            "URL=https://m.kohls.com/dist/css/recomendation/recomend-mcom-css-bundle-rev-18b9a5e7143933eb2f20ec8ef3b7485a.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;gpv_v9;bm_sz;s_sq;_ga_ZLYRBY87M8;mbox;akaalb_m_kohls_com;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC;m_prodMerch", END_INLINE,
            "URL=https://cdns.brsrvr.com/v1/br-trk-5117.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s91741427273748?AQB=1&ndh=1&pf=1&callback=s_c_il[1].doPostbacks&et=1&t=2%2F8%2F2022%205%3A4%3A29%205%20300&d.&nsid=0&jsonv=1&.d&sdid=2C8FA7DC757DC478-1B32B8F6CFACCF4A&mid=78611155637513935743550952407821114794&aamlh=7&ce=UTF-8&ns=kohls&pageName=homepage&g=https%3A%2F%2Fm.kohls.com%2F&r=https%3A%2F%2Fm.kohls.com%2F&c.&k.&pageDomain=m.kohls.com&.k&mcid.&version=4.3.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&products=%3Bproductmerch3&aamb=j8Odv6LonN4r3an7LhD3WZrU1bUpAkFkkiY1ncBR96t2PTI&tnt=557254%3A0%3A0%2C543394%3A1%3A0%2C531276%3A0%3A0%2C567511%3A1%3A0%2C567583%3A1%3A0%2C555886%3A1%3A0%2C548781%3A0%3A0%2C526083%3A0%3A0%2C&c4=homepage&c9=homepage%7Chomepage&v9=m%3Eshopmenu&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=D%3Dv18&v18=fri%7Cweekday%7C05%3A00%20am&c22=2022-09-02&v22=mobile&v39=no%20customer%20id&v40=mcom17&v42=no%20cart&c50=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d%7C05%3A00%20am&c53=homepage&c63=mcom-46f4352e4e7e888ac93c8b7b-b713fc26a3aca27d&c64=VisitorAPI%20Present&v68=homepage&v71=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&v73=no%20loyalty%20id&v86=21_7&v87=hp19&c.&a.&activitymap.&page=m%3Eshopmenu&link=Home&region=home-menu-new&pageIDType=1&.activitymap&.a&.c&pid=m%3Eshopmenu&pidt=1&oid=https%3A%2F%2Fm.kohls.com%2F&ot=A&s=1313x489&c=24&j=1.6&v=N&k=Y&bw=1313&bh=361&AQE=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;AKA_A2;check;rxVisitor;s_ecid;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;_dpm_id.f2d1;s_cc;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;bm_sz;_ga_ZLYRBY87M8;mbox;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC;gpv_v9;s_sq", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC3c54cc4632f0434dae1a686344741ddc-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://s.yimg.com/wi/ytc.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://fc.kohls.com/2.2/w/w-756138/sync/js/", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;AKA_A2;check;rxVisitor;s_ecid;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;s_cc;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;bm_sz;_ga_ZLYRBY87M8;mbox;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC;gpv_v9;s_sq;_dpm_id.f2d1", END_INLINE
    );

    ns_end_transaction("clog_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("tp2_2");
    ns_web_url ("tp2_2",
        "URL=https://p.tvpixel.com/com.snowplowanalytics.snowplow/tp2",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=sp",
        BODY_BEGIN,
            "{"schema":"iglu:com.snowplowanalytics.snowplow/payload_data/jsonschema/1-0-4","data":[{"e":"pv","url":"https://m.kohls.com/","page":"Kohl's | Shop Clothing, Shoes, Home, Kitchen, Bedding, Toys & More","refr":"https://m.kohls.com/","tv":"js-2.14.0","tna":"co","aid":"kohls-0b87d80d-1f10-41e1-9b1a-e53161757636","p":"web","tz":"America/Chicago","lang":"en-US","cs":"UTF-8","res":"1313x489","cd":"24","cookie":"1","eid":"7848ed04-06f5-4974-a26e-6ef201cc2345","dtm":"1662113069459","cx":"eyJzY2hlbWEiOiJpZ2x1OmNvbS5zbm93cGxvd2FuYWx5dGljcy5zbm93cGxvdy9jb250ZXh0cy9qc29uc2NoZW1hLzEtMC0wIiwiZGF0YSI6W3sic2NoZW1hIjoiaWdsdTpjb20uc25vd3Bsb3dhbmFseXRpY3Muc25vd3Bsb3cvd2ViX3BhZ2UvanNvbnNjaGVtYS8xLTAtMCIsImRhdGEiOnsiaWQiOiI5ZTAxYTMwOS0zZjhkLTRhYWEtOGQ2NS02NTY5YzU2NjgzMTQifX0seyJzY2hlbWEiOiJpZ2x1Om9yZy53My9QZXJmb3JtYW5jZVRpbWluZy9qc29uc2NoZW1hLzEtMC0wIiwiZGF0YSI6eyJuYXZpZ2F0aW9uU3RhcnQiOjE2NjIxMTMwNjcyMzksInVubG9hZEV2ZW50U3RhcnQiOjE2NjIxMTMwNjczMTMsInVubG9hZEV2ZW50RW5kIjoxNjYyMTEzMDY3MzE1LCJyZWRpcmVjdFN0YXJ0IjowLCJyZWRpcmVjdEVuZCI6MCwiZmV0Y2hTdGFydCI6MTY2MjExMzA2NzI0NCwiZG9tYWluTG9va3VwU3RhcnQiOjE2NjIxMTMwNjcyNDQsImRvbWFpbkxvb2t1cEVuZCI6MTY2MjExMzA2NzI0NCwiY29ubmVjdFN0YXJ0IjoxNjYyMTEzMDY3MjQ0LCJjb25uZWN0RW5kIjoxNjYyMTEzMDY3MjQ0LCJzZWN1cmVDb25uZWN0aW9uU3RhcnQiOjAsInJlcXVlc3RTdGFydCI6MTY2MjExMzA2NzI1MCwicmVzcG9uc2VTdGFydCI6MTY2MjExMzA2NzI1MSwicmVzcG9uc2VFbmQiOjE2NjIxMTMwNjcyNjYsImRvbUxvYWRpbmciOjE2NjIxMTMwNjczMzIsImRvbUludGVyYWN0aXZlIjoxNjYyMTEzMDY4NTIzLCJkb21Db250ZW50TG9hZGVkRXZlbnRTdGFydCI6MTY2MjExMzA2ODUyNCwiZG9tQ29udGVudExvYWRlZEV2ZW50RW5kIjoxNjYyMTEzMDY4NTI2LCJkb21Db21wbGV0ZSI6MTY2MjExMzA2OTA4NSwibG9hZEV2ZW50U3RhcnQiOjE2NjIxMTMwNjkwODgsImxvYWRFdmVudEVuZCI6MTY2MjExMzA2OTEwOX19XX0","vp":"1313x361","ds":"1298x14478","vid":"1","sid":"fdcd708f-da42-48fb-bc5f-140de4e1a196","duid":"60ed2d0e-ab34-4988-960e-416c631cdea4","stm":"1662113069465"}]}",
        BODY_END,
        INLINE_URLS,
            "URL=https://securepubads.g.doubleclick.net/gpt/pubads_impl_2022083101.js?cb=31069285", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE
    );

    ns_end_transaction("tp2_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("ppub_config_2");
    ns_web_url ("ppub_config_2",
        "URL=https://securepubads.g.doubleclick.net/pagead/ppub_config?ippd=m.kohls.com",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("ppub_config_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("floop_5");
    ns_web_url ("floop_5",
        "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;AKA_A2;check;rxVisitor;s_ecid;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;s_cc;__gads;__gpi;ndcd;178a2b8e00fcaafe5a98fe75be8fb9f3;akacd_BD_ECS_ELB2;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;bm_sz;_ga_ZLYRBY87M8;mbox;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC;gpv_v9;s_sq;_dpm_id.f2d1",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/floop_5_main_url_1_1662113085118.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://sb.scorecardresearch.com/beacon.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=UID", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RCc702e2fb1b91495f93e33f0ce4677284-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("floop_5", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("X0172875_json_2");
    ns_web_url ("X0172875_json_2",
        "URL=https://s.yimg.com/wi/config/10172875.json",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://kohls.demdex.net/dest5.html?d_nsid=0#https%3A%2F%2Fm.kohls.com", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=dextp", END_INLINE
    );

    ns_end_transaction("X0172875_json_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("experiences_2");
    ns_web_url ("experiences_2",
        "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=MCom&pgid=Home&plids=RedesignHP1%7C12%2CRedesignHP2%7C12",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-APP-API_KEY:WOBxT3NyxFvOlrFgTMTnJEtzXzqAtGLl",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"cookieId":"78611155637513935743550952407821114794","mcmId":"78611155637513935743550952407821114794"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://fc.kohls.com/2.2/w/w-756138/init/js/?q=%7B%22e%22%3A757322%2C%22fvq%22%3A%2281p65ns1-93sp-29o6-9q94-30n1n0r37onr1662113023400%22%2C%22oq%22%3A%221313%3A361%3A1313%3A361%3A1313%3A489%22%2C%22wfi%22%3A%22flap-152535%22%2C%22yf%22%3A%7B%7D%2C%22jc%22%3A%22ZPBZYbtva%22%2C%22ov%22%3A%22o2%7C1313k489%201313k489%2024%2024%7C360%7Cra-HF%7Coc1-700%7Csnyfr%7Cuggcf%3A%2F%2Fz.xbuyf.pbz%2F%7CZbmvyyn%2F5.0%20(Yvahk%3B%20Naqebvq%206.0.1%3B%20Zbgb%20T%20(4)%20Ohvyq%2FZCW24.139-64)%20NccyrJroXvg%2F537.36%20(XUGZY%2C%20yvxr%20Trpxb)%20Puebzr%2F58.0.3029.81%20Zbovyr%20Fnsnev%2F537.36%20CGFG%2F1%7Cjt1-78r9qs3735260548%22%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;AKA_A2;check;rxVisitor;s_ecid;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;s_cc;__gads;__gpi;ndcd;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;bm_sz;_ga_ZLYRBY87M8;mbox;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC;gpv_v9;s_sq;_dpm_id.f2d1", END_INLINE
    );

    ns_end_transaction("experiences_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("bidRequest_4");
    ns_web_url ("bidRequest_4",
        "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2ba9460000f&pos=m_btf_bottom_320x50&secure=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=A3"
    );

    ns_end_transaction("bidRequest_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("cygnus_4");
    ns_web_url ("cygnus_4",
        "URL=https://htlb.casalemedia.com/cygnus?s=189641",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"id":"74338773","site":{"page":"https://m.kohls.com/","ref":"https://m.kohls.com/"},"imp":[{"banner":{"topframe":1,"format":[{"w":320,"h":50,"ext":{"sid":"1","siteID":"269251"}}]},"id":"1"}],"ext":{"source":"ixwrapper"},"user":{"eids":[{"source":"adserver.org","uids":[{"id":"d75ee8f5-8352-4a1a-9ab4-61baaba6080a","ext":{"rtiPartner":"TDID"}},{"id":"FALSE","ext":{"rtiPartner":"TDID_LOOKUP"}},{"id":"2022-09-02T10:03:43","ext":{"rtiPartner":"TDID_CREATED_AT"}}]}]},"at":1}",
        BODY_END
    );

    ns_end_transaction("cygnus_4", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("auction_4");
    ns_web_url ("auction_4",
        "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_m_bottom_listings_rec_header&lib=ix&size=320x50&referrer=https%3A%2F%2Fm.kohls.com%2F&v=2.1.2&tmax=1200",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("auction_4", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("fastlane_json_4");
    ns_web_url ("fastlane_json_4",
        "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=43&rp_floor=0.01&rf=https%3A%2F%2Fm.kohls.com%2F&p_screen_res=1313x489&site_id=344110&zone_id=1817912&kw=rp.fastlane&tk_flint=index&rand=0.9311092059458226",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("fastlane_json_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("index_9");
    ns_web_url ("index_9",
        "URL=https://hb.emxdgt.com/?t=1200&ts=1662113069927",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"id":"wWQRAOak","imp":[{"id":"1","tagid":"79817","secure":1,"bidfloor":0.1,"banner":{"format":[{"w":320,"h":50}],"w":320,"h":50}}],"site":{"domain":"m.kohls.com","page":"https://m.kohls.com/","ref":"https://m.kohls.com/"},"ext":{"ver":"1.1.0"}}",
        BODY_END,
        INLINE_URLS,
            "URL=https://ap.lijit.com/rtb/bid?callback=window.headertag.SovrnHtb.adResponseCallback&br=%7B%22id%22%3A%22_Pg2oqTKC%22%2C%22site%22%3A%7B%22domain%22%3A%22m.kohls.com%22%2C%22page%22%3A%22%2F%22%7D%2C%22imp%22%3A%5B%7B%22id%22%3A%22Erv4dULo%22%2C%22banner%22%3A%7B%22w%22%3A320%2C%22h%22%3A50%7D%2C%22tagid%22%3A%22788165%22%7D%5D%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=ljtrtb;ljt_reader", END_INLINE
    );

    ns_end_transaction("index_9", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("translator_4");
    ns_web_url ("translator_4",
        "URL=https://hbopenbid.pubmatic.com/translator?source=index-client",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"id":"1662113069932","at":1,"cur":["USD"],"imp":[{"id":"d5a7a79b-04c5-5901-56a5-0545e115160e","tagId":"3262361","secure":1,"ext":{},"banner":{"w":320,"h":50}}],"site":{"page":"https://m.kohls.com/","ref":"https://m.kohls.com/","publisher":{"id":"160059","domain":"m.kohls.com"},"domain":"m.kohls.com"},"device":{"ua":"Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1","js":1,"dnt":0,"h":489,"w":1313,"language":"en-US","geo":{}},"user":{"geo":{},"eids":[{"source":"adserver.org","uids":[{"id":"d75ee8f5-8352-4a1a-9ab4-61baaba6080a","ext":{"rtiPartner":"TDID"}},{"id":"FALSE","ext":{"rtiPartner":"TDID_LOOKUP"}},{"id":"2022-09-02T10:03:43","ext":{"rtiPartner":"TDID_CREATED_AT"}}]}]},"ext":{"wrapper":{"wp":"ixjs"}}}",
        BODY_END
    );

    ns_end_transaction("translator_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("prebid_4");
    ns_web_url ("prebid_4",
        "URL=https://ib.adnxs.com/ut/v3/prebid",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=icu;uuid2",
        BODY_BEGIN,
            "{"sdk":{"version":"2.4.1"},"hb_source":2,"referrer_detection":{"rd_ifs":null,"rd_ref":"https%3A%2F%2Fm.kohls.com%2F","rd_stk":"https%3A%2F%2Fm.kohls.com%2F","rd_top":null},"tags":[{"ad_types":["banner"],"allow_smaller_sizes":false,"disable_psa":true,"id":20889775,"prebid":true,"primary_size":{"width":320,"height":50},"sizes":[{"width":320,"height":50}],"use_pmt_rule":false,"uuid":"Svy480cn3eeXDy"}]}",
        BODY_END
    );

    ns_end_transaction("prebid_4", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("fastlane_json_5");
    ns_web_url ("fastlane_json_5",
        "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=43&rp_floor=0.01&rf=https%3A%2F%2Fm.kohls.com%2F&p_screen_res=1313x489&site_id=344110&zone_id=1817918&kw=rp.fastlane&tk_flint=index&rand=0.7559034894017624",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("fastlane_json_5", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("auction_5");
    ns_web_url ("auction_5",
        "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_m_top_listings_rec_header&lib=ix&size=320x50&referrer=https%3A%2F%2Fm.kohls.com%2F&v=2.1.2&tmax=1200",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://ap.lijit.com/rtb/bid?callback=window.headertag.SovrnHtb.adResponseCallback&br=%7B%22id%22%3A%22_FRacBqLp%22%2C%22site%22%3A%7B%22domain%22%3A%22m.kohls.com%22%2C%22page%22%3A%22%2F%22%7D%2C%22imp%22%3A%5B%7B%22id%22%3A%22wqBQuWnn%22%2C%22banner%22%3A%7B%22w%22%3A320%2C%22h%22%3A50%7D%2C%22tagid%22%3A%22788167%22%7D%5D%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=ljtrtb;ljt_reader", END_INLINE
    );

    ns_end_transaction("auction_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("index_10");
    ns_web_url ("index_10",
        "URL=https://hb.emxdgt.com/?t=1200&ts=1662113069955",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"id":"xxn68Mho","imp":[{"id":"3","tagid":"79819","secure":1,"bidfloor":0.1,"banner":{"format":[{"w":320,"h":50}],"w":320,"h":50}}],"site":{"domain":"m.kohls.com","page":"https://m.kohls.com/","ref":"https://m.kohls.com/"},"ext":{"ver":"1.1.0"}}",
        BODY_END
    );

    ns_end_transaction("index_10", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("bidRequest_5");
    ns_web_url ("bidRequest_5",
        "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2ba9460000f&pos=m_btf_morebottom_320x50&secure=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=A3"
    );

    ns_end_transaction("bidRequest_5", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("fastlane_json_6");
    ns_web_url ("fastlane_json_6",
        "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=43&rp_floor=0.01&rf=https%3A%2F%2Fm.kohls.com%2F&p_screen_res=1313x489&site_id=344110&zone_id=1817916&kw=rp.fastlane&tk_flint=index&rand=0.5891296364984071",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("fastlane_json_6", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("auction_6");
    ns_web_url ("auction_6",
        "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_m_middle_listings_rec_header&lib=ix&size=320x50&referrer=https%3A%2F%2Fm.kohls.com%2F&v=2.1.2&tmax=1200",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("auction_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("index_11");
    ns_web_url ("index_11",
        "URL=https://hb.emxdgt.com/?t=1200&ts=1662113069966",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"id":"skcFhgXL","imp":[{"id":"2","tagid":"79818","secure":1,"bidfloor":0.1,"banner":{"format":[{"w":320,"h":50}],"w":320,"h":50}}],"site":{"domain":"m.kohls.com","page":"https://m.kohls.com/","ref":"https://m.kohls.com/"},"ext":{"ver":"1.1.0"}}",
        BODY_END
    );

    ns_end_transaction("index_11", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("bidRequest_6");
    ns_web_url ("bidRequest_6",
        "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2ba9460000f&pos=m_btf_middle_320x50&secure=1",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=A3",
        INLINE_URLS,
            "URL=https://ap.lijit.com/rtb/bid?callback=window.headertag.SovrnHtb.adResponseCallback&br=%7B%22id%22%3A%22_eGhbUnG2%22%2C%22site%22%3A%7B%22domain%22%3A%22m.kohls.com%22%2C%22page%22%3A%22%2F%22%7D%2C%22imp%22%3A%5B%7B%22id%22%3A%22wG4JsJ7a%22%2C%22banner%22%3A%7B%22w%22%3A320%2C%22h%22%3A50%7D%2C%22tagid%22%3A%22788166%22%7D%5D%7D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=ljtrtb;ljt_reader", END_INLINE,
            "URL=https://adservice.google.com/adsid/integrator.js?domain=m.kohls.com", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("bidRequest_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("prebid_5");
    ns_web_url ("prebid_5",
        "URL=https://ib.adnxs.com/ut/v3/prebid",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=icu;uuid2",
        BODY_BEGIN,
            "{"sdk":{"version":"2.4.1"},"hb_source":2,"referrer_detection":{"rd_ifs":null,"rd_ref":"https%3A%2F%2Fm.kohls.com%2F","rd_stk":"https%3A%2F%2Fm.kohls.com%2F","rd_top":null},"tags":[{"ad_types":["banner"],"allow_smaller_sizes":false,"disable_psa":true,"id":20889777,"prebid":true,"primary_size":{"width":320,"height":50},"sizes":[{"width":320,"height":50}],"use_pmt_rule":false,"uuid":"iR0lm9wHl6nDNv"}]}",
        BODY_END
    );

    ns_end_transaction("prebid_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("prebid_6");
    ns_web_url ("prebid_6",
        "URL=https://ib.adnxs.com/ut/v3/prebid",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=icu;uuid2",
        BODY_BEGIN,
            "{"sdk":{"version":"2.4.1"},"hb_source":2,"referrer_detection":{"rd_ifs":null,"rd_ref":"https%3A%2F%2Fm.kohls.com%2F","rd_stk":"https%3A%2F%2Fm.kohls.com%2F","rd_top":null},"tags":[{"ad_types":["banner"],"allow_smaller_sizes":false,"disable_psa":true,"id":20889776,"prebid":true,"primary_size":{"width":320,"height":50},"sizes":[{"width":320,"height":50}],"use_pmt_rule":false,"uuid":"dLf9q3D9EN6GvH"}]}",
        BODY_END,
        INLINE_URLS,
            "URL=https://securepubads.g.doubleclick.net/gampad/ads?pvsid=556331592190720&correlator=2450439268741249&eid=31069183%2C31069285%2C31069289&output=ldjh&gdfp_req=1&vrg=2022083101&ptt=17&impl=fifs&iu_parts=17763952%2Chomepage&enc_prev_ius=%2F0%2F1&prev_iu_szs=320x50%7C320x550&fluid=height&ifi=1&adks=2351561951&sfv=1-0-38&fsapi=false&prev_scp=pos%3DHP_Mobile_Top_Super_Bulletin%26env%3Dprod%26channel%3Dmobile&eri=1&cust_params=pgtype%3Dhome&sc=1&cookie=ID%3D3f5222e9b94a8872%3AT%3D1662113022%3AS%3DALNI_MYK01WomvsjECicEXFc9qIJqjXnpA&gpic=UID%3D00000872ab15fe48%3AT%3D1662113022%3ART%3D1662113022%3AS%3DALNI_MZecWrmURrSjs3dbQD9hWjY5SQ45Q&abxe=1&dt=1662113070019&lmt=1662113070&dlt=1662113067332&idt=2558&adxs=0&adys=8024&biw=1298&bih=361&scr_x=0&scr_y=0&btvi=1&ucis=1&oid=2&u_his=2&u_h=489&u_w=1313&u_ah=489&u_aw=1313&u_cd=24&u_sd=1&u_tz=-300&dmc=8&bc=31&uach=WyJMaW51eCIsIiIsIng4NiIsIiIsIjkwLjAuNDQzMC42MSIsW10sZmFsc2UsbnVsbCwiIixbXSxmYWxzZV0.&nvt=1&url=https%3A%2F%2Fm.kohls.com%2F&ref=https%3A%2F%2Fm.kohls.com%2F&frm=20&vis=1&psz=1298x0&msz=1298x6&fws=132&ohw=1298&ga_vid=1350227648.1662113023&ga_sid=1662113070&ga_hid=2024804943&ga_fc=true", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://securepubads.g.doubleclick.net/gampad/ads?pvsid=556331592190720&correlator=4278610482280543&eid=31069183%2C31069285%2C31069289&output=ldjh&gdfp_req=1&vrg=2022083101&ptt=17&impl=fifs&iu_parts=17763952%2Chomepage&enc_prev_ius=%2F0%2F1&prev_iu_szs=320x50%7C320x550&fluid=height&ifi=2&adks=2351561788&sfv=1-0-38&fsapi=false&prev_scp=pos%3DHP_Mobile_Bottom_Super_Bulletin%26env%3Dprod%26channel%3Dmobile&eri=1&cust_params=pgtype%3Dhome&sc=1&cookie=ID%3D3f5222e9b94a8872%3AT%3D1662113022%3AS%3DALNI_MYK01WomvsjECicEXFc9qIJqjXnpA&gpic=UID%3D00000872ab15fe48%3AT%3D1662113022%3ART%3D1662113022%3AS%3DALNI_MZecWrmURrSjs3dbQD9hWjY5SQ45Q&abxe=1&dt=1662113070034&lmt=1662113070&dlt=1662113067332&idt=2558&adxs=0&adys=10447&biw=1298&bih=361&scr_x=0&scr_y=0&btvi=2&ucis=2&oid=2&u_his=2&u_h=489&u_w=1313&u_ah=489&u_aw=1313&u_cd=24&u_sd=1&u_tz=-300&dmc=8&bc=31&uach=WyJMaW51eCIsIiIsIng4NiIsIiIsIjkwLjAuNDQzMC42MSIsW10sZmFsc2UsbnVsbCwiIixbXSxmYWxzZV0.&nvt=1&url=https%3A%2F%2Fm.kohls.com%2F&ref=https%3A%2F%2Fm.kohls.com%2F&frm=20&vis=1&psz=1298x13083&msz=1298x6&fws=132&ohw=1298&ga_vid=1350227648.1662113023&ga_sid=1662113070&ga_hid=2024804943&ga_fc=true", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE
    );

    ns_end_transaction("prebid_6", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("sodar_2");
    ns_web_url ("sodar_2",
        "URL=https://pagead2.googlesyndication.com/getconfig/sodar?sv=200&tid=gpt&tv=2022083101&st=env",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://69ec061b93b05c1ecc68753ed6de0423.safeframe.googlesyndication.com/safeframe/1-0-38/html/container.html", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sp.analytics.yahoo.com/sp.pl?a=10000&d=Fri%2C%2002%20Sep%202022%2010%3A04%3A30%20GMT&n=5d&b=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&.yp=10172875&f=https%3A%2F%2Fm.kohls.com%2F&e=https%3A%2F%2Fm.kohls.com%2F&enc=UTF-8&yv=1.13.0&tagmgr=adobe", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=A3", END_INLINE,
            "URL=https://sp.analytics.yahoo.com/sp.pl?a=10000&b=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&.yp=10172875&f=https%3A%2F%2Fm.kohls.com%2F&e=https%3A%2F%2Fm.kohls.com%2F&enc=UTF-8&yv=1.13.0&et=custom&ea=&ev=&gv=&product_sku=not%20set&product_id=not%20set&conversion_product_name=not%20set&conversion_sku=not%20set&conversion_product_id=not%20set&conversion_revenue=not%20set&tagmgr=adobe", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=A3", END_INLINE
    );

    ns_end_transaction("sodar_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("cygnus_5");
    ns_web_url ("cygnus_5",
        "URL=https://htlb.casalemedia.com/cygnus?s=189641",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"id":"64795044","site":{"page":"https://m.kohls.com/","ref":"https://m.kohls.com/"},"imp":[{"banner":{"topframe":1,"format":[{"w":320,"h":50,"ext":{"sid":"3","siteID":"269250"}}]},"id":"1"}],"ext":{"source":"ixwrapper"},"user":{"eids":[{"source":"adserver.org","uids":[{"id":"d75ee8f5-8352-4a1a-9ab4-61baaba6080a","ext":{"rtiPartner":"TDID"}},{"id":"FALSE","ext":{"rtiPartner":"TDID_LOOKUP"}},{"id":"2022-09-02T10:03:43","ext":{"rtiPartner":"TDID_CREATED_AT"}}]}]},"at":1}",
        BODY_END
    );

    ns_end_transaction("cygnus_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("cygnus_6");
    ns_web_url ("cygnus_6",
        "URL=https://htlb.casalemedia.com/cygnus?s=189641",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"id":"02017131","site":{"page":"https://m.kohls.com/","ref":"https://m.kohls.com/"},"imp":[{"banner":{"topframe":1,"format":[{"w":320,"h":50,"ext":{"sid":"2","siteID":"269249"}}]},"id":"1"}],"ext":{"source":"ixwrapper"},"user":{"eids":[{"source":"adserver.org","uids":[{"id":"d75ee8f5-8352-4a1a-9ab4-61baaba6080a","ext":{"rtiPartner":"TDID"}},{"id":"FALSE","ext":{"rtiPartner":"TDID_LOOKUP"}},{"id":"2022-09-02T10:03:43","ext":{"rtiPartner":"TDID_CREATED_AT"}}]}]},"at":1}",
        BODY_END,
        INLINE_URLS,
            "URL=https://b-code.liadm.com/a-00oc.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC37be6c799a1548d8ba6c480cbbd4cac4-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://securepubads.g.doubleclick.net/gampad/ads?pvsid=556331592190720&correlator=1611472612085384&eid=31069183%2C31069285%2C31069289&output=ldjh&gdfp_req=1&vrg=2022083101&ptt=17&impl=fifs&iu_parts=17763952%2Chomepage&enc_prev_ius=%2F0%2F1&prev_iu_szs=320x50%7C320x50&fluid=height&ifi=3&adks=334370023&sfv=1-0-38&fsapi=false&prev_scp=pos%3Dbottom%26env%3Dprod%26channel%3Dmobile&eri=1&cust_params=pgtype%3Dhome&sc=1&cookie=ID%3D3f5222e9b94a8872%3AT%3D1662113022%3AS%3DALNI_MYK01WomvsjECicEXFc9qIJqjXnpA&gpic=UID%3D00000872ab15fe48%3AT%3D1662113022%3ART%3D1662113022%3AS%3DALNI_MZecWrmURrSjs3dbQD9hWjY5SQ45Q&abxe=1&dt=1662113070151&lmt=1662113070&dlt=1662113067332&idt=2558&adxs=0&adys=10447&biw=1298&bih=361&scr_x=0&scr_y=0&btvi=3&ucis=3&oid=2&u_his=2&u_h=489&u_w=1313&u_ah=489&u_aw=1313&u_cd=24&u_sd=1&u_tz=-300&dmc=8&bc=31&uach=WyJMaW51eCIsIiIsIng4NiIsIiIsIjkwLjAuNDQzMC42MSIsW10sZmFsc2UsbnVsbCwiIixbXSxmYWxzZV0.&nvt=1&url=https%3A%2F%2Fm.kohls.com%2F&ref=https%3A%2F%2Fm.kohls.com%2F&frm=20&vis=1&psz=1298x0&msz=1298x6&fws=132&ohw=1298&ga_vid=1350227648.1662113023&ga_sid=1662113070&ga_hid=2024804943&ga_fc=true", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://tpc.googlesyndication.com/sodar/sodar2.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?guid=ON&;script=0&data=aam=10263239", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://securepubads.g.doubleclick.net/gampad/ads?pvsid=556331592190720&correlator=3663313943661137&eid=31069183%2C31069285%2C31069289&output=ldjh&gdfp_req=1&vrg=2022083101&ptt=17&impl=fifs&iu_parts=17763952%2Chomepage&enc_prev_ius=%2F0%2F1&prev_iu_szs=320x50%7C320x50&fluid=height&ifi=4&adks=334369881&sfv=1-0-38&fsapi=false&prev_scp=pos%3Dtop%26env%3Dprod%26channel%3Dmobile&eri=1&cust_params=pgtype%3Dhome&sc=1&cookie=ID%3D3f5222e9b94a8872%3AT%3D1662113022%3AS%3DALNI_MYK01WomvsjECicEXFc9qIJqjXnpA&gpic=UID%3D00000872ab15fe48%3AT%3D1662113022%3ART%3D1662113022%3AS%3DALNI_MZecWrmURrSjs3dbQD9hWjY5SQ45Q&abxe=1&dt=1662113070177&lmt=1662113070&dlt=1662113067332&idt=2558&adxs=0&adys=10447&biw=1298&bih=361&scr_x=0&scr_y=0&btvi=4&ucis=4&oid=2&u_his=2&u_h=489&u_w=1313&u_ah=489&u_aw=1313&u_cd=24&u_sd=1&u_tz=-300&dmc=8&bc=31&uach=WyJMaW51eCIsIiIsIng4NiIsIiIsIjkwLjAuNDQzMC42MSIsW10sZmFsc2UsbnVsbCwiIixbXSxmYWxzZV0.&nvt=1&url=https%3A%2F%2Fm.kohls.com%2F&ref=https%3A%2F%2Fm.kohls.com%2F&frm=20&vis=1&psz=1298x0&msz=1298x6&fws=132&ohw=1298&ga_vid=1350227648.1662113023&ga_sid=1662113070&ga_hid=2024804943&ga_fc=true", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://securepubads.g.doubleclick.net/gampad/ads?pvsid=556331592190720&correlator=930581275844655&eid=31069183%2C31069285%2C31069289&output=ldjh&gdfp_req=1&vrg=2022083101&ptt=17&impl=fifs&iu_parts=17763952%2Chomepage&enc_prev_ius=%2F0%2F1&prev_iu_szs=320x50%7C320x50&fluid=height&ifi=5&adks=334369848&sfv=1-0-38&fsapi=false&prev_scp=pos%3Dmiddle%26env%3Dprod%26channel%3Dmobile&eri=1&cust_params=pgtype%3Dhome&sc=1&cookie=ID%3D3f5222e9b94a8872%3AT%3D1662113022%3AS%3DALNI_MYK01WomvsjECicEXFc9qIJqjXnpA&gpic=UID%3D00000872ab15fe48%3AT%3D1662113022%3ART%3D1662113022%3AS%3DALNI_MZecWrmURrSjs3dbQD9hWjY5SQ45Q&abxe=1&dt=1662113070194&lmt=1662113070&dlt=1662113067332&idt=2558&adxs=0&adys=10447&biw=1298&bih=361&scr_x=0&scr_y=0&btvi=5&ucis=5&oid=2&u_his=2&u_h=489&u_w=1313&u_ah=489&u_aw=1313&u_cd=24&u_sd=1&u_tz=-300&dmc=8&bc=31&uach=WyJMaW51eCIsIiIsIng4NiIsIiIsIjkwLjAuNDQzMC42MSIsW10sZmFsc2UsbnVsbCwiIixbXSxmYWxzZV0.&nvt=1&url=https%3A%2F%2Fm.kohls.com%2F&ref=https%3A%2F%2Fm.kohls.com%2F&frm=20&vis=1&psz=1298x0&msz=1298x6&fws=132&ohw=1298&ga_vid=1350227648.1662113023&ga_sid=1662113070&ga_hid=2024804943&ga_fc=true", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1018012790/?guid=ON&data=aam=10263239&is_vtc=1&random=4104193889", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("cygnus_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("floop_6");
    ns_web_url ("floop_6",
        "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;AKA_A2;check;rxVisitor;s_ecid;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;s_cc;__gads;__gpi;178a2b8e00fcaafe5a98fe75be8fb9f3;akacd_BD_ECS_ELB2;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;bm_sz;_ga_ZLYRBY87M8;mbox;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC;gpv_v9;s_sq;_dpm_id.f2d1;ndcd",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/floop_6_main_url_1_1662113085122.body",
        BODY_END
    );

    ns_end_transaction("floop_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("floop_7");
    ns_web_url ("floop_7",
        "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;AKA_A2;check;rxVisitor;s_ecid;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;s_cc;__gads;__gpi;178a2b8e00fcaafe5a98fe75be8fb9f3;akacd_BD_ECS_ELB2;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;bm_sz;_ga_ZLYRBY87M8;mbox;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC;gpv_v9;s_sq;_dpm_id.f2d1;ndcd",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/floop_7_main_url_1_1662113085123.body",
        BODY_END
    );

    ns_end_transaction("floop_7", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("floop_8");
    ns_web_url ("floop_8",
        "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;AKA_A2;check;rxVisitor;s_ecid;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;s_cc;__gads;__gpi;178a2b8e00fcaafe5a98fe75be8fb9f3;akacd_BD_ECS_ELB2;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;bm_sz;_ga_ZLYRBY87M8;mbox;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;rxvt;dtPC;gpv_v9;s_sq;_dpm_id.f2d1;ndcd",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/floop_8_main_url_1_1662113085123.body",
        BODY_END
    );

    ns_end_transaction("floop_8", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("bf_7");
    ns_web_url ("bf_7",
        "URL=https://bf27853irn.bf.dynatrace.com/bf?type=js3&sn=v_4_srv_4_sn_IV7VUE896KFTV6I5O956PBOGGHT16VT8_app-3A06d81dff759102c9_1_ol_0_perc_100000_mul_1&svrid=4&flavor=cors&vi=WTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0&modifiedSince=1662094650929&rf=https%3A%2F%2Fm.kohls.com%2F&bp=3&app=06d81dff759102c9&crc=2726199617&en=ldtlho0d&end=1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$a=d%7C-1%7CHome%7CC%7C-%7C113019999_200%7C1662113067167%7Chttps%3A%2F%2Fm.kohls.com%2F%7C%7C%7C%2F%7C1662113018422%2C1%7C1%7C_load_%7C_load_%7C-%7C1662113067240%7C1662113069183%7Cdn%7C3750%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%7Clr%7Chttps%3A%2F%2Fm.kohls.com%2F%2C2%7C9%7C_event_%7C1662113067240%7C_vc_%7CV%7C1943%5Epu%7CVCD%7C1887%7CVCDS%7C1%7CVCS%7C2084%7CVCO%7C3343%7CVCI%7C0%7CVE%7C1295%5Ep234%5Ep684%5Eps%5Es...span.dollar-sign%7CS%7C1196%2C2%7C10%7C_event_%7C1662113067240%7C_wv_%7ClcpE%7CIMG%7ClcpSel%7Csection.hp2-creative.gpo-0901__hero%3Epicture%3Afirst-child%3Eimg%3Anth-child%282%29%7ClcpS%7C324500%7ClcpT%7C594%7ClcpU%7Chttps%3A%2F%2Fmedia.kohlsimg.com%2Fis%2Fimage%2Fkohls%2Fhp-20220901-gpo-10off-d-a%7ClcpLT%7C542%7Cfcp%7C318%7Cfp%7C318%7Ccls%7C0.0847%7Clt%7C988%2C2%7C2%7Cx%7Cxhr%7Cx%7C1662113068213%7C1662113068503%7Cdn%7C1409%7Cxu%7C%2FlBzjNRQPfM%2FSq6c%2F1G5U4A%2FiXEDf6VLOw%2FYnB5eA%2FSm9pFh0%2FURUY%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%7Cxrt%7Cb1662113068221e0f0g0h0i0j0k4l159m162u1132v17w17T-1z11I1%7Cxcs%7C290%7Cxce%7C290%7Crc%7C201%2C2%7C3%7Cx%7Cxhr%7Cx%7C1662113068409%7C1662113068553%7Cdn%7C3724%7Cxu%7C%2Fapi%2Fbrowse%2Fv2%2Fbrowse%2Fcatalog%2Fcategory%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%7Cxrt%7Cb1662113068419e0f0g0h0i0j0k4l4m86v55102w581549T-2z11I1%7Cxcs%7C139%7Cxce%7C144%2C2%7C4%7Cx%7Cxhr%7Cx%7C1662113068435%7C1662113068732%7Cdn%7C3727%7Cxu%7C%2Fapi%2Fv1%2Fbrowse%2Fmonetization%2Fbanners%3Fchannel%3Dmobile%26pageType%3DhomePage%26vid%3D78611155637513935743550952407821114794%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%7Cxrt%7Cb1662113068442e0f0g0h0i0j0k3l189m193u1993v849w1673T-3z11I1%7Cxcs%7C297%7Cxce%7C297%7Crc%7C971%7Crm%7CXHR%20Canceled%2C2%7C5%7Cx%7Cxhr%7Cx%7C1662113068767%7C1662113068946%7Cdn%7C3736%7Cxu%7C%2FlBzjNRQPfM%2FSq6c%2F1G5U4A%2FiXEDf6VLOw%2FYnB5eA%2FSm9pFh0%2FURUY%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%7Cxrt%7Cb1662113068774e0f0g0h0i0j0k3l159m162u1131v17w17T-4z1I1%7Cxcs%7C179%7Cxce%7C179%7Crc%7C201%2C2%7C6%7Cx%7Cxhr%7Cx%7C1662113068812%7C1662113068930%7Cdn%7C3736%7Cxu%7Chttps%3A%2F%2Fc.go-mpulse.net%2Fapi%2Fconfig.json%3Fkey%3D4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T%26d%3Dm.kohls.com%26t%3D5540377%26v%3D1.720.0%26sl%3D0%26si%3Dc108fdad-ef20-423f-aee4-3211a03e39b0-rhkunf%26plugins%3DAK%5EcConfigOverride%5EcContinuity%5EcPageParams%5EcIFrameDelay%5EcAutoXHR%5EcSPA%5EcHistory%5EcAngular%5EcBackbone%5EcEmber%5EcRT%5EcCrossDomain%5EcBW%5EcPaintTiming%5EcNavigationTiming%5EcResourceTiming%5EcMemory%5EcCACHE_RELOAD%5EcErrors%5EcTPAnalytics%5EcUserTiming%5EcAkamai%5EcEarly%5EcEventTiming%5EcLOGN%26acao%3D%26ak.ai%3D223330%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%7Cxrt%7Cb1662113068818e0f0g0h0i0j0k3l90m91u176T-5z11I1%7Cxcs%7C118%7Cxce%7C118%7Crc%7C204%7Crm%7CNo%20Content%2C2%7C7%7C_onload_%7C_load_%7C-%7C1662113069089%7C1662113069110%7Cdn%7C3750%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%2C1%7C11%7C_event_%7C1662113067240%7C_view_%7Csvn%7C%2F%7Csvt%7C1662113018422%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0$dO=m.kohls.com,$rId=RID_2418$rpId=$domR=1662113069085$tvn=%2F$tvt=1662113067240$tvm=i1%3Bk0%3Bh0$tvtrg=1$w=1313$h=361$sw=1313$sh=489$nt=a0b1662113067240e5f5g5h5i5j5k11l12m27o1284p1284q1286r1845s1849t1870v118756w513062M-576870371V0$ni=4g|10$fd=j3.1.1^sr15.4.2$md=mdcc1%2Chttps%3A%2F%2Fm.kohls.com%2F%3Bmdcc2%2CMozilla%2F5.0%20(Linux%5Es%20Android%206.0.1%5Es%20Moto%20G%20(4)%20Build%2FMPJ24.139-64)%20AppleWebKit%2F537.36%20(KHTML%5Ec%20like%20Gecko)%20Chrome%2F58.0.3029.81%20Mobile%20Safari%2F537.36%20PTST%2F1$url=https%3A%2F%2Fm.kohls.com%2F$title=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More$latC=5$app=06d81dff759102c9$vi=WTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0$fId=113068061_316$v=10247220811100421$vID=1662113020005SBVNC9V48OJOAGOMH3HKO8K0MK5NUIMM$time=1662113071238",
        BODY_END
    );

    ns_end_transaction("bf_7", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("URUY_8");
    ns_web_url ("URUY_8",
        "URL=https://m.kohls.com/lBzjNRQPfM/Sq6c/1G5U4A/iXEDf6VLOw/YnB5eA/Sm9pFh0/URUY",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=x-dtpc:4$113068061_316h8vWTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0e0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;X-APP-API_ZIP;AKA_RV;AKA_RV2;AKA_RV3;AKA_RV4;AKA_HP2;AKA_PDP2;AKA_CNC2;AKA_PMP2;AKA_PDP3;AKA_A2;akavpau_msite;check;akacd_RWASP-default-phased-release;rxVisitor;s_ecid;k_deviceId;Correlation-Id;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_mibhv;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;m_deviceID;s_cc;aam_uuid;nuDataSessionId;__gads;__gpi;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_cs_s;_rdt_uuid;IR_gbd;IR_5349;IR_PI;_pin_unauth;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;bm_sz;_ga_ZLYRBY87M8;mbox;dtLatC;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_abck;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;m_prodMerch;gpv_v9;s_sq;_dpm_id.f2d1;ndcd;rxvt;dtPC",
        BODY_BEGIN,
            "{"sensor_data":"7a74G7m23Vrp0o5c9368031.75-1,2,-94,-100,Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1,uaend,12147,20030107,en-US,Gecko,0,0,0,0,408958,3068186,1313,489,1313,489,1313,361,1313,,cpen:0,i1:0,dm:0,cwen:0,non:1,opc:0,fc:0,sc:0,wrc:1,isc:0,vib:1,bat:1,x11:0,x12:1,11023,0.274893735137,831056534092.5,0,loc:-1,2,-94,-131,-1,2,-94,-101,do_en,dm_en,t_en-1,2,-94,-105,0,0,0,0,1708,-1,0;-1,2,-94,-102,0,0,0,0,1708,-1,0;-1,2,-94,-108,-1,2,-94,-110,0,1,600,293,160;1,1,741,569,57;2,1,1550,1291,12;3,1,1580,1295,16;4,1,1594,1297,17;5,1,1884,1306,21;6,3,2388,1306,21,-1;-1,2,-94,-117,-1,2,-94,-111,-1,2,-94,-109,-1,2,-94,-114,-1,2,-94,-103,3,1308;-1,2,-94,-112,https://m.kohls.com/-1,2,-94,-115,1,18060,32,0,0,0,18028,2388,0,1662113068185,10,17780,0,7,2963,1,0,2390,10337,0,B6A6969A0C7F79D7A3819FC0D8E40EDA~-1~YAAQEzPdFxe3UPqCAQAAc7eo/QiVLSXwOxhVu7R2DfnkaVLgg+AHlD1dzcfmTu4HDfv+hzG+805GRHQsoOh4xktoIuwJwAjh09Eap6PHvmbOLjcA3hkUwUw1B2C3fstJUR9mVuJRpHbnt94B+AuLafVo/vMvLmpkro3pjFs18ezK93TVCKLCA87TIORTxgj58RNxxBAZ6KIzT0RU0GZtMXs7dV+WG07b9Iht2t9YO2KbZbr7uXmtaym+FZKHxIY5XghfxAGsZ+Z3ZcZrHv6TBxqOwXAJCJGNROXrJwmYjDfCQvGZF/zeBsO3cmeqKkmMkp40FJ4rZxLwXi7z4+Q/kJRAZxefQWHg8Df3R6OdDYcV78JWN6zqOUajgo2KhwA8jZrc+QW8mOP+R+Flt2ZGCvJRDmNbzx2l/DZyuH6Ro39xT/Ts60dC+ll/0DqKZl1Zjpput6BBQNwMHWC5VOatq0N9WjRpq7wdS4ALOCHp5jK1qQkgjhThj4VinnkHq9ntBwOQsP6LEpgi8EXvY11NqQcFSKWidhCnU4NX~-1~-1~-1,49654,426,434296941,30261689,PiZtE,101879,22,0,-1-1,2,-94,-106,1,2-1,2,-94,-119,-1-1,2,-94,-122,0,0,0,0,1,0,0-1,2,-94,-123,-1,2,-94,-124,-1,2,-94,-126,-1,2,-94,-127,00300044040300043020-1,2,-94,-70,-1127778619;-563023189;dis;;true;true;true;300;true;24;24;true;false;-1-1,2,-94,-80,5383-1,2,-94,-116,745567530-1,2,-94,-118,108340-1,2,-94,-129,,,0,,,,0-1,2,-94,-121,;4;36;0"}",
        BODY_END
    );

    ns_end_transaction("URUY_8", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("headerstats_4");
    ns_web_url ("headerstats_4",
        "URL=https://as-sec.casalemedia.com/headerstats?s=189641&u=https%3A%2F%2Fm.kohls.com%2F&v=3",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"p":"display","d":"mobile","c":"39002588238727","s":"rftPJkPt","w":"1896411662113069541WQQKxCS3zym7M","t":1662113071330,"pg":{"t":1662113069541,"e":[]},"gt":"1200","ac":"227","sl":[{"s":"d5a7a79b-04c5-5901-56a5-0545e115160e","t":1662113069917,"xslots":{"OATHM":{"1":{"brq":1,"bp":1}},"INDX":{"1":{"brq":1,"bp":1}},"TPL":{"1":{"brq":1,"bp":1}},"RUBI":{"1":{"brq":1,"be":1}},"BRT":{"1":{"brq":1,"be":1}},"SVRN":{"1":{"brq":1,"bp":1}},"PUBM":{"1":{"brq":1,"be":1}},"APNX":{"1":{"brq":1,"bp":1}}}},{"s":"identity","t":1662113069550,"xslots":{"LVRAMP":{"before":{"brq":"0","bp":1,"pt":"50"}},"ADSORG":{"before":{"brq":"0","brs":1,"pt":"50"}}}}]}",
        BODY_END,
        INLINE_URLS,
            "URL=https://b-code.liadm.com/sync-container.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("headerstats_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("headerstats_5");
    ns_web_url ("headerstats_5",
        "URL=https://as-sec.casalemedia.com/headerstats?s=189641&u=https%3A%2F%2Fm.kohls.com%2F&v=3",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"p":"display","d":"mobile","c":"39002588238727","s":"2RZIDAQ4","w":"1896411662113069541WQQKxCS3zym7M","t":1662113071334,"pg":{"t":1662113069541,"e":[]},"gt":"1200","ac":"222","sl":[{"s":"ea90a908-e1c0-7496-b3f2-f36137b5ce29","t":1662113069943,"xslots":{"APNX":{"3":{"brq":1,"bp":1}},"RUBI":{"3":{"brq":1,"be":1}},"TPL":{"3":{"brq":1,"bp":1}},"INDX":{"3":{"brq":1,"bp":1}},"PUBM":{"3":{"brq":1,"be":1}},"SVRN":{"3":{"brq":1,"bp":1}},"BRT":{"3":{"brq":1,"be":1}},"OATHM":{"3":{"brq":1,"bp":1}}}}]}",
        BODY_END
    );

    ns_end_transaction("headerstats_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("headerstats_6");
    ns_web_url ("headerstats_6",
        "URL=https://as-sec.casalemedia.com/headerstats?s=189641&u=https%3A%2F%2Fm.kohls.com%2F&v=3",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"p":"display","d":"mobile","c":"39002588238727","s":"Cf6q3hw6","w":"1896411662113069541WQQKxCS3zym7M","t":1662113071357,"pg":{"t":1662113069541,"e":[]},"gt":"1200","ac":"223","sl":[{"s":"2ff528ec-bb6c-f493-9d95-1a35501d8cb2","t":1662113069963,"xslots":{"RUBI":{"2":{"brq":1,"be":1}},"INDX":{"2":{"brq":1,"bp":1}},"TPL":{"2":{"brq":1,"bp":1}},"BRT":{"2":{"brq":1,"be":1}},"OATHM":{"2":{"brq":1,"bp":1}},"SVRN":{"2":{"brq":1,"bp":1}},"PUBM":{"2":{"brq":1,"be":1}},"APNX":{"2":{"brq":1,"bp":1}}}}]}",
        BODY_END,
        INLINE_URLS,
            "URL=https://login.dotomi.com/ucm/UCMController?dtm_com=28&dtm_cid=2683&dtm_cmagic=8420d3&dtm_fid=101&dtm_format=6&cli_promo_id=1&dtm_email_hash=not%20set&dtm_user_id=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=DotomiUser;DotomiSync;DotomiSession_2683", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC5c9a22d5a56847e08ecebe48b81f6d22-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC7705d8cfc8794e0c8ecf7777ec37c7b8-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://tpc.googlesyndication.com/sodar/sodar2/225/runner.html", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/recaptcha/api2/aframe", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("headerstats_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("X776374_2");
    ns_web_url ("X776374_2",
        "URL=https://rcom.dynamicyield.com/v3/recommend/8776374?_=1662113071549",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain; charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"uid":"-1233986090078972675","ctx":{"type":"HOMEPAGE"},"data":[{"wId":137038,"fId":27148,"maxProducts":8,"rules":[{"id":928207,"type":"include","slots":[],"isContextAware":false}],"filtering":[{"type":"exclude","field":"group_id","value":[]}]}],"skusOnly":false}",
        BODY_END,
        INLINE_URLS,
            "URL=https://rp.liadm.com/j?dtstmp=1662113071575&aid=a-00oc&se=e30&duid=0b10d8358f40--01gbytg2cc5vhtb7a1kcbsfwbz&tna=v2.4.2&pu=https%3A%2F%2Fm.kohls.com%2F&ext_s_ecid=MCMID%7C78611155637513935743550952407821114794&wpn=lc-bundle&refr=https%3A%2F%2Fm.kohls.com%2F&c=PG1ldGEgbmFtZT0iZGVzY3JpcHRpb24iIGNvbnRlbnQ9IkVuam95IGZyZWUgc2hpcHBpbmcgYW5kIGVhc3kgcmV0dXJucyBldmVyeSBkYXkgYXQgS29obCdzISBGaW5kIGdyZWF0IHNhdmluZ3Mgb24gY2xvdGhpbmcsIHNob2VzLCB0b3lzLCBob21lIGTDqWNvciwgYXBwbGlhbmNlcyBhbmQgZWxlY3Ryb25pY3MgZm9yIHRoZSB3aG9sZSBmYW1pbHkuCiAiPjx0aXRsZT5Lb2hsJ3MgfCBTaG9wIENsb3RoaW5nLCBTaG9lcywgSG9tZSwgS2l0Y2hlbiwgQmVkZGluZywgVG95cyAmYW1wOyBNb3JlCiAKPC90aXRsZT48bGluayByZWw9ImNhbm9uaWNhbCIgaHJlZj0iaHR0cHM6Ly93d3cua29obHMuY29tIj48dGl0bGUgaWQ9InBlcmNlbnQtb2ZmLXRpdGxlIiBhcmlhLWhpZGRlbj0idHJ1ZSIgdGFiaW5kZXg9Ii0xIj5wZXJjZW50IG9mZjwvdGl0bGU-PHRpdGxlIGlkPSJwZXJjZW50LW9mZi10aXRsZSI-cGVyY2VudCBvZmY8L3RpdGxlPjx0aXRsZSBpZD0icGVyY2VudC1vZmYtdGl0bGUiIGFyaWEtaGlkZGVuPSJ0cnVlIiB0YWJpbmRleD0iLTEiPnBlcmNlbnQgb2ZmPC90aXRsZT48dGl0bGUgaWQ9InN2Zy1idHMtbG9ja3VwLXRleHQiPk1vcmUgc3R5bGUuIE1vcmUgc2F2aW5ncy4gQmFjayB0byBzY2hvb2wuPC90aXRsZT48dGl0bGUgaWQ9InN2Zy1qYi10aXRsZSI-anVtcGluZyBiZWFuczwvdGl0bGU-PHRpdGxlIGlkPSJzby1sb2dvLXQiPlNPLiBHb29kcyBmb3IgbGlmZS48L3RpdGxlPjx0aXRsZSBpZD0idGVrZ2Vhci10Ij5UZWsgR2VhcjwvdGl0bGU-PHRpdGxlIGlkPSJjb252ZXJzZS1sb2dvIj5Db252ZXJzZTwvdGl0bGU-PHRpdGxlIGlkPSJ2YW5zLWxvZ28iPlZhbnMgIm9mZiB0aGUgd2FsbCI8L3RpdGxlPjx0aXRsZSBpZD0ibmlrZXNob2VzIj5OaWtlLjwvdGl0bGU-PHRpdGxlIGlkPSJhZGlkYXNzaG9lcyI-YWRpZGFzPC90aXRsZT48dGl0bGUgaWQ9InVhLWxvZ28tYiI-VW5kZXIgQXJtb3VyPC90aXRsZT48dGl0bGUgaWQ9InNsaWRlci1sb2dvLWxldmlzIj5MZXZpJ3M8L3RpdGxlPjx0aXRsZSBpZD0ibGVlVGl0bGUiPkxlZS48L3RpdGxlPjx0aXRsZSBpZD0id3JhbmdsZXJUaXRsZSI-V3JhbmdsZXI8L3RpdGxlPjx0aXRsZSBpZD0ic29ub21hIj5Tb25vbWEgR29vZHMgZm9yIExpZmU8L3RpdGxlPjx0aXRsZSBpZD0ic28tbG9nby10Ij5TTy4gR29vZHMgZm9yIGxpZmUuPC90aXRsZT48dGl0bGUgaWQ9InNvbm9tYSI-U29ub21hIEdvb2RzIGZvciBMaWZlPC90aXRsZT48dGl0bGUgaWQ9InNvbm9tYSI-U29ub21hIEdvb2RzIGZvciBMaWZlPC90aXRsZT48dGl0bGUgaWQ9InN2Zy1mbHgtaHotdGl0bGUiPkZMWDwvdGl0bGU-PHRpdGxlIGlkPSJzdmctYXB0OS10aXRsZSI-QVBULjk8L3RpdGxlPjx0aXRsZSBpZD0ic3ZnLWpiLXRpdGxlIj5qdW1waW5nIGJlYW5zPC90aXRsZT48dGl0bGUgaWQ9InNvLWxvZ28iPlNPLiBHb29kcyBmb3IgbGlmZS48L3RpdGxlPjx0aXRsZSBpZD0iY3JvZnQtYmFycm93Ij5Dcm9mdCBhbmQgQmFycm93PC90aXRsZT48dGl0bGUgaWQ9InRla2dlYXIiPlRlayBHZWFyPC90aXRsZT48dGl0bGUgaWQ9InN2Zy10aGViaWdvbmUtdGl0bGUiPlRIRSBCSUcgT05FPC90aXRsZT48dGl0bGUgaWQ9IkthbmRTSW50cm8iPlNlcGhvcmEgKyBLb2hsJ3MuPC90aXRsZT48dGl0bGUgaWQ9IkthbmRTSW50cm8wMiI-U2VwaG9yYSArIEtvaGwncy48L3RpdGxlPjx0aXRsZSBpZD0iTGFib3JfRGF5X1NhbGUiPkxhYm9yIERheSBTYWxlPC90aXRsZT48dGl0bGUgaWQ9ImtSZXdhcmRzIj5Lb2hsJ3MgUmV3YXJkczwvdGl0bGU-PHRpdGxlIGlkPSJTQkkiPlNlcGhvcmEgQmVhdXR5IEluc2lkZXI8L3RpdGxlPjx0aXRsZSBpZD0ia1Jld2FyZHMiPktvaGwncyBSZXdhcmRzPC90aXRsZT48dGl0bGUgaWQ9IlNCSSI-U2VwaG9yYSBCZWF1dHkgSW5zaWRlci48L3RpdGxlPjx0aXRsZSBpZD0ibGV2aXN0aXRsZSI-TGV2aSdzPC90aXRsZT48dGl0bGUgaWQ9ImNoYW1waW9uLWxvZ28iPkNoYW1waW9uPC90aXRsZT48dGl0bGUgaWQ9ImFtYXpvbi1zbWlsZS1jb3B5Ij5BbWF6b248L3RpdGxlPjx0aXRsZT5GYWNlYm9vazwvdGl0bGU-PHRpdGxlPlR3aXR0ZXI8L3RpdGxlPjx0aXRsZT5QaW50ZXJlc3Q8L3RpdGxlPjx0aXRsZT5JbnN0YWdyYW08L3RpdGxlPjx0aXRsZT5Zb3VUdWJlPC90aXRsZT4", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cdnssl.clicktale.net/www47/ptc/630f0ceb-a670-47fc-a7d0-b2a2133e9266.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://m.kohls.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sp.analytics.yahoo.com/sp.pl?a=10000&b=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&.yp=10156246&f=https%3A%2F%2Fm.kohls.com%2F&e=https%3A%2F%2Fm.kohls.com%2F&enc=UTF-8&yv=1.13.0&tagmgr=adobe", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=A3", END_INLINE,
            "URL=https://sp.analytics.yahoo.com/sp.pl?a=10000&b=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&.yp=10156246&f=https%3A%2F%2Fm.kohls.com%2F&e=https%3A%2F%2Fm.kohls.com%2F&enc=UTF-8&yv=1.13.0&et=custom&ea=&ev=&gv=&product_sku=not%20set&product_id=not%20set&conversion_sku=not%20set&conversion_product_id=not%20set&conversion_revenue=not%20set&tagmgr=adobe", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=A3", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC8ceaea1fd94a499eb45113af297edb25-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://pagead2.googlesyndication.com/bg/JRDtgcUl_7OUjJ4QO8bVbwNuRTRqDUxuSBYCwiPHS6U.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://pagead2.googlesyndication.com/pagead/sodar?id=sodar2&v=225&li=gpt_2022083101&jk=556331592190720&rc=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("X776374_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("j_2");
    ns_web_url ("j_2",
        "URL=https://rp.liadm.com/j?dtstmp=1662113071575&aid=a-00oc&se=e30&duid=0b10d8358f40--01gbytg2cc5vhtb7a1kcbsfwbz&tna=v2.4.2&pu=https%3A%2F%2Fm.kohls.com%2F&ext_s_ecid=MCMID%7C78611155637513935743550952407821114794&wpn=lc-bundle&refr=https%3A%2F%2Fm.kohls.com%2F&c=PG1ldGEgbmFtZT0iZGVzY3JpcHRpb24iIGNvbnRlbnQ9IkVuam95IGZyZWUgc2hpcHBpbmcgYW5kIGVhc3kgcmV0dXJucyBldmVyeSBkYXkgYXQgS29obCdzISBGaW5kIGdyZWF0IHNhdmluZ3Mgb24gY2xvdGhpbmcsIHNob2VzLCB0b3lzLCBob21lIGTDqWNvciwgYXBwbGlhbmNlcyBhbmQgZWxlY3Ryb25pY3MgZm9yIHRoZSB3aG9sZSBmYW1pbHkuCiAiPjx0aXRsZT5Lb2hsJ3MgfCBTaG9wIENsb3RoaW5nLCBTaG9lcywgSG9tZSwgS2l0Y2hlbiwgQmVkZGluZywgVG95cyAmYW1wOyBNb3JlCiAKPC90aXRsZT48bGluayByZWw9ImNhbm9uaWNhbCIgaHJlZj0iaHR0cHM6Ly93d3cua29obHMuY29tIj48dGl0bGUgaWQ9InBlcmNlbnQtb2ZmLXRpdGxlIiBhcmlhLWhpZGRlbj0idHJ1ZSIgdGFiaW5kZXg9Ii0xIj5wZXJjZW50IG9mZjwvdGl0bGU-PHRpdGxlIGlkPSJwZXJjZW50LW9mZi10aXRsZSI-cGVyY2VudCBvZmY8L3RpdGxlPjx0aXRsZSBpZD0icGVyY2VudC1vZmYtdGl0bGUiIGFyaWEtaGlkZGVuPSJ0cnVlIiB0YWJpbmRleD0iLTEiPnBlcmNlbnQgb2ZmPC90aXRsZT48dGl0bGUgaWQ9InN2Zy1idHMtbG9ja3VwLXRleHQiPk1vcmUgc3R5bGUuIE1vcmUgc2F2aW5ncy4gQmFjayB0byBzY2hvb2wuPC90aXRsZT48dGl0bGUgaWQ9InN2Zy1qYi10aXRsZSI-anVtcGluZyBiZWFuczwvdGl0bGU-PHRpdGxlIGlkPSJzby1sb2dvLXQiPlNPLiBHb29kcyBmb3IgbGlmZS48L3RpdGxlPjx0aXRsZSBpZD0idGVrZ2Vhci10Ij5UZWsgR2VhcjwvdGl0bGU-PHRpdGxlIGlkPSJjb252ZXJzZS1sb2dvIj5Db252ZXJzZTwvdGl0bGU-PHRpdGxlIGlkPSJ2YW5zLWxvZ28iPlZhbnMgIm9mZiB0aGUgd2FsbCI8L3RpdGxlPjx0aXRsZSBpZD0ibmlrZXNob2VzIj5OaWtlLjwvdGl0bGU-PHRpdGxlIGlkPSJhZGlkYXNzaG9lcyI-YWRpZGFzPC90aXRsZT48dGl0bGUgaWQ9InVhLWxvZ28tYiI-VW5kZXIgQXJtb3VyPC90aXRsZT48dGl0bGUgaWQ9InNsaWRlci1sb2dvLWxldmlzIj5MZXZpJ3M8L3RpdGxlPjx0aXRsZSBpZD0ibGVlVGl0bGUiPkxlZS48L3RpdGxlPjx0aXRsZSBpZD0id3JhbmdsZXJUaXRsZSI-V3JhbmdsZXI8L3RpdGxlPjx0aXRsZSBpZD0ic29ub21hIj5Tb25vbWEgR29vZHMgZm9yIExpZmU8L3RpdGxlPjx0aXRsZSBpZD0ic28tbG9nby10Ij5TTy4gR29vZHMgZm9yIGxpZmUuPC90aXRsZT48dGl0bGUgaWQ9InNvbm9tYSI-U29ub21hIEdvb2RzIGZvciBMaWZlPC90aXRsZT48dGl0bGUgaWQ9InNvbm9tYSI-U29ub21hIEdvb2RzIGZvciBMaWZlPC90aXRsZT48dGl0bGUgaWQ9InN2Zy1mbHgtaHotdGl0bGUiPkZMWDwvdGl0bGU-PHRpdGxlIGlkPSJzdmctYXB0OS10aXRsZSI-QVBULjk8L3RpdGxlPjx0aXRsZSBpZD0ic3ZnLWpiLXRpdGxlIj5qdW1waW5nIGJlYW5zPC90aXRsZT48dGl0bGUgaWQ9InNvLWxvZ28iPlNPLiBHb29kcyBmb3IgbGlmZS48L3RpdGxlPjx0aXRsZSBpZD0iY3JvZnQtYmFycm93Ij5Dcm9mdCBhbmQgQmFycm93PC90aXRsZT48dGl0bGUgaWQ9InRla2dlYXIiPlRlayBHZWFyPC90aXRsZT48dGl0bGUgaWQ9InN2Zy10aGViaWdvbmUtdGl0bGUiPlRIRSBCSUcgT05FPC90aXRsZT48dGl0bGUgaWQ9IkthbmRTSW50cm8iPlNlcGhvcmEgKyBLb2hsJ3MuPC90aXRsZT48dGl0bGUgaWQ9IkthbmRTSW50cm8wMiI-U2VwaG9yYSArIEtvaGwncy48L3RpdGxlPjx0aXRsZSBpZD0iTGFib3JfRGF5X1NhbGUiPkxhYm9yIERheSBTYWxlPC90aXRsZT48dGl0bGUgaWQ9ImtSZXdhcmRzIj5Lb2hsJ3MgUmV3YXJkczwvdGl0bGU-PHRpdGxlIGlkPSJTQkkiPlNlcGhvcmEgQmVhdXR5IEluc2lkZXI8L3RpdGxlPjx0aXRsZSBpZD0ia1Jld2FyZHMiPktvaGwncyBSZXdhcmRzPC90aXRsZT48dGl0bGUgaWQ9IlNCSSI-U2VwaG9yYSBCZWF1dHkgSW5zaWRlci48L3RpdGxlPjx0aXRsZSBpZD0ibGV2aXN0aXRsZSI-TGV2aSdzPC90aXRsZT48dGl0bGUgaWQ9ImNoYW1waW9uLWxvZ28iPkNoYW1waW9uPC90aXRsZT48dGl0bGUgaWQ9ImFtYXpvbi1zbWlsZS1jb3B5Ij5BbWF6b248L3RpdGxlPjx0aXRsZT5GYWNlYm9vazwvdGl0bGU-PHRpdGxlPlR3aXR0ZXI8L3RpdGxlPjx0aXRsZT5QaW50ZXJlc3Q8L3RpdGxlPjx0aXRsZT5JbnN0YWdyYW08L3RpdGxlPjx0aXRsZT5Zb3VUdWJlPC90aXRsZT4&n3pc=true",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://cdnssl.clicktale.net/ptc/630f0ceb-a670-47fc-a7d0-b2a2133e9266.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://m.kohls.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?value=0&guid=ON&script=0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://tpc.googlesyndication.com/generate_204?yWtNqg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/1730721_Navy_Dot?wid=800&hei=800&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/4865781_Red_White_Black?wid=800&hei=800&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/5267619_Olive?wid=800&hei=800&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/2247719_Fog_Heather?wid=800&hei=800&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Frisco?wid=800&hei=800&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/5583951_Panda?wid=800&hei=800&op_sharpen=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sli.kohls.com/baker?dtstmp=1662113071864", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=store_location;AKA_A2;check;rxVisitor;s_ecid;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;s_cc;__gads;__gpi;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_cs_id;_rdt_uuid;IR_gbd;IR_5349;IR_PI;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_ga;_gid;_gat;_sctr;bm_sz;_ga_ZLYRBY87M8;mbox;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;gpv_v9;s_sq;_dpm_id.f2d1;ndcd;rxvt;_abck;dtLatC;dtPC;_cs_s", END_INLINE,
            "URL=https://i.liadm.com/s/c/a-00oc?s=MgUIChCTEzIFCH4QkxMyBgiLARCTEzIFCHkQkxMyBgiBARCTEzIFCAkQkxM&cim=&ps=true&ls=true&duid=0b10d8358f40--01gbytg2cc5vhtb7a1kcbsfwbz&ppid=0&euns=0&ci=0&version=sc-v0.2.0&nosync=false&monitorExternalSyncs=false&", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("j_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("batch_4");
    ns_web_url ("batch_4",
        "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1662113071885_71630",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=DYID;DYSES",
        BODY_BEGIN,
            "{"md":{"uid":"-1233986090078972675","sec":"8776374","ses":"5b3e117ce067eeb99b2b5b268b908b68","colors":"d.an.c.ms.","sld":"kohls.com","rurl":"https://m.kohls.com/","ts":"Direct","p":"2","ref":"internal","rref":"https://m.kohls.com/","reqts":1662113070884,"rri":5155656},"data":[{"type":"rcom","timeInMS":502,"isPerBrowser":true,"isPerSection":true},{"type":"imp","expId":1250304,"varIds":[27070038],"expSes":"71849","mech":"1","smech":null,"verId":"11527264","aud":"1408117.1438654.1362538.1362542","expVisitId":"-5734382003328103099","eri":null,"rcomInfo":[{"context":{"type":"HOMEPAGE"},"wId":137038,"fId":27148,"fallback":true,"slots":[{"pId":"77509752","strId":9,"md":{}},{"pId":"17741310","strId":9,"md":{}},{"pId":"62791162","strId":9,"md":{}},{"pId":"99719860","strId":9,"md":{}},{"pId":"21034257","strId":9,"md":{}},{"pId":"99730912","strId":9,"md":{}},{"pId":"33872399","strId":9,"md":{}},{"pId":"61758050","strId":9,"md":{}}]}]}],"sectionId":"8776374"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.google.com/pagead/1p-user-list/1071871169/?value=0&guid=ON&script=0&is_vtc=1&random=318554268", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cdnssl.clicktale.net/pcc/630f0ceb-a670-47fc-a7d0-b2a2133e9266.js?DeploymentConfigName=Malka_20220809&Version=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://m.kohls.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cdnssl.clicktale.net/www/bridge-WR110.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://m.kohls.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://c.clicktale.net/pageview?pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&lv=1662113024&lhd=1662113024&hd=1662113071&pn=2&dw=1298&dh=15756&ww=1313&wh=361&sw=1313&sh=489&dr=https%3A%2F%2Fm.kohls.com%2F&url=https%3A%2F%2Fm.kohls.com%2F&uc=0&la=en-US&cvars=%7B%221%22%3A%5B%22Page%20Name%22%2C%22homepage%22%5D%7D&cvarp=%7B%221%22%3A%5B%22Page%20Name%22%2C%22homepage%22%5D%7D&v=11.40.1&r=369448", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("batch_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("quota_2");
    ns_web_url ("quota_2",
        "URL=https://q-aus1.clicktale.net/quota?enc=raw",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"recordingTypes":[6,7],"url":"https://m.kohls.com/","projectId":2399,"uu":"6d513319-e33c-a1a2-f741-3ecaf18284e9","sn":1,"pn":2}",
        BODY_END,
        INLINE_URLS,
            "URL=https://c.clicktale.net/pageEvent?value=MIewdgZglg5gXAAgLIEMA2BrFB9ATABlwIA58BOIAAA%3D&enc=lzstring&isETR=false&isCustomHashId=false&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=2&r=678021", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://c.clicktale.net/pageEvent?value=EwQwbARgzCCMEFMBmBjALBA7NgrCNasKmEemIIEEIwEADDJGkAA%3D&enc=lzstring&isETR=false&isCustomHashId=true&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=2&r=214154", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("quota_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_21");
    ns_web_url ("recording_21",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=1&ri=20&rst=1662113025036&let=1662113067183&hlm=true&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_21_main_url_1_1662113085127.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://c.clicktale.net/dvar?v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=2&dv=N4IgDgTg9mCMDsIBcIDWUAWAbAzgAgDsoAXPLKAcwoFMATPASwIB9yBPAQy2LcJLMo16TEABoQAQQDSEgPoAJAAoAmPAGEoUVA2rIQAGTzM88qAFtqeAPR5F%2BxUbwAlOtRwMKBEAF8gAAA%3D%3D&enc=lzstring&r=844044", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://mid.rkdms.com/bct?pid=bcccb40a-06d2-44fe-bdd2-a91ef4a5bfd0&&puid=&liid=&_ct=im", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=sessionid;sc", END_INLINE,
            "URL=https://loadus.exelator.com/load/?p=204&g=661&j=0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://aa.agkn.com/adscores/g.pixel?sid=9212291498&_puid=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=ab", END_INLINE,
            "URL=https://sync.crwdcntrl.net/qmap?c=12611&tp=LVIN&gdpr=0&d=https://i.liadm.com/s/41715?bidder_id=127211&bidder_uuid=${profile_id}", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_cc_dc;_cc_id", END_INLINE,
            "URL=https://live.rezync.com/sync?c=0aa2530f29e4f4a05b5d5d9bb35d60c2&p=93c1662463a616a7155169889dd99651&pid=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://match.prod.bidr.io/cookie-sync/liveintent", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://s.thebrighttag.com/csx?tp=1YJNAYe", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://i.liadm.com/s/35637?bidder_id=100905&amp;bidder_uuid=209180804262002690585", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=/s/35637?bidder_id=100905&amp;bidder_uuid=209180804262002690585&_li_chk=true&previous_uuid=24de976b845040b3a02a22fa3a91eeec", END_INLINE,
            "URL=https://p.rfihub.com/cm?pub=39342&in=1&userid=1e750ec8-4c68-4b2d-99ed-2bb27b262ff6%3A1662113072.1525838&forward=https%3A//i.liadm.com/s/56409%3Fbidder_id%3D200442%26bidder_uuid%3D1e750ec8-4c68-4b2d-99ed-2bb27b262ff6%253A1662113072.1525838%26pid%3D500040%26it%3D1%26iv%3D1e750ec8-4c68-4b2d-99ed-2bb27b262ff6%253A1662113072.1525838", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://i.liadm.com/s/56409?bidder_id=200442&bidder_uuid=1e750ec8-4c68-4b2d-99ed-2bb27b262ff6%3A1662113072.1525838&pid=500040&it=1&iv=1e750ec8-4c68-4b2d-99ed-2bb27b262ff6%3A1662113072.1525838", END_INLINE,
            "URL=https://i.liadm.com/s/19948?bidder_id=178256&bidder_uuid=44ede74def9251a9e20a7f89a36d09f1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=/s/19948?bidder_id=178256&bidder_uuid=44ede74def9251a9e20a7f89a36d09f1&_li_chk=true&previous_uuid=633282cbda9545a88308d650fc698c19", END_INLINE,
            "URL=https://i.liadm.com/s/41715?bidder_id=127211", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=/s/41715?bidder_id=127211&_li_chk=true&previous_uuid=eb4bc284833d426690ad514e378e613b", END_INLINE,
            "URL=https://i.liadm.com/s/35637?bidder_id=100905&amp;bidder_uuid=209180804262002690585&_li_chk=true&previous_uuid=24de976b845040b3a02a22fa3a91eeec", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://match.adsrvr.org/track/cmf/generic?ttd_pid=liveintent&ttd_tpi=1&gdpr=0", END_INLINE,
            "URL=https://i.liadm.com/s/19948?bidder_id=178256&bidder_uuid=44ede74def9251a9e20a7f89a36d09f1&_li_chk=true&previous_uuid=633282cbda9545a88308d650fc698c19", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://x.bidswitch.net/sync?dsp_id=42&user_id=", END_INLINE,
            "URL=https://i.liadm.com/s/41715?bidder_id=127211&_li_chk=true&previous_uuid=eb4bc284833d426690ad514e378e613b", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://i6.liadm.com/s/41715?bidder_id=127211", END_INLINE,
            "URL=https://match.prod.bidr.io/cookie-sync/liveintent?_bee_ppp=1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=checkForPermission", END_INLINE,
            "URL=https://match.adsrvr.org/track/cmf/generic?ttd_pid=liveintent&ttd_tpi=1&gdpr=0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=TDID;TDCPM", END_INLINE,
            "URL=https://x.bidswitch.net/sync?dsp_id=42&user_id=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=tuuid;c;tuuid_lu", END_INLINE,
            "URL=https://pagead2.googlesyndication.com/pagead/sodar?id=sodar2&v=225&t=2&li=gpt_2022083101&jk=556331592190720&bg=!YGOlYyfNAAZTikH4c4o7ACkAdvg8WuFx5RsTS6qcDzPdKg1XEGaYLO9GRsaWOXx24D67Qci0zg69xAIAAADAUgAAAAJoAQcKAEHRATIwymD4vYAjZBxP-POzsEwnv07U6H4e3300IbJzIDffPlnmmkhCXZeZdzzGp_se96_tNerSHn9wdD06dO8SC5kCnJ5b_SoEtRQ3G1rgp4HKZ0dACSz9W4ViBVT1vwj9LxcfnUuOSVv2P9wgrHg7YZuFnx2tXvLf3Z-3TVIpw_hO6VJZHoLpmzdc4LrlLssz_XmH9Qu8_dsnkq4WLVipSuWhBDbRMwpkMvrLcIDiQegP1xrDB5dmWjYHRuhlI_C2LLlPAuRfteksErFsaR-H0L12gkSSOh1-E1fkr-lCREr9H29L8y5vRX6Xnt-v9z_ne4VaVr4HC2eKeZtrcCk69S1nZNaRo7Y5ngumsljMNL6niooLaMMFX1wNz4gL1MhPcM4cxXpaV3qJw4pc1cwou_igsrvTph407fWP5X-SoO345u8lLnLJCjHhvqTTqA3R-WeRR3Bt_TNdp0sij6LL5FwLBV3sJ5EPj3BaadY9h4F985F6KaKQrm8qRDHdewUM2RqYjX3amY9ZASaqJO4cW0UVhEG-UAzNLHn1LKc-FxlPnSNT1bvCEHlmFSDsD-SaJcNijpgvNQMcx-7NgcgI53vTEzrR6i3g9oq02u62SunNgR-5KMYuMfoUtyWE2VsyekhvTu8wfFqIGnbtzOtJVFardFyXbq4uF6qZllSx4AMzlYDmqnepULquvRZkCCbBk7jxhEmodzJnFRZ0r6DutTQQL90WJAh_1F_ORZC_WKI0RWQMp3jg3FJ9rDs3UUOZCLQhYoYz4MqpAhx7MVqhe9l5gm98kUxYYUzNUyOh05JEWgwR9Fb0kioJphlOlZNp6uoEDCVjvPAzJYxGaKLlnGhhNrNIrNscNQYrsgBH885kJak1vO3d-lbevTOkYeNH9_7qzvmQ1Eu6P1ZZyxGoBzIxCxNubTBZG1Tz1gFZf9xQN43nokCTCMWoE74iqsMcGCYlsBzXhdkkNtOThgyu", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://i.liadm.com/s/56409?bidder_id=200442&bidder_uuid=1e750ec8-4c68-4b2d-99ed-2bb27b262ff6%3A1662113072.1525838&pid=500040&it=1&iv=1e750ec8-4c68-4b2d-99ed-2bb27b262ff6%3A1662113072.1525838", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=/s/56409?bidder_id=200442&it=1&bidder_uuid=1e750ec8-4c68-4b2d-99ed-2bb27b262ff6:1662113072.1525838&pid=500040&_li_chk=true&iv=1e750ec8-4c68-4b2d-99ed-2bb27b262ff6:1662113072.1525838&previous_uuid=5c1ce5b479794c3c82b413c924fdd375", END_INLINE,
            "URL=https://i.liadm.com/s/35759?bidder_id=44489&bidder_uuid=56bc35b1-0909-4b6f-840d-20add6a418d9", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=/s/35759?bidder_id=44489&bidder_uuid=56bc35b1-0909-4b6f-840d-20add6a418d9&_li_chk=true&previous_uuid=2b875ad444a14faa9c5edca6e7ca317d", END_INLINE,
            "URL=https://i6.liadm.com/s/41715?bidder_id=127211", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://i.liadm.com/s/62491?bidder_id=237139&bidder_uuid=AAArSk7GI6oAAA79igGVzw", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=/s/62491?bidder_id=237139&bidder_uuid=AAArSk7GI6oAAA79igGVzw&_li_chk=true&previous_uuid=b1d891f772aa462785bd2002f9455bcd", END_INLINE,
            "URL=https://s.btstatic.com/tag.js#site=4DPyaxM&referrer=https%3A%2F%2Fm.kohls.com%2F", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://i.liadm.com/s/56409?bidder_id=200442&it=1&bidder_uuid=1e750ec8-4c68-4b2d-99ed-2bb27b262ff6:1662113072.1525838&pid=500040&_li_chk=true&iv=1e750ec8-4c68-4b2d-99ed-2bb27b262ff6:1662113072.1525838&previous_uuid=5c1ce5b479794c3c82b413c924fdd375", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://pippio.com/api/sync?it=1&pid=500040&iv=1e750ec8-4c68-4b2d-99ed-2bb27b262ff6:1662113072.1525838", END_INLINE,
            "URL=https://i.liadm.com/s/35759?bidder_id=44489&bidder_uuid=56bc35b1-0909-4b6f-840d-20add6a418d9&_li_chk=true&previous_uuid=2b875ad444a14faa9c5edca6e7ca317d", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://i6.liadm.com/s/35759?bidder_id=44489&bidder_uuid=56bc35b1-0909-4b6f-840d-20add6a418d9", END_INLINE,
            "URL=https://i.liadm.com/s/62491?bidder_id=237139&bidder_uuid=AAArSk7GI6oAAA79igGVzw&_li_chk=true&previous_uuid=b1d891f772aa462785bd2002f9455bcd", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://i6.liadm.com/s/62491?bidder_id=237139&bidder_uuid=AAArSk7GI6oAAA79igGVzw", END_INLINE,
            "URL=https://c.clicktale.net/dvar?v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=2&dv=N4IgggQg%2BmAqMQAQFpEFlGwEoEYcE4BmFdATwGUAXAewCcBTRMAdwEMGA7egZ28XIDGrADaMATAAYxY5BJzIcEkAC4QEiYgDC1DpVrVhIADThocBCQwAFAFrrFJcpoAyJANICAFuwDmjAOLC1ABGIoiUrAKUAJYCfJJi8hKEyGL4Kmo4iAAi%2BgAOACbUzByIANSIABL0rAX0tMamMPAAGrAkAFJijvSUiABq7IgAZnSIEPSllQCuw8MAtqylqGjsANaIAHL0zIgAqtz13BkAogAeefXRkwKMYI2QzRYrmFiJ%2BFmoWDx5OtzRADdGFY0FZEEsClUanVaJhaKw5rFEFhWPM8ogrJVugkZBIABypboAVgkAFIMuotjpkN9uL8OP8gQ8zPBICRshhsFJ8BpUG40JsqmDYNR0RAllxYTjZASxAAWClZcUcSXMp5s1AcjF2Qh4vGOFwkMACATUaa6KkxYaIBLyHC4wiKpgmvqbahW2KsGI6NXmACKYEQXUwPD6aFN80QAHlaDC4ZE1tEOD5Thcrjc7r7Wu1NRhvqLJvwaAxEM5otw%2BlYsFHsjapBJZETZArVOdLrRrhxbkwQABfIAA&enc=lzstring&r=108331", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://c.clicktale.net/pageEvent?value=CwYwhgHArAJiCMA2KYBGsoE4Ds3oDNF5URtMoBmKgJigmDDE32yAAA%3D%3D&enc=lzstring&isETR=false&isCustomHashId=true&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=2&r=056629", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&referrer=https%3A%2F%2Fm.kohls.com%2F&docReferrer=https%3A%2F%2Fm.kohls.com%2F&H=-1dxqaig", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://pippio.com/api/sync?it=1&pid=500040&iv=1e750ec8-4c68-4b2d-99ed-2bb27b262ff6:1662113072.1525838", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://i6.liadm.com/s/35759?bidder_id=44489&bidder_uuid=56bc35b1-0909-4b6f-840d-20add6a418d9", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://i6.liadm.com/s/62491?bidder_id=237139&bidder_uuid=AAArSk7GI6oAAA79igGVzw", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("recording_21", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("bf_8");
    ns_web_url ("bf_8",
        "URL=https://bf27853irn.bf.dynatrace.com/bf?type=js3&sn=v_4_srv_4_sn_IV7VUE896KFTV6I5O956PBOGGHT16VT8_app-3A06d81dff759102c9_1_ol_0_perc_100000_mul_1&svrid=4&flavor=cors&vi=WTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0&modifiedSince=1662094650929&rf=https%3A%2F%2Fm.kohls.com%2F&bp=3&app=06d81dff759102c9&crc=95405563&en=ldtlho0d&end=1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$a=1%7C8%7CPage%3A%20%7CD%7Cx%7C1662113070573%7C1662113071423%7Cdn%7C5263%7Cxu%7C%2FlBzjNRQPfM%2FSq6c%2F1G5U4A%2FiXEDf6VLOw%2FYnB5eA%2FSm9pFh0%2FURUY%7Csvtrg%7C1%7Csvm%7Ci1%5Esk0%5Esh0%7Ctvtrg%7C1%7Ctvm%7Ci1%5Esk0%5Esh0%7Cxrt%7Cb1662113071245e0f0g0h0i0j0k2l158m161u1315v17w17T-6z1I1%7Cxcs%7C850%7Cxce%7C850%7Crc%7C201%2C2%7C13%7C_event_%7C1662113070573%7C_vc_%7CV%7C-1%5Epc%7CVCD%7C995%7CVCDS%7C0%7CVCS%7C1067%7CVCO%7C1067%7CVCI%7C0%7CS%7C-1%2C1%7C12%7C_event_%7C1662113071380%7C_wv_%7CAAI%7C1%7CfIS%7C3085%7CfID%7C247$rId=RID_2418$rpId=$domR=1662113069085$tvn=%2F$tvt=1662113067240$tvm=i1%3Bk0%3Bh0$tvtrg=1$ni=4g|10$fd=contentsquare$md=mdcc1%2Chttps%3A%2F%2Fm.kohls.com%2F%3Bmdcc2%2CMozilla%2F5.0%20(Linux%5Es%20Android%206.0.1%5Es%20Moto%20G%20(4)%20Build%2FMPJ24.139-64)%20AppleWebKit%2F537.36%20(KHTML%5Ec%20like%20Gecko)%20Chrome%2F58.0.3029.81%20Mobile%20Safari%2F537.36%20PTST%2F1%3Bmdcc8%2CN%2FA$url=https%3A%2F%2Fm.kohls.com%2F$title=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More$latC=1$app=06d81dff759102c9$vi=WTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0$fId=113068061_316$v=10247220811100421$vID=1662113020005SBVNC9V48OJOAGOMH3HKO8K0MK5NUIMM$time=1662113072647",
        BODY_END,
        INLINE_URLS,
            "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210816-sephora-kohls-rewards", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("bf_8", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_22");
    ns_web_url ("recording_22",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=2&ri=1&rst=1662113071979&let=1662113072782&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_22_main_url_1_1662113085129.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://media.kohlsimg.com/is/image/kohls/hp-20220901-exception-split-01-d", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/hp-20220901-exception-split-02-d", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-2020w1226-rewardsplate", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20211105-appicon", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/hp-20220901-kohls-finds-social-d", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20220715-kck", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://c.clicktale.net/pageEvent?value=EYJgZsDsCmwKwA4DMIAmxUENIBYQiU2lx0yQAYA2HARmhAE4lCHoEgAA&enc=lzstring&isETR=false&isCustomHashId=true&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=2&r=434874", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("recording_22", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("bf_9");
    ns_web_url ("bf_9",
        "URL=https://bf27853irn.bf.dynatrace.com/bf?type=js3&sn=v_4_srv_4_sn_IV7VUE896KFTV6I5O956PBOGGHT16VT8_app-3A06d81dff759102c9_1_ol_0_perc_100000_mul_1&svrid=4&flavor=cors&vi=WTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0&modifiedSince=1662094650929&rf=https%3A%2F%2Fm.kohls.com%2F&bp=3&app=06d81dff759102c9&crc=2494116279&en=ldtlho0d&end=1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/bf_9_url_0_1_1662113085130.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RCbb3281f69a1d4511be056bdbcd04b68a-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static.ads-twitter.com/uwt.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC45c5983a76b643f5a46e94f8beb85a30-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://connect.facebook.net/en_US/fbevents.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC025c600150bc46e19ab7207b7f046e39-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://t.co/i/adsct?bci=3&eci=2&event_id=7d7f3d6e-855c-4721-97b3-5918b9913f15&events=%5B%5B%22pageview%22%2C%7B%7D%5D%5D&integration=advertiser&p_id=Twitter&p_user_id=0&pl_id=e0ea6a16-ae12-4092-84ed-787cd28afc57&tw_document_href=https%3A%2F%2Fm.kohls.com%2F&tw_iframe_status=0&tw_order_quantity=0&tw_sale_amount=0&txn_id=nuubl&type=javascript&version=2.3.27", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://analytics.twitter.com/i/adsct?bci=3&eci=2&event_id=7d7f3d6e-855c-4721-97b3-5918b9913f15&events=%5B%5B%22pageview%22%2C%7B%7D%5D%5D&integration=advertiser&p_id=Twitter&p_user_id=0&pl_id=e0ea6a16-ae12-4092-84ed-787cd28afc57&tw_document_href=https%3A%2F%2Fm.kohls.com%2F&tw_iframe_status=0&tw_order_quantity=0&tw_sale_amount=0&txn_id=nuubl&type=javascript&version=2.3.27", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RCee4d5a357164477cbf8bc7a03fb2c6ea-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://connect.facebook.net/signals/config/322052792167924?v=2.9.79&r=stable", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC52a3a89d2834475a96ee97f4c4ee7e9a-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://connect.facebook.net/signals/config/1159959614410832?v=2.9.79&r=stable", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://s.pinimg.com/ct/core.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RCda8cf0bfbca44de282ac118e5640cfd0-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC596e84a98b6d4b959af8e5edb77205d3-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC5aff4df580974580892aa2020496fca0-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RCe54205cedb804f319ae0846419718430-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.facebook.com/tr/?id=322052792167924&ev=PageView&dl=https%3A%2F%2Fm.kohls.com%2F&rl=https%3A%2F%2Fm.kohls.com%2F&if=false&ts=1662113073585&sw=1313&sh=489&v=2.9.79&r=stable&ec=0&o=30&fbp=fb.1.1662113027985.1842180297&it=1662113073456&coo=false&exp=e0&rqm=GET", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.facebook.com/tr/?id=1159959614410832&ev=PageView&dl=https%3A%2F%2Fm.kohls.com%2F&rl=https%3A%2F%2Fm.kohls.com%2F&if=false&ts=1662113073588&sw=1313&sh=489&v=2.9.79&r=stable&ec=0&o=30&fbp=fb.1.1662113027985.1842180297&it=1662113073456&coo=false&exp=e0&rqm=GET", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://analytics.tiktok.com/i18n/pixel/events.js?sdkid=C4N70OPPGM656MIJUMHG&lib=ttq", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_ttp", END_INLINE
    );

    ns_end_transaction("bf_9", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("tag_2");
    ns_web_url ("tag_2",
        "URL=https://tagtracking.vibescm.com/tag",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:script",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://s.pinimg.com/ct/lib/main.55e552f9.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC6bdf645b23a94f23a59cf3730c210195-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC43754683fc8b46efa71e08b82f39b606-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("tag_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("index_12");
    ns_web_url ("index_12",
        "URL=https://ct.pinterest.com/user/?tid=2613489297682&pd=%7B%22em%22%3A%22e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855%22%2C%22pin_unauth%22%3A%22dWlkPVpUYzROR1V6TXpjdE1EYzBNeTAwWlRobExXRmpZbUV0TURRMk1UTm1NV0ZpT1RVMQ%22%7D&cb=1662113073637",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://ct.pinterest.com/v3/?tid=2613489297682&pd=%7B%22em%22%3A%22e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855%22%2C%22pin_unauth%22%3A%22dWlkPVpUYzROR1V6TXpjdE1EYzBNeTAwWlRobExXRmpZbUV0TURRMk1UTm1NV0ZpT1RVMQ%22%7D&event=init&ad=%7B%22loc%22%3A%22https%3A%2F%2Fm.kohls.com%2F%22%2C%22ref%22%3A%22https%3A%2F%2Fm.kohls.com%2F%22%2C%22if%22%3Afalse%2C%22sh%22%3A489%2C%22sw%22%3A1313%2C%22mh%22%3A%2255e552f9%22%2C%22architecture%22%3A%22x86%22%2C%22model%22%3A%22%22%2C%22platform%22%3A%22Linux%22%2C%22platformVersion%22%3A%22%22%2C%22uaFullVersion%22%3A%2290.0.4430.61%22%2C%22ecm_enabled%22%3Afalse%7D&cb=1662113073649", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://ct.pinterest.com/v3/?tid=2616391205865&pd=%7B%22em%22%3A%22e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855%22%2C%22pin_unauth%22%3A%22dWlkPVpUYzROR1V6TXpjdE1EYzBNeTAwWlRobExXRmpZbUV0TURRMk1UTm1NV0ZpT1RVMQ%22%7D&event=init&ad=%7B%22loc%22%3A%22https%3A%2F%2Fm.kohls.com%2F%22%2C%22ref%22%3A%22https%3A%2F%2Fm.kohls.com%2F%22%2C%22if%22%3Afalse%2C%22sh%22%3A489%2C%22sw%22%3A1313%2C%22mh%22%3A%2255e552f9%22%2C%22architecture%22%3A%22x86%22%2C%22model%22%3A%22%22%2C%22platform%22%3A%22Linux%22%2C%22platformVersion%22%3A%22%22%2C%22uaFullVersion%22%3A%2290.0.4430.61%22%2C%22ecm_enabled%22%3Afalse%7D&cb=1662113073650", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC4f870813c50d463486c70bf1dce9bd4c-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/js?id=DC-8632166", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/js?id=DC-11577892&l=dataLayer&cx=c", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/js?id=AW-1071871169&l=dataLayer&cx=c", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/js?id=AW-1018012790&l=dataLayer&cx=c", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googletagmanager.com/gtag/js?id=G-ZLYRBY87M8&l=dataLayer&cx=c", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://sc-static.net/scevent.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=X-AB", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC9f19545dd2c24e9f87f6d7128a2c7544-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://analytics.tiktok.com/i18n/pixel/identify.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_ttp", END_INLINE,
            "URL=https://analytics.tiktok.com/i18n/pixel/config.js?sdkid=C4N70OPPGM656MIJUMHG&hostname=m.kohls.com", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=_ttp", END_INLINE,
            "URL=https://ct.pinterest.com/ct.html", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("index_12", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("init_3");
    ns_web_url ("init_3",
        "URL=https://tr.snapchat.com/init?pids=99ed79db-3aec-4b85-9a61-ed80a2993b62",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("init_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("is_enabled_3");
    ns_web_url ("is_enabled_3",
        "URL=https://tr.snapchat.com/collector/is_enabled?pids=99ed79db-3aec-4b85-9a61-ed80a2993b62&tld=com",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("is_enabled_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("init_4");
    ns_web_url ("init_4",
        "URL=https://tr.snapchat.com/init?pids=99ed79db-3aec-4b85-9a61-ed80a2993b62,8e9bd9e0-8284-4dbd-ab20-145759728098",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("init_4", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("is_enabled_4");
    ns_web_url ("is_enabled_4",
        "URL=https://tr.snapchat.com/collector/is_enabled?pids=99ed79db-3aec-4b85-9a61-ed80a2993b62,8e9bd9e0-8284-4dbd-ab20-145759728098&tld=com",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.google-analytics.com/analytics.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC35b5823f644e4f6caea6de9fb6e9e198-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion_async.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://tr.snapchat.com/p", "METHOD=POST", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Origin:https://m.kohls.com", "HEADER=Content-Type:application/x-www-form-urlencoded", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=sc_at",
            BODY_BEGIN,
            "trackId=1820ae58-3ed0-4777-970e-408c7eee12db&pid=99ed79db-3aec-4b85-9a61-ed80a2993b62&ev=PAGE_VIEW&pl=https%3A%2F%2Fm.kohls.com%2F&ts=1662113073935&rf=https%3A%2F%2Fm.kohls.com%2F&v=1.6.0&bt=1d53c387&m_sl=6676&m_rd=6695&m_pi=1284.014999982901&m_dcl=1286.3999999826774&m_fcps=317.5700000138022&m_pl=1870.0149999931455&m_pv=v2&u_c1=0f36c1ec-e4f6-4791-920c-f1ee231a89b1&u_scsid=2843c17a-5374-4062-ae1a-c004cf6df31d&u_sclid=e998c793-040c-4281-82d9-b9b291967f52",
        BODY_END,
             END_INLINE
    );

    ns_end_transaction("is_enabled_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("pixel_2");
    ns_web_url ("pixel_2",
        "URL=https://analytics.tiktok.com/api/v2/pixel",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=_ttp",
        BODY_BEGIN,
            "{"event":"Pageview","message_id":"messageId-1662113074161-8269473879356-C4N70OPPGM656MIJUMHG","event_id":"","is_onsite":false,"timestamp":"2022-09-02T10:04:34.161Z","context":{"ad":{"sdk_env":"external","jsb_status":2},"user":{"anonymous_id":"3d0b5d48-438d-404c-aa82-4d1190d7769c"},"pixel":{"code":"C4N70OPPGM656MIJUMHG"},"page":{"url":"https://m.kohls.com/","referrer":"https://m.kohls.com/"},"library":{"name":"pixel.js","version":"2.1.33"},"device":{"platform":"android"},"session_id":"sessionId-1662113027961-7884481730050-C4N70OPPGM656MIJUMHG","pageview_id":"pageId-1662113073825-8014793019628-C4N70OPPGM656MIJUMHG","variation_id":"test_2","userAgent":"Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4) Build/MPJ24.139-64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Mobile Safari/537.36 PTST/1"},"properties":{"0":"t","1":"t","2":"q"}}",
        BODY_END,
        INLINE_URLS,
            "URL=https://11577892.fls.doubleclick.net/activityi;src=11577892;type=pagev0;cat=kmnun0;ord=6534889032835;gtm=2od8v0;auiddc=666653719.1662113028;u1=not%20set;u10=https%3A%2F%2Fm.kohls.com%2F;u11=homepage;u13=not%20set;u2=not%20set;u23=Current%20Customer;u24=78611155637513935743550952407821114794;u25=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fm.kohls.com%2F?", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC82863ebc3d2449b8bac7f33870252455-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?random=1662113074208&cv=9&fst=1662113074208&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&u_h=489&u_w=1313&u_ah=489&u_aw=1313&u_cd=24&u_his=2&u_tz=-300&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa8v0&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fm.kohls.com%2F&ref=https%3A%2F%2Fm.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&auid=666653719.1662113028&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?random=1662113074210&cv=9&fst=1662113074210&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&u_h=489&u_w=1313&u_ah=489&u_aw=1313&u_cd=24&u_his=2&u_tz=-300&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa8v0&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fm.kohls.com%2F&ref=https%3A%2F%2Fm.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&auid=666653719.1662113028&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://www.google-analytics.com/collect?v=1&_v=j96&a=2024804943&t=pageview&_s=1&dl=https%3A%2F%2Fm.kohls.com%2F&ul=en-us&de=UTF-8&dt=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&sd=24-bit&sr=1313x489&vp=1298x361&je=0&_u=QCCAgAAB~&jid=&gjid=&cid=1350227648.1662113023&tid=UA-45121696-1&_gid=1529326656.1662113029&cd2=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&cd4=&z=1449771517", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google-analytics.com/collect?v=1&_v=j96&a=2024804943&t=event&ni=1&_s=2&dl=https%3A%2F%2Fm.kohls.com%2F&ul=en-us&de=UTF-8&dt=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&sd=24-bit&sr=1313x489&vp=1298x361&je=0&ec=DY%20Smart%20Action&ea=%5BDY%20Test%5D%20-%20Home%20Store%20ID&el=Experience%201%20(Variation%201)&_u=QCCAgAAB~&jid=&gjid=&cid=1350227648.1662113023&tid=UA-45121696-1&_gid=1529326656.1662113029&cd2=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&cd4=&z=437645359", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google-analytics.com/collect?v=1&_v=j96&a=2024804943&t=event&ni=1&_s=3&dl=https%3A%2F%2Fm.kohls.com%2F&ul=en-us&de=UTF-8&dt=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&sd=24-bit&sr=1313x489&vp=1298x361&je=0&ec=DY%20Smart%20Action&ea=%5BCS-21414%5D%20Sign-in%2FSign-up%20events%20updated&el=Experience%201%20(Variation%201)&_u=QCCAgAAB~&jid=&gjid=&cid=1350227648.1662113023&tid=UA-45121696-1&_gid=1529326656.1662113029&cd2=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&cd4=&z=449466293", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google-analytics.com/collect?v=1&_v=j96&a=2024804943&t=event&ni=1&_s=4&dl=https%3A%2F%2Fm.kohls.com%2F&ul=en-us&de=UTF-8&dt=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&sd=24-bit&sr=1313x489&vp=1298x361&je=0&ec=DY%20Smart%20Action&ea=%5Bdy%20test%5D%20aa%20vs%20dy%20m.com&el=Experience%201%20(Variation%201)&_u=QCCAgAAB~&jid=&gjid=&cid=1350227648.1662113023&tid=UA-45121696-1&_gid=1529326656.1662113029&cd2=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&cd4=&z=1928601967", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google-analytics.com/collect?v=1&_v=j96&a=2024804943&t=event&ni=1&_s=5&dl=https%3A%2F%2Fm.kohls.com%2F&ul=en-us&de=UTF-8&dt=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&sd=24-bit&sr=1313x489&vp=1298x361&je=0&ec=DY%20Smart%20Action&ea=%5BDY%20TEST%5D%20Listen%20to%20A2C&el=Experience%201%20(Variation%201)&_u=QCCAgAAB~&jid=&gjid=&cid=1350227648.1662113023&tid=UA-45121696-1&_gid=1529326656.1662113029&cd2=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&cd4=&z=2066403239", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google-analytics.com/collect?v=1&_v=j96&a=2024804943&t=event&ni=1&_s=6&dl=https%3A%2F%2Fm.kohls.com%2F&ul=en-us&de=UTF-8&dt=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&sd=24-bit&sr=1313x489&vp=1298x361&je=0&ec=DY%20Smart%20Object&ea=PZ22075_CAT_HP%20DY%20Deep%20Learning%20Recs%20-%20Mobile&el=Deep%20Learning%20M.com%20(Test)&_u=QCCAgAAB~&jid=&gjid=&cid=1350227648.1662113023&tid=UA-45121696-1&_gid=1529326656.1662113029&cd2=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&cd4=&z=1544411557", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google-analytics.com/collect?v=1&_v=j96&a=2024804943&t=timing&_s=7&dl=https%3A%2F%2Fm.kohls.com%2F&ul=en-us&de=UTF-8&dt=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&sd=24-bit&sr=1313x489&vp=1298x361&je=0&plt=1849&pdt=15&dns=0&rrt=5&srt=1&tcp=0&dit=1284&clt=1285&_gst=6731&_gbt=6975&_cst=6449&_cbt=6747&_u=QCCAgAAB~&jid=&gjid=&cid=1350227648.1662113023&tid=UA-45121696-1&_gid=1529326656.1662113029&cd2=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&cd4=&z=1997834674", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("pixel_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("floop_9");
    ns_web_url ("floop_9",
        "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;AKA_A2;check;rxVisitor;s_ecid;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;s_cc;__gads;__gpi;178a2b8e00fcaafe5a98fe75be8fb9f3;akacd_BD_ECS_ELB2;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_rdt_uuid;IR_gbd;IR_5349;IR_PI;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_gid;_gat;_sctr;bm_sz;mbox;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;gpv_v9;s_sq;_dpm_id.f2d1;ndcd;rxvt;_abck;dtLatC;dtPC;_cs_id;_cs_s;_ga_ZLYRBY87M8;_ga",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/floop_9_main_url_1_1662113085132.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://adservice.google.com/ddm/fls/z/src=11577892;type=pagev0;cat=kmnun0;ord=6534889032835;gtm=2od8v0;auiddc=*;u1=not%20set;u10=https%3A%2F%2Fm.kohls.com%2F;u11=homepage;u13=not%20set;u2=not%20set;u23=Current%20Customer;u24=78611155637513935743550952407821114794;u25=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fm.kohls.com%2F", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://8632166.fls.doubleclick.net/activityi;src=8632166;type=landi0;cat=unive0;ord=213072578550;gtm=2od8v0;auiddc=666653719.1662113028;u1=not%20set;u10=https%3A%2F%2Fm.kohls.com%2F;u11=homepage;u13=not%20set;u2=not%20set;u23=Current%20Customer;u24=78611155637513935743550952407821114794;u25=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fm.kohls.com%2F?", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC3e1b7009ef914bda83baf865955e4515-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC33673eab55e247c8baa7307900d7c6a0-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1071871169/?random=1662113074208&cv=9&fst=1662112800000&num=1&bg=ffffff&guid=ON&u_h=489&u_w=1313&u_ah=489&u_aw=1313&u_cd=24&u_his=2&u_tz=-300&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa8v0&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fm.kohls.com%2F&ref=https%3A%2F%2Fm.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&async=1&fmt=3&is_vtc=1&random=4014999332&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("floop_9", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_23");
    ns_web_url ("recording_23",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=2&ri=2&rst=1662113071979&let=1662113072946&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_23_main_url_1_1662113085174.body",
        BODY_END
    );

    ns_end_transaction("recording_23", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_24");
    ns_web_url ("recording_24",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=2&ri=3&rst=1662113071979&let=1662113073662&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_24_main_url_1_1662113085177.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.google.com/pagead/1p-user-list/1018012790/?random=1662113074210&cv=9&fst=1662112800000&num=1&bg=ffffff&guid=ON&u_h=489&u_w=1313&u_ah=489&u_aw=1313&u_cd=24&u_his=2&u_tz=-300&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa8v0&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fm.kohls.com%2F&ref=https%3A%2F%2Fm.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&async=1&fmt=3&is_vtc=1&random=575866862&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("recording_24", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_25");
    ns_web_url ("recording_25",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=2&ri=4&rst=1662113071979&let=1662113074197&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_25_main_url_1_1662113085179.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://js.cnnx.link/roi/cnxtag-min.js?id=31851", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RCbc6e4b814cd34be58ad54a9bdf99e612-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://cdn.taboola.com/libtrc/unip/1456663/tfa.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=t_gid", END_INLINE,
            "URL=https://www.redditstatic.com/ads/pixel.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC06328aa0bc0c48f29cbfa045675a420b-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RCc9206bee094e4fc1ac91cf03b05f4a6e-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("recording_25", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("floop_10");
    ns_web_url ("floop_10",
        "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=store_location;AKA_A2;check;rxVisitor;s_ecid;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;crl8.fpcuid;mboxEdgeCluster;_dy_csc_ses;_dy_c_exps;_dycnst;_dyid;_dyjsession;dy_fs_page;_dy_geo;_dy_df_geo;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;_dpm_ses.f2d1;s_cc;__gads;__gpi;178a2b8e00fcaafe5a98fe75be8fb9f3;akacd_BD_ECS_ELB2;_li_dcdm_c;_lc2_fpi;_cs_c;_cs_cvars;_rdt_uuid;IR_gbd;IR_5349;IR_PI;dtCookie;_gcl_au;_fbp;_scid;_tt_enable_cookie;_ttp;_gid;_gat;_sctr;bm_sz;mbox;dtSa;_uetsid;_uetvid;_dy_ses_load_seq;_dy_c_att_exps;_dy_soct;_dyfs;_dy_lu_ses;_dycst;_dy_toffset;gpv_v9;s_sq;_dpm_id.f2d1;ndcd;rxvt;_abck;dtLatC;dtPC;_cs_id;_cs_s;_ga_ZLYRBY87M8;_ga",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/floop_10_main_url_1_1662113085179.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://trc.taboola.com/1456663/trc/3/json?tim=1662113074457&data=%7B%22id%22%3A252%2C%22ii%22%3A%22%2F%22%2C%22it%22%3A%22video%22%2C%22sd%22%3A%22v2_7e4ffdfaaca3d751ced3beb96cf5c12f_4e6d2ce6-a080-452e-9455-33841bab4609-tucta0b5a84_1662113028_1662113028_CNawjgYQl_RYGJ-2oO2vMCABKAEw4QE4kaQOQNWmD0juy9kDUIIEWABgAGiXjKXetYHfjKIBcAE%22%2C%22ui%22%3A%224e6d2ce6-a080-452e-9455-33841bab4609-tucta0b5a84%22%2C%22vi%22%3A1662113074441%2C%22cv%22%3A%2220220828-2-RELEASE%22%2C%22uiv%22%3A%22default%22%2C%22u%22%3A%22https%3A%2F%2Fwww.kohls.com%2F%22%2C%22e%22%3A%22https%3A%2F%2Fm.kohls.com%2F%22%2C%22cb%22%3A%22TFASC.trkCallback%22%2C%22qs%22%3A%22%22%2C%22r%22%3A%5B%7B%22li%22%3A%22rbox-tracking%22%2C%22s%22%3A0%2C%22uim%22%3A%22rbox-tracking%3Apub%3Dkohlsnew-sccnx%3Aabp%3D0%22%2C%22uip%22%3A%22rbox-tracking%22%2C%22orig_uip%22%3A%22rbox-tracking%22%7D%5D%2C%22mpv%22%3Atrue%2C%22mpvd%22%3A%7B%22en%22%3A%22page_view%22%2C%22item-url%22%3A%22https%3A%2F%2Fm.kohls.com%2F%22%2C%22tim%22%3A1662113074453%2C%22ref%22%3A%22https%3A%2F%2Fm.kohls.com%2F%22%2C%22tos%22%3A38413%2C%22ssd%22%3A1%2C%22scd%22%3A88%7D%7D&pubit=i", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=t_gid", END_INLINE,
            "URL=https://cds.taboola.com/?uid=4e6d2ce6-a080-452e-9455-33841bab4609-tucta0b5a84&src=tfa", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=t_gid", END_INLINE,
            "URL=https://adservice.google.com/ddm/fls/z/src=8632166;type=landi0;cat=unive0;ord=213072578550;gtm=2od8v0;auiddc=*;u1=not%20set;u10=https%3A%2F%2Fm.kohls.com%2F;u11=homepage;u13=not%20set;u2=not%20set;u23=Current%20Customer;u24=78611155637513935743550952407821114794;u25=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fm.kohls.com%2F", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://alb.reddit.com/rp.gif?ts=1662113074487&id=t2_90kv23sl&event=PageVisit&m.itemCount=&m.value=&m.valueDecimal=&m.currency=&m.transactionId=&m.customEventName=&m.products=&uuid=214f42b3-ed7a-42cf-a7c0-1c97815d77e8&aaid=&em=&external_id=62109380ff146a9e9b573f74145632abbfc279ddc29bc9d459be441acaf32645&idfa=&integration=reddit&opt_out=0&sh=1313&sw=489&v=rdt_02c59ad6", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://d.impactradius-event.com/A375953-1cd4-4523-a263-b5b3c8c11fb81.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1820b14411e2/RC7c48da855924464dbf1b33bce959dcf7-source.min.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("floop_10", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("X349_3");
    ns_web_url ("X349_3",
        "URL=https://kohls.sjv.io/cur/5349",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "custid=78611155637513935743550952407821114794&_ir=U58%7C1662113026239.a3yoq7s1u74%7C1662113026239",
        BODY_END,
        INLINE_URLS,
            "URL=https://servedby.flashtalking.com/container/1638;12462;1480;iframe/?spotName=Homepage&U3=78611155637513935743550952407821114794&U7=fc7de59e-daa5-21f4-8b5d-bbddb427fd3d&U10=N/A&cachebuster=532671.188068637", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-Dest:iframe", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=flashtalkingad1", END_INLINE,
            "URL=https://servedby.flashtalking.com/segment/modify/xh0;;pixel/?name=Homepage_2019", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=flashtalkingad1", END_INLINE,
            "URL=https://idsync.rlcdn.com/422866.gif?partner_uid=53588F2D83C0A9&", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=pxrc;rlas3", END_INLINE,
            "URL=https://d9.flashtalking.com/d9core", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=flashtalkingad1", END_INLINE
    );

    ns_end_transaction("X349_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("lgc_2");
    ns_web_url ("lgc_2",
        "URL=https://d9.flashtalking.com/lgc",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://servedby.flashtalking.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=flashtalkingad1",
        BODY_BEGIN,
            "tbx=%257B%2522D9_1%2522%253A1662113074610%252C%2522D9_6%2522%253Anull%252C%2522D9_7%2522%253Anull%252C%2522D9_8%2522%253Anull%252C%2522D9_9%2522%253A%255B%255D%252C%2522D9_10%2522%253A%255B%255D%252C%2522D9_61%2522%253A%25222ee030229bea879035c93c913dbd524e%2522%252C%2522D9_67%2522%253A%2522d10e54cb41fb43c5945b4d01b9bde1da%2522%252C%2522D9_18%2522%253A%257B%257D%252C%2522D9_16%2522%253A300%252C%2522D9_4%2522%253A%257B%2522width%2522%253A1313%252C%2522height%2522%253A489%257D%252C%2522D9_14%2522%253A%2522Linux%2520x86_64%2522%252C%2522D9_15%2522%253A%2522en-US%2522%252C%2522D9_19%2522%253A%2522Mozilla%2522%252C%2522D9_123%2522%253A0%252C%2522D9_33%2522%253A%2522YTplbiMjI2I6MjQjIyNjOjMwMCMjI2Q6dHJ1ZSMjI2Y6dHJ1ZSMjI2c6dW5kZWZpbmVkIyMjaTp1bmRlZmluZWQjIyNqOkxpbnV4IHg4Nl82NA%253D%253D%2522%252C%2522D9_34%2522%253A1691054835%252C%2522D9_30%2522%253A%255B%255D%252C%2522D9_52%2522%253A%257B%257D%252C%2522D9_57%2522%253Atrue%252C%2522D9_58%2522%253A%257B%2522DeviceID%2522%253Atrue%257D%252C%2522D9_59%2522%253A%257B%2522UserID%2522%253A%252253588F2D83C0A9%2522%252C%2522TAG%2522%253A%25222%2522%252C%2522AdvID%2522%253A%25221121%2522%252C%2522SiteID%2522%253A%25221638%2522%257D%252C%2522D9_63%2522%253A%2522m.kohls.com%2522%252C%2522D9_64%2522%253A1%252C%2522D9_66%2522%253A%2522%2522%257D",
        BODY_END,
        INLINE_URLS,
            "URL=https://trc.taboola.com/1456663/log/3/unip?en=page_view&item-url=https%3A%2F%2Fm.kohls.com%2F&tim=1662113074453&ui=4e6d2ce6-a080-452e-9455-33841bab4609-tucta0b5a84&ref=https%3A%2F%2Fm.kohls.com%2F&cv=20220828-2-RELEASE&tos=38582&ssd=1&scd=88&vi=1662113074441&ri=bcdf0ba34764466ca5a5fa1dd0ae9873&sd=v2_7e4ffdfaaca3d751ced3beb96cf5c12f_4e6d2ce6-a080-452e-9455-33841bab4609-tucta0b5a84_1662113028_1662113074_CNawjgYQl_RYGImao-2vMCACKAEw4QE4kaQOQNWmD0juy9kDUIIEWABgAGiXjKXetYHfjKIBcAE", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Origin:https://m.kohls.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=t_gid", END_INLINE
    );

    ns_end_transaction("lgc_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("bf_10");
    ns_web_url ("bf_10",
        "URL=https://bf27853irn.bf.dynatrace.com/bf?type=js3&sn=v_4_srv_4_sn_IV7VUE896KFTV6I5O956PBOGGHT16VT8_app-3A06d81dff759102c9_1_ol_0_perc_100000_mul_1&svrid=4&flavor=cors&vi=WTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0&modifiedSince=1662094650929&rf=https%3A%2F%2Fm.kohls.com%2F&bp=3&app=06d81dff759102c9&crc=2460640855&en=ldtlho0d&end=1",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$tvn=%2F$tvt=1662113067240$tvm=i1%3Bk0%3Bh0$tvtrg=1$ni=4g|10$rt=8-1662113067240%3Bhttps%3A%2F%2Fm.kohls.com%2FlBzjNRQPfM%2FSq6c%2F1G5U4A%2FiXEDf6VLOw%2FYnB5eA%2FSm9pFh0%2FURUY%7Cb4005e0f0g0h0i0j0k2l158m161u1315v17w17K1T-6z1I1%7Chttps%3A%2F%2Fas-sec.casalemedia.com%2Fheaderstats%3Fs%3D189641%26u%3Dhttps_253A_252F_252Fm.kohls.com_252F%26v%3D3%7Cb4093e0m113z1I1%7Chttps%3A%2F%2Fas-sec.casalemedia.com%2Fheaderstats%3Fs%3D189641%26u%3Dhttps_253A_252F_252Fm.kohls.com_252F%26v%3D3%7Cb4095e0m109z1I1%7Chttps%3A%2F%2Fas-sec.casalemedia.com%2Fheaderstats%3Fs%3D189641%26u%3Dhttps_253A_252F_252Fm.kohls.com_252F%26v%3D3%7Cb4119e0m100z1I1%7Chttps%3A%2F%2Fb-code.liadm.com%2Fsync-container.js%7Cb4188e0m4I12%7Chttps%3A%2F%2Flogin.dotomi.com%2Fucm%2FUCMController%3Fdtm_5Fcom%3D28%26dtm_5Fcid%3D2683%26dtm_5Fcmagic%3D8420d3%26dtm_5Ffid%3D101%26dtm_5Fformat%3D6%26cli_5Fpromo_5Fid%3D1%26dtm_5Femail_5Fhash%3Dnot_2520set%26dtm_5Fuser_5Fid%3D%7Cb4239e0m137N3O1P1I7%7Chttps%3A%2F%2Fassets.adobedtm.com%2F6d2e783bbfe3%2Fc0c47c5c96c6%2F1820b14411e2%2FRC5c9a22d5a56847e08ecebe48b81f6d22-source.min.js%7Cb4246e0f0g0h0i0j0k2l3m4v519w814I12%7Chttps%3A%2F%2Fassets.adobedtm.com%2F6d2e783bbfe3%2Fc0c47c5c96c6%2F1820b14411e2%2FRC7705d8cfc8794e0c8ecf7777ec37c7b8-source.min.js%7Cb4250e0f0g0h0i0j0k2l2m3v784w1593I12%7Chttps%3A%2F%2Ftpc.googlesyndication.com%2Fsodar%2Fsodar2%2F225%2Frunner.html%7Cb4258e0m9BiI4%7Chttps%3A%2F%2Fwww.google.com%2Frecaptcha%2Fapi2%2Faframe%7Cb4265e0m21Bi2I4%7Chttps%3A%2F%2Frcom.dynamicyield.com%2Fv3%2Frecommend%2F8776374%3F_5F%3D1662113071549%7Cb4310e0f0g0h0i0j0k2l124m126u5832v5083w24857z1I1%7Chttps%3A%2F%2Fcdnssl.clicktale.net%2Fwww47%2Fptc%2F630f0ceb-a670-47fc-a7d0-b2a2133e9266.js%7Cb4357e0m30I12%7Chttps%3A%2F%2Fs.yimg.com%2Fwi%2Fytc.js%7Cb4379e0m16I12%7Chttps%3A%2F%2Fsp.analytics.yahoo.com%2Fsp.pl%7Cb4381e0m49N3O1P1I7%7Chttps%3A%2F%2Fsp.analytics.yahoo.com%2Fsp.pl%7Cb4382e0m51N3O1P1I7%7Chttps%3A%2F%2Fassets.adobedtm.com%2F6d2e783bbfe3%2Fc0c47c5c96c6%2F1820b14411e2%2FRC8ceaea1fd94a499eb45113af297edb25-source.min.js%7Cb4387e0f0g0h0i0j0k2l9m12v311w479I12%7Chttps%3A%2F%2Frp.liadm.com%2Fj%7Cb4433e0m70z1I1%7Chttps%3A%2F%2Fcdnssl.clicktale.net%2Fptc%2F630f0ceb-a670-47fc-a7d0-b2a2133e9266.js%7Cb4520e0m8I12%7Chttps%3A%2F%2Fgoogleads.g.doubleclick.net%2Fpagead%2Fviewthroughconversion%2F1071871169%2F%3Fvalue%3D0%26guid%3DON%26script%3D0%7Cb4562c0d99e106f106g106h106i106j106k119l149m151u64v42w42I7%7Chttps%3A%2F%2Fmedia.kohlsimg.com%2Fis%2Fimage%2Fkohls%2F2245601_5FBlack%3Fwid%3D800%26hei%3D800%26op_5Fsharpen%3D1%7Cb4592e0f0g0h0i0j0m0v30460w30460N3O800P800I7%7Chttps%3A%2F%2Fmedia.kohlsimg.com%2Fis%2Fimage%2Fkohls%2F4569328_5FMedium_5FGray%3Fwid%3D800%26hei%3D800%26op_5Fsharpen%3D1%7Cb4592e0f0g0h0i0j0m0v221432w221432N3O800P800I7%7Chttps%3A%2F%2Fcdnjs.cloudflare.com%2Fajax%2Flibs%2FSwiper%2F4.4.6%2Fjs%2Fswiper.js%7Cb4600e0f0g0h0i0j0k2l14m24v39981w274822K1I12%7Chttps%3A%2F%2Fmedia.kohlsimg.com%2Fis%2Fimage%2Fkohls%2F1730721_5FNavy_5FDot%3Fwid%3D800%26hei%3D800%26op_5Fsharpen%3D1%7Cb4600e0f0g0h0i0j0k15l39m42u32308v31992w31992N3O800P800I7%7Chttps%3A%2F%2Fmedia.kohlsimg.com%2Fis%2Fimage%2Fkohls%2F4865781_5FRed_5FWhite_5FBlack%3Fwid%3D800%26hei%3D800%26op_5Fsharpen%3D1%7Cb4600e0f0g0h0i0j0k15l43m59u43829v43496w43496N3O800P800I7%7Chttps%3A%2F%2Fmedia.kohlsimg.com%2Fis%2Fimage%2Fkohls%2F5267619_5FOlive%3Fwid%3D800%26hei%3D800%26op_5Fsharpen%3D1%7Cb4601e0f0g0h0i0j0k15l58m64u53109v52738w52738N3O800P800I7%7Chttps%3A%2F%2Fmedia.kohlsimg.com%2Fis%2Fimage%2Fkohls%2F2247719_5FFog_5FHeather%3Fwid%3D800%26hei%3D800%26op_5Fsharpen%3D1%7Cb4601e0f0g0h0i0j0k16l64m79u58704v58326w58326N3O800P800I7%7Chttps%3A%2F%2Fmedia.kohlsimg.com%2Fis%2Fimage%2Fkohls%2F1695762_5FFrisco%3Fwid%3D800%26hei%3D800%26op_5Fsharpen%3D1%7Cb4601e0f0g0h0i0j0k16l65m79u41386v41050w41050N3O800P800I7%7Chttps%3A%2F%2Fmedia.kohlsimg.com%2Fis%2Fimage%2Fkohls%2F5583951_5FPanda%3Fwid%3D800%26hei%3D800%26op_5Fsharpen%3D1%7Cb4601e0f0g0h0i0j0k15l69m81u59013v58634w58634N3O800P800I7%7Chttps%3A%2F%2Fi.liadm.com%2Fs%2Fc%2Fa-00oc%7Cb4624e0m231Bi3I4%7Chttps%3A%2F%2Fsli.kohls.com%2Fbaker%3Fdtstmp%3D1662113071864%7Cb4625e0m57I7%7Chttps%3A%2F%2Fasync-px.dynamicyield.com%2Fbatch%3Fcnst%3D1%26_5F%3D1662113071885_5F71630%7Cb4646e0m88I3%7Chttps%3A%2F%2Fcdnssl.clicktale.net%2Fpcc%2F630f0ceb-a670-47fc-a7d0-b2a2133e9266.js%3FDeploymentConfigName%3DMalka_5F20220809%26Version%3D1%7Cb4672e0m17I12%7Chttps%3A%2F%2Fcdnssl.clicktale.net%2Fwww%2Fbridge-WR110.js%7Cb4672e0m13I12%7Chttps%3A%2F%2Fq-aus1.clicktale.net%2Fquota%3Fenc%3Draw%7Cb4714e0f0g0h0i0j0k64l124m125u243v29w29z1I2%7Chttps%3A%2F%2Fc.clicktale.net%2Fpageview%7Cb4722e0f0g0h0i0j0k3l63m64u319I7%7Chttps%3A%2F%2Fc.clicktale.net%2FpageEvent%7Cb4811e0f0g0h0i0j0k3l64m66u319I7%7Chttps%3A%2F%2Fc.clicktale.net%2FpageEvent%7Cb4818e0f0g0h0i0j0k3l63m64u319I7%7Chttps%3A%2F%2Fk-aus1.clicktale.net%2Fv2%2Frecording%3Frt%3D7%26v%3D11.40.1%26pid%3D2399%26uu%3D6d513319-e33c-a1a2-f741-3ecaf18284e9%26sn%3D1%26pn%3D1%26ri%3D20%26rst%3D1662113025036%26let%3D1662113067183%26hlm%3Dtrue%26enc%3Dlzstring%7Cb4819e0f0g0h0i0j0k3l64m65u186z1I2%7Chttps%3A%2F%2Fc.clicktale.net%2Fdvar%7Cb4824e0f0g0h0i0j0k2l62m63u319I7%7Chttps%3A%2F%2Fpagead2.googlesyndication.com%2Fpagead%2Fsodar%7Cb5138e0m38I7$url=https%3A%2F%2Fm.kohls.com%2F$title=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More$latC=1$app=06d81dff759102c9$vi=WTUPVSMATRRKAKRNEWGKSAFKUOLSQPEJ-0$fId=113068061_316$v=10247220811100421$vID=1662113020005SBVNC9V48OJOAGOMH3HKO8K0MK5NUIMM$time=1662113074687",
        BODY_END,
        INLINE_URLS,
            "URL=https://i.flashtalking.com/ft/?aid=1638&uid=D9:Bot&seg=xh0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=flashtalkingad1", END_INLINE,
            "URL=https://www.facebook.com/tr/?id=322052792167924&ev=Microdata&dl=https%3A%2F%2Fm.kohls.com%2F&rl=https%3A%2F%2Fm.kohls.com%2F&if=false&ts=1662113075137&cd[DataLayer]=%5B%5D&cd[Meta]=%7B%22title%22%3A%22Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More%5Cn%20%5Cn%22%2C%22meta%3Adescription%22%3A%22Enjoy%20free%20shipping%20and%20easy%20returns%20every%20day%20at%20Kohl%27s!%20Find%20great%20savings%20on%20clothing%2C%20shoes%2C%20toys%2C%20home%20d%C3%A9cor%2C%20appliances%20and%20electronics%20for%20the%20whole%20family.%5Cn%20%22%2C%22meta%3Akeywords%22%3A%22%22%7D&cd[OpenGraph]=%7B%7D&cd[Schema.org]=%5B%5D&cd[JSON-LD]=%5B%5D&sw=1313&sh=489&v=2.9.79&r=stable&ec=1&o=30&fbp=fb.1.1662113027985.1842180297&it=1662113073456&coo=false&es=automatic&tm=3&exp=e0&rqm=GET", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.facebook.com/tr/?id=1159959614410832&ev=Microdata&dl=https%3A%2F%2Fm.kohls.com%2F&rl=https%3A%2F%2Fm.kohls.com%2F&if=false&ts=1662113075142&cd[DataLayer]=%5B%5D&cd[Meta]=%7B%22title%22%3A%22Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More%5Cn%20%5Cn%22%2C%22meta%3Adescription%22%3A%22Enjoy%20free%20shipping%20and%20easy%20returns%20every%20day%20at%20Kohl%27s!%20Find%20great%20savings%20on%20clothing%2C%20shoes%2C%20toys%2C%20home%20d%C3%A9cor%2C%20appliances%20and%20electronics%20for%20the%20whole%20family.%5Cn%20%22%2C%22meta%3Akeywords%22%3A%22%22%7D&cd[OpenGraph]=%7B%7D&cd[Schema.org]=%5B%5D&cd[JSON-LD]=%5B%5D&sw=1313&sh=489&v=2.9.79&r=stable&ec=1&o=30&fbp=fb.1.1662113027985.1842180297&it=1662113073456&coo=false&es=automatic&tm=3&exp=e0&rqm=GET", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("bf_10", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("recording_26");
    ns_web_url ("recording_26",
        "URL=https://k-aus1.clicktale.net/v2/recording?rt=7&v=11.40.1&pid=2399&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&pn=2&ri=5&rst=1662113071979&let=1662113075185&enc=lzstring",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/recording_26_main_url_1_1662113085182.body",
        BODY_END
    );

    ns_end_transaction("recording_26", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("events_2");
    ns_web_url ("events_2",
        "URL=https://c.clicktale.net/v2/events?v=11.40.1&str=82&di=1273&dc=1835&fl=1859&sr=91&mdh=15756&pn=2&uu=6d513319-e33c-a1a2-f741-3ecaf18284e9&sn=1&lv=1662113024&lhd=1662113024&hd=1662113071&pid=2399&ct=0&enc=raw",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://m.kohls.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "[{"type":0,"ts":13,"x":1313,"y":361},{"type":19,"name":"FCP","val":317.5700000138022,"ts":14},{"type":19,"name":"CLS","val":0.004377553676628462,"ts":97},{"type":19,"name":"CLS","val":0.012981550523338287,"ts":97},{"type":19,"name":"CLS","val":0.08473858056956325,"ts":97},{"type":19,"name":"CLS","val":0.12434800801459034,"ts":97},{"type":19,"name":"FID","val":247.36000003758818,"ts":97},{"type":19,"name":"LCP","val":317.574,"ts":97},{"type":19,"name":"LCP","val":593.545,"ts":97},{"type":19,"name":"TTFB","val":11.765000002924353,"ts":103},{"type":19,"name":"CLS","val":0.9715047107952144,"ts":912},{"type":19,"name":"CLS","val":1.025481983522487,"ts":1153},{"type":19,"name":"CLS","val":1.1256673601822944,"ts":1292},{"type":1,"ts":2164,"x":0,"y":11719,"d":1414},{"type":4,"ts":2207,"x":627,"y":12061,"tgt":"html:eq(0)"},{"type":6,"ts":2340,"x":627,"y":12061,"tgt":"div#open-drawer>div:eq(0)>div:eq(2)>a:eq(0)"},{"type":7,"ts":2779,"x":567,"y":12021,"tgt":"div#open-drawer>div:eq(0)>div:eq(2)>a:eq(0)"},{"type":6,"ts":2779,"x":567,"y":12021,"tgt":"div#open-drawer>div:eq(0)>div:eq(1)>div:eq(0)>div:eq(0)>div:eq(0)>div:eq(4)>div:eq(0)>a:eq(1)>p:eq(1)"},{"type":2,"ts":2781,"x":567,"y":12021,"xRel":17107,"yRel":51079,"tgtHM":"div#open-drawer>div:eq(0)>div:eq(1)>div:eq(0)>div:eq(0)>div:eq(0)>div:eq(4)>div:eq(0)>a:eq(1)>p:eq(1)"},{"type":7,"ts":2794,"x":441,"y":11927,"tgt":"div#open-drawer>div:eq(0)>div:eq(1)>div:eq(0)>div:eq(0)>div:eq(0)>div:eq(4)>div:eq(0)>a:eq(1)>p:eq(1)"},{"type":6,"ts":2828,"x":328,"y":11841,"tgt":"article#hp-equity12>div:eq(1)>div:eq(0)>a:eq(0)"},{"type":7,"ts":2850,"x":287,"y":11799,"tgt":"article#hp-equity12>div:eq(1)>div:eq(0)>a:eq(0)"},{"type":2,"ts":3185,"x":238,"y":11745,"xRel":12016,"yRel":28399,"tgtHM":"div#mcom-header"},{"type":2,"ts":3585,"x":211,"y":11721,"xRel":10653,"yRel":2185,"tgtHM":""}]",
        BODY_END
    );

    ns_end_transaction("events_2", NS_AUTO_STATUS);
    ns_page_think_time(0.078);

}
