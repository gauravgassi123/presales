/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: 
    Date of recording: 08/26/2022 02:02:43
    Flow details:
    Build details: 
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("Main_Page");
    ns_web_url ("Main_Page",
        "URL=https://wiki.lntdccs.com/",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "REDIRECT=YES",
        "LOCATION=https://wiki.lntdccs.com/lntwiki/",
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://wiki.lntdccs.com/lntwiki/index.php/Main_Page", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/index.php/Main_Page", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=skins.timeless&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=site.styles&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/e/e5/L%26T.png/300px-L%26T.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/user-large-grey.svg?4070b", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/arrow-down-grey.svg?46d67", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/search-ltr.svg?402b1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/cat.svg?31aa0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/page-grey.svg?a26bc", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/gear-large-grey.svg?eba28", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/menu-large-grey.svg?7b579", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/gear-grey.svg?27b72", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/talk-grey.svg?b4d04", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/brackets-grey.svg?c043f", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/clock-grey.svg?48197", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/arrow-down-grey.svg?46d67", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/user-large-grey.svg?4070b", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/search-ltr.svg?402b1", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/cat.svg?31aa0", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.skinning/images/external-ltr.svg?59558", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery%2Csite%7Cjquery.client%2Ccookie%7Cmediawiki.String%2CTitle%2Capi%2Cbase%2Ccldr%2Ccookie%2CjqueryMsg%2Clanguage%2Ctoc%2Cutil%7Cmediawiki.libs.pluralruleparser%7Cmediawiki.page.ready%7Cskins.timeless.js&skin=timeless&version=1ptgd", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/page-grey.svg?a26bc", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/favicon.ico", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/user-grey.svg?9781b", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/eyeball-grey.svg?f17bd", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("Main_Page", NS_AUTO_STATUS);
    ns_page_think_time(9.981);

    //Page Auto split for Link 'image' Clicked by User
    ns_start_transaction("index_php");
    ns_web_url ("index_php",
        "URL=https://wiki.lntdccs.com/lntwiki/index.php?title=Special:UserLogin&returnto=Main+Page",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=mediawiki.htmlform.styles%7Cmediawiki.special.userlogin.common.styles%7Cmediawiki.special.userlogin.login.styles%7Cmediawiki.ui%7Cmediawiki.ui.button%2Ccheckbox%2Cinput%2Cradio%7Cskins.timeless&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&safemode=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.special.userlogin.login.styles/images/glyph-people-large.png?707d6", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery%7Cjquery.lengthLimit%7Cmediawiki.htmlform&skin=timeless&version=1yb9e", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.special.userlogin.login.styles/images/glyph-people-large.png?707d6", END_INLINE
    );

    ns_end_transaction("index_php", NS_AUTO_STATUS);
    ns_page_think_time(18.682);

    //Page Auto split for Button 'Log in' Clicked by User
    ns_start_transaction("Main_Page_2");
    ns_web_url ("Main_Page_2",
        "URL=https://wiki.lntdccs.com/lntwiki/index.php?title=Special:UserLogin&returnto=Main+Page",
        "METHOD=POST",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Origin:https://wiki.lntdccs.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=lntwiki_session",
        "REDIRECT=YES",
        "LOCATION=https://wiki.lntdccs.com/lntwiki/index.php/Main_Page",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/Main_Page_2_url_0_1_1661522563653.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/index.php/Main_Page", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=skins.timeless&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=site.styles&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/e/e5/L%26T.png/300px-L%26T.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/page-grey.svg?a26bc", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/talk-grey.svg?b4d04", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/star.svg?2328e", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/eyeball-grey.svg?f17bd", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/brackets-grey.svg?c043f", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/clock-grey.svg?48197", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.skinning/images/external-ltr.svg?59558", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/star.svg?2328e", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery%2Csite%7Cmediawiki.page.watch.ajax&skin=timeless&version=n1xai", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/index.php/Main_Page#Delivery", END_INLINE
    );

    ns_end_transaction("Main_Page_2", NS_AUTO_STATUS);
    ns_page_think_time(11.033);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("Delivery");
    ns_web_url ("Delivery",
        "URL=https://wiki.lntdccs.com/lntwiki/index.php/Delivery",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName",
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=skins.timeless&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=site.styles&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery&skin=timeless&version=bh3lo", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery&skin=timeless&version=bh3lo", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/index.php/Delivery#SOC", END_INLINE
    );

    ns_end_transaction("Delivery", NS_AUTO_STATUS);
    ns_page_think_time(8.991);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("Category_SOC");
    ns_web_url ("Category_SOC",
        "URL=https://wiki.lntdccs.com/lntwiki/index.php/Category:SOC",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName",
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=mediawiki.action.view.categoryPage.styles%7Cmediawiki.helplink%7Cskins.timeless&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=site.styles&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery&skin=timeless&version=bh3lo", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.helplink/images/helpNotice.svg?46d34", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.helplink/images/helpNotice.svg?46d34", END_INLINE
    );

    ns_end_transaction("Category_SOC", NS_AUTO_STATUS);
    ns_page_think_time(66.995);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("SOC_Firewall_Fortigate_SOP");
    ns_web_url ("SOC_Firewall_Fortigate_SOP",
        "URL=https://wiki.lntdccs.com/lntwiki/index.php/SOC:Firewall_Fortigate_SOP",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName",
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=skins.timeless&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=site.styles&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/e/ea/FGT1.png/300px-FGT1.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.skinning/images/external-ltr.svg?59558", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery&skin=timeless&version=bh3lo", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/2/2f/FGT2.png/300px-FGT2.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/9/9c/FGT3.png/300px-FGT3.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/b/b9/FGT4.png/300px-FGT4.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/e/e0/FGT5.png/300px-FGT5.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/1/1a/FGT6.png/300px-FGT6.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/7/73/FGT7.png/300px-FGT7.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/2/24/FGT8.png/300px-FGT8.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/1/16/FGT9.png/300px-FGT9.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/8/85/FGT10.png/300px-FGT10.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/e/e0/FGT5.png/300px-FGT5.png", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/1/1a/FGT6.png/300px-FGT6.png", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/f/fa/FGT11.png/300px-FGT11.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/6/60/FGT12.png/300px-FGT12.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/a/a1/FGT13.png/300px-FGT13.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/e/ea/FGT14.png/300px-FGT14.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/2/23/FGT15.png/300px-FGT15.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/3/33/FGT16.png/300px-FGT16.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/b/bb/FGT17.png/300px-FGT17.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/6/68/FGT18.png/300px-FGT18.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/0/0b/FGT19.png/300px-FGT19.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/4/41/FGT20.png/300px-FGT20.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/d/da/FGT21.png/300px-FGT21.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/6/6a/FGT22.png/300px-FGT22.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/2/2c/FGT23.png/300px-FGT23.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/e/ef/FGT24.png/300px-FGT24.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/e/eb/FGT25.png/300px-FGT25.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/7/71/FGT26.png/300px-FGT26.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/a/af/FGT27.png/300px-FGT27.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/2/2a/FGT28.png/300px-FGT28.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/3/38/FGT29.png/300px-FGT29.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/9/9e/FGT30.png/300px-FGT30.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/d/d9/FGT31.png/300px-FGT31.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/8/8c/FGT32.png/300px-FGT32.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/3/34/FGT33.png/300px-FGT33.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/7/7c/FGT34.png/300px-FGT34.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/e/e7/FGT35.png/300px-FGT35.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE
    );

    ns_end_transaction("SOC_Firewall_Fortigate_SOP", NS_AUTO_STATUS);
    ns_page_think_time(33.434);

}
