/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: Anjali.chauhan
    Date of recording: 09/07/2022 09:37:14
    Flow details:
    Build details: 4.8.0 (build# 129)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("Main_Page");
    ns_web_url ("Main_Page",
        "URL=https://wiki.lntdccs.com/",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "REDIRECT=YES",
        "LOCATION=https://wiki.lntdccs.com/lntwiki/",
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-US,en;q=0.9", "REDIRECT=YES", "LOCATION=https://wiki.lntdccs.com/lntwiki/index.php/Main_Page", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/index.php/Main_Page", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=skins.timeless&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=site.styles&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/e/e5/L%26T.png/300px-L%26T.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/user-large-grey.svg?4070b", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/arrow-down-grey.svg?46d67", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/search-ltr.svg?402b1", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/cat.svg?31aa0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/page-grey.svg?a26bc", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/clock-grey.svg?48197", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/gear-large-grey.svg?eba28", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/menu-large-grey.svg?7b579", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/gear-grey.svg?27b72", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/brackets-grey.svg?c043f", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/talk-grey.svg?b4d04", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/user-large-grey.svg?4070b", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/arrow-down-grey.svg?46d67", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/search-ltr.svg?402b1", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/cat.svg?31aa0", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&skin=timeless", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.skinning/images/external-ltr.svg?59558", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/page-grey.svg?a26bc", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery%2Csite%7Cjquery.client%2Ccookie%7Cmediawiki.String%2CTitle%2Capi%2Cbase%2Ccldr%2Ccookie%2CjqueryMsg%2Clanguage%2Ctoc%2Cutil%7Cmediawiki.libs.pluralruleparser%7Cmediawiki.page.ready%7Cskins.timeless.js&skin=timeless&version=1ptgd", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/user-grey.svg?9781b", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/eyeball-grey.svg?f17bd", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("Main_Page", NS_AUTO_STATUS);
    ns_page_think_time(10.23);

    //Page Auto split for Link 'image' Clicked by User
    ns_start_transaction("index_php");
    ns_web_url ("index_php",
        "URL=https://wiki.lntdccs.com/lntwiki/index.php?title=Special:UserLogin&returnto=Main+Page",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=mediawiki.htmlform.styles%7Cmediawiki.special.userlogin.common.styles%7Cmediawiki.special.userlogin.login.styles%7Cmediawiki.ui%7Cmediawiki.ui.button%2Ccheckbox%2Cinput%2Cradio%7Cskins.timeless&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&safemode=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery%7Cjquery.lengthLimit%7Cmediawiki.htmlform&skin=timeless&version=1yb9e", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.special.userlogin.login.styles/images/glyph-people-large.png?707d6", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.special.userlogin.login.styles/images/glyph-people-large.png?707d6", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery%7Cjquery.lengthLimit%7Cmediawiki.htmlform&skin=timeless&version=1yb9e", END_INLINE
    );

    ns_end_transaction("index_php", NS_AUTO_STATUS);
    ns_page_think_time(10.77);

    //Page Auto split for Button 'Log in' Clicked by User
    ns_start_transaction("Main_Page_2");
    ns_web_url ("Main_Page_2",
        "URL=https://wiki.lntdccs.com/lntwiki/index.php?title=Special:UserLogin&returnto=Main+Page",
        "METHOD=POST",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Origin:https://wiki.lntdccs.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=lntwiki_session",
        "REDIRECT=YES",
        "LOCATION=https://wiki.lntdccs.com/lntwiki/index.php/Main_Page",
        BODY_BEGIN,
            "wpName={Uname}&wpPassword={Pwd}&wploginattempt=Log+in&wpEditToken=%2B%5C&title=Special%3AUserLogin&authAction=login&force=&wpLoginToken={LoginToken}",
        BODY_END,
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/index.php/Main_Page", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=skins.timeless&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=site.styles&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/page-grey.svg?a26bc", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/talk-grey.svg?b4d04", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/star.svg?2328e", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery%2Csite%7Cmediawiki.page.watch.ajax&skin=timeless&version=n1xai", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/e/e5/L%26T.png/300px-L%26T.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/eyeball-grey.svg?f17bd", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/brackets-grey.svg?c043f", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/skins/Timeless/resources/images/clock-grey.svg?48197", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.skinning/images/external-ltr.svg?59558", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery%2Csite%7Cmediawiki.page.watch.ajax&skin=timeless&version=n1xai", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/index.php/Main_Page#Delivery", END_INLINE
    );

    ns_end_transaction("Main_Page_2", NS_AUTO_STATUS);
    ns_page_think_time(19.853);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("Delivery");
    ns_web_url ("Delivery",
        "URL=https://wiki.lntdccs.com/lntwiki/index.php/Delivery",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName",
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=skins.timeless&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=site.styles&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery&skin=timeless&version=bh3lo", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery&skin=timeless&version=bh3lo", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/index.php/Delivery#SOC", END_INLINE
    );

    ns_end_transaction("Delivery", NS_AUTO_STATUS);
    ns_page_think_time(9.356);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("Category_SOC");
    ns_web_url ("Category_SOC",
        "URL=https://wiki.lntdccs.com/lntwiki/index.php/Category:SOC",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName",
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=mediawiki.action.view.categoryPage.styles%7Cmediawiki.helplink%7Cskins.timeless&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=site.styles&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery&skin=timeless&version=bh3lo", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.helplink/images/helpNotice.svg?46d34", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.helplink/images/helpNotice.svg?46d34", END_INLINE
    );

    ns_end_transaction("Category_SOC", NS_AUTO_STATUS);
    ns_page_think_time(7.636);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("Critical_Security_Incident_N");
    ns_web_url ("Critical_Security_Incident_N",
        "URL=https://wiki.lntdccs.com/lntwiki/index.php/Critical_Security_Incident_Notification",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName",
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=skins.timeless&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=site.styles&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/1/16/Pa1.jpg/300px-Pa1.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/9/92/Pa2.jpg/300px-Pa2.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/6/6d/Pa3.jpg/300px-Pa3.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/e/e1/Pa4.jpg/300px-Pa4.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/c/c6/Pa5.jpg/300px-Pa5.jpg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/src/mediawiki.skinning/images/external-ltr.svg?59558", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery&skin=timeless&version=bh3lo", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/1/16/Pa1.jpg/300px-Pa1.jpg", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/e/e1/Pa4.jpg/300px-Pa4.jpg", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/6/6d/Pa3.jpg/300px-Pa3.jpg", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/9/92/Pa2.jpg/300px-Pa2.jpg", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/images/thumb/c/c6/Pa5.jpg/300px-Pa5.jpg", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/index.php/Critical_Security_Incident_Notification#How_to_Notify", END_INLINE
    );

    ns_end_transaction("Critical_Security_Incident_N", NS_AUTO_STATUS);
    ns_page_think_time(91.048);

    //Page Auto split for Link 'image' Clicked by User
    ns_start_transaction("load_php");
    ns_web_url ("load_php",
        "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=mediawiki.notification%2CvisibleTimeout&skin=timeless&version=1ph72",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:script",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName"
    );

    ns_end_transaction("load_php", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("api_php");
    ns_web_url ("api_php",
        "URL=https://wiki.lntdccs.com/lntwiki/api.php",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-Requested-With:XMLHttpRequest",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
        "HEADER=Origin:https://wiki.lntdccs.com",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=lntwiki_session;lntwikiUserID;lntwikiUserName",
        BODY_BEGIN,
            "action=logout&format=json&token=f0e0f2b4ac12b7370eb62f96bb9801e863186587%2B%5C",
        BODY_END
    );

    ns_end_transaction("api_php", NS_AUTO_STATUS);
    ns_page_think_time(0.021);

    ns_start_transaction("index_php_2");
    ns_web_url ("index_php_2",
        "URL=https://wiki.lntdccs.com/lntwiki/index.php?title=Special:UserLogout&returnto=Critical+Security+Incident+Notification",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=lntwikiUserName;UseDC;UseCDNCache",
        INLINE_URLS,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=skins.timeless&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=startup&only=scripts&raw=1&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=site.styles&only=styles&skin=timeless", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/resources/assets/change-your-logo.svg?a60df", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwikiUserName;UseDC;UseCDNCache", END_INLINE,
            "URL=https://wiki.lntdccs.com/lntwiki/load.php?lang=en&modules=jquery&skin=timeless&version=bh3lo", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=lntwikiUserName;UseDC;UseCDNCache", END_INLINE
    );

    ns_end_transaction("index_php_2", NS_AUTO_STATUS);
    ns_page_think_time(5.133);

}
