/*-----------------------------------------------------------------------------
    Name: Guriya
    Recorded By: Guriya
    Date of recording: 09/20/2022 03:31:14
    Flow details:
    Build details: 4.8.0 (build# 134)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void Guriya()
{
    ns_start_transaction("Home");
    ns_web_url ("Home",
        "URL=https://petstore.octoperf.com/actions/Catalog.action",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=https://petstore.octoperf.com/css/jpetstore.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/logo-topbar.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/cart.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/separator.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/sm_fish.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/sm_dogs.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/sm_reptiles.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/sm_cats.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/sm_birds.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/fish_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/dogs_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/cats_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/reptiles_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/birds_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/splash.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE
    );

    ns_end_transaction("Home", NS_AUTO_STATUS);
    ns_page_think_time(3.752);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("sign_in");
    ns_web_url ("sign_in",
        "URL=https://petstore.octoperf.com/actions/Account.action;jsessionid=E588B5A31D6388689302C7A50B2F942E?signonForm=",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID"
    );

    ns_end_transaction("sign_in", NS_AUTO_STATUS);
    ns_page_think_time(10.884);

    //Page Auto split for Button 'Login' Clicked by User
    ns_start_transaction("fish_select");
    ns_web_url ("fish_select",
        "URL=https://petstore.octoperf.com/actions/Account.action",
        "METHOD=POST",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Origin:https://petstore.octoperf.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID",
        BODY_BEGIN,
            "username=TC101189&password=qwerty&signon=Login&_sourcePage=Z3p7RwKrrXK-2lY5rM9s4mXFjc9CA2ZdWbl7EhHJxd5-0hCXa70IqxM-0mLJsJ6hkaiiFquB8SYcUHRVQLMClCjvl4vZ2qwZ6eeUjDQcq8A%3D&__fp=-nmW8kRF2uu-avUIq7VfPBomhGgJ1QGUYyCeYDFbOtZPrVh6RH9Wekwh0s5YnhGP",
        BODY_END,
        INLINE_URLS,
            "URL=https://petstore.octoperf.com/actions/Catalog.action", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE
    );

    ns_end_transaction("fish_select", NS_AUTO_STATUS);
    ns_page_think_time(2.407);

    //Page Auto split for Image Link '' Clicked by User
    ns_start_transaction("Product_Id");
    ns_web_url ("Product_Id",
        "URL=https://petstore.octoperf.com/actions/Catalog.action?viewCategory=&categoryId=FISH",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID"
    );

    ns_end_transaction("Product_Id", NS_AUTO_STATUS);
    ns_page_think_time(1.797);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("Item_ID");
    ns_web_url ("Item_ID",
        "URL=https://petstore.octoperf.com/actions/Catalog.action?viewProduct=&productId=FI-SW-01",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID"
    );

    ns_end_transaction("Item_ID", NS_AUTO_STATUS);
    ns_page_think_time(2.334);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("Add_to_cart");
    ns_web_url ("Add_to_cart",
        "URL=https://petstore.octoperf.com/actions/Catalog.action?viewItem=&itemId=EST-1",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID",
        INLINE_URLS,
            "URL=https://petstore.octoperf.com/images/fish1.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE
    );

    ns_end_transaction("Add_to_cart", NS_AUTO_STATUS);
    ns_page_think_time(2.129);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("checkout");
    ns_web_url ("checkout",
        "URL=https://petstore.octoperf.com/actions/Cart.action?addItemToCart=&workingItemId=EST-1",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID"
    );

    ns_end_transaction("checkout", NS_AUTO_STATUS);
    ns_page_think_time(1.817);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("continue");
    ns_web_url ("continue",
        "URL=https://petstore.octoperf.com/actions/Order.action?newOrderForm=",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID"
    );

    ns_end_transaction("continue", NS_AUTO_STATUS);
    ns_page_think_time(5.151);

    //Page Auto split for Button 'Continue' Clicked by User
    ns_start_transaction("orderconfirm");
    ns_web_url ("orderconfirm",
        "URL=https://petstore.octoperf.com/actions/Order.action",
        "METHOD=POST",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Origin:https://petstore.octoperf.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID",
        BODY_BEGIN,
            "order.cardType=Visa&order.creditCard=999+9999+9999+9999&order.expiryDate=12%2F03&order.billToFirstName=guriya&order.billToLastName=kumari&order.billAddress1=sdfghj&order.billAddress2=hjz&order.billCity=NOIDA&order.billState=UP&order.billZip=201301&order.billCountry=India&newOrder=Continue&_sourcePage=xxeYr1KuskO4e8H4oBiZval8Jheab806T6PFvmGVMoIr21nCibMXVO4wqegXq-2pqApCsDKJsLEufzR7oSynXk5nuErlUvAsV_nnnHcSMf0%3D&__fp=o81Y9LgaQwA1fAfRJ5nII2JNM53tDec37cvLVDAQo1Xz6D8BYda9NK9ZwhZmtw8zu9a5kt7SHNkm3AVq2IipIhW_eeVssCl10kAF7QbSY3LJG7S-GVXmjg%3D%3D",
        BODY_END
    );

    ns_end_transaction("orderconfirm", NS_AUTO_STATUS);
    ns_page_think_time(1.921);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("Payment");
    ns_web_url ("Payment",
        "URL=https://petstore.octoperf.com/actions/Order.action?newOrder=&confirmed=true",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID"
    );

    ns_end_transaction("Payment", NS_AUTO_STATUS);
    ns_page_think_time(22.501);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("Return_menu");
    ns_web_url ("Return_menu",
        "URL=https://petstore.octoperf.com/actions/Catalog.action",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID",
        INLINE_URLS,
            "URL=https://petstore.octoperf.com/images/fish_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/dogs_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/cats_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/reptiles_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/birds_icon.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE,
            "URL=https://petstore.octoperf.com/images/splash.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE
    );

    ns_end_transaction("Return_menu", NS_AUTO_STATUS);
    ns_page_think_time(1.553);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("sign_out");
    ns_web_url ("sign_out",
        "URL=https://petstore.octoperf.com/actions/Account.action?signoff=",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=JSESSIONID",
        INLINE_URLS,
            "URL=https://petstore.octoperf.com/actions/Catalog.action", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=JSESSIONID", END_INLINE
    );

    ns_end_transaction("sign_out", NS_AUTO_STATUS);
    ns_page_think_time(3.579);

}
